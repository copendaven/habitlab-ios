//
//  ChooseNewLevelAvailableCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class ChooseNewLevelAvailableCell: ChooseNewLevelCell {
    var controller: UIViewController?
    
    @IBOutlet weak var levelName: UILabel!
    @IBOutlet weak var levelLevel: UILabel!
    @IBOutlet weak var timeRemaining: UILabel!
    
    var circleTimer = NSTimer()
    
    @IBAction func clickedButton(sender: AnyObject) {
        if controller == nil {
            return
        }
        
        if let levelController = controller as? ChooseNewLevelController {
            if let drive = super.drive {
                if let driveCategory = drive["driveCategory"] as? NSString {
                    if driveCategory.compare("reading") == NSComparisonResult.OrderedSame {
                        levelController.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                        return
                    }
                    
                    if driveCategory.compare("unplugging") == NSComparisonResult.OrderedSame {
                        levelController.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                        return
                    }
                    
                    if driveCategory.compare("running") == NSComparisonResult.OrderedSame {
                        levelController.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                        return
                    }
                    
                    if driveCategory.compare("reconnecting") == NSComparisonResult.OrderedSame {
                        levelController.performSegueWithIdentifier("showClarifyReconnectChooseFriendsFromChooseNewLevel", sender: drive)
                        return
                    }
                    
                    if driveCategory.compare("exercising") == NSComparisonResult.OrderedSame {
                        if let level = drive["level"] as? NSNumber {
                            if level == 1 {
                                levelController.performSegueWithIdentifier("showChooseProgressPermissionsFromChooseNewLevel", sender: drive)
                            } else {
                                levelController.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                            }
                        }
                        
                        return
                    }
                }
            }
        }
    }
    
    func populate(drive: NSDictionary, controller: UIViewController) {
        self.controller = controller
        super.populate(drive, levelName: levelName, levelLevel: levelLevel)
        self.timeRemaining.text = ""
        
        circleTimer.invalidate()
        circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(ChooseNewLevelAvailableCell.refresh), userInfo: nil, repeats: true)
    }
    
    func refresh() {
        if super.drive == nil {
            return
        }
        
        var startDate: NSDate? = nil
        
        if let completedAt = super.drive!["lastCompletedAt"] as? NSString {
            startDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
        }
        
        if let completedAt = super.drive!["lastCompletedAtTimeUTC"] as? NSNumber {
            startDate = Serv.getDateFromServerStringUTC(completedAt)
        }
        
        if startDate == nil {
            return
        }
        
        let endDate = Serv.getDatePlusHours(startDate!, hours: 24)
        
        if endDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(endDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let remainingInterval = endDate!.timeIntervalSinceDate(now)
        timeRemaining.text = Serv.getTimeLabelString(remainingInterval, enforceDays: true, enforceHours: false)
    }
}