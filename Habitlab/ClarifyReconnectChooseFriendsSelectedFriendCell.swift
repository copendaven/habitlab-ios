//
//  ClarifyReconnectChooseFriendsSelectedFriendCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Contacts

class ClarifyReconnectChooseFriendsSelectedFriendCell: UITableViewCell {
    private var controller: UIViewController?
    private var friend: CNContact?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var friendImage: UIImageView!
    @IBOutlet weak var initialLabel: UILabel!
    @IBOutlet weak var imageBorder: UIView!
    
    func populate(controller: UIViewController, friend: CNContact, reconnectAction: String, color: UIColor) {
        self.controller = controller
        self.friend = friend
        
        dispatch_async(dispatch_get_main_queue()) {
            self.friendImage.layer.cornerRadius = 0.5 * self.friendImage.bounds.size.width
            self.initialLabel.layer.cornerRadius = 0.5 * self.initialLabel.bounds.size.width
            self.imageBorder.layer.cornerRadius = 0.5 * self.imageBorder.bounds.size.width
            
            self.titleLabel.text = friend.givenName
            self.descriptionLabel.text = reconnectAction
            
            if let imageData = friend.imageData {
                self.friendImage.hidden = false
                self.initialLabel.hidden = true
                self.friendImage.image = UIImage(data: imageData)
            } else {
                self.friendImage.hidden = true
                self.initialLabel.hidden = false
                
                self.initialLabel.backgroundColor = color
                
                var initialLabelText = ""
                
                if friend.givenName.characters.count > 0 {
                    initialLabelText += friend.givenName[friend.givenName.startIndex.advancedBy(0)..<friend.givenName.startIndex.advancedBy(1)]
                }
                
                if friend.familyName.characters.count > 0 {
                    initialLabelText += friend.familyName[friend.familyName.startIndex.advancedBy(0)..<friend.familyName.startIndex.advancedBy(1)]
                }
                
                if initialLabelText == "" {
                    initialLabelText = "?"
                }
                
                self.initialLabel.text = initialLabelText.uppercaseString
            }
        }
    }
    
    @IBAction func removeFriend(sender: AnyObject) {
        if let viewController = self.controller as? ClarifyReconnectChooseFriendsController, realFriend = self.friend {
            viewController.removeFriend(realFriend)
        }
    }
}