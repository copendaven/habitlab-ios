//
//  FollowersController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class FollowersController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    
    var followers = []
    var refresher: UIRefreshControl?
    
    @IBOutlet weak var noFollowersLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var isOpenedFromPushNotification: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(FollowersController)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = self.tableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(FollowersController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        refresh()
        segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(FollowersController))
    }
    
    func refresh() {
        Serv.showSpinner()
        
        HabitlabRestAPI.getFollowers({ (error, followersJson) -> () in
            Serv.hideSpinner()
            
            if error != nil {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let followers = followersJson as? NSArray {
                self.followers = followers
                self.tableView.reloadData()
                
                if let refr = self.refresher {
                    refr.endRefreshing()
                }
                
                self.noFollowersLabel.hidden = (followers.count > 0)
            } else {
                self.noFollowersLabel.hidden = false
            }
        })
    }

    // MARK: UIViewController Methods

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showPublicProfileFromFollowers") {
            if let viewController = segue.destinationViewController as? PublicProfileController {
                if let userId = sender as? String {
                    viewController.userId = userId
                }
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return followers.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if let follower = followers[indexPath.section] as? NSDictionary {
            if let status = follower["status"] as? NSString {
                switch (status) {
                case "approve":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followersApproveCell", forIndexPath: indexPath) as? FollowersApproveCell {
                        cell.populate(follower, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "follow":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followersFollowCell", forIndexPath: indexPath) as? FollowersFollowCell {
                        cell.populate(follower, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "waiting":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followersWaitingCell", forIndexPath: indexPath) as? FollowersWaitingCell {
                        cell.populate(follower, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "following":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followersFollowingCell", forIndexPath: indexPath) as? FollowersFollowingCell {
                        cell.populate(follower, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "blocked":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followersBlockedCell", forIndexPath: indexPath) as? FollowersBlockedCell {
                        cell.populate(follower, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                default:
                    // Do nothing.
                    break
                }
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
