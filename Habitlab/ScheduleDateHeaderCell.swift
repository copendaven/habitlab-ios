//
//  ScheduleDateHeaderCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ScheduleDateHeaderCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    func populate(drive: NSDictionary) {
        if let verb = drive["verb"] as? NSString {
            self.questionLabel.text = "What days would you like to \(verb as String)?"
        }
        
        Serv.setDriveStatusText(drive, label: detailsLabel)
    }
}