//
//  MessageTableLikesCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/13/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class MessageTableLikesCell: UITableViewCell {
    var likes: NSArray = []
    var controller: UIViewController?
    
    @IBOutlet weak var likesLabel: UILabel!
    
    func populate(controller: UIViewController, likes: NSArray) {
        self.controller = controller
        self.likes = likes
        
        dispatch_async(dispatch_get_main_queue()) {
            self.likesLabel.text = Serv.getLikesText(likes)
        }
    }
    
    @IBAction func clickedLikes(sender: AnyObject) {
        if let viewController = self.controller as? WriteMessageController {
            viewController.showLikers()
        }
    }
}