//
//  ScheduleDateClearWaitingDayCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/20/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ScheduleDateClearWaitingDayCell: UICollectionViewCell {
    private var controller: UIViewController?
    private var date: (year: Int, month: Int, day: Int)?
    
    @IBOutlet weak var dayLabel: UILabel!
    
    func populate(controller: UIViewController, date: (year: Int, month: Int, day: Int)) {
        self.controller = controller
        self.date = date
        
        self.dayLabel.text = "\(date.day)"
    }
    
    @IBAction func clicked(sender: AnyObject) {
        if self.date == nil {
            return
        }
        
        if let controller = self.controller {
            if let viewController = controller as? ScheduleDateController {
                viewController.clickedDate(self.date!)
            }
        }
    }
}
