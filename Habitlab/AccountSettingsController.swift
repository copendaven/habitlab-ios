//
//  AccountSettingsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 01/01/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Analytics
import Google

class AccountSettingsController: GAITrackedViewController {
    @IBAction func signOut(sender: AnyObject) {
        HabitlabAuthentication.deauthenticate({ (success) -> () in
            
            // FBSDKAccessToken
            FBSDKLoginManager().logOut()
            HabitlabAuthentication.habitlabAccessToken = nil
            UserCache().setUser(nil)
            
            let gai = GAI.sharedInstance()
            let tracker = gai.defaultTracker
            tracker.allowIDFACollection = true
            let builder = GAIDictionaryBuilder.createScreenView()
            builder.set("end", forKey: kGAISessionControl)
            tracker.set(kGAIScreenName, value: String(SignupController))
            tracker.send(builder.build() as [NSObject: AnyObject])
            
            if !success {
                return
            }
            
            self.performSegueWithIdentifier("unwindToLogout", sender: self)
        })
    }
    
    @IBAction func deleteAccount(sender: AnyObject) {
        let destroyAlert = UIAlertController(title: "Delete your user", message: "By deleting the user, all data will be lost. Do you want to still delete the user?", preferredStyle: UIAlertControllerStyle.Alert)
        
        destroyAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction!) in
            HabitlabAuthentication.destroy({ (success) -> () in
                if !success {
                    Serv.showErrorPopup("Delete error", message: "We could not delete your user. Send us an email.", okMessage: "Okay, got it.", controller: self, handler: nil)
                }
                
                FBSDKLoginManager().logOut()
                
                let storage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
                
                if let url = NSURL(string: "http://login.facebook.com") {
                    if let cookies = storage.cookiesForURL(url) {
                        for cookie in cookies {
                            let domainName = cookie.domain
                            
                            if let domainRange = domainName.rangeOfString("facebook") {
                                if domainRange.count > 0 {
                                    storage.deleteCookie(cookie)
                                }
                            }
                        }
                        
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                }
                
                HabitlabAuthentication.habitlabAccessToken = nil
                UserCache().setUser(nil)
                
                self.performSegueWithIdentifier("unwindToLogout", sender: self)
            })
        }))
        
        destroyAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
        
        presentViewController(destroyAlert, animated: true, completion: nil)
    }
    
    @IBAction func privacyPolicy(sender: AnyObject) {
        if let checkURL = NSURL(string: "https://www.habitlab.me/terms/HabitlabTerms.pdf") {
            UIApplication.sharedApplication().openURL(checkURL)
        }
    }
    
    @IBAction func supportPage(sender: AnyObject) {
        if let checkURL = NSURL(string: "mailto://info@habitlab.me") {
            UIApplication.sharedApplication().openURL(checkURL)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(AccountSettingsController)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        self.segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(AccountSettingsController))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
