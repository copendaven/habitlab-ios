//
//  PlanUnpluggingController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class PlanUnpluggingController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate {
    private enum SearchState {
        case HideSearchBarShowSelectedFriends
        case ShowSearchBarShowSelectedFriends
        case ShowSearchBarShowFoundFriends
    }
    
    private static var object = NSObject()
    
    var progressLoadingController: ProgressLoadingController?
    var program: NSDictionary?
    var step: NSDictionary?
    
    @IBOutlet weak var friendsTableView: UITableView!
    @IBOutlet weak var planUnpluggingButton: UIButton!
    @IBOutlet weak var tableMarginTop: NSLayoutConstraint!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchTextViewPlaceholder: UILabel!
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageTextViewPlaceholder: UILabel!
    @IBOutlet weak var messageTextViewCountLabel: UILabel!
    
    @IBOutlet weak var shareNowLabel: UILabel!
    @IBOutlet weak var shareButtonImager: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    
    private var searchState: SearchState = SearchState.HideSearchBarShowSelectedFriends
    private var friends = []
    private var serverFriends = []
    private var selectedFriends = []
    private var selectedFriendsIds = Set<String>()
    private var foundFriends = []
    private var foundFriendIds = Set<String>()
    private var sentPlan = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(PlanUnpluggingController)
        
        friendsTableView.delegate = self
        friendsTableView.dataSource = self
        friendsTableView.allowsSelection = false;
        
        tableMarginTop.constant = 0
        searchTextField.delegate = self
        messageTextView.delegate = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PlanUnpluggingController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        self.shareButtonImager.layer.cornerRadius = 0.5 * self.shareButtonImager.bounds.size.width
        self.shareButton.enabled = false
        
        self.messageTextViewPlaceholder.hidden = false
        self.searchTextViewPlaceholder.hidden = false
        self.messageTextViewCountLabel.text = "0/50 characters"
        self.messageTextView.text = ""
        
        self.messageTextViewPlaceholder.sizeToFit()
        
        self.tableMarginTop.constant = 0
        self.sentPlan = false
        self.searchState = SearchState.HideSearchBarShowSelectedFriends
        
        objc_sync_enter(PlanUnpluggingController.object)
        self.foundFriendIds.removeAll()
        self.selectedFriendsIds.removeAll()
        objc_sync_exit(PlanUnpluggingController.object)
        
        let paddingView = UIView(frame: CGRectMake(0, 0, 8, self.searchTextField.frame.height))
        searchTextField.leftView = paddingView
        searchTextField.leftViewMode = UITextFieldViewMode.Always
        
        if let viewController = self.progressLoadingController {
            viewController.hideSilently()
        }
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        refresh()
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if let realStep = self.step {
            if let index = realStep["index"] as? NSNumber {
                dict["index"] = String(index)
            }
            
            if let week = realStep["week"] as? NSNumber {
                dict["week"] = String(week)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(PlanUnpluggingController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(PlanUnpluggingController))
        }
    }
    
    private func refresh() {
        objc_sync_enter(PlanUnpluggingController.object)
        
        switch searchState {
        case SearchState.HideSearchBarShowSelectedFriends:
            self.searchTextField.text = ""
            self.searchTextViewPlaceholder.hidden = false
            self.searchView.hidden = true
            self.tableMarginTop.constant = 0
            self.view.endEditing(true)
            self.foundFriendIds.removeAll()
        case SearchState.ShowSearchBarShowFoundFriends:
            self.searchView.hidden = false
            self.tableMarginTop.constant =  30
        case SearchState.ShowSearchBarShowSelectedFriends:
            self.searchTextField.text = ""
            self.searchView.hidden = false
            self.tableMarginTop.constant =  30
            self.foundFriendIds.removeAll()
        }
        
        if self.serverFriends.count <= 0 {
            objc_sync_exit(PlanUnpluggingController.object)
            self.refreshFromServer()
        } else {
            objc_sync_exit(PlanUnpluggingController.object)
            self.refreshFromClient()
        }
        
        /*
        if let searchText = self.searchTextField.text {
            searchTextViewPlaceholder.hidden = searchText.characters.count > 0
        } else {
            searchTextViewPlaceholder.hidden = false
        }
        
        if let messageText = self.messageTextView.text {
            messageTextViewPlaceholder.hidden = messageText.characters.count > 0
        } else {
            messageTextViewPlaceholder.hidden = false
        }
        */
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        var finalText = ""
        
        if let textInside = textView.text {
            finalText = textInside
            
            let stringLength = text.characters.count
            let resultLength = finalText.characters.count
            
            if stringLength > 0 {
                finalText = finalText + text
            } else {
                if resultLength == 1 {
                    finalText = ""
                } else if resultLength > 1 {
                    finalText = finalText.substringToIndex(finalText.endIndex.predecessor())
                }
            }
        } else {
            finalText = text
        }
        
        if finalText.characters.count > 50 {
            return false
        }
        
        self.messageTextViewPlaceholder.hidden = finalText.characters.count > 0
        self.shareButton.enabled = finalText.characters.count > 0
        
        self.messageTextViewCountLabel.text = "\(min(50, finalText.characters.count))/50 characters"
        
        if finalText.characters.count > 0 {
            self.shareButtonImager.alpha = 1.0
            self.shareNowLabel.alpha = 1.0
        } else {
            self.shareButtonImager.alpha = 0.5
            self.shareNowLabel.alpha = 0.5
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        //self.searchTextViewPlaceholder.hidden = true
        
        self.searchState = SearchState.ShowSearchBarShowSelectedFriends
        self.refresh()
        
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        //self.searchTextViewPlaceholder.hidden = false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var finalText = ""
        
        if let textInside = textField.text {
            finalText = textInside
            
            let stringLength = string.characters.count
            let resultLength = finalText.characters.count
            
            if stringLength > 0 {
                finalText = finalText + string
            } else {
                if resultLength == 1 {
                    finalText = ""
                } else if resultLength > 1 {
                    finalText = finalText.substringToIndex(finalText.endIndex.predecessor())
                }
            }
        } else {
            finalText = string
        }
        
        self.searchTextViewPlaceholder.hidden = finalText.characters.count > 0
        
        self.searchText(finalText)
        return true
    }
    
    private func computeFoundFriends() {
        self.foundFriends = friends.filter({(friend) -> Bool in
            if let id = friend["id"] as? NSString {
                return foundFriendIds.contains(id as String)
            }
            
            return false
        })
    }
    
    private func computeSelectedFriends() {
        self.selectedFriends = friends.filter({(friend) -> Bool in
            if let id = friend["id"] as? NSString {
                return selectedFriendsIds.contains(id as String)
            }
            
            return false
        })
    }
    
    func searchText(searchString: String) {
        objc_sync_enter(PlanUnpluggingController.object)
        
        // Show no results by default (for empty search string).
        if searchString == "" {
            objc_sync_exit(PlanUnpluggingController.object)
  
            self.searchState = SearchState.ShowSearchBarShowSelectedFriends
            refresh()
            
            return
        }
        
        foundFriendIds.removeAll()
        
        for friend in friends {
        
            // Do not find friends among the selected again.
            if let id = friend["id"] as? NSString {
                if let firstName = friend["firstName"] as? NSString {
                    if firstName.rangeOfString(searchString, options: NSStringCompareOptions.CaseInsensitiveSearch).location != NSNotFound {
                        foundFriendIds.insert(id as String)
                    }
                }
                
                if let lastName = friend["lastName"] as? NSString {
                    if lastName.rangeOfString(searchString, options: NSStringCompareOptions.CaseInsensitiveSearch).location != NSNotFound {
                        foundFriendIds.insert(id as String)
                    }
                }
            }
        }
        
        computeFoundFriends()
        objc_sync_exit(PlanUnpluggingController.object)
        
        self.searchState = SearchState.ShowSearchBarShowFoundFriends
        refresh()
    }
    
    @IBAction func clickedAddFriends(sender: AnyObject) {
        self.searchState = SearchState.ShowSearchBarShowSelectedFriends
        self.refresh()
    }
    
    func filterFriends(receivedFriends: NSArray) -> NSArray {
        let processedFriends: NSMutableArray = NSMutableArray()
        processedFriends.addObjectsFromArray(receivedFriends as [AnyObject])
        return processedFriends
    }
    
    func refreshFromClient() {
        objc_sync_enter(PlanUnpluggingController.object)
        
        self.friends = self.filterFriends(self.serverFriends)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.friendsTableView.reloadData()
        }
        
        objc_sync_exit(PlanUnpluggingController.object)
    }
    
    func refreshFromServer() {
        Serv.showSpinner()
        
        HabitlabRestAPI.getFriends({ (error, friendsJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let friendsArray = friendsJson as? NSArray {
                objc_sync_enter(PlanUnpluggingController.object)
                self.serverFriends = NSArray(array: friendsArray)
                objc_sync_exit(PlanUnpluggingController.object)
                
                self.refreshFromClient()
            }
        })
    }
    
    @IBAction func sendPlan(sender: AnyObject) {
        if sentPlan {
            return
        }
        
        if self.step == nil {
            return
        }
        
        if self.program == nil {
            return
        }
        
        let stepId = self.step!["id"] as? NSString
        
        if stepId == nil {
            return
        }
        
        let message = self.messageTextView.text
        
        if message == nil {
            return
        }
        
        var ids: [String] = []
        
        for selectedFriendId in selectedFriendsIds {
            ids.append(selectedFriendId)
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.performSegueWithIdentifier("showProgressLoadingFromPlanUnplugging", sender: self)
        })
        
        self.sentPlan = true
        PlanUnpluggingCache().setStepPlannedUnplugging(stepId! as String)
        let cacheId = NSUUID().UUIDString
        
        Serv.showSpinner()
        
        HabitlabRestAPI.postUnpluggingPlan(ids, stepId: stepId! as String, cacheId: cacheId, message: message!) { (error, result) in
            Serv.hideSpinner()
            
            if error != nil {
                self.sentPlan = false
                PlanUnpluggingCache().removeStepPlannedUnplugging(stepId! as String)
                
                dispatch_async(dispatch_get_main_queue(), {
                    if let viewController = self.progressLoadingController {
                        viewController.hideSilently()
                    }
                })
                
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                if let viewController = self.progressLoadingController {
                    viewController.markDone("Shared", hideInSeconds: 1)
                }
            })
        }
    }
    
    @IBAction func clickedSkipThis(sender: AnyObject) {
        self.performSegueWithIdentifier("showUnpluggingModuleFromPlanUnplugging", sender: nil)
    }

    func doneLoading() {
        self.performSegueWithIdentifier("showUnpluggingModuleFromPlanUnplugging", sender: nil)
    }
    
    func selectFriend(id: String) {
        if selectedFriendsIds.count >= 5 {
            Serv.showErrorPopup("Too many friends", message: "You can choose at most five friends.", okMessage: "Okay, got it", controller: self, handler: nil)
            return
        }
        
        objc_sync_enter(PlanUnpluggingController.object)
        
        if !selectedFriendsIds.contains(id) {
            selectedFriendsIds.insert(id)
            computeSelectedFriends()
        }
        
        objc_sync_exit(PlanUnpluggingController.object)
        
        self.searchState = SearchState.HideSearchBarShowSelectedFriends
        self.refresh()
    }
    
    func deselectFriend(id: String) {
        objc_sync_enter(PlanUnpluggingController.object)
        
        if selectedFriendsIds.contains(id) {
            selectedFriendsIds.remove(id)
            computeSelectedFriends()
        }
        
        objc_sync_exit(PlanUnpluggingController.object)
        
        self.refresh()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressLoadingFromPlanUnplugging" {
            if let viewController = segue.destinationViewController as? ProgressLoadingController {
                self.progressLoadingController = viewController
                viewController.setDelegate(self)
            }
        }
        
        if segue.identifier == "showUnpluggingModuleFromPlanUnplugging" {
            if let viewController = segue.destinationViewController as? UnpluggingModuleController {
                viewController.step = self.step
                viewController.program = self.program
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        switch searchState {
        case SearchState.HideSearchBarShowSelectedFriends, SearchState.ShowSearchBarShowSelectedFriends:
            return selectedFriends.count
        case SearchState.ShowSearchBarShowFoundFriends:
            return foundFriends.count
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
    
        switch searchState {
        case SearchState.ShowSearchBarShowFoundFriends:
            if foundFriends.count >= indexPath.section {
                if let cell = friendsTableView.dequeueReusableCellWithIdentifier("planUnpluggingFoundFriendCell") as? PlanUnpluggingFoundFriendCell {
                    return cell
                }
            }
        case SearchState.ShowSearchBarShowSelectedFriends, SearchState.HideSearchBarShowSelectedFriends:
            if selectedFriends.count >= indexPath.section {
                if let cell = friendsTableView.dequeueReusableCellWithIdentifier("planUnpluggingSelectedFriendCell") as? PlanUnpluggingSelectedFriendCell {
                    return cell
                }
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cellObject: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        switch searchState {
        case SearchState.ShowSearchBarShowFoundFriends:
            if foundFriends.count > indexPath.section {
                if let friend = foundFriends[indexPath.section] as? NSDictionary {
                    if let cell = cellObject as? PlanUnpluggingFoundFriendCell {
                        var showSelected = false
                        
                        if let id = friend["id"] as? NSString {
                            if self.selectedFriendsIds.contains(id as String) {
                                showSelected = true
                            }
                        }
                        
                        cell.populate(self, friend: friend, showSelected: showSelected)
                        return
                    }
                }
            }
        case SearchState.ShowSearchBarShowSelectedFriends, SearchState.HideSearchBarShowSelectedFriends:
            if selectedFriends.count > indexPath.section {
                if let friend = selectedFriends[indexPath.section] as? NSDictionary {
                    if let cell = cellObject as? PlanUnpluggingSelectedFriendCell {
                        cell.populate(self, friend: friend)
                        return
                    }
                }
            }
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
