//
//  PhysioModuleController.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/10/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Analytics

class PhysioModuleController: GAITrackedViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private let serialQueue = dispatch_queue_create("me.habitlab.mobile.physioModuleController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    var program: NSDictionary?
    var step: NSDictionary?
    var exercise: NSDictionary?
    var beforePainLevel: Int?
    var afterPainLevel: Int?
    var exerciseImages: [NSDictionary] = []
    
    var countUpSeconds = 0
    var countDownSeconds = Int.max
    var paused = false
    
    private var countDownTimer = NSTimer()
    private var countUpTimer = NSTimer()
    private var doneTimer = NSTimer()
    private var actions: NSMutableArray = NSMutableArray(array: [ModuleActionType.Info.rawValue, ModuleActionType.Start.rawValue])
    private var actionStates: NSMutableArray = NSMutableArray(array: [ModuleActionState.Disabled.rawValue, ModuleActionState.Disabled.rawValue])
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var imagesPageControl: UIPageControl!
    @IBOutlet weak var actionsCollectionView: UICollectionView!
    @IBOutlet weak var repetitionsLabel: UILabel!
    @IBOutlet weak var repetitionsCounter: UILabel!
    @IBOutlet weak var durationCounter: UILabel!
    
    @IBOutlet weak var durationView: UIView!
    @IBOutlet weak var repetitionsView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(PhysioModuleController)
        
        actionsCollectionView.dataSource = self
        actionsCollectionView.delegate = self
        
        imagesCollectionView.dataSource = self
        imagesCollectionView.delegate = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        self.removeChoosePain()
        
        if step == nil {
            return
        }
        
        if program == nil {
            return
        }
        
        doneTimer.invalidate()
        countDownTimer.invalidate()
        countUpTimer.invalidate()
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let title = drive["title"] as? NSString, level = drive["level"] as? NSNumber {
                    self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
                }
            }
        }
        
        refresh()
        segment()
    }
    
    private func removeChoosePain() {
        if let navigation = self.navigationController {
            var array = navigation.viewControllers
            
            for index in 0..<array.count {
                if let _ = array[index] as? ChoosePhysioPainController {
                    array.removeAtIndex(index)
                    break
                }
            }
            
            navigation.viewControllers = array
        }
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if let realStep = self.step {
            if let index = realStep["index"] as? NSNumber {
                dict["index"] = String(index)
            }
            
            if let week = realStep["week"] as? NSNumber {
                dict["week"] = String(week)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(PhysioModuleController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(PhysioModuleController))
        }
    }
    
    private func refreshActions(showStartButton showStartButton: Bool, showPauseButton: Bool, showDoneButton: Bool) {
        self.actions = NSMutableArray()
        self.actionStates = NSMutableArray()
        
        self.actions.addObject(ModuleActionType.Info.rawValue)
        self.actionStates.addObject(ModuleActionState.Enabled.rawValue)
        
        if showStartButton {
            if paused {
                self.actions.addObject(ModuleActionType.Continue.rawValue)
            } else {
                self.actions.addObject(ModuleActionType.Start.rawValue)
            }
            
            self.actionStates.addObject(ModuleActionState.Enabled.rawValue)
        }
        
        if showPauseButton {
            self.actions.addObject(ModuleActionType.Pause.rawValue)
            self.actionStates.addObject(ModuleActionState.Enabled.rawValue)
        }
        
        if !showStartButton {
            self.actions.addObject(ModuleActionType.Done.rawValue)
            
            if showDoneButton {
                self.actionStates.addObject(ModuleActionState.Enabled.rawValue)
            } else {
                self.actionStates.addObject(ModuleActionState.Disabled.rawValue)
            }
        }
        
        self.actionsCollectionView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        doneTimer.invalidate()
        countDownTimer.invalidate()
        countUpTimer.invalidate()
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    func buttonClicked(actionType: ModuleActionType) {
        switch (actionType) {
        case .Info:
            self.showExerciseInfo()
        case .Done:
            self.completeExercise()
            break
        case .Continue, .Start:
            self.startExercise()
            break
        case .Pause:
            self.pauseExercise()
            break
        default:
            break
        }
    }
    
    private func renderExercise() {
        showExerciseMedia()
        showExerciseTimers()
    }
    
    private func showExerciseMedia() {
        self.exerciseImages.removeAll()
        
        if let realExercise = self.exercise {
            if let mediaItems = realExercise["media"] as? NSArray {
                for mediaItemJson in mediaItems {
                    if let mediaItem = mediaItemJson as? NSDictionary {
                        if let _ = mediaItem["cacheId"] as? NSString {
                            self.exerciseImages.append(mediaItem)
                        }
                    }
                }
            }
        }
        
        self.imagesPageControl.numberOfPages = exerciseImages.count
        self.imagesCollectionView.reloadData()
    }
    
    private func showExerciseTimers() {
        if let realExercise = self.exercise {
            if let repetitions = realExercise["countRepetitions"] as? NSNumber {
                self.repetitionsView.hidden = false
                self.durationView.hidden = true
                
                self.repetitionsCounter.text = Serv.getTimeLabelString(0, enforceDays: false, enforceHours: false)
                self.repetitionsLabel.text = String(repetitions)
                
                return
            } else if let duration = realExercise["duration"] as? NSNumber {
                self.repetitionsView.hidden = true
                self.durationView.hidden = false

                self.durationCounter.text = Serv.getTimeLabelString(Double(duration), enforceDays: false, enforceHours: false)
                
                return
            }
        }
    }
    
    private func showExerciseInfo() {
        self.performSegueWithIdentifier("showPhysioExerciseDescriptionFromPhysioModule", sender: self)
        
        
        // TODO: Fix this!!!
        // segue to the exercise info, and set the content too (possibly in prepare for segue)
    }
    
    private func completeExercise() {
        doneTimer.invalidate()
        countUpTimer.invalidate()
        countDownTimer.invalidate()
        
        if let realExercise = self.exercise, realStep = self.step {
            if let exerciseId = realExercise["id"] as? NSString, index = realExercise["index"] as? NSNumber, stepId = realStep["id"] as? NSString {
                PhysioExerciseCache().setComplete(stepId as String, exerciseId: exerciseId as String, index: Int(index))
                self.refresh()
            }
        }
    }
    
    @IBAction func changePage(sender: AnyObject) {
        let offsetX = CGFloat(imagesPageControl.currentPage) * imagesCollectionView.frame.size.width
        imagesCollectionView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
    }

    func enableDone(timer: NSTimer) {
        doneTimer.invalidate()
        countDownTimer.invalidate()
        
        self.refreshActions(showStartButton: false, showPauseButton: true, showDoneButton: true)
    }
    
    func enableDoneCountUp(timer: NSTimer) {
        self.enableDone(timer)
    }
    
    func enableDoneCountDown(timer: NSTimer) {
        self.enableDone(timer)
        
        doneTimer.invalidate()
        countDownTimer.invalidate()
        
        self.countDownSeconds = 0
        self.durationCounter.text = Serv.getTimeLabelString(Double(self.countDownSeconds), enforceDays: false, enforceHours: false)
    }
    
    func countUp(timer: NSTimer) {
        self.countUpSeconds += 1
        self.repetitionsCounter.text = Serv.getTimeLabelString(Double(self.countUpSeconds), enforceDays: false, enforceHours: false)
        
        self.enableDoneCountUp(timer)
        
        if let realExercise = self.exercise {
            if let _ = realExercise["countRepetitions"] as? NSNumber {
                self.countUpTimer.invalidate()
                self.countUpTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(PhysioModuleController.countUp(_:)), userInfo: nil, repeats: false)
            }
        }
    }
    
    func countDown(timer: NSTimer) {
        self.countDownSeconds = max(self.countDownSeconds - 1, 0)
        self.durationCounter.text = Serv.getTimeLabelString(Double(self.countDownSeconds), enforceDays: false, enforceHours: false)
        
        if let realExercise = self.exercise {
            if let _ = realExercise["duration"] as? NSNumber {
                if self.countDownSeconds > 0 {
                    self.countDownTimer.invalidate()
                    self.countDownTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(PhysioModuleController.countDown(_:)), userInfo: nil, repeats: false)
                } else {
                    self.enableDoneCountDown(timer)
                }
            }
        }
    }
    
    private func startExercise() {
        if let realExercise = self.exercise {
            self.refreshActions(showStartButton: false, showPauseButton: true, showDoneButton: false)
            
            if let _ = realExercise["countRepetitions"] as? NSNumber {
                if !self.paused {
                    self.countUpSeconds = 0
                }
                
                self.countUpTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(PhysioModuleController.countUp(_:)), userInfo: nil, repeats: false)
            } else if let duration = realExercise["duration"] as? NSNumber {
                if !self.paused {
                    self.countDownSeconds = Int(duration)
                }
                
                self.countDownTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(PhysioModuleController.countDown(_:)), userInfo: nil, repeats: false)
            }
            
            self.paused = false
        }
        
        if let realStep = self.step {
            if let stepId = realStep["id"] as? NSString {
                if !ProgramStepStartedCache().isStepStarted(stepId as String) {
                    HabitlabRestAPI.putProgramStepStarted("Habitlab physio tracker", stepId: stepId as String, handler: { (error, response) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            return
                        }
                        
                        ProgramStepStartedCache().setStepStarted(stepId as String)
                    })
                }
            }
        }
    }
    
    private func pauseExercise() {
        doneTimer.invalidate()
        countUpTimer.invalidate()
        countDownTimer.invalidate()
        
        self.paused = true
        self.refreshActions(showStartButton: true, showPauseButton: false, showDoneButton: false)
    }
    
    private func stopExercise() {
        doneTimer.invalidate()
        countUpTimer.invalidate()
        countDownTimer.invalidate()
        
        self.paused = false
        self.refreshActions(showStartButton: true, showPauseButton: false, showDoneButton: false)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showChoosePhysioPainFromPhysioModule" {
            if let viewController = segue.destinationViewController as? ChoosePhysioPainController {
                viewController.program = self.program
                viewController.step = self.step
                viewController.beforePainLevel = self.beforePainLevel
                viewController.painTiming = ChoosePhysioPainTiming.AfterExercising
            }
        }
        
        if segue.identifier == "showPhysioExerciseDescriptionFromPhysioModule" {
            if let viewController = segue.destinationViewController as? PhysioExerciseDescriptionController {
                viewController.exercise = self.exercise
                viewController.step = self.step
            }
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView == self.imagesCollectionView {
            let pageWidth = scrollView.frame.size.width
            let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
            
            print("page = \(page)")
            self.imagesPageControl.currentPage = page
        }
    }
    
    func refresh() {
        self.paused = false
        
        if let realProgram = self.program, realStep = self.step {
            if let drive = realProgram["drive"] as? NSDictionary, stepId = realStep["id"] as? NSString {
                if let physioExercises = drive["physioExercises"] as? NSArray {
                    
                    // Load the physio exercises in the cache.
                    for physioExerciseJson in physioExercises {
                        if let physioExercise = physioExerciseJson as? NSDictionary {
                            if let physioExerciseId = physioExercise["id"] as? NSString, index = physioExercise["index"] as? NSNumber {
                                PhysioExerciseCache().setCache(stepId as String, exerciseId: physioExerciseId as String, index: Int(index))
                            }
                        }
                    }
                    
                    let firstIncompleteIndex = 1 + PhysioExerciseCache().getLastCompleteExerciseIndex(stepId as String)
                    
                    self.exercise = nil
                    
                    // Set the exercise with the first incomplete index.
                    for physioExerciseJson in physioExercises {
                        if let physioExercise = physioExerciseJson as? NSDictionary {
                            if let index = physioExercise["index"] as? NSNumber {
                                if index == firstIncompleteIndex {
                                    self.exercise = physioExercise
                                    
                                    if let exerciseId = physioExercise["id"] as? NSString {
                                        if PhysioExerciseCache().getDontShow(stepId as String, exerciseId: exerciseId as String) == false {
                                            self.showExerciseInfo()
                                        }
                                    }
                                    
                                    self.refreshActions(showStartButton: true, showPauseButton: false, showDoneButton: false)
                                    self.renderExercise()
                                }
                            }
                        }
                    }
                    
                    // All exercises have been completed.
                    if self.exercise == nil && firstIncompleteIndex > physioExercises.count {
                        self.performSegueWithIdentifier("showChoosePhysioPainFromPhysioModule", sender: self)
                    }
                }
            }
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if collectionView == actionsCollectionView {
            return 1
        }
        
        if collectionView == imagesCollectionView {
            return 1
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == actionsCollectionView {
            return actions.count
        }
        
        if collectionView == imagesCollectionView {
            return self.exerciseImages.count
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if collectionView == actionsCollectionView {
            return CGSizeMake(90, 125)
        }
        
        if collectionView == imagesCollectionView {
            let screenWidth = CGFloat(imagesCollectionView.frame.size.width)
            return CGSizeMake(screenWidth, 240)
        }
        
        return CGSizeMake(0, 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        if collectionView == actionsCollectionView {
            let screenWidth = Int(actionsCollectionView.frame.size.width)
            var contentWidth = Int(actions.count * 90) // This is the size for one collection item with button and label
            
            if screenWidth <= contentWidth {
                contentWidth = screenWidth / 90 * 90
            }
            
            let padding = CGFloat((screenWidth - contentWidth) / 2)
            return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
        }
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if collectionView == actionsCollectionView {
            if actions.count <= indexPath.item {
                return defaultCell
            }
            
            if let action = actions[indexPath.item] as? String, actionState = actionStates[indexPath.item] as? String {
                if let cell = actionsCollectionView.dequeueReusableCellWithReuseIdentifier("moduleActionCollectionCell", forIndexPath: indexPath) as? ModuleActionCollectionCell {
                    if let moduleActionType = ModuleActionType(rawValue: action), moduleActionState = ModuleActionState(rawValue: actionState) {
                        cell.populate(actionType: moduleActionType, actionState: moduleActionState, controller: self)
                        return cell
                    }
                }
            }
        }
        
        if collectionView == imagesCollectionView {
            if exerciseImages.count <= indexPath.item {
                return defaultCell
            }
            
            let imageDictionary = exerciseImages[indexPath.item]
            
            if let cell = imagesCollectionView.dequeueReusableCellWithReuseIdentifier("physioModuleImageCell", forIndexPath: indexPath) as? PhysioModuleImageCell {
                cell.populate(imageDictionary)
                return cell
            }
        }
    
        return defaultCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
}
