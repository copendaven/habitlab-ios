//
//  NewFeedController.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class NewFeedController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var feedTableView: UITableView!
    @IBOutlet weak var newUpdatesButton: UIButton!
    
    @IBOutlet weak var overlay: UIView!
    
    @IBOutlet weak var overlayTitleLabel: UILabel!
    @IBOutlet weak var overlayContentLabel: UILabel!
    
    @IBOutlet weak var overlayArrowEnvelopeImage: UIImageView!
    @IBOutlet weak var overlayArrowPlusImage: UIImageView!
    
    @IBOutlet weak var noContentTitleLabel: UILabel!
    @IBOutlet weak var noContentContentLabel: UILabel!
    
    @IBOutlet weak var noContentArrowImage: UIImageView!
    
    @IBOutlet weak var noContentButton: UIButton!
    
    private var openedFirstTime = false
    private var friendJoinedDictionary: NSDictionary?
    
    // Serial queue for server notifications object
    private static let serverNotificationsQueue = dispatch_queue_create("me.habitlab.mobile.newFeedController.serverNotificationsQueue", DISPATCH_QUEUE_SERIAL)
    
    // Serial queue for cache shown object.
    private static let cacheShownQueue = dispatch_queue_create("me.habitlab.mobile.newFeedController.cacheShownQueue", DISPATCH_QUEUE_SERIAL)

    // Serial queue for downloaded count object.
    private static let downloadedCountQueue = dispatch_queue_create("me.habitlab.mobile.newFeedController.downloadedCountQueue", DISPATCH_QUEUE_SERIAL)

    private var downloadedCount = 0
    private let downloadedWindow = 10
    private var cacheShown = false
    private var serverNotifications = []
    private var notifications = []
    private var cellHeights = NSMutableDictionary()
    
    var isOpenedFromPushNotification: Bool = false
    
    private var refresher: UIRefreshControl = UIRefreshControl()
    
    var cacheCommentedMobileNotificationId: String?
    var cacheCommentedMobileNotificationCacheId: String?
    var cacheLikedMobileNotificationId: String?
    var cacheLikedMobileNotificationCacheId: String?
    
    // MARK: - Lifecycle handlers
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(NewFeedController)
        
        // Set the delegate for the feed table view
        feedTableView.delegate = self
        feedTableView.dataSource = self
        self.shyNavBarManager.scrollView = self.feedTableView
        
        // Set the refresher for the feed table view
        refresher.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        refresher.addTarget(self, action: #selector(NewFeedController.refreshPullToRefresh), forControlEvents: UIControlEvents.ValueChanged)
        feedTableView.addSubview(refresher)
        
        // Set the back bar item.
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        // Set scroll speed for feed table.
        feedTableView.decelerationRate = UIScrollViewDecelerationRateNormal
        
        // Disable the selection
        feedTableView.allowsSelection = false
        
        // Set infinite scroll for the feed table controller.
        feedTableView.infiniteScrollIndicatorMargin = 40
        feedTableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            self.refreshFromServer(true, isFromUpdateButton: false)
        }
        
        self.registerForNotifications()
    }
    
    private func registerForNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NewFeedController.handleFriendJoinedNotification(_:)), name: "friendJoined", object: nil)
    }
    
    private func unregisterForNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "friendJoined", object: nil)
    }
    
    func handleFriendJoinedNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if friendJoinedDictionary != nil {
                return
            }
            
            friendJoinedDictionary = userInfo
            
            self.overlayTitleLabel.hidden = true
            self.overlayContentLabel.hidden = true
            self.overlayArrowPlusImage.hidden = true
            self.overlayArrowEnvelopeImage.hidden = true
            
            if let friendJoined = self.friendJoinedDictionary {
                if let aps = friendJoined["aps"] as? NSDictionary {
                    if let payload = aps["payload"] as? NSDictionary {
                        if let friendId = payload["friendId"] as? NSString {
                            HabitlabRestAPI.getUser(friendId as String, handler: { (error, friendJson) in
                                if error == nil && friendJson != nil {
                                    if let friend = friendJson as? NSDictionary {
                                        if let userTracking = payload["userTracking"] as? NSString {
                                            HabitlabRestAPI.postTrackUser(userTracking as String, handler: { (error, response) in
                                                // Do nothing.
                                            })
                                        }
                                        
                                        let sendDictionary = NSMutableDictionary()
                                        sendDictionary.setValue(friendId as String, forKey: "id")
                                        sendDictionary.setValue("friendJoined", forKey: "name")
                                        sendDictionary.setValue("Follow now".uppercaseString, forKey: "actionTitle")
                                        
                                        if let firstName = friend["firstName"] as? NSString {
                                            sendDictionary.setValue("\(firstName) just joined Habitlab", forKey: "title")
                                            sendDictionary.setValue("\(firstName) is more likely to succeed in building a good habit if you two connect.", forKey: "description")
                                        }
                                        
                                        if let photo = friend["facebookPhoto"] as? NSString {
                                            sendDictionary.setValue(photo as String, forKey: "photoURL")
                                        } else {
                                            sendDictionary.setValue("AppIconRound", forKey: "photoNamed")
                                        }
                                        
                                        self.performSegueWithIdentifier("showFriendJoinedFullScreenOverlayPopup", sender: sendDictionary)
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        self.openedFirstTime = false
        self.friendJoinedDictionary = nil
        
        if let user = UserCache().getUser() {
            if let userId = user["id"] as? NSString {
                if !NSUserDefaults.standardUserDefaults().boolForKey("\(userId as String)-visited-feed") {
                    self.openedFirstTime = true
                    
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "\(userId as String)-visited-feed")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            }
        }
        
        self.showState(false)
        
        newUpdatesButton.layer.cornerRadius = 0.5 * newUpdatesButton.bounds.size.height
        
        if let navigationController = self.navigationController {
            refresher.bounds = CGRectMake(refresher.bounds.origin.x, refresher.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher.bounds.size.width, refresher.bounds.size.height)
        } else {
            refresher.bounds = CGRectMake(refresher.bounds.origin.x, refresher.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher.bounds.size.width, refresher.bounds.size.height)
        }
        
        dispatch_async(NewFeedController.cacheShownQueue) {
            if self.cacheShown {
                self.refreshCache()
            } else {
                self.cacheShown = true
            }
            
            dispatch_async(NewFeedController.serverNotificationsQueue) {
                if self.serverNotifications.count <= 0 {
                    self.refreshFromServer(false, isFromUpdateButton: false)
                } else {
                    self.refreshFromClient(false, isFromUpdateButton: false)
                }
            }
        }
        
        self.registerForNotifications()
        self.segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(NewFeedController))
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.unregisterForNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    @IBAction func clickedOverlay(sender: AnyObject) {
        self.overlay.hidden = true
        
        self.overlayTitleLabel.hidden = true
        self.overlayContentLabel.hidden = true
        
        self.overlayArrowEnvelopeImage.hidden = true
        self.overlayArrowPlusImage.hidden = true
    }
    
    private func showState(shouldHaveContent: Bool) {
        if shouldHaveContent && self.notifications.count <= 0 {
            self.noContentTitleLabel.hidden = false
            self.noContentContentLabel.hidden = false
            
            self.noContentTitleLabel.text = "Inspire your friends!"
            self.noContentContentLabel.text = "Start to build a good habit and inspire friends by sharing. It makes the effort count double!"
            
            self.noContentArrowImage.hidden = false
            
            self.noContentButton.hidden = false
            self.noContentButton.enabled = true
        } else {
            self.noContentTitleLabel.hidden = true
            self.noContentContentLabel.hidden = true
            
            self.noContentArrowImage.hidden = true
            
            self.noContentButton.hidden = true
            self.noContentButton.enabled = false
        }
        
        if openedFirstTime {
            self.overlay.hidden = false
            self.overlayTitleLabel.hidden = false
            self.overlayContentLabel.hidden = false
            
            var hasFriends = false
            
            if let user = UserCache().getUser() {
                if let countFriends = user["countFriends"] as? NSNumber {
                    if Int(countFriends) > 0 {
                        hasFriends = true
                    }
                }
            }
            
            if hasFriends {
                self.overlayTitleLabel.text = "Find your friends!"
                self.overlayContentLabel.text = "When trying to build a good habit, friends are a great source of inspiration and daily motivation. Use the + icon in the top right corner of this screen to see friends already using Habitlab."
                
                self.overlayArrowEnvelopeImage.hidden = true
                self.overlayArrowPlusImage.hidden = false
            } else {
                self.overlayTitleLabel.text = "Make it human!"
                self.overlayContentLabel.text = "Your close friends and family are a powerful force on your way to building a good habit. Use the invite icon in the navigation bar to invite them to Habitlab.\nAnd who knows, you might also inspire someone to make a change."
                
                self.overlayArrowEnvelopeImage.hidden = false
                self.overlayArrowPlusImage.hidden = true
            }
            
            openedFirstTime = false
        }
    }
    
    // MARK: - Refreshers
    
    func refreshPullToRefresh() {
        self.refreshFromServer(false, isFromUpdateButton: false)
    }
    
    private func refreshCache() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            // Get the notifications from the server.
            HabitlabRestAPI.getNotificationsFeed(0, limit: self.downloadedWindow, handler: { (error, newNotificationsJson) -> () in
                if error != nil {
                    return
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    let oldNotifications = NSArray(array: self.notifications)
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                        var existingIds = Set<String>()
                        
                        for oldNotificationJson in oldNotifications {
                            if let oldNotification = oldNotificationJson as? NSDictionary {
                                if let oldNotificationId = oldNotification["id"] as? NSString {
                                    existingIds.insert(oldNotificationId as String)
                                }
                            }
                        }
                        
                        var existingCacheIds = Set<String>()
                        
                        for oldNotificationJson in oldNotifications {
                            if let oldNotification = oldNotificationJson as? NSDictionary {
                                if let oldNotificationCacheId = oldNotification["cacheId"] as? NSString {
                                    existingCacheIds.insert(oldNotificationCacheId as String)
                                }
                            }
                        }
                        
                        var new = false
                        
                        if let newNotifications = newNotificationsJson as? NSArray {
                            for newNotificationJson in newNotifications {
                                if new {
                                    break
                                }
                                
                                if let newNotification = newNotificationJson as? NSDictionary {
                                    var found = false
                                    
                                    if let newNotificationType = newNotification["mobileNotificationType"] as? NSString {
                                        if (newNotificationType as String).compare("program-step-complete") == NSComparisonResult.OrderedSame {
                                            found = true
                                        }
                                    }
                                    
                                    if let newNotificationType = newNotification["mobileNotificationType"] as? NSString {
                                        if (newNotificationType as String).compare("program-first-step-complete") == NSComparisonResult.OrderedSame {
                                            found = true
                                        }
                                    }
                                    
                                    if let newNotificationId = newNotification["id"] as? NSString {
                                        if existingIds.contains(newNotificationId as String) {
                                            found = true
                                        }
                                    }
                                    
                                    if let newNotificationCacheId = newNotification["cacheId"] as? NSString {
                                        if existingCacheIds.contains(newNotificationCacheId as String) {
                                            found = true
                                        }
                                    }
                                    
                                    if found == false {
                                        new = true
                                    }
                                }
                            }
                        }
                        
                        if new {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.newUpdatesButton.hidden = false
                            })
                        }
                    })
                })
            })
        }
    }
    
    private func computeCommentsHeight(countComments: Int) -> CGFloat {
        switch (countComments) {
        case 0:
            return CGFloat(0)
        case 1:
            return CGFloat(47)
        case 2:
            return CGFloat(68)
        case 3:
            return CGFloat(89)
        default:
            return CGFloat(100)
        }
    }
    
    private func precomputeCellHeight(mobileNotification: NSDictionary, index: Int) {
        let countComments = Serv.countComments(mobileNotification)
        
        var height = CGFloat(0)
        
        // Add the height of the header
        height += 104
        
        // Add the height of the comments box
        height += computeCommentsHeight(countComments)
        
        if let mobileNotificationType = mobileNotification["mobileNotificationType"] as? String {
            switch mobileNotificationType {
            case "program-first-step-complete", "program-step-complete":
                if Serv.hasCapture(mobileNotification) {
                    
                    // Add the height of the picture
                    height += UIScreen.mainScreen().bounds.width
                }
                
                break
            case "thinking-about-you", "sharing-emotions", "unplugging-plan", "reconnect-action", "physio-progress-chart":
                
                // Add the height of the thinking about you cloud
                height += UIScreen.mainScreen().bounds.width
                
                break
            default:
                
                // Do not do anything.
                break
            }
        }
        
        cellHeights.setObject(height, forKey: index)
    }
    
    private func refreshFromClient(isInfiniteScroll: Bool, isFromUpdateButton: Bool) {
        dispatch_async(NewFeedController.serverNotificationsQueue) {
            let filteredNotifications = self.filterNotificationsForFeed(self.serverNotifications)
            
            // Precompute the heights of these cells!
            for i in 0 ..< filteredNotifications.count {
                if let mobileNotification = filteredNotifications[i] as? NSDictionary {
                    self.precomputeCellHeight(mobileNotification, index: i)
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.notifications = filteredNotifications
                
                self.feedTableView.reloadData()
                
                self.showState(true)
                
                if isInfiniteScroll {
                    self.feedTableView.finishInfiniteScroll()
                }
                
                self.refresher.endRefreshing()
            
                if isFromUpdateButton {
                    self.scrollToTop()
                }
            })
        }
    }
    
    private func refreshFromServer(isInfiniteScroll: Bool, isFromUpdateButton: Bool) {
        dispatch_async(NewFeedController.downloadedCountQueue) {
            if (!isInfiniteScroll) {
                self.downloadedCount = 0
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.newUpdatesButton.hidden = true
                Serv.showSpinner()
            })
         
            let skip = self.downloadedCount
            let limit = self.downloadedWindow
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                HabitlabRestAPI.getNotificationsFeed(skip, limit: limit, handler: { (error, notificationsJson) -> () in
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        Serv.hideSpinner()
                    })
                    
                    if (error != nil) {
                        dispatch_async(dispatch_get_main_queue(), {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        })
                        
                        return
                    }
                    
                    if let notificationsArray = notificationsJson as? NSArray {
                        dispatch_async(NewFeedController.serverNotificationsQueue, {
                            if !isInfiniteScroll {
                                self.serverNotifications = notificationsArray
                                let serverNotes = NSArray(array: self.serverNotifications)
                                
                                dispatch_async(NewFeedController.downloadedCountQueue, {
                                    self.downloadedCount = serverNotes.count
                                    
                                    self.refreshFromClient(isInfiniteScroll, isFromUpdateButton: isFromUpdateButton)
                                })
                            } else {
                                let serverNotes = NSArray(array: self.serverNotifications)
                                
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                    
                                    var set = Set<String>()
                                    let array = NSMutableArray()
                                    
                                    for notificationJson in serverNotes {
                                        if let notification = notificationJson as? NSDictionary {
                                            if let id = notification["id"] as? NSString {
                                                if !set.contains(id as String) {
                                                    set.insert(id as String)
                                                    array.addObject(notificationJson)
                                                }
                                            }
                                        }
                                    }
                                    
                                    for notificationJson in notificationsArray {
                                        if let notification = notificationJson as? NSDictionary {
                                            if let id = notification["id"] as? NSString {
                                                if !set.contains(id as String) {
                                                    set.insert(id as String)
                                                    array.addObject(notificationJson)
                                                }
                                            }
                                        }
                                    }
                                    
                                    dispatch_async(NewFeedController.serverNotificationsQueue, {
                                        self.serverNotifications = array
                                        let serverNotesCount = array.count
                                        
                                        dispatch_async(NewFeedController.downloadedCountQueue, {
                                            self.downloadedCount = serverNotesCount
                                            
                                            self.refreshFromClient(isInfiniteScroll, isFromUpdateButton: isFromUpdateButton)
                                        })
                                    })
                                })
                            }
                        })
                    }
                })
            })
        }
    }
    
    // Must be called from the main thread.
    func refreshFromClientComment() {
        dispatch_async(dispatch_get_main_queue()) {
            if let indexPaths = self.feedTableView.indexPathsForVisibleRows {
                for indexPath in indexPaths {
                    if self.notifications.count <= indexPath.row - 1 {
                        continue
                    }
                    
                    if indexPath.row <= 0 {
                        continue
                    }
                    
                    if let notification = self.notifications[indexPath.row - 1] as? NSDictionary {
                        self.precomputeCellHeight(notification, index: indexPath.row - 1)
                    }
                }
                
                self.refreshFromClient(false, isFromUpdateButton: false)
                
                // feedTableView.reloadRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.None)
            }
        }
    }
    
    // Must be called from the main thread.
    func refreshFromClientLike() {
        dispatch_async(dispatch_get_main_queue()) {
            
            if let indexPaths = self.feedTableView.indexPathsForVisibleRows {
                for indexPath in indexPaths {
                    let cellObject = self.feedTableView.cellForRowAtIndexPath(indexPath)
                    
                    if self.notifications.count <= indexPath.row - 1 {
                        continue
                    }
                    
                    if indexPath.row <= 0 {
                        continue
                    }
                    
                    if let mobileNotification = self.notifications[indexPath.row - 1] as? NSMutableDictionary {
                        
                        // Add the cache likes.
                        Serv.addCacheLikes(mobileNotification)
                        
                        if let cell = cellObject as? NormalFeedCellWithComments {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                cell.setLiking()
                            })
                            
                            continue
                        }
                        
                        if let cell = cellObject as? NormalFeedCellWithCaptureAndComments {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                cell.setLiking()
                            })
                            
                            continue
                        }
                        
                        if let cell = cellObject as? ThinkingAboutYouFeedCellWithComments {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                cell.setLiking()
                            })
                            
                            continue
                        }
                        
                        if let cell = cellObject as? ReconnectActionFeedCellWithComments {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                cell.setLiking()
                            })
                            
                            continue
                        }
                        
                        if let cell = cellObject as? PhysioProgressChartFeedCellWithComments {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                cell.setLiking()
                            })
                            
                            continue
                        }
                        
                        if let cell = cellObject as? UnpluggingPlanFeedCellWithComments {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                cell.setLiking()
                            })
                            
                            continue
                        }
                        
                        if let cell = cellObject as? SharingEmotionsFeedCellWithComments {
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                                cell.setLiking()
                            })
                            
                            continue
                        }
                    }
                }
            }
        }
    }

    private func filterNotificationsForFeed(receivedNotifications: NSArray) -> NSArray {
        let processedNotifications: NSMutableArray = NSMutableArray()
        
        let caches = InspirationCache().getCaches()
        
        for cacheElement in caches {
            if let cache = cacheElement as? NSMutableDictionary {
                if let cacheCacheId = cache["cacheId"] as? String {
                    var found = false
                    
                    for i in 0 ..< receivedNotifications.count {
                        if let notification = receivedNotifications[i] as? NSDictionary {
                            if let notificationCacheId = notification["cacheId"] as? String {
                                if cacheCacheId.compare(notificationCacheId) == NSComparisonResult.OrderedSame {
                                    found = true
                                }
                            }
                        }
                        
                        if found {
                            break
                        }
                    }
                    
                    if !found {
                        processedNotifications.addObject(cache)
                    }
                }
            }
        }
        
        processedNotifications.addObjectsFromArray(receivedNotifications as [AnyObject])
        
        let cachedNotifications: NSMutableArray = NSMutableArray()
        
        for i in 0 ..< processedNotifications.count {
            if let notification = processedNotifications[i] as? NSDictionary {
                let cachedNotification: NSMutableDictionary = NSMutableDictionary(dictionary: notification)
                Serv.addCacheComments(cachedNotification)
                Serv.addCacheLikes(cachedNotification)
                cachedNotifications.addObject(cachedNotification)
            }
        }
        
        let result: NSMutableArray = NSMutableArray()
        
        for i in 0 ..< cachedNotifications.count {
            if let notification = cachedNotifications[i] as? NSDictionary {
                if isNotificationTypeAllowed(notification) {
                    result.addObject(notification)
                }
            }
        }
        
        return result as NSArray
    }
    
    private func isNotificationTypeAllowed(notification: NSDictionary) -> Bool {
        if let type = notification["mobileNotificationType"] as? NSString {
            switch (type as String) {
            case "program-complete", "program-first-step-complete", "program-step-complete", "program-signup-level-1", "program-step-overdue-first", "program-step-overdue-second", "thinking-about-you", "sharing-emotions", "unplugging-plan", "reconnect-action", "physio-progress-chart":
                return true
            default:
                return false
            }
        }
        
        return false
    }
    
    // Mark: - UI updaters
    
    private func scrollToTop() {
        dispatch_async(dispatch_get_main_queue()) {
            let numberOfSections = self.feedTableView.numberOfSections
            
            if numberOfSections < 1 {
                return
            }
            
            let numberOfRows = self.feedTableView.numberOfRowsInSection(0)
            
            if numberOfRows < 1 {
                return
            }
            
            let indexPath = NSIndexPath(forRow: NSNotFound, inSection: 0)
            self.feedTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: false)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func clickedNewUpdates(sender: AnyObject) {
        self.refreshFromServer(false, isFromUpdateButton: true)
    }
    
    @IBAction func newFeedWroteMessage(segue: UIStoryboardSegue) {
        refreshFromClientComment()
    }

    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showWriteMessageFromFeed") {
            if let viewController = segue.destinationViewController as? WriteMessageController {
                if let notification = sender as? NSDictionary {
                    viewController.notification = notification
                }
            }
        }
        
        if (segue.identifier == "showPublicProfileFromFeed") {
            if let viewController = segue.destinationViewController as? PublicProfileController {
                if let userId = sender as? String {
                    viewController.userId = userId
                }
            }
        }
        
        if segue.identifier == "showFriendJoinedFullScreenOverlayPopup" {
            if let dictionary = sender as? NSDictionary {
                if let viewController = segue.destinationViewController as? FullScreenOverlayPopup {
                    viewController.setController(self)
                    
                    var id: String = ""
                    var title: String? = nil
                    var description: String? = nil
                    var photoURL: String? = nil
                    var photoNamed: String? = nil
                    var actionTitle: String? = nil
                    
                    if let realTitle = dictionary["title"] as? NSString {
                        title = realTitle as String
                    }
                    
                    if let realDescription = dictionary["description"] as? NSString {
                        description = realDescription as String
                    }
                    
                    if let realPhotoURL = dictionary["photoURL"] as? NSString {
                        photoURL = realPhotoURL as String
                    }
                    
                    if let realPhotoNamed = dictionary["photoNamed"] as? NSString {
                        photoNamed = realPhotoNamed as String
                    }
                    
                    if let realActionTitle = dictionary["actionTitle"] as? NSString {
                        actionTitle = realActionTitle as String
                    }
                    
                    if let realId = dictionary["id"] as? NSString {
                        id = realId as String
                    }
                    
                    let action = {(dict: NSDictionary) -> () in
                        if let idInside = dict["id"] as? NSString {
                            Serv.showSpinner()
                            
                            HabitlabRestAPI.postFollow(idInside as String, handler: { (error, response) -> () in
                                Serv.hideSpinner()
                                
                                if error != nil {
                                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                    return
                                }
                            })
                        }
                    }
                    
                    var buttons: [FullScreenOverlayPopupButton] = []
                    let sentDictionary = NSMutableDictionary()
                    sentDictionary.setValue(id, forKey: "id")
                    
                    buttons.append(FullScreenOverlayPopupButton(type: FullScreenOverlayPopupButtonType.Green, title: actionTitle, dictionary: sentDictionary, action: action))
                    
                    if photoURL != nil {
                        viewController.setContent(title, description: description, photoURL: photoURL, buttons: buttons)
                    } else {
                        viewController.setContent(title, description: description, photoNamed: photoNamed, buttons: buttons)
                    }
                }
            }
        }
    }
    
    func closedPopup(name: String) {
        if name == "friendJoined" {
            self.friendJoinedDictionary = nil
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        
        if notifications.count <= indexPath.row - 1 {
            return UITableViewAutomaticDimension
        }
        
        if let height = cellHeights.objectForKey(indexPath.row - 1) as? CGFloat {
            return height
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("priorFeedCell", forIndexPath: indexPath) as? PriorFeedCell {
                return cell
            }
            
            return defaultCell
        }
        
        if notifications.count <= indexPath.row - 1 {
            return defaultCell
        }
        
        if let mobileNotification = notifications[indexPath.row - 1] as? NSMutableDictionary {
            
            if let mobileNotificationType = mobileNotification["mobileNotificationType"] as? String {
                switch mobileNotificationType {
                    
                case "program-complete", "program-signup-level-1":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("normalFeedCellWithComments", forIndexPath: indexPath) as? NormalFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                case "program-first-step-complete", "program-step-complete":
                    if Serv.hasCapture(mobileNotification) {
                        if let cell = tableView.dequeueReusableCellWithIdentifier("normalFeedCellWithCaptureAndComments", forIndexPath: indexPath) as? NormalFeedCellWithCaptureAndComments {
                            cell.layoutIfNeeded()
                            return cell
                        }
                    }
                    
                    if let cell = tableView.dequeueReusableCellWithIdentifier("normalFeedCellWithComments", forIndexPath: indexPath) as? NormalFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                case "program-step-overdue-first", "program-step-overdue-second":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("overdueFeedCellWithComments", forIndexPath: indexPath) as? OverdueFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                case "thinking-about-you":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("thinkingAboutYouFeedCellWithComments", forIndexPath: indexPath) as? ThinkingAboutYouFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                case "reconnect-action":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("reconnectActionFeedCellWithComments", forIndexPath: indexPath) as? ReconnectActionFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                case "physio-progress-chart":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("physioProgressChartFeedCellWithComments", forIndexPath: indexPath) as? PhysioProgressChartFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                case "unplugging-plan":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("unpluggingPlanFeedCellWithComments", forIndexPath: indexPath) as? UnpluggingPlanFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                case "sharing-emotions":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("sharingEmotionsFeedCellWithComments", forIndexPath: indexPath) as? SharingEmotionsFeedCellWithComments {
                        cell.layoutIfNeeded()
                        return cell
                    }
                    
                default:
                    return defaultCell
                }
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cellObject: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    
        if indexPath.row == 0 {
            return
        }
        
        if notifications.count <= indexPath.row - 1 {
            return
        }
        
        if let mobileNotification = notifications[indexPath.row - 1] as? NSMutableDictionary {
            
            // Add the cache comments.
            Serv.addCacheComments(mobileNotification)
            
            // Add the cache likes.
            Serv.addCacheLikes(mobileNotification)
            
            if let cell = cellObject as? NormalFeedCellWithComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
            
            if let cell = cellObject as? NormalFeedCellWithCaptureAndComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.captureImage.image = nil
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
        
            if let cell = cellObject as? OverdueFeedCellWithComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
        
            if let cell = cellObject as? ThinkingAboutYouFeedCellWithComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.messageLabel.text = ""
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
            
            if let cell = cellObject as? ReconnectActionFeedCellWithComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
            
            if let cell = cellObject as? PhysioProgressChartFeedCellWithComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
            
            if let cell = cellObject as? UnpluggingPlanFeedCellWithComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.messageLabel.text = ""
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
            
            if let cell = cellObject as? SharingEmotionsFeedCellWithComments {
                cell.cellMessage.text = ""
                cell.cellTime.text = ""
                cell.firstComment.text = ""
                cell.missingComments.text = ""
                cell.secondToLastComment.text = ""
                cell.lastComment.text = ""
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    cell.populate(false, notification: mobileNotification, controller: self, showButtons: true)
                })
                
                return
            }
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
