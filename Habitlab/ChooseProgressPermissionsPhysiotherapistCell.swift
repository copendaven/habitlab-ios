//
//  ChooseProgressPermissionsPhysiotherapistCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ChooseProgressPermissionsPhysiotherapistCell: UITableViewCell {
    var controller: UIViewController?
    
    @IBOutlet weak var switchControl: UISwitch!
    
    func populate(controller: UIViewController, subcategory: NSDictionary?) {
        self.controller = controller
        
        switchControl.addTarget(self, action: #selector(ChooseProgressPermissionsPhysiotherapistCell.switchChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        if let realSubcategory = subcategory {
            if let physioProgram = realSubcategory["physioProgram"] as? NSDictionary {
                if let physioProgramPermission = physioProgram["shareProgressWithPhysiotherapist"] as? NSNumber {
                    if physioProgramPermission == 1 {
                        self.switchControl.on = true
                    } else {
                        self.switchControl.on = false
                    }
                } else {
                    self.switchControl.on = false
                }
            }
        }
    }
    
    func switchChanged(sw: UISwitch) {
        if let viewController = self.controller as? ChooseProgressPermissionsController {
            viewController.switchChangedPhysiotherapist(sw.on)
        }
    }
}
