//
//  MessageTableComment.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/4/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class MessageTableComment: UITableViewCell, UITextViewDelegate {
    var controller: UIViewController?
    var notification: NSDictionary?
    let placeholderText = "Write a comment here"
    var commented = false
    
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBAction func sendMessage(sender: AnyObject) {
        if commented {
            return
        }
        
        if controller == nil {
            return
        }
        
        if notification == nil {
            return
        }
        
        if messageTextView.text == placeholderText || messageTextView.text.compare("") == NSComparisonResult.OrderedSame {
            Serv.showErrorPopup("Please write a message", message: "You cannot send an empty message.", okMessage: "Okay, got it.", controller: controller!, handler: nil)
            return
        }
        
        if let notificationId = notification!["id"] as? NSString, notificationBaseId = notification!["mobileNotificationBase"] as? NSString, notificationCacheId = notification!["cacheId"] as? NSString {
            sendMessageInternal(notificationId as String, notificationBaseId: notificationBaseId as String, notificationCacheId: notificationCacheId as String)
            return
        }
        
        if let notificationId = notification!["id"] as? NSString, notificationBaseId = notification!["mobileNotificationBase"] as? NSString {
            sendMessageInternal(notificationId as String, notificationBaseId: notificationBaseId as String)
            return
        }
        
        if let notificationCacheId = notification!["cacheId"] as? NSString {
            sendMessageInternal(notificationCacheId as String)
            return
        }
    }
    
    private func sendMessageInternal(mobileNotificationCacheId: String) {
        HabitlabRestAPI.getFakeNotification(mobileNotificationCacheId as String, handler: { (error, fakeNotificationJson) -> () in
            if (error != nil) {
                Serv.showErrorPopup("Send message error", message: "The post you want to comment is not yet ready. Try again in a few seconds.", okMessage: "Okay, got it.", controller: self.controller!, handler: nil)
                return
            }
            
            if let fakeNotification = fakeNotificationJson as? NSDictionary {
                if let mobileNotificationId = fakeNotification["id"] as? NSString, notificationBaseId = fakeNotification["mobileNotificationBase"] as? NSString {
                    self.sendMessageInternal(mobileNotificationId as String, notificationBaseId: notificationBaseId as String, notificationCacheId: mobileNotificationCacheId)
                    return
                }
                
                Serv.showErrorPopup("Send message error", message: "The post you want to comment is not yet ready. Try again in a few seconds.", okMessage: "Okay, got it.", controller: self.controller!, handler: nil)
            }
        })
    }
    
    private func sendMessageInternal(notificationId: String, notificationBaseId: String) {
        if let message = messageTextView.text {
            self.commented = true
            
            if let user = UserCache().getUser() {
                let cacheId = NSUUID().UUIDString
                
                MobileNotificationCommentCache().addComment(cacheId, owner: user, mobileNotificationId: notificationId, mobileNotificationBaseId: notificationBaseId, message: message)
                
                if let writeMessageController = self.controller! as? WriteMessageController {
                    let sender = NSMutableDictionary()
                    sender.setObject(notificationId, forKey: "mobileNotificationId")
                    writeMessageController.performSegueWithIdentifier("showFeedFromWriteMessage", sender: sender)
                }
                
                Serv.showSpinner()
                
                HabitlabRestAPI.postComment(notificationId as String, notificationBaseId: notificationBaseId as String, cacheId: cacheId, message: message, handler: { (error, response) -> () in
                    Serv.hideSpinner()
                    
                    if (error != nil) {
                        self.commented = false
                        Serv.showErrorPopupFromError(error, controller: self.controller!, handler: nil)
                    }
                })
            }
        }
    }
    
    private func sendMessageInternal(notificationId: String, notificationBaseId: String, notificationCacheId: String) {
        if let message = messageTextView.text {
            self.commented = true
            
            if let user = UserCache().getUser() {
                let cacheId = NSUUID().UUIDString
                
                MobileNotificationCommentCache().addComment(cacheId, owner: user, mobileNotificationId: notificationId, mobileNotificationBaseId: notificationBaseId, mobileNotificationCacheId: notificationCacheId, message: message)
                
                if let writeMessageController = self.controller! as? WriteMessageController {
                    let sender = NSMutableDictionary()
                    sender.setObject(notificationId, forKey: "mobileNotificationId")
                    writeMessageController.performSegueWithIdentifier("showFeedFromWriteMessage", sender: sender)
                }
                
                Serv.showSpinner()
                
                HabitlabRestAPI.postComment(notificationId as String, notificationBaseId: notificationBaseId as String, cacheId: cacheId, message: message, handler: { (error, response) -> () in
                    Serv.hideSpinner()
                    
                    if (error != nil) {
                        self.commented = false
                        Serv.showErrorPopupFromError(error, controller: self.controller!, handler: nil)
                    }
                })
            }
        }
    }

    func populate(controller: UIViewController, notification: NSDictionary) {
        self.controller = controller
        self.notification = notification
        
        messageTextView.delegate = self
        messageTextView.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5);
        messageTextView.text = placeholderText
        messageTextView.textColor = UIColor.lightGrayColor()
        
        commented = false
    }
    
    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageTableComment.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageTableComment.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if self.controller == nil {
            return
        }
        
        if let writeMessageController = self.controller! as? WriteMessageController {
            if let userInfo = notification.userInfo {
                if let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                    var tabBarHeight = CGFloat(0.0)
                    
                    if let tabBarController = writeMessageController.tabBarController {
                        let tabbarSize = tabBarController.tabBar.frame
                        tabBarHeight = tabbarSize.height
                    }
                    
                    let keyboardSize = keyboardFrame.CGRectValue().size
                    let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
                    
                    writeMessageController.commentsTable.contentInset = contentInset
                    writeMessageController.commentsTable.scrollIndicatorInsets = contentInset
                    
                    let tableRect = writeMessageController.commentsTable.frame
                    let compareRect = CGRectMake(0.0, 0.0, tableRect.size.width, tableRect.size.height - keyboardSize.height + tabBarHeight)
                    
                    let screenSize = UIScreen.mainScreen().bounds
                    let screenWidth = screenSize.width
                    
                    let headerHeight = CGFloat(90)
                    let commentsHeight = CGFloat(56 * writeMessageController.comments.count)
                    let likesHeight = CGFloat(writeMessageController.likes != nil ? 56 : 0)
                    let imageHeight = CGFloat(writeMessageController.image != nil ? screenWidth : 0)
                    
                    let offsetHeight = CGFloat(headerHeight + commentsHeight + likesHeight + imageHeight)
                    
                    let textboxFrame = CGRectMake(0.0, offsetHeight - tabBarHeight, messageTextView.frame.size.width, messageTextView.frame.size.height)
                    let textboxPoint = CGPointMake(0.0, offsetHeight + messageTextView.frame.size.height)
                    
                    if !CGRectContainsPoint(compareRect, textboxPoint) {
                        writeMessageController.commentsTable.scrollRectToVisible(textboxFrame, animated: false)
                    }
                }
            }
        }
    }

    func keyboardWillHide(notification: NSNotification) {
        if self.controller == nil {
            return
        }
        
        if let writeMessageController = self.controller! as? WriteMessageController {
            if let userInfo = notification.userInfo {
                if let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                    let keyboardSize = keyboardFrame.CGRectValue().size
                    let contentInset = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize.height, 0.0)
                    writeMessageController.commentsTable.contentInset = contentInset
                    writeMessageController.commentsTable.scrollIndicatorInsets = contentInset
                    writeMessageController.view.endEditing(true)
                }
            }
        }
    }
    
    // MARK: UITextViewDelegate methods.
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == placeholderText {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
}
