//
//  ModuleActionCollectionCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/18/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

enum ModuleActionType: String {
    case Start = "Start"
    case Continue = "Continue"
    case Stop = "Stop"
    case Done = "Done"
    case GiveUp = "GiveUp"
    case ThinkingAboutYou = "ThinkingAboutYou"
    case Back = "Back"
    case Later = "Later"
    case Now = "Now"
    case TryAgain = "TryAgain"
    case Info = "Info"
    case Pause = "Pause"
}

enum ModuleActionState: String {
    case Disabled = "Disabled"
    case Enabled = "Enabled"
}

class ModuleActionCollectionCell: UICollectionViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    private var actionType: ModuleActionType?
    private var controller: UIViewController?
    
    func populate(actionType actionType: ModuleActionType, actionState: ModuleActionState, controller: UIViewController) {
        self.actionType = actionType
        self.controller = controller
        
        self.button.layer.cornerRadius = 0.5 * self.button.bounds.size.width
        self.image.layer.cornerRadius = 0.5 * self.image.bounds.size.width
        
        switch actionState {
        case .Disabled:
            image.alpha = 0.5
            button.enabled = false
        case .Enabled:
            image.alpha = 1.0
            button.enabled = true
        }
        
        switch (actionType) {
        case .Start:
            label.text = "START"
            image.image = UIImage(named: "CTAStartRed")
            break
        case .Stop:
            label.text = "CANCEL"
            image.image = UIImage(named: "CTACancelRed")
            break
        case .Done:
            label.text = "DONE"
            image.image = UIImage(named: "CTACheckmarkRed")
            break
        case .ThinkingAboutYou:
            label.text = "THOUGHTS"
            image.image = UIImage(named: "CTAThoughtsRed")
            break
        case .GiveUp:
            label.text = "GIVE UP"
            image.image = UIImage(named: "CTACancelRed")
            break
        case .Back:
            label.text = "BACK"
            image.image = UIImage(named: "CTACancelRed")
            break
        case .Later:
            label.text = "LATER"
            image.image = UIImage(named: "CTACancelRed")
            break
        case .Now:
            label.text = "NOW"
            image.image = UIImage(named: "CTAStartRed")
            break
        case .TryAgain:
            label.text = "TRY AGAIN"
            image.image = UIImage(named: "CTAStartRed")
            break
        case .Info:
            label.text = "INFO"
            image.image = UIImage(named: "CTAInfoRed")
        case .Pause:
            label.text = "PAUSE"
            image.image = UIImage(named: "CTAPauseRed")
        case .Continue:
            label.text = "Continue"
            image.image = UIImage(named: "CTAStartRed")
        }
    }

    @IBAction func clickedButton(sender: AnyObject) {
        if let realActionType = self.actionType, realController = self.controller {
            if let runningModuleController = realController as? RunningModuleController {
                runningModuleController.buttonClicked(realActionType)
            }
            
            if let unpluggingModuleController = realController as? UnpluggingModuleController {
                unpluggingModuleController.buttonClicked(realActionType)
            }
            
            if let reconnectingModuleController = realController as? ReconnectModuleChooseMethodController {
                reconnectingModuleController.buttonClicked(realActionType)
            }
            
            if let physioModuleController = realController as? PhysioModuleController {
                physioModuleController.buttonClicked(realActionType)
            }
        }
    }
}
