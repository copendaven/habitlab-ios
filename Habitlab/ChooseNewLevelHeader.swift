//
//  ChooseNewLevelHeader.swift
//  Habitlab
//
//  Created by Vlad Manea on 1/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ChooseNewLevelHeader: UITableViewCell {
    @IBOutlet weak var programName: UILabel!
    @IBOutlet weak var programLevels: UILabel!
    @IBOutlet weak var programDescription: UILabel!
    
    func populate(drive: NSDictionary, levels: Int) {
        self.programLevels.text = "\(levels) LEVEL\(levels == 1 ? "" : "S")"
        
        if let title = drive["title"] as? NSString {
            self.programName.text = title as String
        }
        
        if let description = drive["description"] as? NSString {
            self.programDescription.text = description as String
            self.programDescription.sizeToFit()
        }
    }
}
