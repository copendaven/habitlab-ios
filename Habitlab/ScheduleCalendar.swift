//
//  ScheduleCalendar.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/15/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

enum ScheduleCalendarWeekRepresentation {
    case Complete
    case Pending
    case Future
}

enum ScheduleCalendarState {
    case ErrorTooManyWeeks
    case ErrorTooManyDaysInWeek
    case ErrorNotPrefix
    case ErrorDateBeforeToday
    
    case SuccessNoDaySelected
    case SuccessFirstDayOfFirstWeekSelected
    case SuccessFirstWeekSelected
    case SuccessAFewWeeksSelected
    case SuccessAllWeeksSelected
}

enum ScheduleCalendarDayColor: String {
    case ClearBlocked = "clearBlocked"
    case ClearWaiting = "clearWaiting"
    case WhitePending = "whitePending"
    case GreenComplete = "greenComplete"
    case GreenSelected = "greenSelected"
    case Empty = "empty"
}

class ScheduleCalendar: NSObject {
    private var dateIndexes = Set<Int>()
    private var daysCache = NSMutableDictionary()
    private var daysCacheBefore = NSMutableDictionary()
    private var daysCacheAfter = NSMutableDictionary()
    private var monthOfCurrentWeek: (year: Int, month: Int)?
    private var daysCacheArray: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] = []
    private var daysCacheArraySelected: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] = []
    
    private var countWeeks: Int
    private var countDays: Int
    
    init(countWeeks: Int, countDays: Int) {
        self.countWeeks = countWeeks
        self.countDays = countDays
        
        super.init()
        self.recomputeDays()
    }
    
    func getRepresentation(date: (year: Int, month: Int, day: Int)) -> Int {
        return 512 * date.year + 32 * date.month + date.day
    }
    
    func getMonthOfCurrentWeek() -> (year: Int, month: Int)? {
        return monthOfCurrentWeek
    }
    
    func getRepresentation(date: Int) -> (year: Int, month: Int, day: Int) {
        let year = date / 512
        let month = (date - year * 512) / 32
        let day = (date - year * 512 - month * 32)
        
        return (year, month, day)
    }
    
    func isEnabled(date: (year: Int, month: Int, day: Int)) -> Bool {
        let representation = getRepresentation(date)
        return dateIndexes.contains(representation)
    }
    
    func toggleDate(date: (year: Int, month: Int, day: Int), bulkCount: Int) -> ScheduleCalendarState {
        let representation = getRepresentation(date)
        
        if dateIndexes.contains(representation) {
            return removeDate(date, withBulkCount: bulkCount != 1)
        } else {
            return addDate(date, bulkCount: bulkCount)
        }
    }
    
    func isLeapYear(year: Int) -> Bool {
        return (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)
    }
    
    func getLastDay(date: (year: Int, month: Int)) -> Int {
        switch date.month {
        case 1, 3, 5, 7, 8, 10, 12:
            return 31
        case 4, 6, 9, 11:
            return 30
        case 2:
            if isLeapYear(date.year) {
                return 29
            }
            
            return 28
        default:
            return 1
        }
    }
    
    func getNextMonth(month: (year: Int, month: Int)) -> (year: Int, month: Int) {
        var monthValue = month.month
        var yearValue = month.year
        
        monthValue = monthValue + 1
        
        if monthValue > 12 {
            monthValue = 1
            yearValue = yearValue + 1
        }
        
        return (year: yearValue, month: monthValue)
    }
    
    func getState() -> ScheduleCalendarState {
        let minElement = dateIndexes.minElement()
        let maxElement = dateIndexes.maxElement()
        
        if minElement == nil || maxElement == nil {
            return ScheduleCalendarState.SuccessNoDaySelected
        }
        
        let tupleNow = getNow()
        
        if tupleNow == nil {
            return ScheduleCalendarState.ErrorDateBeforeToday
        }
        
        let minTuple = getRepresentation(minElement!)
        
        if compareDates(minTuple, otherDate: tupleNow!) < 0 {
            return ScheduleCalendarState.ErrorDateBeforeToday
        }
        
        let maxTuple = getRepresentation(maxElement!)
        
        let interval = getDaysBetween(minTuple, otherDate: maxTuple)
        
        if interval >= countWeeks * 7 {
            return ScheduleCalendarState.ErrorTooManyWeeks
        }
        
        let lastWeek = interval / 7
        let daysInEachWeek = getDaysInEachWeek()
        
        for week in 0...lastWeek {
            if daysInEachWeek.objectForKey(week) as! Int > countDays {
                return ScheduleCalendarState.ErrorTooManyDaysInWeek
            }
        }
        
        let lastCompleteWeek = getLastCompleteWeek()
        
        if lastCompleteWeek == nil {
            return ScheduleCalendarState.ErrorNotPrefix
        }
        
        if lastCompleteWeek == -1 {
            return ScheduleCalendarState.SuccessFirstDayOfFirstWeekSelected
        }
        
        if lastCompleteWeek == countWeeks - 1 {
            return ScheduleCalendarState.SuccessAllWeeksSelected
        }
        
        if lastCompleteWeek == 0 {
            return ScheduleCalendarState.SuccessFirstWeekSelected
        }
        
        return ScheduleCalendarState.SuccessAFewWeeksSelected
    }
    
    func countSelectedDaysInCurrentWeek() -> Int {
        if let lastCompleteWeek = getLastCompleteWeek() {
            let daysInEachWeek = getDaysInEachWeek()
            
            if lastCompleteWeek >= countWeeks - 1 {
                return 0
            }
            
            if let daysInNextWeekObject = daysInEachWeek.objectForKey(lastCompleteWeek + 1) {
                if let daysInNextWeek = daysInNextWeekObject as? Int {
                    return daysInNextWeek
                }
            }
        }
        
        return 0
    }
    
    func getMonths() -> [(year: Int, month: Int)]? {
        var minTupleNullable: (year: Int, month: Int, day: Int)?
        
        if let minElement = dateIndexes.minElement() {
            minTupleNullable = getRepresentation(minElement)
        } else {
            minTupleNullable = getNow()
        }
        
        if let minTuple = minTupleNullable {
            let maxTuple = getDatePlusDays(minTuple, days: 7 * countWeeks - 1)
            return getMonths(minTuple, maxTuple: maxTuple)
        }
        
        return nil
    }
    
    private func recomputeDaysArray() {
        daysCacheArray.removeAll()
        daysCacheArraySelected.removeAll()
        
        let sortCmp = { (key1: AnyObject, key2: AnyObject) -> Bool in
            if let key1int = key1 as? Int, key2int = key2 as? Int {
                return key1int < key2int
            }
            
            return false
        }
        
        let beforeKeys = daysCacheBefore.allKeys.sort(sortCmp)
        
        for key in beforeKeys {
            if let entry = daysCacheBefore.objectForKey(key) {
                if let dict = entry as? NSDictionary {
                    if let value = getDate(dict) {
                        daysCacheArray.append(value)
                    }
                }
            }
        }
        
        let innerKeys = daysCache.allKeys.sort(sortCmp)
        
        for key in innerKeys {
            if let entry = daysCache.objectForKey(key) {
                if let dict = entry as? NSDictionary {
                    if let value = getDate(dict) {
                        daysCacheArray.append(value)
                        
                        if value.color == ScheduleCalendarDayColor.GreenSelected {
                            daysCacheArraySelected.append(value)
                        }
                    }
                }
            }
        }
        
        let afterKeys = daysCacheAfter.allKeys.sort(sortCmp)
        
        for key in afterKeys {
            if let entry = daysCacheAfter.objectForKey(key) {
                if let dict = entry as? NSDictionary {
                    if let value = getDate(dict) {
                        daysCacheArray.append(value)
                    }
                }
            }
        }
    }
    
    func getDays() -> [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] {
        var response: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] = []
        response.appendContentsOf(daysCacheArray)
        return response
    }
    
    func countSelectedDays() -> Int {
        return daysCacheArraySelected.count
    }
    
    func getSelectedDays() -> [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] {
        var response: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] = []
        response.appendContentsOf(daysCacheArraySelected)
        return response
    }
    
    func getDaysByMonth(month: (year: Int, month: Int)) -> [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] {
        var response: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] = []
        
        let days = getDays()
        
        for day in days {
            if day.forMonth.year == month.year && day.forMonth.month == month.month {
                response.append(day)
            }
        }
        
        return response
    }
    
    func getDay(date: (year: Int, month: Int, day: Int), forMonth: (year: Int, month: Int)) -> (date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))? {
        let representation = getRepresentation(date)
        
        if forMonth.month == date.month && forMonth.year == date.year {
            if let dayObject = daysCache.objectForKey(representation) {
                if let day = dayObject as? NSDictionary {
                    if let value = getDate(day) {
                        return value
                    }
                }
            }
        } else {
            if let dayObject = daysCacheBefore.objectForKey(representation) {
                if let day = dayObject as? NSDictionary {
                    if let value = getDate(day) {
                        return value
                    }
                }
            }
            
            if let dayObject = daysCacheAfter.objectForKey(representation) {
                if let day = dayObject as? NSDictionary {
                    if let value = getDate(day) {
                        return value
                    }
                }
            }
        }
        
        return nil
    }
    
    func getMonthPreOffset(startDate: (year: Int, month: Int, day: Int)) -> Int? {
        let componentsDate = NSDateComponents()
        componentsDate.year = startDate.year
        componentsDate.month = startDate.month
        componentsDate.day = startDate.day
        
        if let calendar = Serv.calendar {
            if let date = calendar.dateFromComponents(componentsDate) {
                return Serv.getWeekDay(date)
            }
        }
        
        return nil
    }
    
    func getMonthPostOffset(endDate: (year: Int, month: Int, day: Int)) -> Int? {
        let componentsDate = NSDateComponents()
        componentsDate.year = endDate.year
        componentsDate.month = endDate.month
        componentsDate.day = endDate.day
        
        if let calendar = Serv.calendar {
            if let date = calendar.dateFromComponents(componentsDate) {
                if let weekDay = Serv.getWeekDay(date) {
                    return 6 - weekDay
                }
            }
        }
        
        return nil
    }
    
    func compareDates(date: (year: Int, month: Int, day: Int), otherDate: (year: Int, month: Int, day: Int)) -> Int {
        if date.year > otherDate.year {
            return 1
        }
        
        if date.year < otherDate.year {
            return -1
        }
        
        if date.month > otherDate.month {
            return 1
        }
        
        if date.month < otherDate.month {
            return -1
        }
        
        if date.day > otherDate.day {
            return 1
        }
        
        if date.day < otherDate.day {
            return -1
        }
        
        return 0
    }
    
    func getNow() -> (year: Int, month: Int, day: Int)? {
        // Return the month starting from the current date if no date has been selected. :)
        let dateNow = NSDate()
        
        if let calendar = Serv.calendar {
            let dayNow = calendar.component(NSCalendarUnit.Day, fromDate: dateNow)
            let monthNow = calendar.component(NSCalendarUnit.Month, fromDate: dateNow)
            let yearNow = calendar.component(NSCalendarUnit.Year, fromDate: dateNow)
            return (year: yearNow, month: monthNow, day: dayNow)
        }
        
        return nil
    }
    
    func getNextDate(date: (year: Int, month: Int, day: Int)) -> (year: Int, month: Int, day: Int) {
        var year = date.year
        var month = date.month
        var day = date.day
        
        day = day + 1
        
        switch month {
        case 1, 3, 5, 7, 8, 10, 12:
            if day > 31 {
                day = 1
                month = month + 1
            }
        case 2:
            if isLeapYear(year) && day > 29 {
                day = 1
                month = month + 1
            } else if day > 28 {
                day = 1
                month = month + 1
            }
        case 4, 6, 9, 11:
            if day > 30 {
                day = 1
                month = month + 1
            }
        default:
            break
        }
        
        if month > 12 {
            month = 1
            year = year + 1
        }
        
        return (year, month, day)
    }
    
    private func getPreviousDate(date: (year: Int, month: Int, day: Int)) -> (year: Int, month: Int, day: Int) {
        var year = date.year
        var month = date.month
        var day = date.day
        
        day = day - 1
        
        if day <= 0 {
            month = month - 1
            
            if month <= 0 {
                year = year - 1
                month = 12
            }
            
            switch month {
            case 1, 3, 5, 7, 8, 10, 12:
                day = 31
            case 2:
                if isLeapYear(year) {
                    day = 29
                } else {
                    day = 28
                }
            case 4, 6, 9, 11:
                day = 30
            default:
                break
            }
        }
        
        return (year, month, day)
    }
    
    private func getDaysBetween(date: (year: Int, month: Int, day: Int), otherDate: (year: Int, month: Int, day: Int)) -> Int {
        let comparison = compareDates(date, otherDate: otherDate)
        
        if comparison > 0 {
            return getDaysBetween(otherDate, otherDate: date)
        }
        
        // Date1 is before otherDate
        var steps: Int = 0
        var theDate = date
        
        while compareDates(theDate, otherDate: otherDate) < 0 {
            steps = steps + 1
            theDate = getNextDate(theDate)
        }
        
        return steps
    }
    
    private func getDatePlusDays(date: (year: Int, month: Int, day: Int), days: Int) -> (year: Int, month: Int, day: Int) {
        if days <= 0 {
            return date
        }
        
        return getDatePlusDays(getNextDate(date), days: days - 1)
    }
    
    private func getDateMinusDays(date: (year: Int, month: Int, day: Int), days: Int) -> (year: Int, month: Int, day: Int) {
        if days <= 0 {
            return date
        }
        
        return getDateMinusDays(getPreviousDate(date), days: days - 1)
    }
    
    private func getLastCompleteWeek() -> Int? {
        let minElement = dateIndexes.minElement()
        let maxElement = dateIndexes.maxElement()
        
        if minElement == nil || maxElement == nil {
            return nil
        }
        
        let minTuple = getRepresentation(minElement!)
        let maxTuple = getRepresentation(maxElement!)
        
        let interval = getDaysBetween(minTuple, otherDate: maxTuple)
        
        if interval >= countWeeks * 7 {
            return nil
        }
        
        let daysInEachWeek = getDaysInEachWeek()
        let lastWeek = interval / 7
        var lastCompleteWeek = -1
        
        for week in 0...lastWeek {
            if daysInEachWeek.objectForKey(week) as! Int == countDays {
                lastCompleteWeek = week
            } else {
                break
            }
        }
        
        if lastCompleteWeek + 2 <= lastWeek {
            for week in (lastCompleteWeek + 2)...lastWeek {
                if daysInEachWeek.objectForKey(week) as! Int > 0 {
                    return nil
                }
            }
        }
        
        return lastCompleteWeek
    }
    
    private func getDaysInEachWeek() -> NSDictionary {
        let minElement = dateIndexes.minElement()
        let maxElement = dateIndexes.maxElement()
        
        if minElement == nil || maxElement == nil {
            return NSDictionary()
        }
        
        let minTuple = getRepresentation(minElement!)
        let maxTuple = getRepresentation(maxElement!)
        
        let interval = getDaysBetween(minTuple, otherDate: maxTuple)
        
        let daysInEachWeek = NSMutableDictionary()
        let lastWeek = interval / 7
        
        for week in 0...lastWeek {
            daysInEachWeek.setObject(0, forKey: week)
        }
        
        var tuple = minTuple
        var index = 0
        
        while compareDates(tuple, otherDate: maxTuple) <= 0 {
            if dateIndexes.contains(getRepresentation(tuple)) {
                let weekOfTuple = index / 7
                
                let currentCount = daysInEachWeek.objectForKey(weekOfTuple) as! Int
                daysInEachWeek.setObject(1 + currentCount, forKey: weekOfTuple)
            }
            
            tuple = getNextDate(tuple)
            index = index + 1
        }
        
        return daysInEachWeek
    }
    
    private func getMonths(minTuple: (year: Int, month: Int, day: Int), maxTuple: (year: Int, month: Int, day: Int)) -> [(year: Int, month: Int)]? {
        var monthYear: (year: Int, month: Int)  = (year: minTuple.year, month: minTuple.month)
        var response: [(year: Int, month: Int)] = []
        
        while monthYear.month != maxTuple.month || monthYear.year != maxTuple.year {
            response.append((year: monthYear.year, month: monthYear.month))
            monthYear = getNextMonth(monthYear)
        }
        
        response.append((year: monthYear.year, month: monthYear.month))
        return response
    }
    
    private func addDate(date: (year: Int, month: Int, day: Int), bulkCount: Int) -> ScheduleCalendarState {
        var dates: [(year: Int, month: Int, day: Int)] = []
        
        for i in 0..<bulkCount {
            let newDate = getDatePlusDays(date, days: i)
            dates.append(newDate)
        }
        
        for date in dates {
            let representation = getRepresentation(date)
        
            if dateIndexes.contains(representation) {
                return self.getState()
            }
        }
        
        for date in dates {
            let representation = getRepresentation(date)
            dateIndexes.insert(representation)
        }
        
        var state = getState()
        
        switch state {
        case .SuccessAllWeeksSelected, .SuccessAFewWeeksSelected, .SuccessFirstWeekSelected, .SuccessFirstDayOfFirstWeekSelected, .SuccessNoDaySelected:
            recomputeDays()
            return state
        case .ErrorNotPrefix, .ErrorTooManyDaysInWeek, .ErrorTooManyWeeks, .ErrorDateBeforeToday:
            for date in dates {
                let representation = getRepresentation(date)
                dateIndexes.remove(representation)
            }
            
            state = getState()
            return state
        }
    }
    
    private func removeDate(date: (year: Int, month: Int, day: Int), withBulkCount: Bool) -> ScheduleCalendarState {
        if (withBulkCount) {
            dateIndexes.removeAll()
            recomputeDays()
            return getState()
        }
        
        let representation = getRepresentation(date)
        
        if !dateIndexes.contains(representation) {
            return getState()
        }
        
        dateIndexes.remove(representation)
        
        var state = getState()
        
        switch state {
        case .SuccessAllWeeksSelected, .SuccessAFewWeeksSelected, .SuccessFirstWeekSelected, .SuccessFirstDayOfFirstWeekSelected, .SuccessNoDaySelected:
            recomputeDays()
            return state
        case .ErrorNotPrefix, .ErrorTooManyDaysInWeek, .ErrorTooManyWeeks, .ErrorDateBeforeToday:
            dateIndexes.insert(representation)
            state = getState()
            return state
        }
    }
    
    private func getDictionary(date: (date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))) -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setObject(date.date.year, forKey: "year")
        dictionary.setObject(date.date.month, forKey: "month")
        dictionary.setObject(date.date.day, forKey: "day")
        dictionary.setObject(date.color.rawValue, forKey: "color")
        
        dictionary.setObject(date.forMonth.month, forKey: "forMonthMonth")
        dictionary.setObject(date.forMonth.year, forKey: "forMonthYear")
        
        if date.weekNumber != nil {
            dictionary.setObject(date.weekNumber!, forKey: "weekNumber")
        }
        
        return dictionary
    }
    
    private func getDate(dictionary: NSDictionary) -> (date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))? {
        if let year = dictionary.objectForKey("year") as? Int, month = dictionary.objectForKey("month") as? Int, day = dictionary.objectForKey("day") as? Int, colorString = dictionary.objectForKey("color") as? String, forMonthMonth = dictionary.objectForKey("forMonthMonth") as? Int, forMonthYear = dictionary.objectForKey("forMonthYear") as? Int {
            if let color = ScheduleCalendarDayColor(rawValue: colorString) {
                if let weekNumber = dictionary.objectForKey("weekNumber") as? Int {
                    return (date: (year: year, month: month, day: day), color: color, weekNumber: weekNumber, forMonth: (year: forMonthYear, month: forMonthMonth))
                } else {
                    return (date: (year: year, month: month, day: day), color: color, weekNumber: nil, forMonth: (year: forMonthYear, month: forMonthMonth))
                }
            }
        }
        
        return nil
    }
    
    private func recomputeDaysNow() {
        
        // Get the today
        if let now = getNow(), months = getMonths() {
            for month in months {
                let minMonthTuple: (year: Int, month: Int, day: Int) = (year: month.year, month: month.month, day: 1)
                let maxMonthTuple: (year: Int, month: Int, day: Int) = (year: month.year, month: month.month, day: getLastDay((month.year, month: month.month)))
                
                var monthPreOffset = 0
                var monthPostOffset = 0
                
                if let monthPreOffsetValue = getMonthPreOffset(minMonthTuple) {
                    monthPreOffset = monthPreOffsetValue // number of days before the month
                }
                
                if let monthPostOffsetValue = getMonthPostOffset(maxMonthTuple) {
                    monthPostOffset = monthPostOffsetValue // number of days after the month
                }
                
                let firstDate = getDateMinusDays(minMonthTuple, days: monthPreOffset)
                let lastDate = getDatePlusDays(maxMonthTuple, days: monthPostOffset)
                
                var tuple = firstDate
                
                while compareDates(tuple, otherDate: lastDate) <= 0 {
                    if compareDates(tuple, otherDate: minMonthTuple) < 0 {
                        
                        // Date is before the first of the month, it must be empty.
                        let representation = getRepresentation(tuple)
                        let forMonth = (year: minMonthTuple.year, month: minMonthTuple.month)
                        let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.Empty, nil, forMonth))
                        daysCacheBefore.setObject(dictionary, forKey: representation)
                        
                    } else if compareDates(tuple, otherDate: maxMonthTuple) > 0 {
                        
                        // Date is after the last of the month, it must be empty.
                        let representation = getRepresentation(tuple)
                        let forMonth = (year: maxMonthTuple.year, month: maxMonthTuple.month)
                        let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.Empty, nil, forMonth))
                        daysCacheAfter.setObject(dictionary, forKey: representation)
                        
                    } else {
                        
                        if compareDates(tuple, otherDate: now) < 0 {
                            
                            // Date is before today, it must be blocked.
                            let representation = getRepresentation(tuple)
                            let forMonth = (year: tuple.year, month: tuple.month)
                            let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.ClearBlocked, nil, forMonth))
                            daysCache.setObject(dictionary, forKey: representation)
                            
                        } else {
                            
                            // Date is not selected, and it is part of another week, so it must be clear and waiting for future selection.
                            let representation = getRepresentation(tuple)
                            let forMonth = (year: tuple.year, month: tuple.month)
                            let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.ClearWaiting, nil, forMonth))
                            daysCache.setObject(dictionary, forKey: representation)
                            
                        }
                    }
                    
                    tuple = getNextDate(tuple)
                }
            }
        }
        
        recomputeDaysArray()
    }
    
    private func recomputeDays() {
        daysCache.removeAllObjects()
        daysCacheBefore.removeAllObjects()
        daysCacheAfter.removeAllObjects()
        monthOfCurrentWeek = nil
        
        let state = getState()
        
        switch state {
        case .SuccessAllWeeksSelected, .SuccessAFewWeeksSelected, .SuccessFirstWeekSelected, .SuccessFirstDayOfFirstWeekSelected, .SuccessNoDaySelected:
            break // Do nothing in case of success
        case .ErrorNotPrefix, .ErrorTooManyDaysInWeek, .ErrorTooManyWeeks, .ErrorDateBeforeToday:
            recomputeDaysNow()
            return
        }
        
        let minElement = dateIndexes.minElement()
        
        if minElement == nil {
            recomputeDaysNow()
            return
        }
        
        let minTuple = getRepresentation(minElement!)
        let maxWeekTuple: (year: Int, month: Int, day: Int) = getDatePlusDays(minTuple, days: 7 * countWeeks - 1)
        
        let months = getMonths()
        
        if months == nil {
            recomputeDaysNow()
            return
        }
        
        let lastCompleteWeek = getLastCompleteWeek()
        
        if lastCompleteWeek == nil {
            recomputeDaysNow()
            return
        }
        
        var tupleIndex = -1
        var foundFirst = false
        
        for month in months! {
            let minMonthTuple: (year: Int, month: Int, day: Int) = (year: month.year, month: month.month, day: 1)
            let maxMonthTuple: (year: Int, month: Int, day: Int) = (year: month.year, month: month.month, day: getLastDay((month.year, month: month.month)))
            
            var monthPreOffset = 0
            var monthPostOffset = 0
            
            if let monthPreOffsetValue = getMonthPreOffset(minMonthTuple) {
                monthPreOffset = monthPreOffsetValue // number of days before the month
            }
            
            if let monthPostOffsetValue = getMonthPostOffset(maxMonthTuple) {
                monthPostOffset = monthPostOffsetValue // number of days after the month
            }
        
            let firstDate = getDateMinusDays(minMonthTuple, days: monthPreOffset)
            let lastDate = getDatePlusDays(maxMonthTuple, days: monthPostOffset)
            
            var tuple = firstDate
            
            while compareDates(tuple, otherDate: lastDate) <= 0 {
                if compareDates(tuple, otherDate: minMonthTuple) < 0 {
                    
                    // Date is before the first of the month, it must be empty.
                    let representation = getRepresentation(tuple)
                    let forMonth = (year: minMonthTuple.year, month: minMonthTuple.month)
                    let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.Empty, nil, forMonth))
                    daysCacheBefore.setObject(dictionary, forKey: representation)
                    
                } else if compareDates(tuple, otherDate: maxMonthTuple) > 0 {
                    
                    // Date is after the last of the month, it must be empty.
                    let representation = getRepresentation(tuple)
                    let forMonth = (year: maxMonthTuple.year, month: maxMonthTuple.month)
                    let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.Empty, nil, forMonth))
                    daysCacheAfter.setObject(dictionary, forKey: representation)
                    
                } else {
                    let nowValue = getNow()
                    
                    if nowValue != nil && compareDates(tuple, otherDate: nowValue!) < 0 {
                        
                        // Date is before today, it must be blocked.
                        let representation = getRepresentation(tuple)
                        let forMonth = (year: tuple.year, month: tuple.month)
                        let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.ClearBlocked, nil, forMonth))
                        daysCache.setObject(dictionary, forKey: representation)
                        
                    } else if compareDates(tuple, otherDate: maxWeekTuple) > 0 {
                        
                        // Date is after max week tuple, it must be blocked.
                        let representation = getRepresentation(tuple)
                        let forMonth = (year: tuple.year, month: tuple.month)
                        let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.ClearBlocked, nil, forMonth))
                        daysCache.setObject(dictionary, forKey: representation)
                        
                    } else {
                        foundFirst = foundFirst || dateIndexes.contains(getRepresentation(tuple))
                        
                        var weekOfTuple: Int? = nil
                        
                        if foundFirst {
                            tupleIndex = tupleIndex + 1
                            weekOfTuple = tupleIndex / 7
                        }
                        
                        if dateIndexes.contains(getRepresentation(tuple)) {
                            
                            // Date is selected, it must be marked as selected.
                            let representation = getRepresentation(tuple)
                            let forMonth = (year: tuple.year, month: tuple.month)
                            let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.GreenSelected, weekOfTuple, forMonth))
                            daysCache.setObject(dictionary, forKey: representation)
                            
                        } else {
                            switch state {
                            case .SuccessAllWeeksSelected:
                                
                                // Date is not selected, but it is part of a fully scheduled week.
                                let representation = getRepresentation(tuple)
                                let forMonth = (year: tuple.year, month: tuple.month)
                                let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.GreenComplete, weekOfTuple, forMonth))
                                daysCache.setObject(dictionary, forKey: representation)
                                
                            case .SuccessFirstDayOfFirstWeekSelected:
                                if weekOfTuple == 0 {
                                    
                                    // Date is not selected, but part of the first week which is to be fully scheduled.
                                    let representation = getRepresentation(tuple)
                                    let forMonth = (year: tuple.year, month: tuple.month)
                                    let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.WhitePending, weekOfTuple, forMonth))
                                    daysCache.setObject(dictionary, forKey: representation)
                                    
                                } else {
                                    
                                    // Date is not selected, and it is part of another week, so it must be clear and waiting for future selection.
                                    let representation = getRepresentation(tuple)
                                    let forMonth = (year: tuple.year, month: tuple.month)
                                    let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.ClearWaiting, weekOfTuple, forMonth))
                                    daysCache.setObject(dictionary, forKey: representation)
                                    
                                }
                            case .SuccessFirstWeekSelected, .SuccessAFewWeeksSelected:
                                if weekOfTuple != nil && lastCompleteWeek >= weekOfTuple! {
                                    
                                    // Date is not selected, but part of a fully scheduled week.
                                    let representation = getRepresentation(tuple)
                                    let forMonth = (year: tuple.year, month: tuple.month)
                                    let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.GreenComplete, weekOfTuple, forMonth))
                                    daysCache.setObject(dictionary, forKey: representation)
                                    
                                } else if weekOfTuple != nil && lastCompleteWeek == weekOfTuple! - 1 {
                                    
                                    // Date is not selected, but part of the current week to be fully scheduled.
                                    let representation = getRepresentation(tuple)
                                    let forMonth = (year: tuple.year, month: tuple.month)
                                    let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.WhitePending, weekOfTuple, forMonth))
                                    daysCache.setObject(dictionary, forKey: representation)
                                    
                                    if monthOfCurrentWeek == nil {
                                        monthOfCurrentWeek = (year: tuple.year, month: tuple.month)
                                    }
                                } else {
                                    
                                    // Date is not selected, and not part of the current week to be fully scheduled, so it must be clear and waiting for future selection.
                                    let representation = getRepresentation(tuple)
                                    let forMonth = (year: tuple.year, month: tuple.month)
                                    let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.ClearWaiting, weekOfTuple, forMonth))
                                    daysCache.setObject(dictionary, forKey: representation)
                                    
                                }
                            case .SuccessNoDaySelected, .ErrorNotPrefix, .ErrorTooManyWeeks, .ErrorDateBeforeToday, .ErrorTooManyDaysInWeek:
                                
                                // This is a weird state, but the date may be just clear waiting.
                                let representation = getRepresentation(tuple)
                                let forMonth = (year: tuple.year, month: tuple.month)
                                let dictionary = getDictionary((tuple, ScheduleCalendarDayColor.ClearWaiting, weekOfTuple, forMonth))
                                daysCache.setObject(dictionary, forKey: representation)
                                
                            }
                        }
                    }
                }
                
                tuple = getNextDate(tuple)
            }
        }
        
        recomputeDaysArray()
    }
}