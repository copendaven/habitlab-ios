//
//  ThinkingAboutYouFeedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 26/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class ThinkingAboutYouFeedCell: FeedCell {
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var cellMessage: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var messageLabelLeft: NSLayoutConstraint!
    @IBOutlet weak var messageLabelTop: NSLayoutConstraint!
    @IBOutlet weak var messageLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var messageLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var thoughtCloudImage: UIImageView!
    
    @IBAction override func like(sender: AnyObject) {
        super.like(sender);
    }
    
    @IBAction override func comment(sender: AnyObject) {
        super.comment(sender)
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction override func clickMore(sender: AnyObject) {
        super.clickMore(sender)
    }
    
    func setLiking() {
        if let note = super.notification {
            super.setLiking(note, likingButton: self.likeButton, showLikeButton: true)
        }
    }
    
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, showButtons: Bool) {
        super.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, commentingButton: commentButton, likingButton: likeButton, optionsButton: optionsButton, showButtons: showButtons)
        
        dispatch_async(dispatch_get_main_queue()) {
            if let message = notification["message"] as? NSString {
                self.messageLabel.text = message as String
            }
            
            if let _ = notification["ownerFacebookPhoto"] as? NSString {
                self.userPhoto.layer.borderWidth = 4.0
                self.userPhoto.layer.borderColor = UIColor.whiteColor().CGColor
            }
        
            let screenSize = UIScreen.mainScreen().bounds
            let screenWidth = screenSize.width
            
            if screenWidth == 320 {
                
                // iPhone 4s, 5, 5s, 5se
                self.messageLabelLeft.constant = 32
                self.messageLabelTop.constant = 108
                self.messageLabelWidth.constant = 224
                self.messageLabelHeight.constant = 108
                
            } else if screenWidth == 375 {
                
                // iPhone 6, 6s
                self.messageLabelLeft.constant = 36
                self.messageLabelTop.constant = 110
                self.messageLabelWidth.constant = 264
                self.messageLabelHeight.constant = 140
                
            } else if screenWidth == 414 {
                
                // iPhone 6+, 6s+
                self.messageLabelLeft.constant = 40
                self.messageLabelTop.constant = 112
                self.messageLabelWidth.constant = 264
                self.messageLabelHeight.constant = 140
                
            }
        }
    }
}
