//
//  ProfileController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class ProfileController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    
    static var cacheShown: Bool = false
    static var cacheValid: Bool = true
    static var cacheDate: NSDate = NSDate()
    static var object = NSObject()
    static var caching = NSObject()
    
    @IBOutlet weak var newUpdatesButton: UIButton!
    @IBOutlet weak var profileTableView: UITableView!
    
    let downloadedWindow = 10
    var downloadedCount = 0
    var serverNotifications = []
    var notifications = []
    var refresher: UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screenName = String(ProfileController)
        
        objc_sync_enter(ProfileController.caching)
        ProfileController.cacheValid = true
        ProfileController.cacheShown = false
        ProfileController.cacheDate = NSDate()
        objc_sync_exit(ProfileController.caching)
    
        profileTableView.delegate = self
        profileTableView.dataSource = self
        self.shyNavBarManager.scrollView = self.profileTableView
        
        profileTableView.estimatedRowHeight = 80
        profileTableView.rowHeight = UITableViewAutomaticDimension
        // profileTableView.backgroundView = nil
        // profileTableView.backgroundColor = UIColor.clearColor()
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ProfileController.refreshPullToRefresh), forControlEvents: UIControlEvents.ValueChanged)
        profileTableView.addSubview(refresher!)
        
        newUpdatesButton.hidden = true
        // newUpdatesButton.layer.cornerRadius = 0.5 * newUpdatesButton.bounds.size.height
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        profileTableView.infiniteScrollIndicatorMargin = 40
        
        profileTableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            self.refreshFromServer(true)
        }
        
        objc_sync_enter(ProfileController.caching)
        if ProfileController.cacheShown {
            objc_sync_exit(ProfileController.caching)
            refreshCache()
        } else {
            ProfileController.cacheShown = true
            objc_sync_exit(ProfileController.caching)
        }
        
        objc_sync_enter(ProfileController.object)
        
        if self.serverNotifications.count <= 0 {
            objc_sync_exit(ProfileController.object)
            self.refreshFromServer(false)
        } else {
            objc_sync_exit(ProfileController.object)
            self.refreshFromClient(false)
        }
        
        segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(ProfileController))
    }
    
    func refreshPullToRefresh() {
        self.refreshFromServer(false)
    }
    
    func refreshCache() {
        objc_sync_enter(ProfileController.caching)
        
        if let calendar = Serv.calendar {
            if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: -1, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                if ProfileController.cacheDate.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                    ProfileController.cacheValid = false
                }
            }
        }
        
        // newUpdatesButton.hidden = ProfileController.cacheValid
        
        objc_sync_exit(ProfileController.caching)
    }
    
    @IBAction func clickedNewUpdates(sender: AnyObject) {
        self.refreshFromServer(false)
    }
    
    func refreshFromClient(isInfiniteScroll: Bool) {
        objc_sync_enter(ProfileController.object)
        
        self.notifications = self.filterNotificationsForProfile(self.serverNotifications)
        self.profileTableView.reloadData()
        
        if isInfiniteScroll {
            self.profileTableView.finishInfiniteScroll()
        }
        
        objc_sync_exit(ProfileController.object)
        
        if let refr = self.refresher {
            refr.endRefreshing()
        }
    }
    
    func refreshFromServer(isInfiniteScroll: Bool) {
        if (!isInfiniteScroll) {
            objc_sync_enter(ProfileController.object)
            downloadedCount = 0
            objc_sync_exit(ProfileController.object)
        }
        
        Serv.showSpinner()
        
        objc_sync_enter(ProfileController.caching)
        ProfileController.cacheValid = true
        ProfileController.cacheDate = NSDate()
        objc_sync_exit(ProfileController.caching)
        
        self.refreshCache()
        
        objc_sync_enter(ProfileController.object)
        let skip = downloadedCount
        let limit = downloadedWindow
        objc_sync_exit(ProfileController.object)
        
        HabitlabRestAPI.getNotificationsProfile(skip, limit: limit, handler: { (error, notificationsJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let notificationsArray = notificationsJson as? NSArray {
                objc_sync_enter(ProfileController.object)
                
                if !isInfiniteScroll {
                    self.serverNotifications = notificationsArray
                } else {
                    var set = Set<String>()
                    let array = NSMutableArray()
                    
                    for notificationJson in self.serverNotifications {
                        if let notification = notificationJson as? NSDictionary {
                            if let id = notification["id"] as? NSString {
                                if !set.contains(id as String) {
                                    set.insert(id as String)
                                    array.addObject(notificationJson)
                                }
                            }
                        }
                    }
                    
                    for notificationJson in notificationsArray {
                        if let notification = notificationJson as? NSDictionary {
                            if let id = notification["id"] as? NSString {
                                if !set.contains(id as String) {
                                    set.insert(id as String)
                                    array.addObject(notificationJson)
                                }
                            }
                        }
                    }
                    
                    self.serverNotifications = array
                }
                
                self.downloadedCount = self.serverNotifications.count
                
                objc_sync_exit(ProfileController.object)
                
                self.refreshFromClient(isInfiniteScroll)
            }
        })
    }
    
    func filterNotificationsForProfile(receivedNotifications: NSArray) -> NSArray {
        let processedNotifications: NSMutableArray = NSMutableArray()
        
        let caches = InspirationCache().getCaches()
        
        for cacheElement in caches {
            if let cache = cacheElement as? NSDictionary {
                if let cacheCacheId = cache["cacheId"] as? String {
                    var found = false
                    
                    for i in 0 ..< receivedNotifications.count {
                        if let notification = receivedNotifications[i] as? NSDictionary {
                            if let notificationCacheId = notification["cacheId"] as? String {
                                if cacheCacheId.compare(notificationCacheId) == NSComparisonResult.OrderedSame {
                                    found = true
                                }
                            }
                        }
                        
                        if found {
                            break
                        }
                    }
                    
                    if !found {
                        processedNotifications.addObject(cache)
                    }
                }
            }
        }
        
        processedNotifications.addObjectsFromArray(receivedNotifications as [AnyObject])
        
        let cachedNotifications: NSMutableArray = NSMutableArray()
        
        for i in 0 ..< processedNotifications.count {
            if let notification = processedNotifications[i] as? NSDictionary {
                let cachedNotification: NSMutableDictionary = NSMutableDictionary(dictionary: notification)
                Serv.addCacheComments(cachedNotification)
                Serv.addCacheLikes(cachedNotification)
                cachedNotifications.addObject(cachedNotification)
            }
        }
        
        let result: NSMutableArray = NSMutableArray()
        
        // Add a new object for the first element!
        result.addObject(NSDictionary())
        
        for i in 0 ..< cachedNotifications.count {
            if let notification = cachedNotifications[i] as? NSDictionary {
                result.addObject(notification)
            }
        }
        
        return result as NSArray
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showWriteMessageFromProfile") {
            if let viewController = segue.destinationViewController as? WriteMessageController {
                if let notification = sender as? NSDictionary {
                    viewController.notification = notification
                }
            }
        }
        
        if (segue.identifier == "showPublicProfileFromProfile") {
            if let viewController = segue.destinationViewController as? PublicProfileController {
                if let userId = sender as? String {
                    viewController.userId = userId
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("priorFeedCell", forIndexPath: indexPath) as? PriorFeedCell {
                cell.populate(self)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
            
            return defaultCell
        }
        
        if indexPath.row == 1 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("profileInfoCell", forIndexPath: indexPath) as? ProfileInfoCell {
                cell.populate(self)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        objc_sync_enter(ProfileController.object)
        
        if notifications.count <= indexPath.row - 1 {
            objc_sync_exit(ProfileController.object)
            return defaultCell
        }
        
        if let mobileNotification = notifications[indexPath.row - 1] as? NSMutableDictionary {
            Serv.addCacheComments(mobileNotification)
            Serv.addCacheLikes(mobileNotification)
            
            if let mobileNotificationType = mobileNotification["mobileNotificationType"] as? String {
                switch mobileNotificationType {
                case "program-clarified", "program-complete", "program-scheduled", "program-signup", "program-signup-level-1", "user-comment-mobile-notification", "user-like-mobile-notification", "user-follow-user":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("normalFeedCell", forIndexPath: indexPath) as? NormalFeedCell {
                        cell.populate(true, notification: mobileNotification, controller: self, showButtons: false)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        objc_sync_exit(ProfileController.object)
                        return cell
                    }
                    
                case "program-first-step-complete", "program-step-complete":
                    if Serv.hasCapture(mobileNotification) {
                        if let cell = tableView.dequeueReusableCellWithIdentifier("normalFeedCellWithCapture", forIndexPath: indexPath) as? NormalFeedCellWithCapture {
                            cell.populate(true, notification: mobileNotification, controller: self, showButtons: false)
                            cell.selectionStyle = UITableViewCellSelectionStyle.None
                            objc_sync_exit(ProfileController.object)
                            return cell
                        }
                    }
                    
                    if let cell = tableView.dequeueReusableCellWithIdentifier("normalFeedCell", forIndexPath: indexPath) as? NormalFeedCell {
                        cell.populate(true, notification: mobileNotification, controller: self, showButtons: false)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        objc_sync_exit(ProfileController.object)
                        return cell
                    }
                    
                case "program-step-overdue-first", "program-step-overdue-second", "program-forfeited", "program-quitted" :
                    if let cell = tableView.dequeueReusableCellWithIdentifier("overdueFeedCell", forIndexPath: indexPath) as? OverdueFeedCell {
                        cell.populate(true, notification: mobileNotification, controller: self, showButtons: false)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        objc_sync_exit(ProfileController.object)
                        return cell
                    }
                    
                default:
                    objc_sync_exit(ProfileController.object)
                    return defaultCell
                }
            }
        }
        
        objc_sync_exit(ProfileController.object)
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: Unwind segues
    
    @IBAction func profileWroteMessage(segue: UIStoryboardSegue) {
        // Nothing.
    }
}

/*



if let mobileNotificationType = mobileNotification["mobileNotificationType"] as? String {
switch mobileNotificationType {
case "program-clarified", "program-complete", "program-scheduled", "program-signup", "program-signup-level-1", "user-comment-mobile-notification", "user-like-mobile-notification", "user-follow-user":
if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? NormalFeedCell {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}

case "program-first-step-complete", "program-step-complete":
if Serv.hasCapture(mobileNotification) {
if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? NormalFeedCellWithCapture {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}
}

if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? NormalFeedCell {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}

case "program-step-overdue-first", "program-step-overdue-second", "program-forfeited", "program-quitted" :
if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? OverdueFeedCell {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}

default:
break // Do nothing.
}
}





if let mobileNotificationType = mobileNotification["mobileNotificationType"] as? String {
switch mobileNotificationType {
case "program-clarified", "program-complete", "program-scheduled", "program-signup", "program-signup-level-1", "user-comment-mobile-notification", "user-like-mobile-notification", "user-follow-user":
if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? NormalFeedCell {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}

case "program-first-step-complete", "program-step-complete":
if Serv.hasCapture(mobileNotification) {
if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? NormalFeedCellWithCapture {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}
}

if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? NormalFeedCell {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}

case "program-step-overdue-first", "program-step-overdue-second", "program-forfeited", "program-quitted" :
if let cell = profileTableView.cellForRowAtIndexPath(indexPath) as? OverdueFeedCell {
cell.populate(true, object: mobileNotification, controller: self, showButtons: false)
continue
}

default:
break // Do nothing.
}
}








*/
