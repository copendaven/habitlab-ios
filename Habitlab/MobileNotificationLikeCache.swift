//
//  MobileNotificationLikeCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/25/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData

class MobileNotificationLikeCache: NSObject {
    private static var object: NSObject = NSObject()
    private static var cachesByMobileNotificationId: NSMutableDictionary = NSMutableDictionary()
    private static var cachesByMobileNotificationCacheId: NSMutableDictionary = NSMutableDictionary()
    
    private func cleanupMobileNotificationId() {
        if let calendar = Serv.calendar {
            if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                let mobileNotificationIds = NSMutableArray()
                
                for (mobileNotificationId, cacheObject) in MobileNotificationLikeCache.cachesByMobileNotificationId {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                mobileNotificationIds.addObject(mobileNotificationId)
                            }
                        }
                    }
                }
                
                for mobileNotificationId in mobileNotificationIds {
                    MobileNotificationLikeCache.cachesByMobileNotificationId.removeObjectForKey(mobileNotificationId)
                }
            }
        }
    }
    
    private func cleanupMobileNotificationCacheId() {
        if let calendar = Serv.calendar {
            if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                let mobileNotificationCacheIds = NSMutableArray()
                
                for (mobileNotificationCacheId, cacheObject) in MobileNotificationLikeCache.cachesByMobileNotificationCacheId {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                mobileNotificationCacheIds.addObject(mobileNotificationCacheId)
                            }
                        }
                    }
                }
                
                for mobileNotificationCacheId in mobileNotificationCacheIds {
                    MobileNotificationLikeCache.cachesByMobileNotificationCacheId.removeObjectForKey(mobileNotificationCacheId)
                }
            }
        }
    }
    
    static func erase() {
        objc_sync_enter(MobileNotificationLikeCache.object)
        MobileNotificationLikeCache.cachesByMobileNotificationId.removeAllObjects()
        MobileNotificationLikeCache.cachesByMobileNotificationCacheId.removeAllObjects()
        objc_sync_exit(MobileNotificationLikeCache.object)
    }
    
    func setCache(cacheId: String, owner: NSDictionary, mobileNotificationId: String, mobileNotificationBaseId: String, mobileNotificationCacheId: String) {
        objc_sync_enter(MobileNotificationLikeCache.object)
        
        cleanupMobileNotificationId()
        
        let newMobileNotificationLike = NSMutableDictionary()
        
        newMobileNotificationLike.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationLike.setObject(owner, forKey: "owner")
        newMobileNotificationLike.setObject(mobileNotificationId, forKey: "mobileNotification")
        newMobileNotificationLike.setObject(mobileNotificationBaseId, forKey: "mobileNotificationBase")
        newMobileNotificationLike.setObject(mobileNotificationCacheId, forKey: "mobileNotificationCache")
        newMobileNotificationLike.setObject(NSDate(), forKey: "cachedAt")
        
        MobileNotificationLikeCache.cachesByMobileNotificationId.setObject(newMobileNotificationLike, forKey: mobileNotificationId)
        MobileNotificationLikeCache.cachesByMobileNotificationCacheId.setObject(newMobileNotificationLike, forKey: mobileNotificationCacheId)
        
        objc_sync_exit(MobileNotificationLikeCache.object)
    }
    
    func setCache(cacheId: String, owner: NSDictionary, mobileNotificationId: String, mobileNotificationBaseId: String) {
        objc_sync_enter(MobileNotificationLikeCache.object)
        
        cleanupMobileNotificationId()
        
        let newMobileNotificationLike = NSMutableDictionary()
        
        newMobileNotificationLike.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationLike.setObject(owner, forKey: "owner")
        newMobileNotificationLike.setObject(mobileNotificationId, forKey: "mobileNotification")
        newMobileNotificationLike.setObject(mobileNotificationBaseId, forKey: "mobileNotificationBase")
        newMobileNotificationLike.setObject(NSDate(), forKey: "cachedAt")
        
        MobileNotificationLikeCache.cachesByMobileNotificationId.setObject(newMobileNotificationLike, forKey: mobileNotificationId)
        
        objc_sync_exit(MobileNotificationLikeCache.object)
    }
    
    func setCache(cacheId: String, owner: NSDictionary, mobileNotificationCacheId: String) {
        objc_sync_enter(MobileNotificationLikeCache.object)
        
        cleanupMobileNotificationCacheId()
        
        let newMobileNotificationLike = NSMutableDictionary()
        
        newMobileNotificationLike.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationLike.setObject(owner, forKey: "owner")
        newMobileNotificationLike.setObject(mobileNotificationCacheId, forKey: "mobileNotificationCache")
        newMobileNotificationLike.setObject(NSDate(), forKey: "cachedAt")
        
        MobileNotificationLikeCache.cachesByMobileNotificationCacheId.setObject(newMobileNotificationLike, forKey: mobileNotificationCacheId)
        
        objc_sync_exit(MobileNotificationLikeCache.object)
    }
    
    func getCacheByMobileNotificationId(mobileNotificationId: String) -> NSDictionary? {
        objc_sync_enter(MobileNotificationLikeCache.object)
        
        if let cacheObject = MobileNotificationLikeCache.cachesByMobileNotificationId.objectForKey(mobileNotificationId) {
            if let cache = cacheObject as? NSDictionary {
                objc_sync_exit(MobileNotificationLikeCache.object)
                return cache
            }
        }
        
        objc_sync_exit(MobileNotificationLikeCache.object)
        return nil
    }
    
    func getCacheByMobileNotificationCacheId(mobileNotificationCacheId: String) -> NSDictionary? {
        objc_sync_enter(MobileNotificationLikeCache.object)
        
        if let cacheObject = MobileNotificationLikeCache.cachesByMobileNotificationCacheId.objectForKey(mobileNotificationCacheId) {
            if let cache = cacheObject as? NSDictionary {
                objc_sync_exit(MobileNotificationLikeCache.object)
                return cache
            }
        }
        
        objc_sync_exit(MobileNotificationLikeCache.object)
        return nil
    }
}
