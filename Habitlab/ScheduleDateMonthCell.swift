//
//  ScheduleDateMonthCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/20/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ScheduleDateMonthCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private var month: (year: Int, month: Int)?
    private var controller: UIViewController?
    private var scheduleCalendar: ScheduleCalendar?
    private var initialDate: NSDate?
    private var days: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))]? = nil
    
    @IBOutlet weak var daysCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var daysCollectionView: DynamicCollectionView!
    
    func populate(month: (year: Int, month: Int), controller: UIViewController, scheduleCalendar: ScheduleCalendar, initialDate: NSDate?) {
        self.month = month
        self.controller = controller
        self.scheduleCalendar = scheduleCalendar
        self.initialDate = initialDate
        
        self.monthLabel.text = getMonthString()
        self.days = scheduleCalendar.getDaysByMonth(month)
        
        daysCollectionView.delegate = self
        daysCollectionView.dataSource = self
        
        setDaysCollectionViewHeight()
        daysCollectionView.reloadData()
    }
    
    private func setDaysCollectionViewHeight() {
        var height = daysCollectionView.frame.size.height
        let screenSize = UIScreen.mainScreen().bounds
        let screenWidth = screenSize.width
        
        if let daysAll = self.days {
            height = CGFloat(screenWidth) * CGFloat(7 + daysAll.count) / CGFloat(7 * 7)
        }
        
        daysCollectionView.frame = CGRectMake(0, 0, screenWidth, height)
        daysCollectionView.layoutIfNeeded()
        
        daysCollectionViewHeight.constant = height
        daysCollectionView.updateConstraintsIfNeeded()
    }
    
    private func filterDaysByMonth(month: (year: Int, month: Int), days: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))]?) -> [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))]? {
        if let daysArray = days {
            var response: [(date: (year: Int, month: Int, day: Int), color: ScheduleCalendarDayColor, weekNumber: Int?, forMonth: (year: Int, month: Int))] = []
            
            for day in daysArray {
                if day.date.month == month.month && day.date.year == month.year {
                    response.append(day)
                }
            }
            
            return response
        }
        
        return nil
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let daysObject = self.days {
            return 7 + daysObject.count
        }
        
        return 7
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenWidth = Int(daysCollectionView.frame.size.width)
        let cellTotalWidth = screenWidth / 7
        let cellActualWidth = CGFloat(cellTotalWidth - 2)
        let cellActualHeight = CGFloat(cellActualWidth)
        
        return CGSizeMake(cellActualWidth, cellActualHeight) // This is the size for one collection item with button and label
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenWidth = Int(daysCollectionView.frame.size.width)
        let cellTotalWidth = screenWidth / 7
        
        let padding = CGFloat((screenWidth - cellTotalWidth * 7) / 2)
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if scheduleCalendar == nil {
            return defaultCell
        }
        
        if month == nil {
            return defaultCell
        }
        
        if controller == nil {
            return defaultCell
        }
        
        if 0 <= indexPath.item && indexPath.item < 7 {
            if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateWeekDayCell", forIndexPath: indexPath) as? ScheduleDateWeekDayCell {
                cell.populate(Serv.weekDayNames[indexPath.item])
                return cell
            }
        }
        
        if let daysObject = self.days {
            let entry = daysObject[indexPath.item - 7]
        
            if let day = scheduleCalendar!.getDay(entry.date, forMonth: self.month!) {
                let color = day.color.rawValue
                
                switch color {
                case ScheduleCalendarDayColor.ClearBlocked.rawValue:
                    if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateClearBlockedDayCell", forIndexPath: indexPath) as? ScheduleDateClearBlockedDayCell {
                        cell.populate(self.controller!, date: entry.date)
                        return cell
                    }
                case ScheduleCalendarDayColor.ClearWaiting.rawValue:
                    if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateClearWaitingDayCell", forIndexPath: indexPath) as? ScheduleDateClearWaitingDayCell {
                        cell.populate(self.controller!, date: entry.date)
                        return cell
                    }
                case ScheduleCalendarDayColor.GreenComplete.rawValue:
                    if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateGreenCompleteDayCell", forIndexPath: indexPath) as? ScheduleDateGreenCompleteDayCell {
                        cell.populate(self.controller!, date: entry.date)
                        return cell
                    }
                case ScheduleCalendarDayColor.GreenSelected.rawValue:
                    if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateGreenSelectedDayCell", forIndexPath: indexPath) as? ScheduleDateGreenSelectedDayCell {
                        cell.populate(self.controller!, date: entry.date)
                        return cell
                    }
                case ScheduleCalendarDayColor.WhitePending.rawValue:
                    if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateWhitePendingDayCell", forIndexPath: indexPath) as? ScheduleDateWhitePendingDayCell {
                        cell.populate(self.controller!, date: entry.date)
                        return cell
                    }
                case ScheduleCalendarDayColor.Empty.rawValue:
                    if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateEmptyDayCell", forIndexPath: indexPath) as? ScheduleDateEmptyDayCell {
                        return cell
                    }
                default:
                    break
                }
            }
            
            let now = scheduleCalendar!.getNow()
            
            if now != nil && scheduleCalendar!.compareDates(entry.date, otherDate: now!) < 0 {
                if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateClearBlockedDayCell", forIndexPath: indexPath) as? ScheduleDateClearBlockedDayCell {
                    cell.populate(self.controller!, date: entry.date)
                    return cell
                }
            } else {
                if let cell = daysCollectionView.dequeueReusableCellWithReuseIdentifier("scheduleDateClearWaitingDayCell", forIndexPath: indexPath) as? ScheduleDateClearWaitingDayCell {
                    cell.populate(self.controller!, date: entry.date)
                    return cell
                }
            }
        }
    
        return defaultCell
    }
    
    private func getMonthString() -> String {
        if self.month == nil {
            return ""
        }
        
        switch (self.month!.month) {
        case 1:
            return "January"
        case 2:
            return "February"
        case 3:
            return "March"
        case 4:
            return "April"
        case 5:
            return "May"
        case 6:
            return "June"
        case 7:
            return "July"
        case 8:
            return "August"
        case 9:
            return "September"
        case 10:
            return "October"
        case 11:
            return "November"
        case 12:
            return "December"
        default:
            return ""
        }
    }
}