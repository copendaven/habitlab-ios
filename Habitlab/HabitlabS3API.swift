//
//  HabitlabS3API.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/10/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import AWSS3

class HabitlabS3API: NSObject {
    static func uploadStepImage(stepId: String, data: NSData, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage("upload-step-\(stepId).jpg", bucketKey: "programstep\(stepId).jpg", contentType: "image/jpeg", data: data, handler: handler, progress: progress)
    }
    
    static func downloadStepImage(stepId: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage("download-step-\(stepId).png", bucket: Configuration.getS3ImageBucket(), bucketKey: "programstep\(stepId).jpg", handler: handler, progress: progress)
    }
    
    static func downloadPhysioImage(cacheId: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage("download-physio-\(cacheId).jpg", bucket: Configuration.getS3PhysioBucket(), bucketKey: "physioimage\(cacheId).jpg", handler: handler, progress: progress)
    }
    
    static func uploadContactImage(contactId: String, data: NSData, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage("upload-contact-\(contactId).jpg", bucketKey: "contact\(contactId).jpg", contentType: "image/jpeg", data: data, handler: handler, progress: progress)
    }
    
    static func downloadContactImage(contactId: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage("download-contact-\(contactId).png", bucket: Configuration.getS3ImageBucket(), bucketKey: "contact\(contactId).jpg", handler: handler, progress: progress)
    }
    
    private static func uploadImage(temporaryAddress: String, bucketKey: String, contentType: String, data: NSData, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        let path = NSTemporaryDirectory().stringByAppendingString(temporaryAddress)
        data.writeToFile(path as String, atomically: true)
        
        if let uploadRequest = AWSS3TransferManagerUploadRequest() {
            let bucket = Configuration.getS3ImageBucket()
            
            uploadRequest.bucket = bucket
            uploadRequest.ACL = AWSS3ObjectCannedACL.BucketOwnerRead
            uploadRequest.key = "\(bucket)/\(bucketKey)"
            uploadRequest.contentType = contentType
            
            let url = NSURL(fileURLWithPath: path as String)
            uploadRequest.body = url
            
            if let realProgress = progress {
                uploadRequest.uploadProgress = realProgress
            }
            
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
            transferManager.upload(uploadRequest).continueWithBlock({ (task) -> AnyObject? in
                if task.error != nil {
                    handler(task.error)
                    return false
                }
                
                handler(nil)
                return nil
            })
        }
    }
    
    private static func downloadImage(temporaryAddress: String, bucket: String, bucketKey: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        let path = NSTemporaryDirectory().stringByAppendingString(temporaryAddress)
        let url = NSURL(fileURLWithPath: path as String)
        
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        
        downloadRequest.bucket = bucket
        downloadRequest.key = "\(bucket)/\(bucketKey)"
        downloadRequest.downloadingFileURL = url
        
        if let realProgress = progress {
            downloadRequest.downloadProgress = realProgress
        }
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.download(downloadRequest).continueWithBlock({ (task) -> AnyObject? in
            if task.error != nil {
                handler(task.error, nil)
                return false
            }
            
            let error = NSError(domain: "S3ClientError", code: 404, userInfo: nil)
            
            if let image = UIImage(contentsOfFile: path) {
                let data = UIImageJPEGRepresentation(image, 1.0)
                
                if data == nil {
                    handler(error, nil)
                    return false
                }
                
                handler(nil, data!)
                return true
            }
            
            do {
                throw error
            } catch let error as NSError {
                handler(error, nil)
            }
            
            return true
        })
    }
}
