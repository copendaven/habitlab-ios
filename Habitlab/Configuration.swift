//
//  Configuration.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics
import Segment_Mixpanel
import HockeySDK
import AWSCognito
import Google

let environment = Environment.Release

class Configuration: NSObject {
    static func getEndpoint() -> String {
        switch environment {
        case Environment.Development:
            return "http://localhost:1337"
        case Environment.Staging:
            return "http://habitlab-staging.herokuapp.com"
        case Environment.Release:
            return "https://www.habitlab.me"
        }
    }
    
    static func getS3PhysioBucket() -> String {
        switch environment {
        case Environment.Development:
            return "habitlabphysiodevelopment"
        case Environment.Staging:
            return "habitlabphysiostaging"
        case Environment.Release:
            return "habitlabphysiorelease"
        }
    }
    
    static func getS3ImageBucket() -> String {
        switch environment {
        case Environment.Development:
            return "habitlabstepuploadsdevelopment"
        case Environment.Staging:
            return "habitlabstepuploadsstaging"
        case Environment.Release:
            return "habitlabstepuploadsrelease"
        }
    }
    
    static func initializeGoogleAnalytics() {
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai.trackerWithTrackingId("UA-77824936-1")
        gai.trackUncaughtExceptions = true  // report uncaught exceptions
    }
    
    static func initializeSegment() {
        var configuration = SEGAnalyticsConfiguration()
        
        switch environment {
        case Environment.Development, Environment.Staging:
            configuration = SEGAnalyticsConfiguration(writeKey: "RGY6K9Q0EsZ7iHoXzekPXapBgbuBDwF0")
        case Environment.Release:
            configuration = SEGAnalyticsConfiguration(writeKey: "snOiG6DKXltkSlJL2PLEyMA2YJNZB6f6")
        }
        
        configuration.flushAt = 5
        configuration.use(SEGMixpanelIntegrationFactory.instance())
        SEGAnalytics.setupWithConfiguration(configuration)
    }
    
    static func initializeHockeyApp() {
        let sharedHockeyManager = BITHockeyManager.sharedHockeyManager()
        sharedHockeyManager.configureWithIdentifier("cbed92cca2dc4799abcecc9d178a9149")
        sharedHockeyManager.startManager()
        sharedHockeyManager.authenticator.authenticateInstallation()
    }
    
    static func initializeAmazonCognito() {
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.EUWest1,
            identityPoolId:"eu-west-1:0caf0f9b-ba60-4f60-a067-4b7325da3a0d")
        
        let defaultServiceConfiguration = AWSServiceConfiguration(region:.EUWest1, credentialsProvider:credentialsProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = defaultServiceConfiguration
    }
    
    static func initializeFacebook(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    static func initializeFacebook(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) {
        FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    static func initializeHeapAnalytics() {
        /*
        switch environment {
        case Environment.Development, Environment.Staging:
        Heap.setAppId("982966411")
        case Environment.Release:
        Heap.setAppId("3897758375")
        }
        
        #if DEBUG
        Heap.enableVisualizer();
        #endif
        */
    }
}
