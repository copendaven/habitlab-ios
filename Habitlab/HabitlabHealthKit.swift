//
//  HealthKit.swift
//  Habitlab
//
//  Created by Vlad Manea on 23/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import Foundation
import HealthKit

class HabitlabHealthKit {
    static func requestDistanceWalkingRunningAuthorization(completion: (NSError?) -> ()) {
        if HKHealthStore.isHealthDataAvailable() {
            let permissions = NSSet(array: [
                HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
                HKObjectType.workoutType()
            ])
            
            HKHealthStore().requestAuthorizationToShareTypes(nil, readTypes: permissions as? Set<HKObjectType>, completion: { (success, error) -> Void in
                if error != nil {
                    completion(error)
                    return
                }
                
                completion(nil)
            })
        }
    }
    
    static func countRunningKilometers(startDate: NSDate, endDate: NSDate, completion: (NSError?, Double?) -> ()) {
        if (!HKHealthStore.isHealthDataAvailable()) {
            completion(NSError(domain: "Health data is unavailable on this device", code: 0, userInfo: nil), nil)
            return
        }
        
        // Predicate to read only running workouts
        let predicateRunningWorkouts = HKQuery.predicateForWorkoutsWithWorkoutActivityType(HKWorkoutActivityType.Running)
        
        // Predicate to read only timely samples
        let predicateTimes = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: HKQueryOptions.None)
        
        // Compound predicate for both predicates
        let queryPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateRunningWorkouts, predicateTimes])
        
        // Order the workouts by date descending
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierEndDate, ascending: false)
        
        // Create the query
        let workoutQuery = HKSampleQuery(sampleType: HKWorkoutType.workoutType(), predicate: queryPredicate, limit: 0, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            var kilometers = 0.0
            
            if error != nil {
                completion(error, kilometers)
                return
            }
            
            if let workouts = results as? [HKWorkout] {
                for workout in workouts {
                    if let totalDistance = workout.totalDistance {
                        kilometers += totalDistance.doubleValueForUnit(HKUnit.meterUnitWithMetricPrefix(HKMetricPrefix.Kilo))
                    }
                }
            }
            
            completion(nil, kilometers)
        }
        
        // Execute the query
        HKHealthStore().executeQuery(workoutQuery)
    }
}
