//
//  PushNotificationService.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit


enum PushNotificationType: String {
    case FeedPushNotification = "feedPushNotification"
    case ProfilePushNotification = "profilePushNotification"
    case CurrentHabitPushNotification = "currentHabitPushNotification"
    case FollowingPushNotification = "followingPushNotification"
    case FollowersPushNotification = "followersPushNotification"
    case PublicProfilePushNotification = "publicProfilePushNotification"
}

enum PushNotificationOrigin: String {
    case Background = "background"
    case Outside = "outside"
    case Active = "active"
}

extension UIApplication {
    func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = base as? UINavigationController {
            return topViewController(navigationController.visibleViewController)
        }
        
        if let tabsController = base as? UITabBarController {
            if let selectedController = tabsController.selectedViewController {
                return topViewController(selectedController)
            }
        }
        
        if let presentedController = base?.presentedViewController {
            return topViewController(presentedController)
        }
        
        return base
    }
}

extension UIViewController {
    
    func handleStartupPushNotification(origin: PushNotificationOrigin) -> Bool {
        return handleStartupPushNotification(nil, origin: origin)
    }
    
    func handleStartupPushNotification(pushNotification: [NSObject: AnyObject]?, origin: PushNotificationOrigin) -> Bool {
        var existingPushNotification = pushNotification
        
        if existingPushNotification == nil || existingPushNotification!.count <= 0 {
            existingPushNotification = PushNotificationCache().getPushNotification(origin)
        }
        
        if existingPushNotification == nil || existingPushNotification!.count <= 0 {
            return false
        }
        
        if let actualController = UIApplication.sharedApplication().topViewController(self) {
            if actualController != self {
                return actualController.handleStartupPushNotification(existingPushNotification!, origin: origin)
            }
            
            self.receivedPushNotification(existingPushNotification!, origin: origin)
            return true
        }
        
        return false
    }
    
    // Remove push notification observers
    
    func receivedPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let type = payload["type"] as? NSString {
                        
                        // I am Emil, and I received this notification...
                        switch (type as String) {
                            
                        // OK Friend joined. Show following.
                        case "friend-joined":
                            self.handleFriendJoinedPushNotification(info, origin: origin)
                            break
                            
                        // ? Friends joined, invitation to follow. Show following.
                        case "follow-your-friends":
                            self.handleFollowYourFriendsPushNotification(info, origin: origin)
                            break
                            
                        // ? Thinking about you.
                        case "thinking-about-you":
                            self.handleThinkingAboutYouPushNotification(info, origin: origin)
                            break
                            
                        case "unplugging-plan":
                            self.handleUnpluggingPlanPushNotification(info, origin: origin)
                            break
                            
                        // OK I am now following Christina. Show profile (for now).
                        case "user-follow":
                            self.handleUserFollowPushNotification(info, origin: origin)
                            break
                            
                        // OK I am inspired by Christina. Show the inspiration post.
                        case "followed-user-inspired":
                            self.handleFollowedUserInspiredPushNotification(info, origin: origin)
                            break
                            
                        // OK I see that Christina completed. Show public profile.
                        case "followed-user-completed-program":
                            self.handleFollowedUserCompletedStepProgramPushNotification(info, origin: origin)
                            break
                            
                        // N/A I see that Christina completed. Show feed.
                        case "followed-user-completed-level":
                            break // Not implemented on the server side.
                            
                        // I see that Christina completed. Show feed.
                        case "followed-user-completed-step":
                            self.handleFollowedUserCompletedStepProgramPushNotification(info, origin: origin)
                            break
                            
                        // OK Christina would like to follow me. Send me to my profile (for now).
                        case "user-follow-attempt":
                            self.handleUserFollowAttemptPushNotification(info, origin: origin)
                            break
                            
                        // OK Put this in the profile, knowing it contains the stuff about me in the feed too.
                        case "user-like":
                            self.handleUserLikeCommentRespondPushNotification(info, origin: origin)
                            break
                            
                        // OK Put this in the profile, knowing it contains the stuff about me in the feed too.
                        case "user-comment":
                            self.handleUserLikeCommentRespondPushNotification(info, origin: origin)
                            break
                            
                        // OK Put this in the profile, knowing it contains the stuff about me in the feed too.
                        case "owner-reply":
                            self.handleUserLikeCommentRespondPushNotification(info, origin: origin)
                            break
                            
                        // OK I should go to my program progress.
                        case "program-day-before":
                            self.handleCurrentHabitPushNotification(info, origin: origin)
                            break
                            
                        // OK I should go to my program progress.
                        case "program-step-activity":
                            self.handleCurrentHabitPushNotification(info, origin: origin)
                            break
                            
                        // OK I should go to my program progress.
                        case "program-step-zeroth-check":
                            break // Not implemented on the server side.
                            
                        // OK I should go to my program progress.
                        case "program-step-first-check":
                            self.handleCurrentHabitPushNotification(info, origin: origin)
                            break
                            
                        // OK I should go to my program progress.
                        case "program-step-fuckup":
                            self.handleCurrentHabitPushNotification(info, origin: origin)
                            break
                            
                        // OK for both cases. I should go to my program progress if it is for me, or to the feed if it is for someone else.
                        case "program-step-second-check":
                            var screen = PushNotificationType.FeedPushNotification
                            
                            if let payload = aps["payload"] as? NSDictionary {
                                if let aboutMe = payload["aboutMe"] as? NSNumber {
                                    if aboutMe == 1 {
                                        screen = PushNotificationType.CurrentHabitPushNotification
                                    }
                                }
                            }
                            
                            // OK.
                            if screen == PushNotificationType.CurrentHabitPushNotification {
                                self.handleCurrentHabitPushNotification(info, origin: origin)
                            }
                            
                            // OK.
                            if screen == PushNotificationType.FeedPushNotification {
                                self.handleProgramStepSecondCheckFeed(info, origin: origin)
                            }
                            
                            break
                            
                        // OK I failed this program, show me the circle.
                        case "program-fuckup":
                            self.handleCurrentHabitPushNotification(info, origin: origin)
                            break
                            
                        // OK I failed this program due to not signing up the next level, show me the habits page.
                        case "program-fuckup-next-level":
                            self.handleYourHabitsPushNotification(info, origin: origin)
                            break
                            
                        // OK I should go to my program progress.
                        case "program-complete-signup-next-check":
                            self.handleCurrentHabitPushNotification(info, origin: origin)
                            break
                            
                        default: break
                            // Do nothing.
                        }
                    }
                }
            }
        }
    }
    
    private func handleFriendJoinedPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let friendId = payload["friendId"] as? NSString {
                        
                        // Do not show the same thing again.
                        if let selfViewController = self as? PublicProfileController {
                            if let selfUserId = selfViewController.userId {
                                if selfUserId.compare(friendId as String) == NSComparisonResult.OrderedSame {
                                    return
                                }
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            if let _ = UserCache().getUser() {
                                if let navigationController = self.navigationController {
                                    if let storyboard = navigationController.storyboard {
                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("publicProfileController") as? PublicProfileController {
                                            viewController.isOpenedFromPushNotification = true
                                            viewController.userId = friendId as String
                                            navigationController.pushViewController(viewController, animated: true)
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
            }
        case PushNotificationOrigin.Active:
            if let _ = self as? NewFeedController {
                NSNotificationCenter.defaultCenter().postNotificationName("friendJoined", object: nil, userInfo: info)
            }
            
            break // Do nothing.
        }
    }
    
    private func handleFollowYourFriendsPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    
                    // Do not open the same controller.
                    if let _ = self as? FollowingController {
                        return
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        // Do not open the same controller.
                        if let _ = self as? FollowingController {
                            return
                        }
                        
                        if let _ = UserCache().getUser() {
                            if let navigationController = self.navigationController {
                                if let storyboard = navigationController.storyboard {
                                    if let viewController = storyboard.instantiateViewControllerWithIdentifier("followingController") as? FollowingController {
                                        viewController.isOpenedFromPushNotification = true
                                        viewController.pushNotificationPayload = payload
                                        navigationController.pushViewController(viewController, animated: true)
                                    }
                                }
                            }
                        }
                    })
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleUserFollowPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let followedId = payload["followedId"] as? NSString {
                        
                        // Do not show the same thing again.
                        if let selfViewController = self as? PublicProfileController {
                            if let selfUserId = selfViewController.userId {
                                if selfUserId.compare(followedId as String) == NSComparisonResult.OrderedSame {
                                    return
                                }
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            if let _ = UserCache().getUser() {
                                if let navigationController = self.navigationController {
                                    if let storyboard = navigationController.storyboard {
                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("publicProfileController") as? PublicProfileController {
                                            viewController.isOpenedFromPushNotification = true
                                            viewController.userId = followedId as String
                                            navigationController.pushViewController(viewController, animated: true)
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleFollowedUserInspiredPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let user = UserCache().getUser() {
                        if let ownerId = user["id"] as? NSString, programStepId = payload["programStepId"] as? NSString {
                            
                            // First, look for a program step complete mobile notification.
                            HabitlabRestAPI.getFilteredNotification(ownerId as String, programStepObject: programStepId as String, mobileNotificationType: "program-step-complete", handler: { (error, notificationJson) in
                                if notificationJson != nil && error == nil {
                                    if let notification = notificationJson as? NSDictionary {
                                        
                                        // Do not show the same thing again.
                                        if let selfViewController = self as? WriteMessageController {
                                            if let selfNotification = selfViewController.notification {
                                                if let selfNotificationId = selfNotification["id"] as? NSString {
                                                    if let notificationId = notification["id"] as? NSString {
                                                        if (selfNotificationId as String).compare(notificationId as String) == NSComparisonResult.OrderedSame {
                                                            return
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        dispatch_async(dispatch_get_main_queue(), {
                                            if let _ = UserCache().getUser() {
                                                if let navigationController = self.navigationController {
                                                    if let storyboard = navigationController.storyboard {
                                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("writeMessageControllerFeed") as? WriteMessageController {
                                                            viewController.isOpenedFromPushNotification = true
                                                            viewController.notification = notification
                                                            navigationController.pushViewController(viewController, animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                        
                                        return
                                    }
                                }
                                
                                // If none was found, look for a program first step complete mobile notification.
                                HabitlabRestAPI.getFilteredNotification(ownerId as String, programStepObject: programStepId as String, mobileNotificationType: "program-first-step-complete", handler: { (error, notificationJson) in
                                    if error != nil {
                                        return
                                    }
                                    
                                    if notificationJson != nil {
                                        if let notification = notificationJson as? NSDictionary {
                                            
                                            // Do not show the same thing again.
                                            if let selfViewController = self as? WriteMessageController {
                                                if let selfNotification = selfViewController.notification {
                                                    if let selfNotificationId = selfNotification["id"] as? NSString {
                                                        if let notificationId = notification["id"] as? NSString {
                                                            if (selfNotificationId as String).compare(notificationId as String) == NSComparisonResult.OrderedSame {
                                                                return
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            dispatch_async(dispatch_get_main_queue(), {
                                                if let _ = UserCache().getUser() {
                                                    if let navigationController = self.navigationController {
                                                        if let storyboard = navigationController.storyboard {
                                                            if let viewController = storyboard.instantiateViewControllerWithIdentifier("writeMessageControllerFeed") as? WriteMessageController {
                                                                viewController.isOpenedFromPushNotification = true
                                                                viewController.notification = notification
                                                                navigationController.pushViewController(viewController, animated: true)
                                                            }
                                                        }
                                                    }
                                                }
                                            })
                                        }
                                    }
                                })
                            })
                        }
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleFollowedUserCompletedStepProgramPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let completerId = payload["completerId"] as? NSString {
                        
                        // Do not show the same thing again.
                        if let selfViewController = self as? PublicProfileController {
                            if let selfUserId = selfViewController.userId {
                                if selfUserId.compare(completerId as String) == NSComparisonResult.OrderedSame {
                                    return
                                }
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            if let _ = UserCache().getUser() {
                                if let navigationController = self.navigationController {
                                    if let storyboard = navigationController.storyboard {
                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("publicProfileController") as? PublicProfileController {
                                            viewController.isOpenedFromPushNotification = true
                                            viewController.userId = completerId as String
                                            navigationController.pushViewController(viewController, animated: true)
                                        }
                                    }
                                }
                            }
                        })
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleUserFollowAttemptPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            
            // Do not open the same controller.
            if let _ = self as? FollowersController {
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                
                // Do not open the same controller.
                if let _ = self as? FollowersController {
                    return
                }
                
                if let _ = UserCache().getUser() {
                    if let navigationController = self.navigationController {
                        if let storyboard = navigationController.storyboard {
                            if let viewController = storyboard.instantiateViewControllerWithIdentifier("followersController") as? FollowersController {
                                viewController.isOpenedFromPushNotification = true
                                navigationController.pushViewController(viewController, animated: true)
                            }
                        }
                    }
                }
            })
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleUserLikeCommentRespondPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let user = UserCache().getUser() {
                        if let ownerId = user["id"] as? NSString, mobileNotificationBaseId = payload["mobileNotificationBaseId"] as? NSString {
                            HabitlabRestAPI.getFilteredNotification(ownerId as String, mobileNotificationBase: mobileNotificationBaseId as String, handler: { (error, notificationJson) in
                                if notificationJson != nil && error == nil {
                                    if let notification = notificationJson as? NSDictionary {
                                        
                                        // Do not show the same thing again.
                                        if let selfViewController = self as? WriteMessageController {
                                            if let selfNotification = selfViewController.notification {
                                                if let selfNotificationId = selfNotification["id"] as? NSString {
                                                    if let notificationId = notification["id"] as? NSString {
                                                        if (selfNotificationId as String).compare(notificationId as String) == NSComparisonResult.OrderedSame {
                                                            return
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        dispatch_async(dispatch_get_main_queue(), {
                                            if let _ = UserCache().getUser() {
                                                if let navigationController = self.navigationController {
                                                    if let storyboard = navigationController.storyboard {
                                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("writeMessageControllerFeed") as? WriteMessageController {
                                                            viewController.isOpenedFromPushNotification = true
                                                            viewController.notification = notification
                                                            navigationController.pushViewController(viewController, animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleThinkingAboutYouPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let user = UserCache().getUser() {
                        if let ownerId = user["id"] as? NSString, mobileNotificationBaseId = payload["mobileNotificationBaseId"] as? NSString {
                            HabitlabRestAPI.getFilteredNotification(ownerId as String, mobileNotificationBase: mobileNotificationBaseId as String, handler: { (error, notificationJson) in
                                if notificationJson != nil && error == nil {
                                    if let notification = notificationJson as? NSDictionary {
                                        
                                        // Do not show the same thing again.
                                        if let selfViewController = self as? WriteMessageController {
                                            if let selfNotification = selfViewController.notification {
                                                if let selfNotificationId = selfNotification["id"] as? NSString {
                                                    if let notificationId = notification["id"] as? NSString {
                                                        if (selfNotificationId as String).compare(notificationId as String) == NSComparisonResult.OrderedSame {
                                                            return
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        dispatch_async(dispatch_get_main_queue(), {
                                            if let _ = UserCache().getUser() {
                                                if let navigationController = self.navigationController {
                                                    if let storyboard = navigationController.storyboard {
                                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("writeMessageControllerFeed") as? WriteMessageController {
                                                            viewController.isOpenedFromPushNotification = true
                                                            viewController.notification = notification
                                                            navigationController.pushViewController(viewController, animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleUnpluggingPlanPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let user = UserCache().getUser() {
                        if let ownerId = user["id"] as? NSString, mobileNotificationBaseId = payload["mobileNotificationBaseId"] as? NSString {
                            HabitlabRestAPI.getFilteredNotification(ownerId as String, mobileNotificationBase: mobileNotificationBaseId as String, handler: { (error, notificationJson) in
                                if notificationJson != nil && error == nil {
                                    if let notification = notificationJson as? NSDictionary {
                                        
                                        // Do not show the same thing again.
                                        if let selfViewController = self as? WriteMessageController {
                                            if let selfNotification = selfViewController.notification {
                                                if let selfNotificationId = selfNotification["id"] as? NSString {
                                                    if let notificationId = notification["id"] as? NSString {
                                                        if (selfNotificationId as String).compare(notificationId as String) == NSComparisonResult.OrderedSame {
                                                            return
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        dispatch_async(dispatch_get_main_queue(), {
                                            if let _ = UserCache().getUser() {
                                                if let navigationController = self.navigationController {
                                                    if let storyboard = navigationController.storyboard {
                                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("writeMessageControllerFeed") as? WriteMessageController {
                                                            viewController.isOpenedFromPushNotification = true
                                                            viewController.notification = notification
                                                            navigationController.pushViewController(viewController, animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleCurrentHabitPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let programId = payload["programId"] as? NSString {
                        HabitlabRestAPI.getProgram(programId as String, handler: { (error, programJson) in
                            if programJson != nil && error == nil {
                                if let program = programJson as? NSDictionary {
                                    
                                    // Do not show the same thing again.
                                    if let selfViewController = self as? CurrentHabitController {
                                        if let selfProgram = selfViewController.program {
                                            if let selfProgramId = selfProgram["id"] as? NSString {
                                                if let programId = program["id"] as? NSString {
                                                    if (selfProgramId as String).compare(programId as String) == NSComparisonResult.OrderedSame {
                                                        return
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    dispatch_async(dispatch_get_main_queue(), {
                                        if let _ = UserCache().getUser() {
                                            if let navigationController = self.navigationController {
                                                if let storyboard = navigationController.storyboard {
                                                    if let viewController = storyboard.instantiateViewControllerWithIdentifier("currentHabitController") as? CurrentHabitController {
                                                        viewController.isOpenedFromPushNotification = true
                                                        viewController.program = program
                                                        navigationController.pushViewController(viewController, animated: true)
                                                    }
                                                }
                                            }
                                        }
                                    })
                                }
                            }
                        })
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
    
    private func handleYourHabitsPushNotification(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let _ = self as? HabitsController {
                return
            }
            
            Serv.showSpinner()
            
            HabitlabRestAPI.getActiveProgramsByUser({ (error, programsJson) -> () in
                Serv.hideSpinner()
                
                // An ugly hack to redirect Habits screen to the CTA screen of the only existing program, if any.
                if programsJson != nil {
                    if let programsArray = programsJson as? NSArray {
                        if programsArray.count == 1 {
                            if let program = programsArray[0] as? NSDictionary {
                                dispatch_async(dispatch_get_main_queue(), {
                                    if let _ = UserCache().getUser() {
                                        if let navigationController = self.navigationController {
                                            if let storyboard = navigationController.storyboard {
                                                if let viewController = storyboard.instantiateViewControllerWithIdentifier("habitsController") as? HabitsController {
                                                    objc_sync_enter(HabitsController.object)
                                                    HabitsController.singleProgram = program
                                                    objc_sync_exit(HabitsController.object)
                                                    viewController.isOpenedFromPushNotification = true
                                                    navigationController.pushViewController(viewController, animated: true)
                                                }
                                            }
                                        }
                                    }
                                })
                            } else {
                                dispatch_async(dispatch_get_main_queue(), {
                                    if let _ = UserCache().getUser() {
                                        if let navigationController = self.navigationController {
                                            if let storyboard = navigationController.storyboard {
                                                if let viewController = storyboard.instantiateViewControllerWithIdentifier("habitsController") as? HabitsController {
                                                    viewController.isOpenedFromPushNotification = true
                                                    navigationController.pushViewController(viewController, animated: true)
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            })
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }

    private func handleProgramStepSecondCheckFeed(info: [NSObject: AnyObject], origin: PushNotificationOrigin) {
        switch origin {
        case PushNotificationOrigin.Background, PushNotificationOrigin.Outside:
            if let aps = info["aps"] as? NSDictionary {
                if let payload = aps["payload"] as? NSDictionary {
                    if let user = UserCache().getUser() {
                        if let ownerId = user["id"] as? NSString, programStepId = payload["programStepId"] as? NSString {
                            HabitlabRestAPI.getFilteredNotification(ownerId as String, programStepObject: programStepId as String, mobileNotificationType: "program-step-overdue-second", handler: { (error, notificationJson) in
                                if notificationJson != nil && error == nil {
                                    if let notification = notificationJson as? NSDictionary {
                                        
                                        // Do not show the same thing again.
                                        if let selfViewController = self as? WriteMessageController {
                                            if let selfNotification = selfViewController.notification {
                                                if let selfNotificationId = selfNotification["id"] as? NSString {
                                                    if let notificationId = notification["id"] as? NSString {
                                                        if (selfNotificationId as String).compare(notificationId as String) == NSComparisonResult.OrderedSame {
                                                            return
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        dispatch_async(dispatch_get_main_queue(), {
                                            if let _ = UserCache().getUser() {
                                                if let navigationController = self.navigationController {
                                                    if let storyboard = navigationController.storyboard {
                                                        if let viewController = storyboard.instantiateViewControllerWithIdentifier("writeMessageControllerFeed") as? WriteMessageController {
                                                            viewController.isOpenedFromPushNotification = true
                                                            viewController.notification = notification
                                                            navigationController.pushViewController(viewController, animated: true)
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                }
            }
        case PushNotificationOrigin.Active:
            break // Do nothing.
        }
    }
}
