//
//  ReviewAppController.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/23/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import MessageUI
import FBSDKShareKit
import Analytics

class ReviewAppController: GAITrackedViewController, FBSDKAppInviteDialogDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var leaveReviewButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ReviewAppController)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    @IBAction func clickedClose(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func clickedLeaveReview(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://itunes.apple.com/app/id1071630321")!)
        
        Serv.showSpinner()
        
        HabitlabRestAPI.postAcceptIosAppReview { (error, result) in
            Serv.hideSpinner()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        leaveReviewButton.layer.cornerRadius = 0.5 * leaveReviewButton.bounds.size.height
        self.hasExitedUnplugCheck()
        self.segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(ReviewAppController))
    }
    
    // MARK: UIViewController Methods
    
    @IBAction func clickInviteFriendsThroughFacebook() {
        let content = FBSDKAppInviteContent()
        content.appLinkURL = NSURL(string: "https://fb.me/1025193340894092")
        // content.previewImageURL = NSURL(string: "https://www.habitlab.me/images/app-banner/1200x628.png")
        
        FBSDKAppInviteDialog.showFromViewController(self, withContent: content, delegate: self)
    }
    
    @IBAction func clickInviteFriendsThroughTwitter() {
        
    }
    
    @IBAction func clickInviteFriendsThroughEmail() {
        if MFMailComposeViewController.canSendMail() {
            let composeViewController = MFMailComposeViewController()
            composeViewController.mailComposeDelegate = self
            composeViewController.setSubject("Go find me on Habitlab!")
            composeViewController.setMessageBody("Hi, join me on Habitlab, a community for close friends and family to follow and inspire each other while building good habits – and make them stick. Follow this link: https://www.habitlab.me", isHTML: false)
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending email", message: "This device cannot send emails.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    @IBAction func clickInviteFriendsThroughSMS() {
        if MFMessageComposeViewController.canSendText() {
            let composeViewController = MFMessageComposeViewController()
            composeViewController.body = "Hi, join me on Habitlab, a community for close friends and family to follow and inspire each other while building good habits – and make them stick. Follow this link: https://www.habitlab.me"
            composeViewController.messageComposeDelegate = self
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending SMS", message: "This device cannot send SMS.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if error != nil {
            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
        }
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        if result == MessageComposeResultFailed {
            Serv.showErrorPopup("Error while sending SMS", message: "The SMS could not be sent.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        
        if results != nil {
            print(results)
        }
        
        // Do nothing
    }
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!) {
        // Do nothing
        
        if error != nil {
            print(error)
        }
    }
}
