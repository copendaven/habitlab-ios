//
//  ShowcaseSwipeController.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/29/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ShowcaseSwipeController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var backgroundScrollView: UIScrollView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        signupButton.layer.cornerRadius = 0.5 * signupButton.bounds.size.height
        
        backgroundScrollView.delegate = self
        
        let screenSize = UIScreen.mainScreen().bounds.size
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        backgroundScrollView.contentSize = CGSizeMake(screenWidth, screenHeight)
        
        for i in 1...5 {
            let image = UIImage(named: "HBL_TumblrScreens_\(i)")
            let imageView = UIImageView(image: image)
            imageView.contentMode = UIViewContentMode.ScaleAspectFill
            imageView.frame = CGRectMake(CGFloat(i - 1) * screenWidth, 0, screenWidth, screenHeight)
            backgroundScrollView.addSubview(imageView)
        }
        
        backgroundScrollView.contentSize = CGSizeMake(screenWidth * 5, screenHeight)
        
        pageControl.addTarget(self, action: #selector(ShowcaseSwipeController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func changePage(sender: AnyObject) {
        let offsetX = CGFloat(pageControl.currentPage) * backgroundScrollView.frame.size.width
        backgroundScrollView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    // Facebook Delegate Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    @IBAction func clickedSignup(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}