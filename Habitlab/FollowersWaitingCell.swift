//
//  FollowersWaitingCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowersWaitingCell: FollowersCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var deletedUnfollow = false
    
    func populate(follower: NSDictionary, controller: UIViewController) {
        super.populate(follower, controller: controller, userName: userName, userImage: userImage)
        deletedUnfollow = false
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickCancelRequestButton(sender: AnyObject) {
        if deletedUnfollow {
            return
        }
        
        if follower != nil && controller != nil {
            if let user = follower!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    let cancelRequestAlert = UIAlertController(title: "Cancel request", message: "By cancelling the request to follow, your friend will not see your follow request anymore.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    cancelRequestAlert.addAction(UIAlertAction(title: "Cancel request", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction!) in
                        self.deletedUnfollow = true
                        Serv.showSpinner()
                        
                        HabitlabRestAPI.deleteUnfollow(id as String, handler: { (error, followed) -> () in
                            Serv.hideSpinner()
                            
                            if error != nil {
                                self.deletedUnfollow = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                            
                            if let followersController = self.controller! as? FollowersController {
                                followersController.refresh()
                            }
                        })
                    }))
                
                    cancelRequestAlert.addAction(UIAlertAction(title: "Keep request", style: .Cancel, handler: nil))
                    
                    self.controller!.presentViewController(cancelRequestAlert, animated: true, completion: nil)
                }
            }
        }
    }
}
