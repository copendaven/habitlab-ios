//
//  ProgramStepInspireCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ProgramStepInspireCache: NSObject {
    private static var caches = Set<String>()
    private static var object = NSObject()
    
    static func erase() {
        objc_sync_enter(ProgramStepInspireCache.object)
        ProgramStepInspireCache.caches.removeAll()
        objc_sync_exit(ProgramStepInspireCache.object)
    }
    
    func setStepInspired(stepId: String) {
        objc_sync_enter(ProgramStepInspireCache.object)
        ProgramStepInspireCache.caches.insert(stepId)
        objc_sync_exit(ProgramStepInspireCache.object)
    }
    
    func removeStepInspired(stepId: String) {
        objc_sync_enter(ProgramStepInspireCache.object)
        ProgramStepInspireCache.caches.remove(stepId)
        objc_sync_exit(ProgramStepInspireCache.object)
    }
    
    func isStepInspired(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepInspireCache.object)
        let result = ProgramStepInspireCache.caches.contains(stepId)
        objc_sync_exit(ProgramStepInspireCache.object)
        return result
    }
}