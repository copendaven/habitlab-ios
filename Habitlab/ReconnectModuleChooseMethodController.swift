//
//  ReconnectModuleChooseMethodController.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import MessageUI
import FBSDKShareKit
import FBSDKMessengerShareKit
import Analytics

class ReconnectModuleChooseMethodController: GAITrackedViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, FBSDKSharingDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var contact: NSDictionary?
    var color: UIColor?
    var program: NSDictionary?
    var step: NSDictionary?
    
    var progressDoneController: ProgressDoneController?
    var progressDoneBackgroundColor: ProgressDoneBackgroundColor?
    
    var clickedMethod = false
    
    private var friendlyMessages: [String]?
    private var suggestionsFirst: [String]?
    private var doneStatus: String?
    private var suggestionSecond: String?
    
    private var actions: NSMutableArray = NSMutableArray(array: [ModuleActionType.Later.rawValue])
    
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var smsImage: UIImageView!
    @IBOutlet weak var facebookImage: UIImageView!
    @IBOutlet weak var emailImage: UIImageView!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var smsButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    
    @IBOutlet weak var contactBorder: UIView!
    @IBOutlet weak var initialLabel: UILabel!
    @IBOutlet weak var contactImage: UIImageView!
    
    @IBOutlet weak var actionsCollectionView: UICollectionView!
    @IBOutlet weak var connectWithLabel: UILabel!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ReconnectModuleChooseMethodController)
        
        actionsCollectionView.dataSource = self
        actionsCollectionView.delegate = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        self.navigationItem.title = ""
        
        self.handleContact()
        
        if let viewController = self.progressDoneController {
            viewController.hideSilently()
        }
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        setCanEmail()
        setCanSMS()
        setCanPhone()
        setCanFacebook()
        setPhoto()
        refreshActions()
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if let realStep = self.step {
            if let index = realStep["index"] as? NSNumber {
                dict["index"] = String(index)
            }
            
            if let week = realStep["week"] as? NSNumber {
                dict["week"] = String(week)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ReconnectModuleChooseMethodController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ReconnectModuleChooseMethodController))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }

    private func handleContact() {
        self.contactBorder.layer.cornerRadius = 0.5 * self.contactBorder.bounds.size.width
        self.contactImage.layer.cornerRadius = 0.5 * self.contactImage.bounds.size.width
        self.initialLabel.layer.cornerRadius = 0.5 * self.initialLabel.bounds.size.width
        
        if let realContact = self.contact {
            var initialLabelText = ""
            var firstName: String? = nil
            var lastName: String? = nil
            
            if let realContactFirstName = realContact["firstName"] as? NSString {
                if (realContactFirstName as String).characters.count > 0 {
                    firstName = realContactFirstName as String
                }
            }
            
            if firstName == nil {
                if let realUser = realContact["user"] as? NSDictionary {
                    if let realUserFirstName = realUser["firstName"] as? NSString {
                        firstName = realUserFirstName as String
                    }
                }
            }
            
            if let realFirstName = firstName {
                if realFirstName.characters.count > 0 {
                    dispatch_async(dispatch_get_main_queue(), {
                        self.connectWithLabel.text = "Connect with \(realFirstName)"
                    })
                    
                    initialLabelText += realFirstName[realFirstName.startIndex.advancedBy(0)..<realFirstName.startIndex.advancedBy(1)]
                }
            }
            
            if let realContactLastName = realContact["lastName"] as? NSString {
                if (realContactLastName as String).characters.count > 0 {
                    lastName = realContactLastName as String
                }
            }
            
            if lastName == nil {
                if let realUser = realContact["user"] as? NSDictionary {
                    if let realUserLastName = realUser["lastName"] as? NSString {
                        lastName = realUserLastName as String
                    }
                }
            }
            
            if let realLastName = lastName {
                if realLastName.characters.count > 0 {
                    initialLabelText += realLastName[realLastName.startIndex.advancedBy(0)..<realLastName.startIndex.advancedBy(1)]
                }
            }
            
            if initialLabelText == "" {
                initialLabelText = "?"
            }
            
            var hasPhoto = false
            
            if let user = realContact["user"] as? NSDictionary {
                if let photo = user["facebookPhoto"] as? NSString {
                    hasPhoto = true
                    
                    Serv.loadImage(photo as String, handler: { (error, data) -> () in
                        if error != nil {
                            print(error)
                        }
                        
                        if data != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.contactImage.hidden = false
                                self.contactImage.image = UIImage(data: data!)
                            })
                        }
                    })
                }
            }
            
            if !hasPhoto {
                if let identifier = realContact["identifier"] as? NSString {
                    
                    // Read the image from the photo cache first, if possible.
                    if let imageData = PhotoCache().getCache("contact\(identifier)") {
                        hasPhoto = true
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.contactImage.hidden = false
                            self.contactImage.image = UIImage(data: imageData)
                        })
                    }
                }
            }
            
            if !hasPhoto {
                if let contactHasPhoto = realContact["hasPhoto"] as? NSNumber, identifier = realContact["identifier"] as? NSString {
                    if contactHasPhoto == 1 {
                        hasPhoto = true
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                            
                            // If the photo cache did not contain the image, I will download it from Amazon...
                            HabitlabS3API.downloadContactImage(identifier as String, handler: { (error, imageData) -> () in
                                if error != nil {
                                    return
                                }
                                
                                if imageData != nil {
                                    PhotoCache().setCache("contact\(identifier)", image: imageData!)
                                    
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.contactImage.hidden = false
                                        self.contactImage.image = UIImage(data: imageData!)
                                    })
                                }
                            }, progress: nil)
                        })
                    }
                }
            }
            
            if !hasPhoto {
                self.contactImage.hidden = true
                self.initialLabel.hidden = false
                
                if let realColor = self.color {
                    self.initialLabel.backgroundColor = realColor
                } else {
                    self.initialLabel.backgroundColor = UIColor.redColor()
                }
  
                self.initialLabel.text = initialLabelText.uppercaseString
            }
        }
    }
    
    private func startStep() {
        self.clickedMethod = true
        self.refreshActions()
        
        if let realStep = self.step {
            if let stepId = realStep["id"] as? NSString {
                if !ProgramStepStartedCache().isStepStarted(stepId as String) {
                    HabitlabRestAPI.putProgramStepStarted("Habitlab reconnect tracker", stepId: stepId as String, handler: { (error, response) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            return
                        }
                        
                        ProgramStepStartedCache().setStepStarted(stepId as String)
                        return
                    })
                }
            }
        }
    }
    
    func buttonClicked(actionType: ModuleActionType) {
        switch (actionType) {
        case .Done:
            print("Clicked done")
            self.handleDone()
            break
        case .Later:
            self.clickedLater()
            print("Clicked later")
            break
        default:
            break
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressDoneFromReconnectModuleChooseMethod" {
            if let viewController = segue.destinationViewController as? ProgressDoneController {
                self.progressDoneController = viewController
                viewController.setDelegate(self)
                
                if let backgroundColor = self.progressDoneBackgroundColor {
                    viewController.setBackgroundColor(backgroundColor)
                }
                
                viewController.setDoneCaptions(friendlyMessages, doneStatus: doneStatus, suggestionsFirst: suggestionsFirst, suggestionSecond: suggestionSecond)
                viewController.markDone()
            }
        }
    }
    
    private func clickedLater() {
        self.performSegueWithIdentifier("showCurrentHabitFromReconnectModuleChooseMethod", sender: self)
    }
    
    private func handleDone() {
        if step == nil {
            return
        }
        
        if program == nil {
            return
        }
        
        if contact == nil {
            return
        }
        
        if let owner = program!["owner"] as? NSDictionary, stepId = step!["id"] as? NSString, programId = program!["id"] as? NSString, contactId = contact!["id"] as? NSString {
            
            let cacheId = NSUUID().UUIDString
            
            if ProgramStepCompleteCache().isStepComplete(stepId as String) {
                return
            }
            
            ProgramStepCompleteCache().setStepComplete(stepId as String)
            ProgramStepCompleteReconnectContactCache().setStepComplete(stepId as String, contact: contact!)
            
            CurrentHabitController.setInspirationCache(cacheId, program: self.program!, step: self.step!, owner: owner)
            
            Serv.showSpinner()
            
            HabitlabRestAPI.putProgramStepReconnectContact(contactId as String, programId: programId as String, stepId: stepId as String, handler: { (error, response) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    ProgramStepCompleteReconnectContactCache().unsetStepComplete(stepId as String)
                    return
                }
                
                Serv.showSpinner()
                
                HabitlabRestAPI.putProgramStepComplete("Habitlab reconnect tracker", stepId: stepId as String, cacheId: cacheId, handler: { (error, stepJson) -> () in
                    Serv.hideSpinner()
                    
                    if error != nil {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        ProgramStepCompleteCache().unsetStepComplete(stepId as String)
                        return
                    }
                    
                    HabitsController.resetPrograms()
                    self.progressDoneStarted()
                })
            })
        }
    }
    
    private func progressDoneStarted() {
        if step == nil || program == nil {
            return
        }
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Red
        
        friendlyMessages = ["Hooray!", "Well done!", "Rockstar!", "You rock!", "Way to go!", "Woohoo!", "You did it!", "One down!", "Alrighty!"]
        suggestionsFirst = ["Make this count even more", "Double the impact", "Spread the good vibes", "Make sure to celebrate"]
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        doneStatus = "Loop complete"
        suggestionSecond = "by inspiring your friends!"
        
        if let drive = self.program!["drive"] as? NSDictionary, week = self.step!["week"] as? NSNumber, index = self.step!["index"] as? NSNumber {
            if let countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                let weekInt = Int(week)
                let countWeeksInt = Int(countWeeks)
                let indexInt = Int(index)
                let countStepsPerWeekInt = Int(countStepsPerWeek)
                
                if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt - 1 {
                    
                    // We are dealing with the last loop in the level, so the level is complete!
                    self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Green
                    
                    // We are dealing with the last loop in the level, so the level is complete!
                    doneStatus = "Level complete"
                    suggestionSecond = "by inspiring your friends!"
                    
                    if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                        let levelInt = Int(level)
                        let countLevels = Int(countLevels)
                        
                        if levelInt >= countLevels {
                            
                            // We are dealing with the last level in the program, so the program is complete!
                            doneStatus = "Program complete"
                            suggestionSecond = "by inspiring your friends!"
                            
                            // We are dealing with the last level in the program, so the program is complete!
                            self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Blue
                        }
                    }
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.performSegueWithIdentifier("showProgressDoneFromReconnectModuleChooseMethod", sender: self)
        })
    }
    
    func progressDoneFinished() {
        self.performSegueWithIdentifier("showCurrentHabitFromReconnectModuleChooseMethod", sender: self)
    }
    
    private func refreshActions() {
        self.actions = NSMutableArray()
        
        self.actions.addObject(ModuleActionType.Later.rawValue)
        
        if clickedMethod {
            self.actions.addObject(ModuleActionType.Done.rawValue)
        } else {
            if let realStep = self.step {
                if let stepId = realStep["id"] as? NSString {
                    if ProgramStepStartedCache().isStepStarted(stepId as String) {
                        self.actions.addObject(ModuleActionType.Done.rawValue)
                    }
                }
            }
        }
        
        self.actionsCollectionView.reloadData()
    }
    
    private func setCanEmail() {
        var canEmail = false
        
        if MFMailComposeViewController.canSendMail() {
            canEmail = true
        }
        
        emailButton.enabled = canEmail
        emailImage.alpha = 0.5 + 0.5 * (canEmail ? 1 : 0)
    }
    
    private func setCanPhone() {
        var canPhone = false
        
        if let realContact = self.contact {
            if let phone = realContact["phone"] as? NSString {
                if let url = NSURL(string: "tel://\(phone)") {
                    if UIApplication.sharedApplication().canOpenURL(url) {
                        canPhone = true
                    }
                }
            }
        }
        
        phoneButton.enabled = canPhone
        phoneImage.alpha = 0.5 + 0.5 * (canPhone ? 1 : 0)
    }
    
    private func setCanFacebook() {
        var canFacebook = false
        
        if let realContact = self.contact {
            if let user = realContact["user"] as? NSDictionary {
                if let facebookId = user["facebookId"] as? NSString {
                    if let url = NSURL(string: "fb-messenger://user-thread/\(facebookId as String)") {
                        if UIApplication.sharedApplication().canOpenURL(url) {
                            canFacebook = true
                        }
                    }
                }
            }
        }
        
        if let url = NSURL(string: "fb-messenger://") {
            if UIApplication.sharedApplication().canOpenURL(url) {
                canFacebook = true
            }
        }
        
        facebookButton.enabled = canFacebook
        facebookImage.alpha = 0.5 + 0.5 * (canFacebook ? 1 : 0)
    }
    
    private func setCanSMS() {
        var canSMS = false
        
        if MFMessageComposeViewController.canSendText() {
            canSMS = true
        }
    
        smsButton.enabled = canSMS
        smsImage.alpha = 0.5 + 0.5 * (canSMS ? 1 : 0)
    }
    
    // Done.
    @IBAction func clickInviteContactThroughFacebook() {
        if let realContact = self.contact {
            if let user = realContact["user"] as? NSDictionary {
                if let facebookId = user["facebookId"] as? NSString {
                    if let url = NSURL(string: "fb-messenger://user-thread/\(facebookId as String)") {
                        if UIApplication.sharedApplication().canOpenURL(url) {
                            UIApplication.sharedApplication().openURL(url)
                            self.startStep()
                            return
                        }
                    }
                }
            }
        }
    
        if let url = NSURL(string: "fb-messenger://") {
            if UIApplication.sharedApplication().canOpenURL(url) {
                UIApplication.sharedApplication().openURL(url)
                self.startStep()
                return
            }
        }
    }
    
    @IBAction func clickInviteContactThroughPhone() {
        if let realContact = self.contact {
            if let phone = realContact["phone"] as? NSString {
                if let url = NSURL(string: "tel:\(phone)") {
                    if UIApplication.sharedApplication().canOpenURL(url) {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
            }
        }
        
        self.startStep()
    }
    
    // Done.
    @IBAction func clickInviteContactThroughEmail() {
        if MFMailComposeViewController.canSendMail() {
            let composeViewController = MFMailComposeViewController()
            composeViewController.mailComposeDelegate = self
            
            if let realContact = self.contact {
                if let email = realContact["email"] as? NSString {
                    composeViewController.setToRecipients([email as String])
                }
            }
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending email", message: "This device cannot send emails.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
        
        self.startStep()
    }
    
    @IBAction func clickInviteContactThroughSMS() {
        if MFMessageComposeViewController.canSendText() {
            let composeViewController = MFMessageComposeViewController()
            composeViewController.messageComposeDelegate = self
            
            if let realContact = self.contact {
                if let phone = realContact["phone"] as? NSString {
                    composeViewController.recipients = [phone as String]
                }
            }
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending SMS", message: "This device cannot send SMS.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
        
        self.startStep()
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if error != nil {
            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
        }
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        if result == MessageComposeResultFailed {
            Serv.showErrorPopup("Error while sending SMS", message: "The SMS could not be sent.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!) {
        print("Cancelled")
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        if let realError = error {
            print(realError)
        }
    }
    
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        // Do nothing.
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.actions.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(90, 125) // This is the size for one collection item with button and label
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenWidth = Int(actionsCollectionView.frame.size.width)
        var contentWidth = Int(actions.count * 90) // This is the size for one collection item with button and label
        
        if screenWidth <= contentWidth {
            contentWidth = screenWidth / 90 * 90
        }
        
        let padding = CGFloat((screenWidth - contentWidth) / 2)
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if actions.count <= indexPath.item {
            return defaultCell
        }
        
        if let action = actions[indexPath.item] as? String {
            if let cell = actionsCollectionView.dequeueReusableCellWithReuseIdentifier("moduleActionCollectionCell", forIndexPath: indexPath) as? ModuleActionCollectionCell {
                if let moduleActionType = ModuleActionType(rawValue: action) {
                    cell.populate(actionType: moduleActionType, actionState: ModuleActionState.Enabled, controller: self)
                    return cell
                }
            }
        }
        
        return defaultCell
    }
    
    private func getFirstName() -> String? {
        var firstName: String? = nil
        
        if let realContact = self.contact {
            if let realFirstName = realContact["firstName"] as? NSString {
                firstName = realFirstName as String
            }
            
            if let user = realContact["user"] as? NSDictionary {
                if let realFirstName = user["firstName"] as? NSString {
                    firstName = realFirstName as String
                }
            }
        }
        
        return firstName
    }
    
    private func getLastName() -> String? {
        var lastName: String? = nil
        
        if let realContact = self.contact {
            if let realLastName = realContact["lastName"] as? NSString {
                lastName = realLastName as String
            }
            
            if let user = realContact["user"] as? NSDictionary {
                if let realLastName = user["lastName"] as? NSString {
                    lastName = realLastName as String
                }
            }
        }
        
        return lastName
    }
    
    private func getReason() -> String? {
        var reason: String? = nil
        
        if let realContact = self.contact {
            if let realReason = realContact["reason"] as? NSString {
                reason = realReason as String
            }
        }
        
        return reason
    }
    
    private func setPhoto() {
        var hasPhoto = false
        
        if let realContact = self.contact {
            if let user = realContact["user"] as? NSDictionary {
                if let photo = user["facebookPhoto"] as? NSString {
                    hasPhoto = true
                    self.initialLabel.hidden = true
                    
                    Serv.loadImage(photo as String, handler: { (error, data) -> () in
                        if error != nil {
                            print(error)
                        }
                        
                        if data != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.contactImage.hidden = false
                                self.contactImage.image = UIImage(data: data!)
                            })
                        }
                    })
                }
            }
            
            if !hasPhoto {
                if let identifier = realContact["identifier"] as? NSString {
                    
                    // Read the image from the photo cache first, if possible.
                    if let imageData = PhotoCache().getCache("contact\(identifier)") {
                        hasPhoto = true
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.contactImage.hidden = false
                            self.contactImage.image = UIImage(data: imageData)
                        })
                    }
                }
            }
            
            if !hasPhoto {
                if let contactHasPhoto = realContact["hasPhoto"] as? NSNumber, identifier = realContact["identifier"] as? NSString {
                    if contactHasPhoto == 1 {
                        hasPhoto = true
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                            
                            // If the photo cache did not contain the image, I will download it from Amazon...
                            HabitlabS3API.downloadContactImage(identifier as String, handler: { (error, imageData) -> () in
                                if error != nil {
                                    return
                                }
                                
                                if imageData != nil {
                                    PhotoCache().setCache("contact\(identifier)", image: imageData!)
                                    
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.contactImage.hidden = false
                                        self.contactImage.image = UIImage(data: imageData!)
                                    })
                                }
                                }, progress: nil)
                        })
                    }
                }
            }
        }
        
        if !hasPhoto {
            self.contactImage.hidden = true
            self.initialLabel.hidden = false
            self.initialLabel.backgroundColor = color
            
            var initialLabelText = ""
            
            if let realFirstName = getFirstName() {
                if realFirstName.characters.count > 0 {
                    initialLabelText += realFirstName[realFirstName.startIndex.advancedBy(0)..<realFirstName.startIndex.advancedBy(1)]
                }
            }
            
            if let realLastName = getLastName() {
                if realLastName.characters.count > 0 {
                    initialLabelText += realLastName[realLastName.startIndex.advancedBy(0)..<realLastName.startIndex.advancedBy(1)]
                }
            }
            
            if initialLabelText == "" {
                initialLabelText = "?"
            }
            
            self.initialLabel.text = initialLabelText.uppercaseString
        }
    }
}
