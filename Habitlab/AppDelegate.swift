//
//  AppDelegate.swift
//  Habitlab
//
//  Created by Vlad Manea on 06/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import Contacts

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var contactStore = CNContactStore()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // Initializations for services
        Configuration.initializeFacebook(application, didFinishLaunchingWithOptions: launchOptions)
        Configuration.initializeAmazonCognito()
        Configuration.initializeHockeyApp()
        Configuration.initializeHeapAnalytics()
        Configuration.initializeSegment()
        Configuration.initializeGoogleAnalytics()
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), nil, LockNotifierCallback.notifierProc(), "com.apple.springboard.lockcomplete", nil, CFNotificationSuspensionBehavior.DeliverImmediately)
        
        if let options = launchOptions {
            if let userInfo = options[UIApplicationLaunchOptionsRemoteNotificationKey] as? [NSObject : AnyObject] {
                
                // Handle the push notification.
                PushNotificationCache().setPushNotification(userInfo, origin: PushNotificationOrigin.Outside, overwrite: true)
            }
        }
        
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        Configuration.initializeFacebook(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        
        FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return true
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if !notificationSettings.types.contains([.Alert, .Badge, .Sound]) {
            print("No permissions for notification :(")
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>")
        var deviceTokenString: String = (deviceToken.description as NSString).stringByTrimmingCharactersInSet(characterSet).stringByReplacingOccurrencesOfString(" ", withString: "") as String
        deviceTokenString = deviceTokenString.uppercaseString
        
        Serv.showSpinner()
        
        HabitlabRestAPI.postIosAppToken(deviceTokenString, indicator: nil, handler: { (error, result) -> () in
            Serv.hideSpinner()
            
            if error != nil {
                if self.window != nil {
                    Serv.showErrorPopupFromError(error, controller: self.window!.rootViewController, handler: nil)
                }
                
                return
            }
        
            NSUserDefaults.standardUserDefaults().setObject(deviceTokenString, forKey: "deviceToken")
            NSUserDefaults.standardUserDefaults().synchronize()
        })
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Couldnt register for remote notifications with error: \(error)")
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        if UIApplication.sharedApplication().applicationState == UIApplicationState.Inactive {
        
            // Handle the push notification.
            PushNotificationCache().setPushNotification(userInfo, origin: PushNotificationOrigin.Background, overwrite: false)
        
        } else if UIApplication.sharedApplication().applicationState == UIApplicationState.Active {
            
            if let window = self.window {
                if let rootViewController = window.rootViewController {
                    rootViewController.handleStartupPushNotification(userInfo, origin: PushNotificationOrigin.Active)
                }
            }
        }
        
        // call the completion handler, pass in NoData, since no new data was fetched from the server.
        completionHandler(UIBackgroundFetchResult.NoData)
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        /*
        for timer in Serv.getExistingProgramStepTimers() {
            NSRunLoop.currentRunLoop().addTimer(timer as! NSTimer, forMode: NSDefaultRunLoopMode)
        }
        */
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        /*
        for timer in Serv.getExistingProgramStepTimers() {
            timer.invalidate()
        }
        */
        
        NSLog("Application will resign active")
        print("Application will resign active")
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        NSLog("Application did enter background")
        print("Application did enter background")
        
        let state = UIApplication.sharedApplication().applicationState
        
        if state == UIApplicationState.Inactive {
            NSLog("Sent through lock screen")
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let taskId = self.beginBackgroundUpdateTask()
                Serv.resetCaches()
                self.endBackgroundUpdateTask(taskId)
            }
        } else if state == UIApplicationState.Background {
            if NSUserDefaults.standardUserDefaults().boolForKey("kDisplayStatusLocked") {
                NSLog("Sent through lock screen")
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let taskId = self.beginBackgroundUpdateTask()
                    Serv.resetCaches()
                    self.endBackgroundUpdateTask(taskId)
                }
            } else {
                NSLog("Sent to background by home button")
        
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let taskId = self.beginBackgroundUpdateTask()
                    
                    // Mark all unplug steps as exited.
                    ProgramStepUnplugCache().hasExitedUnplugSendLocalNotification()
                    
                    Serv.resetCaches()
                    self.endBackgroundUpdateTask(taskId)
                }
            }
        }
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "kDisplayStatusLocked")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        NSLog("Application will enter foreground")
        print("Application will enter foreground")
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        // Activate events for Facebook App.
        FBSDKAppEvents.activateApp()
        
        // Resume any unplug steps in progress.
        ProgramStepUnplugCache().resume(UnpluggingModuleController.UnplugFailAfterSeconds)

        // Trigger the push notification from origin backround if necessary.
        if let window = self.window {
            if let rootViewController = window.rootViewController {
                rootViewController.handleStartupPushNotification(PushNotificationOrigin.Background)
            }
        }
    
        NSLog("Application did become active")
        print("Application did become active")
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
        self.saveContext()
        
        NSLog("Application will terminate")
        print("Application will terminate")
    }
    
    func beginBackgroundUpdateTask() -> UIBackgroundTaskIdentifier {
        return UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler(nil)
    }
    
    func endBackgroundUpdateTask(taskID: UIBackgroundTaskIdentifier) {
        UIApplication.sharedApplication().endBackgroundTask(taskID)
    }
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }
    
    func requestForAccessContacts(completionHandler: (accessGranted: Bool) -> Void) {
        let authorizationStatus = CNContactStore.authorizationStatusForEntityType(CNEntityType.Contacts)
        
        switch authorizationStatus {
        case .Authorized:
            completionHandler(accessGranted: true)
            
        case .Denied, .NotDetermined:
            self.contactStore.requestAccessForEntityType(CNEntityType.Contacts, completionHandler: { (access, accessError) -> Void in
                if access {
                    completionHandler(accessGranted: access)
                } else if authorizationStatus == CNAuthorizationStatus.Denied {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        let message = "\(accessError!.localizedDescription)\n\nPlease allow the app to access your contacts through the Settings."
                        print(message)
                    })
                }
            })
            
        default:
            completionHandler(accessGranted: false)
        }
    }
    
    
    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.habitlab.Habitlab" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Habitlab", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options)
        } catch {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                if let window = self.window {
                    if let viewController = window.rootViewController {
                        Serv.showErrorPopup("Data store error", message: "There was an error creating or loading the saved data of this application version.", okMessage: "Okay, got it.", controller: viewController, handler: nil)
                    }
                }
            })
            
            // Replace this with code to handle the error appropriately.
            // abort causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            // abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if let window = self.window {
                        if let viewController = window.rootViewController {
                            Serv.showErrorPopup("Data store error", message: "There was an error saving the data of this application version.", okMessage: "Okay, got it.", controller: viewController, handler: nil)
                        }
                    }
                })
                
                // Replace this with code to handle the error appropriately.
                // abort causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                // abort()
            }
        }
    }
}

