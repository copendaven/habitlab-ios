//
//  ChoosePhysioPainController.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Contacts
import Analytics

enum ChoosePhysioPainTiming: String {
    case BeforeExercising = "BeforeExercising"
    case AfterExercising = "AfterExercising"
}

class ChoosePhysioPainController: GAITrackedViewController {
    var program: NSDictionary?
    var step: NSDictionary?
    var painTiming: ChoosePhysioPainTiming?
    var beforePainLevel: Int?
    var afterPainLevel: Int?
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var painImage: UIImageView!
    @IBOutlet weak var painLabel: UILabel!
    
    @IBOutlet weak var painButton10: UIButton!
    @IBOutlet weak var painButton9: UIButton!
    @IBOutlet weak var painButton8: UIButton!
    @IBOutlet weak var painButton7: UIButton!
    @IBOutlet weak var painButton6: UIButton!
    @IBOutlet weak var painButton5: UIButton!
    @IBOutlet weak var painButton4: UIButton!
    @IBOutlet weak var painButton3: UIButton!
    @IBOutlet weak var painButton2: UIButton!
    @IBOutlet weak var painButton1: UIButton!
    @IBOutlet weak var painButton0: UIButton!
    
    private var friendlyMessages: [String]?
    private var suggestionsFirst: [String]?
    private var doneStatus: String?
    private var suggestionSecond: String?
    
    private var countDownTimer = NSTimer()
    private var countUpTimer = NSTimer()
    private var doneTimer = NSTimer()
    private var actions: NSMutableArray = NSMutableArray(array: [ModuleActionType.Info.rawValue, ModuleActionType.Start.rawValue])
    private var actionStates: NSMutableArray = NSMutableArray(array: [ModuleActionState.Disabled.rawValue, ModuleActionState.Disabled.rawValue])
    
    var progressDoneController: ProgressDoneController?
    var progressDoneBackgroundColor: ProgressDoneBackgroundColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ChoosePhysioPainController)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        self.navigationItem.title = "Pain scale".uppercaseString
        
        self.painButton0.layer.cornerRadius = 0.5 * self.painButton0.bounds.size.width
        self.painButton1.layer.cornerRadius = 0.5 * self.painButton1.bounds.size.width
        self.painButton2.layer.cornerRadius = 0.5 * self.painButton2.bounds.size.width
        self.painButton3.layer.cornerRadius = 0.5 * self.painButton3.bounds.size.width
        self.painButton4.layer.cornerRadius = 0.5 * self.painButton4.bounds.size.width
        self.painButton5.layer.cornerRadius = 0.5 * self.painButton5.bounds.size.width
        self.painButton6.layer.cornerRadius = 0.5 * self.painButton6.bounds.size.width
        self.painButton7.layer.cornerRadius = 0.5 * self.painButton7.bounds.size.width
        self.painButton8.layer.cornerRadius = 0.5 * self.painButton8.bounds.size.width
        self.painButton9.layer.cornerRadius = 0.5 * self.painButton9.bounds.size.width
        self.painButton10.layer.cornerRadius = 0.5 * self.painButton10.bounds.size.width
        
        self.painButton0.layer.borderWidth = 1
        self.painButton1.layer.borderWidth = 1
        self.painButton2.layer.borderWidth = 1
        self.painButton3.layer.borderWidth = 1
        self.painButton4.layer.borderWidth = 1
        self.painButton5.layer.borderWidth = 1
        self.painButton6.layer.borderWidth = 1
        self.painButton7.layer.borderWidth = 1
        self.painButton8.layer.borderWidth = 1
        self.painButton9.layer.borderWidth = 1
        self.painButton10.layer.borderWidth = 1
        
        self.painButton0.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton1.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton2.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton3.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton4.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton5.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton6.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton7.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton8.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton9.layer.borderColor = UIColor.whiteColor().CGColor
        self.painButton10.layer.borderColor = UIColor.whiteColor().CGColor
        
        if let viewController = self.progressDoneController {
            viewController.hideSilently()
        }
        
        self.refresh()
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
                
                if let realPainTiming = self.painTiming {
                    dict["painTiming"] = realPainTiming.rawValue
                }
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ChoosePhysioPainController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ChoosePhysioPainController))
        }
    }
    
    private func handleDone() {
        if let realStep = self.step, realProgram = self.program, realBeforePainLevel = self.beforePainLevel, realAfterPainLevel = self.afterPainLevel {
            if let owner = realProgram["owner"] as? NSDictionary, stepId = realStep["id"] as? NSString {
                let cacheId = NSUUID().UUIDString
                
                if ProgramStepCompleteCache().isStepComplete(stepId as String) {
                    return
                }
                
                ProgramStepCompleteCache().setStepComplete(stepId as String)
                
                CurrentHabitController.setInspirationCache(cacheId, program: realProgram, step: realStep, owner: owner)
                
                Serv.showSpinner()
                
                HabitlabRestAPI.putProgramStepCompleteWithPainLevels("Habitlab physio tracker", stepId: stepId as String, cacheId: cacheId, beforePainLevel: realBeforePainLevel, afterPainLevel: realAfterPainLevel, handler: { (error, stepJson) -> () in
                    Serv.hideSpinner()
                    
                    if error != nil {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        ProgramStepCompleteCache().unsetStepComplete(stepId as String)
                        return
                    }
                    
                    HabitsController.resetPrograms()
                    self.progressDoneStarted()
                })
            }
        }
    }
    
    private func progressDoneStarted() {
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Red
        
        friendlyMessages = ["Hooray!", "Well done!", "Rockstar!", "You rock!", "Way to go!", "Woohoo!", "You did it!", "One down!", "Alrighty!"]
        suggestionsFirst = ["Make this count even more", "Double the impact", "Spread the good vibes", "Make sure to celebrate"]
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        doneStatus = "Loop complete"
        suggestionSecond = "by sharing your progress!"
        
        if let realProgram = self.program, realStep = self.step {
            if let drive = realProgram["drive"] as? NSDictionary, week = realStep["week"] as? NSNumber, index = realStep["index"] as? NSNumber {
                if let countWeeks = drive["countWeeks"] as? NSNumber {
                    let weekInt = Int(week)
                    let countWeeksInt = Int(countWeeks)
                    let indexInt = Int(index)
                    
                    var countStepsPerWeekInt = 0
                    
                    if let countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                        countStepsPerWeekInt = Int(countStepsPerWeek)
                    } else if let countStepsPerDay = drive["countStepsPerDay"] as? NSNumber {
                        countStepsPerWeekInt = 7 * Int(countStepsPerDay)
                    }
                    
                    if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt {
                        
                        // We are dealing with the last loop in the level, so the level is complete!
                        self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Green
                        
                        // We are dealing with the last loop in the level, so the level is complete!
                        doneStatus = "Level complete"
                        suggestionSecond = "by sharing your progress!"
                        
                        if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                            let levelInt = Int(level)
                            let countLevels = Int(countLevels)
                            
                            if levelInt >= countLevels {
                                
                                // We are dealing with the last level in the program, so the program is complete!
                                doneStatus = "Program complete"
                                suggestionSecond = "by sharing your progress!"
                                
                                // We are dealing with the last level in the program, so the program is complete!
                                self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Blue
                            }
                        }
                    }
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.performSegueWithIdentifier("showProgressDoneFromChoosePhysioPain", sender: self)
            })
        }
    }
    
    func progressDoneFinished() {
        self.performSegueWithIdentifier("showCurrentHabitFromChoosePhysioPain", sender: nil)
    }
    
    func refresh() {
        if let realPainTiming = self.painTiming {
            switch realPainTiming {
            case .BeforeExercising:
                if let _ = self.beforePainLevel {
                    self.doneButton.enabled = true
                    self.doneButton.backgroundColor = UIColor(red: 0.21, green: 0.84, blue: 0.59, alpha: 0.9)
                } else {
                    self.doneButton.enabled = false
                    self.doneButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5)
                }
            case .AfterExercising:
                if let _ = self.afterPainLevel {
                    self.doneButton.enabled = true
                    self.doneButton.backgroundColor = UIColor(red: 0.21, green: 0.84, blue: 0.59, alpha: 0.9)
                } else {
                    self.doneButton.enabled = false
                    self.doneButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5)
                }
            }
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressDoneFromChoosePhysioPain" {
            if let viewController = segue.destinationViewController as? ProgressDoneController {
                self.progressDoneController = viewController
                viewController.setDelegate(self)
                
                if let backgroundColor = self.progressDoneBackgroundColor {
                    viewController.setBackgroundColor(backgroundColor)
                }
                
                viewController.setDoneCaptions(friendlyMessages, doneStatus: doneStatus, suggestionsFirst: suggestionsFirst, suggestionSecond: suggestionSecond)
                viewController.markDone()
            }
        }
        
        if (segue.identifier == "showPhysioModuleFromChoosePhysioPainBefore") {
            if let viewController = segue.destinationViewController as? PhysioModuleController {
                viewController.program = self.program
                viewController.step = self.step
                viewController.beforePainLevel = self.beforePainLevel
            }
        }
        
        if (segue.identifier == "showPhysioModuleFromChoosePhysioPainAfter") {
            if let viewController = segue.destinationViewController as? PhysioModuleController {
                viewController.program = self.program
                viewController.step = self.step
                viewController.beforePainLevel = self.beforePainLevel
                viewController.afterPainLevel = self.afterPainLevel
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    @IBAction func clickDone(sender: AnyObject) {
        if let timing = self.painTiming {
            switch timing {
            case .BeforeExercising:
                self.performSegueWithIdentifier("showPhysioModuleFromChoosePhysioPainBefore", sender: self)
            case .AfterExercising:
                self.handleDone()
            }
        }
    }
    
    private func selectPainLevel(level: Int) {
        if let realPainTiming = self.painTiming {
            switch realPainTiming {
            case .BeforeExercising:
                self.beforePainLevel = level
            case .AfterExercising:
                self.afterPainLevel = level
            }
        }
        
        painButton0.backgroundColor = UIColor.clearColor()
        painButton0.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton1.backgroundColor = UIColor.clearColor()
        painButton1.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton2.backgroundColor = UIColor.clearColor()
        painButton2.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton3.backgroundColor = UIColor.clearColor()
        painButton3.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton4.backgroundColor = UIColor.clearColor()
        painButton4.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton5.backgroundColor = UIColor.clearColor()
        painButton5.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton6.backgroundColor = UIColor.clearColor()
        painButton6.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton7.backgroundColor = UIColor.clearColor()
        painButton7.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton8.backgroundColor = UIColor.clearColor()
        painButton8.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton9.backgroundColor = UIColor.clearColor()
        painButton9.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        painButton10.backgroundColor = UIColor.clearColor()
        painButton10.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        
        switch level {
        case 0:
            self.painImage.image = UIImage(named: "PhysioPainNoPain")
            self.painLabel.text = "No pain"
            painButton0.backgroundColor = UIColor.whiteColor()
            painButton0.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 1:
            self.painImage.image = UIImage(named: "PhysioPainFrom1To3")
            self.painLabel.text = "Very mild"
            painButton1.backgroundColor = UIColor.whiteColor()
            painButton1.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 2:
            self.painImage.image = UIImage(named: "PhysioPainFrom1To3")
            self.painLabel.text = "Discomforting"
            painButton2.backgroundColor = UIColor.whiteColor()
            painButton2.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 3:
            self.painImage.image = UIImage(named: "PhysioPainFrom1To3")
            self.painLabel.text = "Tolerable"
            painButton3.backgroundColor = UIColor.whiteColor()
            painButton3.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 4:
            self.painImage.image = UIImage(named: "PhysioPainFrom4To6")
            self.painLabel.text = "Distressing"
            painButton4.backgroundColor = UIColor.whiteColor()
            painButton4.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 5:
            self.painImage.image = UIImage(named: "PhysioPainFrom4To6")
            self.painLabel.text = "Very distressing"
            painButton5.backgroundColor = UIColor.whiteColor()
            painButton5.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 6:
            self.painImage.image = UIImage(named: "PhysioPainFrom4To6")
            self.painLabel.text = "Intense"
            painButton6.backgroundColor = UIColor.whiteColor()
            painButton6.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 7:
            self.painImage.image = UIImage(named: "PhysioPainFrom7To10")
            self.painLabel.text = "Very intense"
            painButton7.backgroundColor = UIColor.whiteColor()
            painButton7.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 8:
            self.painImage.image = UIImage(named: "PhysioPainFrom7To10")
            self.painLabel.text = "Utterly horrible"
            painButton8.backgroundColor = UIColor.whiteColor()
            painButton8.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 9:
            self.painImage.image = UIImage(named: "PhysioPainFrom7To10")
            self.painLabel.text = "Excruciating unbearable"
            painButton9.backgroundColor = UIColor.whiteColor()
            painButton9.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        case 10:
            self.painImage.image = UIImage(named: "PhysioPainFrom7To10")
            self.painLabel.text = "Unimaginable unspeakable"
            painButton10.backgroundColor = UIColor.whiteColor()
            painButton10.setTitleColor(UIColor.redColor(), forState: UIControlState.Normal)
        default:
            // Do nothing.
            break
        }
        
        self.refresh()
    }
    
    @IBAction func clickPainLevel0(sender: AnyObject) {
        self.selectPainLevel(0)
    }
    
    @IBAction func clickPainLevel1(sender: AnyObject) {
        self.selectPainLevel(1)
    }
    
    @IBAction func clickPainLevel2(sender: AnyObject) {
        self.selectPainLevel(2)
    }
    
    @IBAction func clickPainLevel3(sender: AnyObject) {
        self.selectPainLevel(3)
    }
    
    @IBAction func clickPainLevel4(sender: AnyObject) {
        self.selectPainLevel(4)
    }
    
    @IBAction func clickPainLevel5(sender: AnyObject) {
        self.selectPainLevel(5)
    }
    
    @IBAction func clickPainLevel6(sender: AnyObject) {
        self.selectPainLevel(6)
    }
    
    @IBAction func clickPainLevel7(sender: AnyObject) {
        self.selectPainLevel(7)
    }
    
    @IBAction func clickPainLevel8(sender: AnyObject) {
        self.selectPainLevel(8)
    }
    
    @IBAction func clickPainLevel9(sender: AnyObject) {
        self.selectPainLevel(9)
    }
    
    @IBAction func clickPainLevel10(sender: AnyObject) {
        self.selectPainLevel(10)
    }
}
