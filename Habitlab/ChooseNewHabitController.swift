//
//  ChooseNewHabitController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class ChooseNewHabitController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    var categories = []
    var refresher: UIRefreshControl?
    
    @IBOutlet weak var habitsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ChooseNewHabitController)
        
        habitsTableView.delegate = self
        habitsTableView.dataSource = self
        habitsTableView.estimatedRowHeight = 60
        habitsTableView.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = habitsTableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ChooseNewHabitController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        habitsTableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        refresh()
        segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(ChooseNewHabitController))
    }
    
    func filterCategories(categories: NSArray) -> NSArray {
        let result: NSMutableArray = NSMutableArray()
        
        // Add a new object for the first element!
        result.addObjectsFromArray(categories as [AnyObject])
        
        return result
    }
    
    func refresh() {
        CategoryCache.getDriveCategories { (error, categoriesJson) -> () in
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let categories = categoriesJson {
                self.categories = self.filterCategories(categories)
                self.habitsTableView.reloadData()
                
                if let refr = self.refresher {
                    refr.endRefreshing()
                }
            }
        }
    }

    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showChooseNewProgramFromChooseNewHabit") {
            if let viewController = segue.destinationViewController as? ChooseNewProgramController {
                if let category = sender as? NSDictionary {
                    viewController.category = category
                }
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return categories.count + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewHabitHeader", forIndexPath: indexPath)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell
        }
        
        if let category = categories[indexPath.section - 1] as? NSDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewHabitCell", forIndexPath: indexPath) as? ChooseNewHabitCell {
                cell.populate(category, controller: self)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Do nothing
    }
}
