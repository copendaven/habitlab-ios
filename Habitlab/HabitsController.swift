//
//  HabitsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class HabitsController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource {
    private enum ShowState {
        case NoSignup
        case SignupNoContent
        case Content
    }
    
    static var singleProgram: NSDictionary?
    static var singleProgramLock = NSObject()
    static var object = NSObject()
    
    private static var serverPrograms = []
    private static var programs = []
    
    var refresher: UIRefreshControl?
    var isOpenedFromPushNotification = false
    
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var signupArrow: UIImageView!
    
    @IBOutlet weak var noSignupView: UIView!
    @IBOutlet weak var noSignupTitleLabel: UILabel!
    @IBOutlet weak var noSignupContentLabel: UILabel!
    
    @IBOutlet weak var programsTableView: UITableView!
    @IBOutlet weak var newUpdatesButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(HabitsController)
        
        programsTableView.delegate = self
        programsTableView.dataSource = self
        programsTableView.estimatedRowHeight = 100
        programsTableView.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = self.programsTableView
        
        let add = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(HabitsController.clickedAddNavigationButton))
        self.navigationItem.rightBarButtonItem = add
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(HabitsController.refreshFromServer), forControlEvents: UIControlEvents.ValueChanged)
        programsTableView.addSubview(refresher!)
        
        newUpdatesButton.hidden = true
        // newUpdatesButton.layer.cornerRadius = 0.5 * newUpdatesButton.bounds.size.height
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    func clickedAddNavigationButton() {
        self.performSegueWithIdentifier("showChooseAHabitFromHabits", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showState()
        
        self.hasExitedUnplugCheck()
        
        self.signupButton.enabled = false
        
        self.noSignupView.hidden = true
        self.noSignupTitleLabel.hidden = true
        self.noSignupContentLabel.hidden = true
        
        self.signupButton.hidden = true
        self.signupArrow.hidden = true
        
        if self.handleStartupPushNotification(PushNotificationOrigin.Outside) {
            return
        }
        
        objc_sync_enter(HabitsController.singleProgramLock)
        
        if let program = HabitsController.singleProgram {
            self.performSegueWithIdentifier("showCurrentHabitFromHabits", sender: program)
            HabitsController.singleProgram = nil
        }
        
        objc_sync_exit(HabitsController.singleProgramLock)
    
        objc_sync_enter(HabitsController.object)
        
        if HabitsController.serverPrograms.count <= 0 {
            objc_sync_exit(HabitsController.object)
            self.refreshFromServer()
        } else {
            objc_sync_exit(HabitsController.object)
            self.refreshFromClient()
        }
        
        segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(HabitsController))
    }
    
    static func resetPrograms() {
        objc_sync_enter(HabitsController.object)
        HabitsController.serverPrograms = []
        objc_sync_exit(HabitsController.object)
    }
    
    @IBAction func clickedNewUpdatesButton(sender: AnyObject) {
        self.refreshFromServer()
    }
    
    func refreshFromServer() {
        Serv.showSpinner()
        
        HabitlabRestAPI.getActiveProgramsByUser({ (error, programsJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let programsArray = programsJson as? NSArray {
                objc_sync_enter(HabitsController.object)
                HabitsController.serverPrograms = programsArray
                objc_sync_exit(HabitsController.object)
                
                self.refreshFromClient()
            }
        })
    }
    
    func refreshFromClient() {
        objc_sync_enter(HabitsController.object)
        
        HabitsController.programs = self.filterUndergoingAndCompletePrograms(HabitsController.serverPrograms)
        self.programsTableView.reloadData()
        
        objc_sync_exit(HabitsController.object)
        
        self.showState()
        
        if let refr = self.refresher {
            refr.endRefreshing()
        }
    }

    private func showState() {
        objc_sync_enter(HabitsController.object)
        
        var showState = ShowState.Content
        
        if HabitsController.programs.count > 0 {
            showState = ShowState.Content
        } else {
            showState = ShowState.NoSignup
            
            if let user = UserCache().getUser() {
                if let userId = user["id"] as? NSString {
                    if NSUserDefaults.standardUserDefaults().boolForKey("\(userId as String)-signed-up-level") {
                        showState = ShowState.SignupNoContent
                    }
                }
            }
        }
        
        objc_sync_exit(HabitsController.object)
        
        switch showState {
        case ShowState.NoSignup:
            if let user = UserCache().getUser() {
                if let firstName = user["firstName"] as? NSString {
                    self.noSignupTitleLabel.text = "Welcome \(firstName)"
                } else {
                    self.noSignupTitleLabel.text = "Welcome"
                }
            }
            
            self.noSignupContentLabel.text = "It’s great to see you here! To get started, press the button below to find your first habit."
            
            self.signupButton.enabled = true
            self.signupButton.hidden = false
            self.noSignupView.hidden = false
            self.noSignupTitleLabel.hidden = false
            self.noSignupContentLabel.hidden = false
            self.signupArrow.hidden = false
        case ShowState.SignupNoContent:
            if let user = UserCache().getUser() {
                if let firstName = user["firstName"] as? NSString {
                    self.noSignupTitleLabel.text = "Hi \(firstName)"
                } else {
                    self.noSignupTitleLabel.text = "Hi"
                }
            }
            
            self.noSignupContentLabel.text = "Great to see you again! Find a habit to build by pressing the button below."
            self.noSignupContentLabel.sizeToFit()
            
            self.signupButton.enabled = true
            self.signupButton.hidden = false
            self.noSignupView.hidden = false
            self.noSignupTitleLabel.hidden = false
            self.noSignupContentLabel.hidden = false
            self.signupArrow.hidden = false
        case ShowState.Content:
            self.signupButton.enabled = false
            self.signupButton.hidden = true
            self.noSignupView.hidden = true
            self.noSignupTitleLabel.hidden = true
            self.noSignupContentLabel.hidden = true
            self.signupArrow.hidden = true
        }
    }

    func filterUndergoingAndCompletePrograms(programs: NSArray) -> NSArray {
        let undergoingPrograms: NSMutableArray = NSMutableArray()
        
        for program in programs {
            if let forfeited = program["forfeited"] as? NSNumber, quitted = program["quitted"] as? NSNumber, complete = program["complete"] as? NSNumber, continued = program["continued"] as? NSNumber {
                if forfeited == 0 && quitted == 0 && (complete == 0 || continued == 0) {
                    undergoingPrograms.addObject(program)
                }
            }
        }
        
        return undergoingPrograms
    }
    
    @IBAction func clickedPlus(sender: AnyObject) {
        self.performSegueWithIdentifier("showChooseAHabitFromHabits", sender: self)
    }
    
    // MARK: Unwind segues
    
    @IBAction func habitsControllerCreatedHabit(segue: UIStoryboardSegue) {
        refreshFromServer()
    }
    
    @IBAction func habitsControllerCreatedDateHabit(segue: UIStoryboardSegue) {
        refreshFromServer()
    }
    
    @IBAction func habitsControllerRemovedHabit(segue: UIStoryboardSegue) {
        refreshFromServer()
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return HabitsController.programs.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if let program = HabitsController.programs[indexPath.section] as? NSDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("habitsCell", forIndexPath: indexPath) as? HabitsCell {
                cell.populate(program, controller: self)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showCurrentHabitFromHabits") {
            if let viewController = segue.destinationViewController as? CurrentHabitController {
                if let program = sender as? NSDictionary {
                    viewController.program = program
                }
            }
        }
    }
}
