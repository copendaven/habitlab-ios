//
//  FollowingUnfollowedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowingUnfollowedCell: FollowingCell {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    var followed = false
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickFollowButton(sender: AnyObject) {
        if followed {
            return
        }
        
        if following != nil && controller != nil {
            if let user = following!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    self.followed = true
                    Serv.showSpinner()
                    
                    HabitlabRestAPI.postFollow(id as String, handler: { (error, response) -> () in
                        Serv.hideSpinner()
                        
                        if error != nil {
                            self.followed = false
                            Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                            return
                        }
                        
                        if let followingController = self.controller! as? FollowingController {
                            followingController.refresh()
                        }
                    })
                }
            }
        }
    }
    
    func populate(following: NSDictionary, controller: UIViewController) {
        super.populate(following, controller: controller, userName: userName, userImage: userImage)
        followed = false
    }
}
