//
//  ClarifyReconnectChooseFriendsEmptyCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ClarifyReconnectChooseFriendsEmptyCell: UITableViewCell {
    private var controller: UIViewController?
    private var nextToSelect: Bool?
    
    @IBOutlet weak var plusImage: UIImageView!
    
    func populate(controller: UIViewController, nextToSelect: Bool) {
        self.controller = controller
        self.nextToSelect = nextToSelect
        
        plusImage.layer.cornerRadius = 25
        
        if nextToSelect {
            plusImage.alpha = 1.00
        } else {
            plusImage.alpha = 0.75
        }
    }
    
    @IBAction func addFriend(sender: AnyObject) {
        if let realNextToSelect = self.nextToSelect {
            if realNextToSelect {
                if let viewController = self.controller as? ClarifyReconnectChooseFriendsController {
                    viewController.clickAddFriend()
                }
            }
        }
    }
}
