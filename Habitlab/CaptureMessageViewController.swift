//
//  CaptureMessageViewController.swift
//  Habitlab
//
//  Created by Vlad Manea on 1/19/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class CaptureMessageViewController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    var stepId: String?
    var programId: String?
    var image: UIImage?
    
    @IBOutlet weak var commentsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(CaptureMessageViewController)
        
        commentsTable.dataSource = self
        commentsTable.delegate = self
        
        commentsTable.estimatedRowHeight = 660
        commentsTable.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        self.refresh()
        self.segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(CaptureMessageViewController))
    }
    
    func refresh() {
        commentsTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        // Message table header.
        if stepId != nil && programId != nil && image != nil {
            if let cell = tableView.dequeueReusableCellWithIdentifier("messageCaptureComment", forIndexPath: indexPath) as? MessageCaptureComment {
                cell.populate(self, stepId: self.stepId!, programId: self.programId!, image: self.image!)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let commentCell = cell as? MessageCaptureComment {
            commentCell.registerForKeyboardNotifications()
        }
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let commentCell = cell as? MessageCaptureComment {
            commentCell.deregisterFromKeyboardNotifications()
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
