//
//  ClarifyReconnectChooseFriendsHeader.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ClarifyReconnectChooseFriendsHeader: UITableViewCell {
    private var controller: UIViewController?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func populate(controller: UIViewController, countWeeks: Int, countFriends: Int) {
        self.controller = controller
        
        self.titleLabel.text = "Choose \(countFriends) friend\(countFriends == 1 ? "" : "s")"
        self.descriptionLabel.text = "you would like to re-connect with\nwithin the next \(countWeeks) week\(countWeeks == 1 ? "" : "s")"
    }
}