//
//  PlanUnpluggingFoundFriendCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/7/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class PlanUnpluggingFoundFriendCell: UITableViewCell {
    private var controller: UIViewController?
    private var friend: NSDictionary?
    
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    
    func populate(controller: UIViewController, friend: NSDictionary, showSelected: Bool) {
        self.controller = controller
        self.friend = friend
        
        self.whiteView.hidden = !showSelected
        
        userPhoto.layer.cornerRadius = 0.5 * userPhoto.bounds.size.width
        
        if let firstName = friend["firstName"] as? NSString, lastName = friend["lastName"] as? NSString {
            self.userName.text = "\(firstName) \(lastName)"
        }
        
        if let photo = friend["facebookPhoto"] as? NSString {
            Serv.loadImage(photo as String, handler: { (error, data) in
                if error != nil {
                    print(error)
                }
                
                if data != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.userPhoto.image = UIImage(data: data!)
                    })
                }
            })
        }
    }
    
    @IBAction func selectedFriend(sender: AnyObject) {
        if let realFriend = self.friend {
            if let id = realFriend["id"] as? NSString {
                if let viewController = self.controller as? PlanUnpluggingController {
                    viewController.selectFriend(id as String)
                }
            }
        }
    }
}
