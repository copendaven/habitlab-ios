//
//  FollowersFollowingCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowersFollowingCell: FollowersCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var deletedUnfollow = false
    
    func populate(follower: NSDictionary, controller: UIViewController) {
        super.populate(follower, controller: controller, userName: userName, userImage: userImage)
        self.deletedUnfollow = false
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickUnfollowButton(sender: AnyObject) {
        if deletedUnfollow {
            return
        }
        
        if follower != nil && controller != nil {
            if let user = follower!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    let cancelRequestAlert = UIAlertController(title: "Unfollow", message: "By unfollowing, you will not get updates about your friend anymore.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    cancelRequestAlert.addAction(UIAlertAction(title: "Unfollow", style: .Default, handler: { (action: UIAlertAction!) in
                        self.deletedUnfollow = true
                        Serv.showSpinner()
                        
                        HabitlabRestAPI.deleteUnfollow(id as String, handler: { (error, followed) -> () in
                            Serv.hideSpinner()
                            
                            if error != nil {
                                self.deletedUnfollow = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                            
                            if let followersController = self.controller! as? FollowersController {
                                followersController.refresh()
                            }
                        })
                    }))
                
                    cancelRequestAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
                    self.controller!.presentViewController(cancelRequestAlert, animated: true, completion: nil)
                }
            }
        }
    }
}
