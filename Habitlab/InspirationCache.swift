//
//  InspirationCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/25/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData

class InspirationCache: NSObject {
    private static var object: NSObject = NSObject()
    private static var caches: NSMutableDictionary = NSMutableDictionary()
    
    private func cleanup() {
        if let calendar = Serv.calendar {
            if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                let stepIds = NSMutableArray()
                
                for (stepId, cacheObject) in InspirationCache.caches {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                stepIds.addObject(stepId)
                            }
                        }
                    }
                }
                
                for stepId in stepIds {
                    InspirationCache.caches.removeObjectForKey(stepId)
                }
            }
        }
    }
    
    static func erase() {
        objc_sync_enter(InspirationCache.object)
        InspirationCache.caches.removeAllObjects()
        objc_sync_exit(InspirationCache.object)
    }
    
    func setCache(stepId: String, cacheId: String, ownerId: String?, ownerFirstName: String?, ownerLastName: String?, ownerFacebookPhoto: String?, userSubjectId: String?, userSubjectFirstName: String?, userSubjectLastName: String?, userSubjectFacebookPhoto: String?, programStepObjectId: String?, programStepObjectCaptured: Int?, programObjectId: String?, programObjectDriveId: String?, programObjectDriveCategoryName: String?, programObjectDriveSubcategoryName: String?, programObjectDriveTitle: String?, programObjectDriveStep: String?, programObjectDriveLevel: Int?) {
        objc_sync_enter(InspirationCache.object)
        
        cleanup()
        
        let newInspiration = NSMutableDictionary()
        
        newInspiration.setObject(stepId, forKey: "stepId")
        newInspiration.setObject(cacheId, forKey: "cacheId")
        newInspiration.setObject("program-step-complete", forKey: "mobileNotificationType")
        newInspiration.setObject(NSDate(), forKey: "cachedAt")
        newInspiration.setObject(false, forKey: "dummyAttribute")
        
        // Set owner
        
        if ownerId != nil {
            newInspiration.setObject(ownerId!, forKey: "ownerId")
        }
        
        if ownerFirstName != nil {
            newInspiration.setObject(ownerFirstName!, forKey: "ownerFirstName")
        }
        
        if ownerLastName != nil {
            newInspiration.setObject(ownerLastName!, forKey: "ownerLastName")
        }
        
        if ownerFacebookPhoto != nil {
            newInspiration.setObject(ownerFacebookPhoto!, forKey: "ownerFacebookPhoto")
        }
        
        // Set user subject
        
        if userSubjectId != nil {
            newInspiration.setObject(userSubjectId!, forKey: "userSubjectId")
        }
        
        if userSubjectFirstName != nil {
            newInspiration.setObject(userSubjectFirstName!, forKey: "userSubjectFirstName")
        }
        
        if userSubjectLastName != nil {
            newInspiration.setObject(userSubjectLastName!, forKey: "userSubjectLastName")
        }
        
        if userSubjectFacebookPhoto != nil {
            newInspiration.setObject(userSubjectFacebookPhoto!, forKey: "userSubjectFacebookPhoto")
        }
        
        // Set program step object
        
        if programStepObjectId != nil {
            newInspiration.setObject(programStepObjectId!, forKey: "programStepObjectId")
        }
        
        if programStepObjectCaptured != nil {
            newInspiration.setObject(programStepObjectCaptured!, forKey: "programStepObjectCaptured")
        }
        
        // Set program object
        
        if programObjectId != nil {
            newInspiration.setObject(programObjectId!, forKey: "programObjectId")
        }
        
        if programObjectDriveId != nil {
            newInspiration.setObject(programObjectDriveId!, forKey: "programObjectDriveId")
        }
        
        if programObjectDriveLevel != nil {
            newInspiration.setObject(programObjectDriveLevel!, forKey: "programObjectDriveLevel")
        }
        
        if programObjectDriveCategoryName != nil {
            newInspiration.setObject(programObjectDriveCategoryName!, forKey: "programObjectDriveCategoryName")
        }
        
        if programObjectDriveSubcategoryName != nil {
            newInspiration.setObject(programObjectDriveSubcategoryName!, forKey: "programObjectDriveSubcategoryName")
        }
        
        if programObjectDriveTitle != nil {
            newInspiration.setObject(programObjectDriveTitle!, forKey: "programObjectDriveTitle")
        }
        
        if programObjectDriveStep != nil {
            newInspiration.setObject(programObjectDriveStep!, forKey: "programObjectDriveStep")
        }
        
        // Set a dummy attribute
        newInspiration.setObject("now", forKey: "humanTime")
        
        InspirationCache.caches.setObject(newInspiration, forKey: stepId)
        
        objc_sync_exit(InspirationCache.object)
    }
    
    func getCaches() -> NSArray {
        objc_sync_enter(InspirationCache.object)
        var array = [NSDictionary]()
        
        for (_, cacheObject) in InspirationCache.caches {
            if let cache = cacheObject as? NSDictionary {
                array.append(cache)
            }
        }
        
        objc_sync_exit(InspirationCache.object)
        return array
    }
    
    func getCache(stepId: String) -> NSDictionary? {
        objc_sync_enter(InspirationCache.object)
        
        if let cacheObject = InspirationCache.caches.objectForKey(stepId) {
            if let cache = cacheObject as? NSDictionary {
                objc_sync_exit(InspirationCache.object)
                return cache
            }
        }
        
        objc_sync_exit(InspirationCache.object)
        return nil
    }
}
