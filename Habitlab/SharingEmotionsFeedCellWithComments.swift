//
//  SharingEmotionsFeedCellWithComments.swift
//  Habitlab
//
//  Created by Vlad Manea on 26/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class SharingEmotionsFeedCellWithComments: FeedCell {
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellMessage: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var firstComment: UILabel!
    @IBOutlet weak var missingComments: UILabel!
    @IBOutlet weak var secondToLastComment: UILabel!
    @IBOutlet weak var lastComment: UILabel!
    
    @IBOutlet weak var firstCommentTop: NSLayoutConstraint!
    @IBOutlet weak var firstCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var missingCommentTop: NSLayoutConstraint!
    @IBOutlet weak var missingCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var lastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var lastCommentBottom: NSLayoutConstraint!
    
    @IBOutlet weak var commentsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var oneZero: UIImageView!
    @IBOutlet weak var twoZero: UIImageView!
    @IBOutlet weak var twoOne: UIImageView!
    @IBOutlet weak var threeZeroFourZero: UIImageView!
    @IBOutlet weak var threeOneFourOne: UIImageView!
    @IBOutlet weak var threeTwo: UIImageView!
    @IBOutlet weak var fourZero: UIImageView!
    @IBOutlet weak var fourOne: UIImageView!
    
    @IBAction override func clickMore(sender: AnyObject) {
        super.clickMore(sender)
    }
    
    @IBAction override func comment(sender: AnyObject) {
        super.comment(sender)
    }
    
    @IBAction override func like(sender: AnyObject) {
        super.like(sender)
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    private func getCommenting() -> NSDictionary {
        let commenting = NSMutableDictionary()
        
        commenting.setObject(firstComment, forKey: "firstComment")
        commenting.setObject(missingComments, forKey: "missingComments")
        commenting.setObject(secondToLastComment, forKey: "secondToLastComment")
        commenting.setObject(lastComment, forKey: "lastComment")
        
        commenting.setObject(firstCommentTop, forKey: "firstCommentTop")
        commenting.setObject(firstCommentBottom, forKey: "firstCommentBottom")
        commenting.setObject(missingCommentTop, forKey: "missingCommentTop")
        commenting.setObject(missingCommentBottom, forKey: "missingCommentBottom")
        commenting.setObject(secondToLastCommentTop, forKey: "secondToLastCommentTop")
        commenting.setObject(secondToLastCommentBottom, forKey: "secondToLastCommentBottom")
        commenting.setObject(lastCommentTop, forKey: "lastCommentTop")
        commenting.setObject(lastCommentBottom, forKey: "lastCommentBottom")
        
        commenting.setObject(commentsView, forKey: "commentsView")
        commenting.setObject(commentsViewHeight, forKey: "commentsViewHeight")
        
        return commenting
    }
    
    func setLiking() {
        if let note = super.notification {
            super.setLiking(note, likingButton: self.likeButton, showLikeButton: true)
        }
    }
    
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, showButtons: Bool) {
        let commenting = getCommenting()
        
        super.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, commentingButton: commentButton, commenting: commenting, likingButton: likeButton, optionsButton: optionsButton, showButtons: showButtons)
        
        dispatch_async(dispatch_get_main_queue()) {
            if let emotions = notification["emotions"] as? NSArray {
                switch emotions.count {
                case 1:
                    if let emotions0 = emotions[0] as? NSString {
                        if let emotion0 = EmotionType(rawValue: emotions0 as String) {
                            self.oneZero.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion0)
                        }
                    }
                    
                    self.oneZero.hidden = false
                    self.twoZero.hidden = true
                    self.twoOne.hidden = true
                    self.threeZeroFourZero.hidden = true
                    self.threeOneFourOne.hidden = true
                    self.threeTwo.hidden = true
                    self.fourZero.hidden = true
                    self.fourOne.hidden = true
                case 2:
                    if let emotions0 = emotions[0] as? NSString {
                        if let emotion0 = EmotionType(rawValue: emotions0 as String) {
                            self.twoZero.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion0)
                        }
                    }
                    
                    if let emotions1 = emotions[1] as? NSString {
                        if let emotion1 = EmotionType(rawValue: emotions1 as String) {
                            self.twoOne.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion1)
                        }
                    }
                    
                    self.oneZero.hidden = true
                    self.twoZero.hidden = false
                    self.twoOne.hidden = false
                    self.threeZeroFourZero.hidden = true
                    self.threeOneFourOne.hidden = true
                    self.threeTwo.hidden = true
                    self.fourZero.hidden = true
                    self.fourOne.hidden = true
                case 3:
                    if let emotions0 = emotions[0] as? NSString {
                        if let emotion0 = EmotionType(rawValue: emotions0 as String) {
                            self.threeZeroFourZero.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion0)
                        }
                    }
                    
                    if let emotions1 = emotions[1] as? NSString {
                        if let emotion1 = EmotionType(rawValue: emotions1 as String) {
                            self.threeOneFourOne.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion1)
                        }
                    }
                    
                    if let emotions2 = emotions[2] as? NSString {
                        if let emotion2 = EmotionType(rawValue: emotions2 as String) {
                            self.threeTwo.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion2)
                        }
                    }
                    
                    self.oneZero.hidden = true
                    self.twoZero.hidden = true
                    self.twoOne.hidden = true
                    self.threeZeroFourZero.hidden = false
                    self.threeOneFourOne.hidden = false
                    self.threeTwo.hidden = false
                    self.fourZero.hidden = true
                    self.fourOne.hidden = true
                case 4:
                    if let emotions0 = emotions[0] as? NSString {
                        if let emotion0 = EmotionType(rawValue: emotions0 as String) {
                            self.threeZeroFourZero.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion0)
                        }
                    }
                    
                    if let emotions1 = emotions[1] as? NSString {
                        if let emotion1 = EmotionType(rawValue: emotions1 as String) {
                            self.threeOneFourOne.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion1)
                        }
                    }
                    
                    if let emotions2 = emotions[2] as? NSString {
                        if let emotion2 = EmotionType(rawValue: emotions2 as String) {
                            self.fourZero.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion2)
                        }
                    }
                    
                    if let emotions3 = emotions[3] as? NSString {
                        if let emotion3 = EmotionType(rawValue: emotions3 as String) {
                            self.fourOne.image = SharingEmotionsChooseEmotionsController.getFeedImage(emotion3)
                        }
                    }
                    
                    self.oneZero.hidden = true
                    self.twoZero.hidden = true
                    self.twoOne.hidden = true
                    self.threeZeroFourZero.hidden = false
                    self.threeOneFourOne.hidden = false
                    self.threeTwo.hidden = true
                    self.fourZero.hidden = false
                    self.fourOne.hidden = false
                default:
                    self.oneZero.hidden = true
                    self.twoZero.hidden = true
                    self.twoOne.hidden = true
                    self.threeZeroFourZero.hidden = true
                    self.threeOneFourOne.hidden = true
                    self.threeTwo.hidden = true
                    self.fourZero.hidden = true
                    self.fourOne.hidden = true
                }
            }
        }
    }
}
