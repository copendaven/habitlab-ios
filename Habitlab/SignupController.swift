//
//  ViewController.swift
//  Habitlab
//
//  Created by Vlad Manea on 06/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Analytics
import Google
import AccountKit
import MobileCoreServices

class SignupController: UIViewController, UIScrollViewDelegate, AKFViewControllerDelegate {
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var emailSignupButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var showcaseSwipe: UIView!
    
    @IBOutlet weak var showcaseSwipeScrollView: UIScrollView!
    @IBOutlet weak var showcaseSwipePageControl: UIPageControl!
    @IBOutlet weak var showcaseSwipeSignupButton: UIButton!
    
    private var sawShowcaseSwipe = false
    
    var facebookPermissions = ["public_profile", "email", "user_friends"];
    let accountKit = AKFAccountKit(responseType: AKFResponseType.AccessToken)
    var pendingLoginViewController: AKFViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        if let realPendingLoginViewController = accountKit.viewControllerForLoginResume() as? AKFViewController {
            pendingLoginViewController = realPendingLoginViewController
        }
    }
    
    @IBAction func clickedTerms(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "https://www.habitlab.me/terms/HabitlabTerms.pdf")!)
    }
    
    @IBAction func clickedShowcaseSwipeSignup(sender: AnyObject) {
        self.hideShowcaseSwipe()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showcaseSwipeSignupButton.layer.cornerRadius = 0.5 * self.showcaseSwipeSignupButton.bounds.size.height
        self.showcaseSwipeScrollView.delegate = self
        
        self.hideSigninSignupButtons()
        
        if let _ = UserCache().getUser() {
            let authenticationMethod = UserCache().getAuthenticationMethod()
            authenticate(authenticationMethod)
            return
        }
        
        if !self.sawShowcaseSwipe {
            self.sawShowcaseSwipe = true
            self.initializeShowcaseSwipe()
            self.showShowcaseSwipe()
        }
        
        self.showSigninSignupButtons()
    }
    
    private func initializeShowcaseSwipe() {
        let screenSize = UIScreen.mainScreen().bounds.size
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        showcaseSwipeScrollView.contentSize = CGSizeMake(screenWidth, screenHeight)
        
        for i in 1...5 {
            let image = UIImage(named: "HBL_TumblrScreens_\(i)")
            let imageView = UIImageView(image: image)
            imageView.contentMode = UIViewContentMode.ScaleAspectFill
            imageView.frame = CGRectMake(CGFloat(i - 1) * screenWidth, 0, screenWidth, screenHeight)
            showcaseSwipeScrollView.addSubview(imageView)
        }
        
        showcaseSwipeScrollView.contentSize = CGSizeMake(screenWidth * 5, screenHeight)
        showcaseSwipePageControl.addTarget(self, action: #selector(ShowcaseSwipeController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func changePage(sender: AnyObject) {
        let offsetX = CGFloat(showcaseSwipePageControl.currentPage) * showcaseSwipeScrollView.frame.size.width
        showcaseSwipeScrollView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        showcaseSwipePageControl.currentPage = Int(pageNumber)
    }
    
    func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AKFAccessToken!, state: String!) {
        self.authenticateHabitlab(AuthenticationMethod.AccountKit, token: accessToken.tokenString, handler: self.authenticationHandler)
    }
    
    func viewController(viewController: UIViewController!, didFailWithError error: NSError!) {
        print(error)
    }
    
    private func showShowcaseSwipe() {
        self.showcaseSwipe.hidden = false
    }
    
    private func hideShowcaseSwipe() {
        self.showcaseSwipe.hidden = true
    }
    
    // Facebook Delegate Methods
    
    @IBAction func signupFacebook(sender: AnyObject) {
        authenticate(AuthenticationMethod.Facebook)
    }
    
    @IBAction func signinFacebook(sender: AnyObject) {
        authenticate(AuthenticationMethod.Facebook)
    }
    
    @IBAction func signupWithEmailButtonPressed() {
        self.authenticateEmail()
    }
    
    func hideSigninSignupButtons() {
        signinButton.hidden = true
        signupButton.hidden = true
        termsButton.hidden = true
        emailSignupButton.hidden = true
    }
    
    func showSigninSignupButtons() {
        signinButton.hidden = false
        signupButton.hidden = false
        termsButton.hidden = false
        emailSignupButton.hidden = false
    }
    
    func authenticateEmail() {
        let inputState = NSUUID().UUIDString
        
        if let viewController = accountKit.viewControllerForEmailLoginWithEmail(nil, state: inputState) as? AKFViewController {
            viewController.delegate = self
            
            if let uiViewController = viewController as? UIViewController {
                self.presentViewController(uiViewController, animated: true, completion: nil)
            }
        }
    }
    
    func authenticate(method: AuthenticationMethod) {
        switch method {
        case .AccountKit:
            if let token = accountKit.currentAccessToken {
                self.authenticateHabitlab(method, token: token.tokenString, handler: self.authenticationHandler)
                return
            }
            
            if let realPendingLoginViewController = pendingLoginViewController {
                realPendingLoginViewController.delegate = self
                
                if let uiRealPendingLoginViewController = realPendingLoginViewController as? UIViewController {
                    self.presentViewController(uiRealPendingLoginViewController, animated: true, completion: nil)
                }
            }
            
        case .Facebook:
            if let token = FBSDKAccessToken.currentAccessToken() {
                self.authenticateHabitlab(method, token: token.tokenString, handler: self.authenticationHandler)
                return
            }
            
            Serv.showSpinner()
            
            let loginManager = FBSDKLoginManager()
            
            loginManager.logInWithReadPermissions(facebookPermissions, fromViewController: self, handler: { (result, error) -> Void in
                Serv.hideSpinner()
                
                if error != nil {
                    self.showSigninSignupButtons()
                    Serv.showErrorPopup("Facebook login error", message: "Facebook could not log you in. Try again.", okMessage: "Okay, got it.", controller: self, handler: nil)
                    return
                }
                
                if let token = FBSDKAccessToken.currentAccessToken() {
                    self.authenticateHabitlab(method, token: token.tokenString, handler: self.authenticationHandler)
                }
            })
        }
    }
    
    func authenticateHabitlab(method: AuthenticationMethod, token: String?, handler: () -> ()) {
        if let realToken = token {
            Serv.showSpinner()
        
            HabitlabAuthentication.authenticate(realToken, method: method, handler: { (error, user) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    self.showSigninSignupButtons()
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    
                    HabitlabAuthentication.deauthenticate({ (success) -> () in
                        FBSDKLoginManager().logOut()
                        HabitlabAuthentication.habitlabAccessToken = nil
                        UserCache().setUser(nil)
                        
                        let gai = GAI.sharedInstance()
                        let tracker = gai.defaultTracker
                        tracker.allowIDFACollection = true
                        let builder = GAIDictionaryBuilder.createScreenView()
                        builder.set("end", forKey: kGAISessionControl)
                        tracker.set(kGAIScreenName, value: String(SignupController))
                        tracker.send(builder.build() as [NSObject: AnyObject])
                    })
                    
                    return
                }
                
                if user != nil {
                    UserCache().setUser(user!)
                    self.registerAuthenticatedUserForAnalytics(handler)
                }
            })
        } else {
            self.showSigninSignupButtons()
            Serv.showErrorPopup("Facebook login error", message: "Facebook did not give us access. Try again.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    func registerAuthenticatedUserForAnalytics(handler: () -> ()) {
        if let user = UserCache().getUser() {
            if let userId = user["id"] as? NSString {
                
                // SEGMENT
                
                var traits: Dictionary<String, AnyObject> = Dictionary()
                traits["id"] = userId as String
                
                if let firstName = user["firstName"] as? NSString {
                    traits["firstName"] = firstName as String
                }
                
                if let lastName = user["lastName"] as? NSString {
                    traits["lastName"] = lastName as String
                }
                
                if let facebookId = user["facebookId"] as? NSString {
                    traits["facebookId"] = facebookId as String
                }
                
                if let facebookTokenForBusiness = user["facebookTokenForBusiness"] as? NSString {
                    traits["facebookTokenForBusiness"] = facebookTokenForBusiness as String
                }
                
                if let facebookEmail = user["facebookEmail"] as? NSString {
                    traits["email"] = facebookEmail as String
                }
                
                if let createdAt = user["createdAt"] as? NSString {
                    traits["createdAt"] = createdAt as String
                }
                
                if let facebookPhoto = user["facebookPhoto"] as? NSString {
                    traits["avatar"] = facebookPhoto as String
                }
                
                SEGAnalytics.sharedAnalytics().identify(userId as String, traits: traits)
                
                let gai = GAI.sharedInstance()
                let tracker = gai.defaultTracker
                tracker.allowIDFACollection = true
                let builder = GAIDictionaryBuilder.createScreenView()
                builder.set("start", forKey: kGAISessionControl)
                tracker.set(kGAIScreenName, value: String(SignupController))
                tracker.set(kGAIUserId, value: userId as String)
                tracker.send(builder.build() as [NSObject: AnyObject])
                
                handler()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }

    func authenticationHandler() {
        Serv.showSpinner()
        
        HabitlabRestAPI.getActiveProgramsByUser({ (error, programsJson) -> () in
            Serv.hideSpinner()
            
            // An ugly hack to redirect Habits screen to the CTA screen of the only existing program, if any.
            if programsJson != nil {
                if let programsArray = programsJson as? NSArray {
                    if programsArray.count == 1 {
                        if let program = programsArray[0] as? NSDictionary {
                            objc_sync_enter(HabitsController.singleProgramLock)
                            HabitsController.singleProgram = program
                            objc_sync_exit(HabitsController.singleProgramLock)
                        }
                    }
                }
            }
            
            self.performSegueWithIdentifier("showMainTabsFromSignup", sender: self)
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showMainTabsFromSignup") {
            if let viewController = segue.destinationViewController as? MainTabsController {
                viewController.askPushNotificationPermission()
            }
        }
        
        if (segue.identifier == "showShowcaseSwipeFromSignup") {
            if let _ = segue.destinationViewController as? ShowcaseSwipeController {
                self.showSigninSignupButtons()
            }
        }
    }
    
    @IBAction func signupControllerPrepareForLogout(segue: UIStoryboardSegue){
        
    }
}