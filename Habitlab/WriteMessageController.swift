//
//  WriteMessageController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class WriteMessageController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    var notification: NSDictionary?
    var comments = []
    var image: String?
    var likes: NSArray?
    
    var isOpenedFromPushNotification: Bool = false
    
    @IBOutlet weak var commentsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(WriteMessageController)
        
        commentsTable.dataSource = self
        commentsTable.delegate = self
        commentsTable.estimatedRowHeight = 60
        commentsTable.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        self.refresh()
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realNotification = self.notification {
            if let type = realNotification["mobileNotificationType"] as? NSString {
                dict["mobileNotificationType"] = type as String
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(WriteMessageController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(WriteMessageController))
        }
    }
    
    func filterComments(comments: NSArray) -> NSArray {
        let filteredComments: NSMutableArray = NSMutableArray()
        
        for commentJson in comments {
            if let comment = commentJson as? NSDictionary {
                filteredComments.addObject(comment)
            }
        }
        
        return filteredComments
    }
    
    func refresh() {
        if notification != nil {
            self.comments = []
            
            if let comments = notification!["comments"] as? NSArray {
                self.comments = self.filterComments(comments)
            }
            
            self.likes = nil
            
            if let likes = notification!["likes"] as? NSArray {
                if likes.count > 0 {
                    self.likes = likes
                }
            }
            
            var theImage: String? = nil
            
            if let stepId = notification!["programStepObjectId"] as? NSString, captured = notification!["programStepObjectCaptured"] as? NSNumber {
                if captured == 1 {
                    theImage = "programstep\(stepId as String)"
                }
            }
            
            if let _ = notification!["thinkingAboutYouState"] as? NSString {
                theImage = nil
            }
            
            if let _ = notification!["emotions"] as? NSArray {
                theImage = nil
            }
            
            self.image = theImage
            self.commentsTable.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func showLikers() {
        if let realNotification = self.notification {
            self.performSegueWithIdentifier("showLikersFromWriteMessage", sender: realNotification)
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count + 2 + (likes != nil ? 1 : 0) + (image != nil ? 1 : 0)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if self.notification == nil {
            return defaultCell
        }
        
        // Message table header.
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("messageTableHeader", forIndexPath: indexPath) as? MessageTableHeader {
                cell.populate(notification!, aboutMe: false)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        // Message table image.
        if indexPath.row == 1 && image != nil {
            if let cell = tableView.dequeueReusableCellWithIdentifier("messageTableImageCell", forIndexPath: indexPath) as? MessageTableImageCell {
                cell.populate(self.notification!)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        // Message table likes.
        if likes != nil && (indexPath.row == 2 && image != nil || indexPath.row == 1 && image == nil) {
            if let cell = tableView.dequeueReusableCellWithIdentifier("messageTableLikesCell", forIndexPath: indexPath) as? MessageTableLikesCell {
                cell.populate(self, likes: self.likes!)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        // Message table textfield.
        if indexPath.row == comments.count + 1 + (likes != nil ? 1 : 0) + (image != nil ? 1 : 0) {
            if let cell = tableView.dequeueReusableCellWithIdentifier("messageTableComment", forIndexPath: indexPath) as? MessageTableComment {
                cell.populate(self, notification: self.notification!)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        // Message table comment.
        if let comment = comments[indexPath.row - 1 - (likes != nil ? 1 : 0) - (image != nil ? 1 : 0)] as? NSDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("messageTableCell", forIndexPath: indexPath) as? MessageTableCell {
                cell.populate(comment)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let commentCell = cell as? MessageTableComment {
            commentCell.registerForKeyboardNotifications()
        }
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let commentCell = cell as? MessageTableComment {
            commentCell.deregisterFromKeyboardNotifications()
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showFeedFromWriteMessage" {
            if let feedController = segue.destinationViewController as? NewFeedController, dict = sender as? NSDictionary {
                if let mobileNotificationId = dict["mobileNotificationId"] as? NSString {
                    feedController.cacheCommentedMobileNotificationId = mobileNotificationId as String
                }
                
                if let mobileNotificationCacheId = dict["mobileNotificationCacheId"] as? NSString {
                    feedController.cacheCommentedMobileNotificationCacheId = mobileNotificationCacheId as String
                }
            }
        }
        
        if segue.identifier == "showLikersFromWriteMessage" {
            if let likersController = segue.destinationViewController as? LikersController, dict = sender as? NSDictionary {
                if let mobileNotificationBaseId = dict["mobileNotificationBase"] as? NSString {
                    likersController.mobileNotificationBaseId = mobileNotificationBaseId as String
                }
            }
        }
    }
}