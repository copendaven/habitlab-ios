//
//  FollowingYourselfCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowingYourselfCell: FollowingCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    func populate(following: NSDictionary, controller: UIViewController) {
        super.populate(following, controller: controller, userName: userName, userImage: userImage)
    }
}
