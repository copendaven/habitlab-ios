//
//  ClarifyReconnectChooseMessageController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Contacts
import Analytics
import Analytics

class ClarifyReconnectChooseMessageController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    private static var object = NSObject()
    
    @IBOutlet weak var templatesTableView: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    var serverTemplates = []
    var templates = []
    
    var selectedTemplate: NSDictionary?
    var contact: CNContact?
    var color: UIColor?
    var drive: NSDictionary?
    
    var refresher: UIRefreshControl?
    
    var downloadedCount = 0
    var downloadedWindow = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ClarifyReconnectChooseMessageController)
        
        templatesTableView.estimatedRowHeight = templatesTableView.rowHeight
        templatesTableView.rowHeight = UITableViewAutomaticDimension
        
        templatesTableView.delegate = self
        templatesTableView.dataSource = self
        self.shyNavBarManager.scrollView = templatesTableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ClarifyReconnectChooseMessageController.refreshPullToRefresh), forControlEvents: UIControlEvents.ValueChanged)
        templatesTableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        templatesTableView.infiniteScrollIndicatorMargin = 40
        
        templatesTableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            self.refreshFromServer(true)
        }
        
        if let realDrive = self.drive {
            if let title = realDrive["title"] as? NSString, level = realDrive["level"] as? NSNumber {
                self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
            }
        }
        
        objc_sync_enter(ClarifyReconnectChooseMessageController.object)
        
        if self.serverTemplates.count <= 0 {
            objc_sync_exit(ClarifyReconnectChooseMessageController.object)
            self.refreshFromServer(false)
        } else {
            objc_sync_exit(ClarifyReconnectChooseMessageController.object)
            self.refreshFromClient(false)
        }
        
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realDrive = self.drive {
            if let categ = realDrive["driveCategory"] as? NSString {
                dict["category"] = categ as String
            }
            
            if let subcateg = realDrive["driveSubcategory"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
            
            if let level = realDrive["level"] as? NSNumber {
                dict["level"] = String(level)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseMessageController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseMessageController))
        }
    }
    
    func refreshPullToRefresh() {
        self.refreshFromServer(false)
    }
    
    @IBAction func clickedDone(sender: AnyObject) {
        self.performSegueWithIdentifier("showClarifyReconnectChooseFriendsFromClarifyReconnectChooseMessage", sender: self)
    }
    
    func refreshFromClient(isInfiniteScroll: Bool) {
        objc_sync_enter(ClarifyReconnectChooseMessageController.object)
        
        self.templates = NSArray(array: self.serverTemplates)
        self.templatesTableView.reloadData()
        
        if isInfiniteScroll {
            self.templatesTableView.finishInfiniteScroll()
        }
        
        objc_sync_exit(ClarifyReconnectChooseMessageController.object)
        
        if let refr = self.refresher {
            refr.endRefreshing()
        }
    }
    
    func refreshFromServer(isInfiniteScroll: Bool) {
        if (!isInfiniteScroll) {
            objc_sync_enter(ClarifyReconnectChooseMessageController.object)
            downloadedCount = 0
            objc_sync_exit(ClarifyReconnectChooseMessageController.object)
        }
        
        if (contact == nil) {
            return
        }
        
        objc_sync_enter(ClarifyReconnectChooseMessageController.object)
        let skip = downloadedCount
        let limit = downloadedWindow
        objc_sync_exit(ClarifyReconnectChooseMessageController.object)
        
        Serv.showSpinner()
        
        HabitlabRestAPI.postReconnectReasonTemplates(contact!.givenName, skip: skip, limit: limit, handler: { (error, templatesJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let templatesArray = templatesJson as? NSArray {
                objc_sync_enter(ClarifyReconnectChooseMessageController.object)
                
                if !isInfiniteScroll {
                    self.serverTemplates = templatesArray
                } else {
                    var set = Set<String>()
                    let array = NSMutableArray()
                    
                    for templateJson in self.serverTemplates {
                        if let template = templateJson as? NSDictionary {
                            if let id = template["id"] as? NSString {
                                if !set.contains(id as String) {
                                    set.insert(id as String)
                                    array.addObject(templateJson)
                                }
                            }
                        }
                    }
                    
                    for templateJson in templatesArray {
                        if let template = templateJson as? NSDictionary {
                            if let id = template["id"] as? NSString {
                                if !set.contains(id as String) {
                                    set.insert(id as String)
                                    array.addObject(templateJson)
                                }
                            }
                        }
                    }
                    
                    self.serverTemplates = array
                }
                
                self.downloadedCount = self.serverTemplates.count
                
                objc_sync_exit(ClarifyReconnectChooseMessageController.object)
                
                self.refreshFromClient(isInfiniteScroll)
            }
        })
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showClarifyReconnectChooseFriendsFromClarifyReconnectChooseMessage") {
            if let viewController = segue.destinationViewController as? ClarifyReconnectChooseFriendsController {
                if let realSelectedtemplate = self.selectedTemplate, realContact = self.contact, realColor = self.color {
                    if let reason = realSelectedtemplate["reason"] as? NSString {
                        viewController.addFriend(realContact, message: reason as String, color: realColor)
                    }
                }
            }
        }
    }
    
    func setSelected(template: NSDictionary) {
        self.selectedTemplate = template
        templatesTableView.reloadData()
        self.saveButton.alpha = 1.0
        self.saveButton.enabled = true
        self.saveButton.backgroundColor = UIColor(red: 0.21, green: 0.84, blue: 0.59, alpha: 1.0)
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return templates.count + 1
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("clarifyReconnectChooseMessageHeaderCell", forIndexPath: indexPath) as? ClarifyReconnectChooseMessageHeaderCell, realContact = self.contact, realColor = self.color {
                cell.populate(realContact, contactPhotoBackgroundColor: realColor)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
            
            return defaultCell
        }
        
        objc_sync_enter(ClarifyReconnectChooseMessageController.object)
        
        if templates.count <= indexPath.section {
            objc_sync_exit(ClarifyReconnectChooseMessageController.object)
            
            if let cell = tableView.dequeueReusableCellWithIdentifier("priorFeedCell", forIndexPath: indexPath) as? PriorFeedCell {
                cell.populate(self)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
            
            return defaultCell
        }
        
        if let template = templates[indexPath.section - 1] as? NSMutableDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("clarifyReconnectChooseMessageTemplateCell", forIndexPath: indexPath) as? ClarifyReconnectChooseMessageTemplateCell {
                
                var selected = false
                
                if let thisTemplateId = template["id"] as? NSString {
                    if selectedTemplate != nil {
                        if let selectedTemplateId = selectedTemplate!["id"] as? NSString {
                            if (thisTemplateId as String).compare(selectedTemplateId as String) == NSComparisonResult.OrderedSame {
                                selected = true
                            }
                        }
                    }
                }
                
                cell.populate(self, template: template, selected: selected)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                objc_sync_exit(ClarifyReconnectChooseMessageController.object)
                return cell
            }
        }
        
        objc_sync_exit(ClarifyReconnectChooseMessageController.object)
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    
}
