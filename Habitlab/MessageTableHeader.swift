//
//  MessageTableHeader.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/4/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class MessageTableHeader: UITableViewCell {
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet weak var notificationTime: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!

    func populate(notification: NSDictionary, aboutMe: Bool) {
        userPhoto.layer.cornerRadius = userPhoto.frame.size.width / 2
        userPhoto.clipsToBounds = true
        
        if let humanTime = notification["humanTime"] as? NSString {
            notificationTime.text = humanTime as String
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            Serv.setNotificationMessage(notification, label: self.notificationMessage)
        }
        
        if let userSubjectFacebookPhoto = notification["userSubjectFacebookPhoto"] as? NSString {
            Serv.loadImage(userSubjectFacebookPhoto as String, handler: { (error, data) -> () in
                if error != nil {
                    print(error)
                }
                
                if data != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.userPhoto.image = UIImage(data: data!)
                    })
                }
            })
        }
    }
}
