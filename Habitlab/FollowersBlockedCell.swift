//
//  FollowersBlockedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowersBlockedCell: FollowersCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var followUnapproved = false
    
    func populate(follower: NSDictionary, controller: UIViewController) {
        super.populate(follower, controller: controller, userName: userName, userImage: userImage)
        followUnapproved = false
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickUnapproveButton(sender: AnyObject) {
        if self.followUnapproved {
            return
        }
        
        if follower != nil && controller != nil {
            if let user = follower!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    let cancelRequestAlert = UIAlertController(title: "Unapprove follow", message: "By unapproving the follow, your friend will not follow you anymore.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    cancelRequestAlert.addAction(UIAlertAction(title: "Unapprove", style: .Default, handler: { (action: UIAlertAction!) in
                        self.followUnapproved = true
                        Serv.showSpinner()
                        
                        HabitlabRestAPI.putFollowUnapprove(id as String, handler: { (error, followed) -> () in
                            Serv.hideSpinner()
                            
                            if error != nil {
                                self.followUnapproved = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                            
                            if let followersController = self.controller! as? FollowersController {
                                followersController.refresh()
                            }
                        })
                    }))
                    
                    cancelRequestAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
                    self.controller!.presentViewController(cancelRequestAlert, animated: true, completion: nil)
                }
            }
        }
    }
}
