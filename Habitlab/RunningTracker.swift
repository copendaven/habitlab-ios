//
//  RunningTracker.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/18/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreLocation

class RunningTracker: NSObject, CLLocationManagerDelegate {
    static var runningTracker: RunningTracker?
    
    private var locationManager: CLLocationManager = CLLocationManager()
    private var step: NSDictionary?
    private var program: NSDictionary?
    private var controller: UIViewController?

    init(program: NSDictionary, step: NSDictionary, controller: UIViewController) {
        super.init()
        
        self.step = step
        self.program = program
        self.controller = controller
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.activityType = CLActivityType.Fitness
        self.locationManager.distanceFilter = 10.0
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    static func stop() {
        if let tracker = RunningTracker.runningTracker {
            tracker.locationManager.stopUpdatingLocation()
        }
    }
    
    static func start() {
        if let tracker = RunningTracker.runningTracker {
            tracker.locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        print("Update")
        
        var delta = newLocation.distanceFromLocation(oldLocation)
        delta = max(delta, 0.0)
        delta = min(delta, 20.0)
        
        // TODO: Remove this
        // if environment == Environment.Development || environment == Environment.Staging {
        //     delta += 1000
        // }
        
        if let viewController = self.controller as? RunningModuleController {
            viewController.addDelta(delta)
        }
    }
}
