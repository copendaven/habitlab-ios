//
//  ChooseNewHabitCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class ChooseNewHabitCell: UITableViewCell {
    var category: NSDictionary?
    var controller: UIViewController?
    
    @IBOutlet weak var habitPhoto: UIImageView!
    @IBOutlet weak var habitName: UILabel!
    
    @IBAction func clickedNext(sender: AnyObject) {
        if let habitController = controller as? ChooseNewHabitController {
            if let realCategory = category {
                habitController.performSegueWithIdentifier("showChooseNewProgramFromChooseNewHabit", sender: realCategory)
            }
        }
    }
    
    func populate(category: NSDictionary, controller: UIViewController) {
        self.category = category
        self.controller = controller
        
        if let name = category["name"] as? NSString {
            habitName.text = name as String
        }
        
        if let str = category["str"] as? NSString {
            if let image = UIImage(named: str as String) {
                habitPhoto.image = image
            }
        }
    }
}
