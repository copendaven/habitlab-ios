//
//  PublicProfileController.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class PublicProfileController: GAITrackedViewController {
    var userId: String?
    var isOpenedFromPushNotification: Bool = false
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var countHabits: UIButton!
    @IBOutlet weak var countFollowers: UIButton!
    @IBOutlet weak var countFollowing: UIButton!
    
    @IBOutlet weak var publicProfileCaption: UINavigationItem!
    
    var followBlocked = false
    var followUnblocked = false
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        refresh()
        segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(PublicProfileController))
    }
    
    func clickBlock() {
        if followBlocked {
            return
        }
        
        if userId == nil {
            return
        }
        
        let blockAlert = UIAlertController(title: "Block", message: "By blocking, your friend cannot follow you anymore.", preferredStyle: UIAlertControllerStyle.Alert)
        
        blockAlert.addAction(UIAlertAction(title: "Block", style: .Default, handler: { (action: UIAlertAction!) in
            self.followBlocked = true
            Serv.showSpinner()
            
            HabitlabRestAPI.putFollowBlock(self.userId!, handler: { (error, user) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    self.followBlocked = false
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                self.refresh()
            })
        }))
        
        blockAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
        
        self.presentViewController(blockAlert, animated: true, completion: nil)
    }
    
    func clickUnblock() {
        if followUnblocked {
            return
        }
        
        if userId == nil {
            return
        }
        
        let unblockAlert = UIAlertController(title: "Unblock", message: "By unblocking, your friend can follow you again.", preferredStyle: UIAlertControllerStyle.Alert)
        
        unblockAlert.addAction(UIAlertAction(title: "Unblock", style: .Default, handler: { (action: UIAlertAction!) in
            self.followUnblocked = true
            Serv.showSpinner()
            
            HabitlabRestAPI.putFollowUnblock(self.userId!, handler: { (error, user) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    self.followUnblocked = false
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                self.refresh()
            })
        }))
    
        unblockAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
        
        self.presentViewController(unblockAlert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(PublicProfileController)
        
        self.userPhoto.layer.cornerRadius = self.userPhoto.frame.size.width / 2
        self.userPhoto.layer.borderWidth = 3.0
        self.userPhoto.layer.borderColor = UIColor.whiteColor().CGColor
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        followBlocked = false
        followUnblocked = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func refresh() {
        if userId == nil {
            return
        }
        
        Serv.showSpinner()
        
        HabitlabRestAPI.getUser(userId!, handler: { (error, userJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let user = userJson as? NSDictionary {
                if let photo = user["facebookPhoto"] as? NSString {
                    Serv.loadImage(photo as String, handler: { (error, data) -> () in
                        if error != nil {
                            print(error)
                        }
                        
                        if data != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.userPhoto.image = UIImage(data: data!)
                            })
                        }
                    })
                }
                
                if let firstName = user["firstName"] as? NSString, lastName = user["lastName"] as? NSString {
                    self.userName.text = "\(firstName) \(lastName)"
                    // self.publicProfileCaption.title = "\(firstName.uppercaseString) \(lastName.uppercaseString)"
                }
                
                if let countFollowers = user["countFollowers"] as? NSNumber {
                    self.countFollowers.setTitle(String(countFollowers), forState: UIControlState.Normal)
                }
                
                if let countFollowing = user["countFollowing"] as? NSNumber {
                    self.countFollowing.setTitle(String(countFollowing), forState: UIControlState.Normal)
                }
                
                if let countHabits = user["countHabits"] as? NSNumber {
                    self.countHabits.setTitle(String(countHabits), forState: UIControlState.Normal)
                }
                
                self.navigationItem.rightBarButtonItem = nil
                
                if let followship = user["followship"] as? NSDictionary {
                    if let blocked = followship["blocked"] as? NSNumber {
                        if blocked == 1 {
                            var image = UIImage(named: "SmallCheckmarkRed")
                            image = image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(PublicProfileController.clickUnblock))
                        } else {
                            var image = UIImage(named: "SmallBlockedWhite")
                            image = image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target: self, action: #selector(PublicProfileController.clickBlock))
                        }
                    }
                }
            }
        })
    }
}
