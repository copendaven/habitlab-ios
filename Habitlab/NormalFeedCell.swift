//
//  NormalFeedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 26/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class NormalFeedCell: FeedCell {
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellMessage: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    
    @IBAction override func comment(sender: AnyObject) {
        super.comment(sender)
    }
    
    @IBAction override func clickMore(sender: AnyObject) {
        super.clickMore(sender)
    }
    
    @IBAction override func like(sender: AnyObject) {
        super.like(sender)
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    func setLiking() {
        if let note = super.notification {
            super.setLiking(note, likingButton: self.likeButton, showLikeButton: true)
        }
    }
    
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, showButtons: Bool) {
        super.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, commentingButton: commentButton, likingButton: likeButton, optionsButton: optionsButton, showButtons: showButtons)
    }
}
