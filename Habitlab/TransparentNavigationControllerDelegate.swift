//
//  TransparentNavigationControllerDelegate.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/13/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class TransparentNavigationControllerDelegate: NSObject, UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil // FadeInAnimator()
    }
}