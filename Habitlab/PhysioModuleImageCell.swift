//
//  PhysioModuleImageCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/17/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class PhysioModuleImageCell: UICollectionViewCell {
    private let serialQueue = dispatch_queue_create("me.habitlab.mobile.physioModuleController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    @IBOutlet weak var exerciseImageView: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    
    func populate(mediaItem: NSDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            self.progressView.hidden = true
            self.exerciseImageView.image = nil
        }
        
        if let cacheId = mediaItem["cacheId"] as? NSString {
            dispatch_async(self.serialQueue) {
                dispatch_async(dispatch_get_main_queue(), {
                    self.progressView.hidden = false
                })
                
                // Read the image from the photo cache first, if possible.
                if let imageData = PhotoCache().getCache("physioimage\(cacheId)") {
                    let image = UIImage(data: imageData)
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        UIView.animateWithDuration(0.35, animations: {
                            self.exerciseImageView.image = image
                            
                        })
                        
                        self.progressView.hidden = true
                    })
                    
                    return
                }
                
                // If the photo cache did not contain the image, I will download it from Amazon...
                HabitlabS3API.downloadPhysioImage(cacheId as String, handler: { (error, data) in
                    if let realData = data {
                        PhotoCache().setCache("physioimage\(cacheId as String)", image: realData)
                        let image = UIImage(data: realData)
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            self.exerciseImageView.image = image
                            self.progressView.hidden = true
                        })
                    }
                    }, progress: {(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) -> () in
                        var progress = Float(0.0)
                        
                        if totalBytesExpectedToWrite >= 1 {
                            progress = Float(totalBytesWritten / totalBytesExpectedToWrite)
                        }
                        
                        progress = 0.1 + 0.9 * progress
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            self.progressView.setProgress(progress, animated: false)
                        })
                })
            }
        }
    }
}