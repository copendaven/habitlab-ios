//
//  SubcategoryCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/4/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class SubcategoryCache: NSObject {
    private static var subcategories: NSMutableDictionary = NSMutableDictionary()
    private static var object: NSObject = NSObject()
    
    static func erase() {
        objc_sync_enter(SubcategoryCache.object)
        SubcategoryCache.subcategories.removeAllObjects()
        objc_sync_exit(SubcategoryCache.object)
    }
    
    static func getDriveSubcategoriesByCategory(category: String, handler: (NSError?, NSArray?) -> ()) {
        objc_sync_enter(SubcategoryCache.object)
        
        if let subcatsObject = subcategories.objectForKey(category) {
            if let subcats = subcatsObject as? NSArray {
                let returnedCategories = NSArray(array: subcats)
                handler(nil, returnedCategories)
                objc_sync_exit(SubcategoryCache.object)
                return
            }
        }
        
        Serv.showSpinner()
        
        HabitlabRestAPI.getDriveSubcategoriesByCategory(category as String, handler: { (error, subcategoriesJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                handler(error, nil)
                objc_sync_exit(SubcategoryCache.object)
                return
            }
            
            if let subcategories = subcategoriesJson as? NSArray {
                print(subcategories)
                
                SubcategoryCache.subcategories.setObject(subcategories, forKey: category)
                let returnedSubcategories = NSArray(array: subcategories)
                handler(nil, returnedSubcategories)
                objc_sync_exit(SubcategoryCache.object)
            }
        })
    }
}