//
//  UnpluggingPlanFriendCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class UnpluggingPlanFriendCell: UICollectionViewCell {
    private var controller: UIViewController?
    private var photo: String?
    
    @IBOutlet weak var userPhoto: UIImageView!
    
    func populate(controller: UIViewController, photo: String) {
        self.controller = controller
        self.photo = photo
        
        self.userPhoto!.layer.cornerRadius = self.userPhoto!.frame.size.width / 2
        self.userPhoto!.clipsToBounds = true
        self.userPhoto!.layer.borderWidth = 3.0
        self.userPhoto!.layer.borderColor = UIColor.whiteColor().CGColor
        
        Serv.loadImage(photo as String, handler: { (error, data) -> () in
            if error != nil {
                print(error)
            }
            
            if data != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.userPhoto!.image = UIImage(data: data!)
                })
            }
        })
    }
}