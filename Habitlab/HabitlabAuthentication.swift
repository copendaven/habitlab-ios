//
//  HabitlabAuthentication.swift
//  Habitlab
//
//  Created by Vlad Manea on 06/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class HabitlabAuthentication: NSObject {
    static var habitlabAccessToken: String? = nil
    
    static func authenticate(accessToken: String, method: AuthenticationMethod, handler: (NSError?, NSDictionary?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try requestDecoratedWithToken(accessToken, method: method, address: HabitlabRestAPI.Endpoint + "/session")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true) { (error, json) -> () in
            if error != nil {
                handler(error, nil)
                return
            }
            
            if let token = json!["token"] as? String, user = json!["user"] as? NSDictionary {
                habitlabAccessToken = token
                
                handler(nil, user)
                return
            }
            
            let error = NSError(domain: "Habitlab did not provide a token or a user.", code: 0, userInfo: nil)
            handler(error, nil)
        }
    }
    
    static func deauthenticate(handler: (Bool) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try requestDecoratedWithHabitlabToken(HabitlabRestAPI.Endpoint + "/session")
        } catch _ as NSError {
            handler(false)
            return
        }
        
        request!.HTTPMethod = "DELETE"
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true) { (error, json) -> () in
            if error != nil {
                handler(false)
                return
            }
            
            habitlabAccessToken = nil
            handler(true)
        }
    }
    
    static func destroy(handler: (Bool) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try requestDecoratedWithHabitlabToken(HabitlabRestAPI.Endpoint + "/user/me")
        } catch _ as NSError {
            handler(false)
            return
        }
        
        request!.HTTPMethod = "DELETE"
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true) { (error, json) -> () in
            if error != nil {
                handler(false)
                return
            }
            
            habitlabAccessToken = nil
            handler(true)
        }
    }
    
    static func requestDecoratedWithToken(accessToken: String, method: AuthenticationMethod, address: String) throws -> NSMutableURLRequest {
        let params = ["access_token": accessToken] as Dictionary<String, String>
        let request = NSMutableURLRequest(URL: NSURL(string: address)!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        do {
            try request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
        } catch let error as NSError {
            throw error
        }
        
        request.addValue("Habitlab", forHTTPHeaderField: "x-habitlab-mobile-app")
        
        switch method {
        case .AccountKit:
            request.addValue("AccountKit", forHTTPHeaderField: "x-habitlab-authentication-method")
        case .Facebook:
            request.addValue("Facebook", forHTTPHeaderField: "x-habitlab-authentication-method")
        }
        
        return request
    }
    
    static func requestDecoratedWithHabitlabToken(address: String) throws -> NSMutableURLRequest {
        let request = NSMutableURLRequest(URL: NSURL(string: address)!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        if habitlabAccessToken == nil {
            throw NSError(domain: "HabitlabAuthenticationError", code: 403, userInfo: ["name": "Missing token error", "message": "The access token is missing."])
        }
        
        request.addValue(habitlabAccessToken!, forHTTPHeaderField: "x-access-token")
        return request
    }
}
