//
//  UnpluggingModuleController.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/10/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class UnpluggingModuleController: GAITrackedViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    static var UnplugFailAfterSeconds = 10.0
    
    var step: NSDictionary?
    var program: NSDictionary?
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var goalLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var noticeLabel: UILabel!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var circle: KDCircularProgress!
    
    var progressDoneController: ProgressDoneController?
    var progressDoneBackgroundColor: ProgressDoneBackgroundColor?
    
    private var timer = NSTimer()
    private var actions: NSMutableArray = NSMutableArray()
    
    private var friendlyMessages: [String]?
    private var suggestionsFirst: [String]?
    private var doneStatus: String?
    private var suggestionSecond: String?
    
    @IBOutlet weak var actionsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(UnpluggingModuleController)
        
        actionsCollectionView.dataSource = self
        actionsCollectionView.delegate = self
        
        circle.progressThickness = 0.25
        circle.trackThickness = 0.25
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        removePlanUnplugging()
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        if let viewController = self.progressDoneController {
            viewController.hideSilently()
        }
        
        self.hideNavigationButtons()
        
        let screenSize = UIScreen.mainScreen().bounds
        let screenHeight = screenSize.height
        
        // Hide the notice label for iPhone 4s
        if screenHeight <= 480 {
            self.noticeLabel.hidden = true
        }
        
        self.timer.invalidate()
        self.timer = NSTimer.scheduledTimerWithTimeInterval(0.25, target: self, selector: #selector(UnpluggingModuleController.refresh), userInfo: nil, repeats: true)
        self.refresh()
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if let realStep = self.step {
            if let index = realStep["index"] as? NSNumber {
                dict["index"] = String(index)
            }
            
            if let week = realStep["week"] as? NSNumber {
                dict["week"] = String(week)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(UnpluggingModuleController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(UnpluggingModuleController))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
        
        self.showNavigationButtons()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        ProgramStepUnplugCache().hasExitedUnplugPrepareAlert()
        self.timer.invalidate()
    }
    
    private func removePlanUnplugging() {
        if let navigation = self.navigationController {
            var array = navigation.viewControllers
            
            for index in 0..<array.count {
                if let _ = array[index] as? PlanUnpluggingController {
                    array.removeAtIndex(index)
                    break
                }
            }
            
            navigation.viewControllers = array
        }
    }
    
    func buttonClicked(actionType: ModuleActionType) {
        switch (actionType) {
        case .Done:
            self.progressDoneStarted()
            break
        case .Later:
            self.performSegueWithIdentifier("showCurrentHabitFromUnpluggingModule", sender: nil)
            break
        case .Back:
            self.performSegueWithIdentifier("showCurrentHabitFromUnpluggingModule", sender: nil)
            break
        case .Start:
            self.startUnplug()
            break
        case .Now:
            self.startUnplug()
            break
        case .TryAgain:
            self.startUnplug()
            break
        case .GiveUp:
            self.giveUpUnplug()
            break
        default:
            break
        }
    }
    
    func refresh() {
        if step == nil || program == nil {
            self.statusLabel.text = ""
            self.distanceLabel.text = ""
            self.goalLabel.text = ""
            self.noticeLabel.text = ""
            
            return
        }
        
        print("Progress...")
        
        if let stepId = step!["id"] as? NSString, dueDate = Serv.getHardEndDate(step!) {
            var isTracking = false
            var isDone = false
            var isExpired = false
            var isFailed = false
            
            if ProgramStepUnplugCache().isDone(stepId as String) {
                isDone = true
                print("Progress for done")
                
                self.statusLabel.text = "YOU MADE IT!"
                self.distanceLabel.text = "00:00"
                self.goalLabel.text = ""
                self.noticeLabel.text = ""
                
                timer.invalidate()
            } else if ProgramStepUnplugCache().isExpired(stepId as String) {
                isExpired = true
                print("Progress for expired")
                
                self.statusLabel.text = "YOU FAILED THIS STEP."
                
                if let drive = program!["drive"] as? NSDictionary {
                    if let countMinutesPerStep = drive["countMinutesPerStep"] as? NSNumber {
                        let countMinutes = Double(60 * Int(countMinutesPerStep))
                
                        if let time = ProgramStepUnplugCache().getTime(stepId as String) {
                            let remainingTime = max(0, countMinutes - time)
                            let timeLabelString = Serv.getTimeLabelStringByLargestCalendarUnit(remainingTime)
                            self.distanceLabel.text = timeLabelString
                        
                            let calendarUnit = Serv.getLargestCalendarUnitDownToMinute(remainingTime)
                            let calendarUnitValue = Serv.getLargestCalendarUnitValueDownToMinute(remainingTime)
                            
                            let calendarUnitString = getCalendarUnitAsString(calendarUnit, amount: calendarUnitValue)
                            self.goalLabel.text = calendarUnitString
                        }
                    }
                }
                
                self.noticeLabel.text = ""
                
                timer.invalidate()
                
            } else if ProgramStepUnplugCache().isFailed(stepId as String) {
                isFailed = true
                print("Progress for failed")
                
                self.statusLabel.text = "YOU GAVE UP"
                self.distanceLabel.text = "it's ok"
                self.goalLabel.text = ""
                self.noticeLabel.text = "Try again!"
            } else if ProgramStepUnplugCache().isTracking(stepId as String) {
                isTracking = true
                print("Progress for tracking")
                
                self.statusLabel.text = ""
                
                if let time = ProgramStepUnplugCache().getTime(stepId as String) {
                    if let drive = program!["drive"] as? NSDictionary {
                        if let countMinutesPerStep = drive["countMinutesPerStep"] as? NSNumber {
                            let countMinutesInSeconds = Double(60 * Int(countMinutesPerStep))
                            
                            let remainingTime = countMinutesInSeconds - time
                            
                            // TODO: Unmark the remaining time as smaller
                            // if environment == Environment.Development || environment == Environment.Staging {
                            //     remainingTime -= countMinutesInSeconds
                            //     remainingTime += 5
                            // }
                            
                            let timeLabelString = Serv.getTimeLabelStringByLargestCalendarUnit(max(0, remainingTime))
                            self.distanceLabel.text = timeLabelString
                            
                            if remainingTime <= 0 {
                                self.handleDone(stepId as String)
                            }
                        }
                    }
                }
                
                self.goalLabel.text = ""
                self.noticeLabel.text = "Enjoy the freedom!"
                
                ProgramStepUnplugCache().update(stepId as String, dueDate: dueDate)
            } else /* Not tracking, not done, not expired, not failed. */ {
                print("Progress for none of the above")
                
                self.statusLabel.text = "UNPLUG FOR"
                
                if let drive = program!["drive"] as? NSDictionary {
                    if let countMinutesPerStep = drive["countMinutesPerStep"] as? NSNumber {
                        let countMinutes = Double(60 * Int(countMinutesPerStep))
                        let timeLabelString = Serv.getTimeLabelStringByLargestCalendarUnit(countMinutes)
                        self.distanceLabel.text = timeLabelString
                        
                        let calendarUnit = Serv.getLargestCalendarUnitDownToMinute(countMinutes)
                        let calendarUnitValue = Serv.getLargestCalendarUnitValueDownToMinute(countMinutes)
                        
                        let calendarUnitString = getCalendarUnitAsString(calendarUnit, amount: calendarUnitValue)
                        self.goalLabel.text = calendarUnitString
                    }
                }
                
                self.noticeLabel.text = ""
                
                print("Handling unplug")
                handleUnplug(stepId as String, dueDate: dueDate)
            }
            
            self.refreshActions(isTracking, isDone: isDone, isExpired: isExpired, isFailed: isFailed)
        }
    }
    
    private func getCalendarUnitAsString(calendarUnit: NSCalendarUnit, amount: Double) -> String {
        var calendarUnitAsString = ""
        
        switch calendarUnit {
        case NSCalendarUnit.Day:
            if amount.compare(1.0) == NSComparisonResult.OrderedSame {
                calendarUnitAsString = "DAY"
            } else {
                calendarUnitAsString = "DAYS"
            }
        case NSCalendarUnit.Hour:
            if amount.compare(1.0) == NSComparisonResult.OrderedSame {
                calendarUnitAsString = "HOUR"
            } else {
                calendarUnitAsString = "HOURS"
            }
        case NSCalendarUnit.Minute:
            if amount.compare(1.0) == NSComparisonResult.OrderedSame {
                calendarUnitAsString = "MINUTE"
            } else {
                calendarUnitAsString = "MINUTES"
            }
        default:
            break
        }
        
        return calendarUnitAsString
    }
    
    private func handleDone(stepId: String) {
        if step == nil {
            return
        }
        
        if program == nil {
            return
        }
        
        if let owner = program!["owner"] as? NSDictionary, dueDate = Serv.getHardEndDate(step!) {
            ProgramStepUnplugCache().stop(stepId as String, dueDate: dueDate)
            ProgramStepUnplugCache().complete(stepId as String, dueDate: dueDate)
            
            let cacheId = NSUUID().UUIDString
            
            if ProgramStepCompleteCache().isStepComplete(stepId as String) {
                return
            }
            
            ProgramStepCompleteCache().setStepComplete(stepId as String)
            
            CurrentHabitController.setInspirationCache(cacheId, program: self.program!, step: self.step!, owner: owner)
            
            Serv.showSpinner()
            
            HabitlabRestAPI.putProgramStepComplete("Habitlab unplug tracker", stepId: stepId as String, cacheId: cacheId, handler: { (error, stepJson) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    ProgramStepCompleteCache().unsetStepComplete(stepId as String)
                    return
                }
                
                HabitsController.resetPrograms()
                self.refresh()
            })
        }
    }
    
    private func progressDoneStarted() {
        if step == nil || program == nil {
            return
        }
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Red
        
        friendlyMessages = ["Hooray!", "Well done!", "Rockstar!", "You rock!", "Way to go!", "Woohoo!", "You did it!", "One down!", "Alrighty!"]
        suggestionsFirst = ["Make this count even more", "Double the impact", "Spread the good vibes", "Make sure to celebrate"]
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        doneStatus = "Loop complete"
        suggestionSecond = "by inspiring your friends!"
        
        if let drive = program!["drive"] as? NSDictionary, week = self.step!["week"] as? NSNumber, index = self.step!["index"] as? NSNumber {
            if let countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                let weekInt = Int(week)
                let countWeeksInt = Int(countWeeks)
                let indexInt = Int(index)
                let countStepsPerWeekInt = Int(countStepsPerWeek)
                
                if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt - 1 {
                    
                    // We are dealing with the last loop in the level, so the level is complete!
                    doneStatus = "Level complete"
                    suggestionSecond = "by inspiring your friends!"
                    
                    // We are dealing with the last loop in the level, so the level is complete!
                    self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Green
                    
                    if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                        let levelInt = Int(level)
                        let countLevels = Int(countLevels)
                        
                        if levelInt >= countLevels {
                            
                            // We are dealing with the last level in the program, so the program is complete!
                            self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Blue
                            
                            // We are dealing with the last level in the program, so the program is complete!
                            doneStatus = "Program complete"
                            suggestionSecond = "by inspiring your friends!"
                        }
                    }
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.performSegueWithIdentifier("showProgressDoneFromUnpluggingModule", sender: self)
        })
    }
    
    func progressDoneFinished() {
        self.performSegueWithIdentifier("showCurrentHabitFromUnpluggingModule", sender: nil)
    }
    
    private func handleUnplug(stepId: String, dueDate: NSDate) -> Bool {
        if ProgramStepUnplugCache().isStarted(stepId as String) {
            if ProgramStepUnplugCache().isDelayed(stepId as String, delayInSeconds: 5.0) {
                print("It is failed")
                return ProgramStepUnplugCache().fail(stepId as String, dueDate: dueDate)
            }
        }
        
        return false
    }
    
    private func refreshActions(isTracking: Bool, isDone: Bool, isExpired: Bool, isFailed: Bool) {
        self.actions = NSMutableArray()
        
        if isDone {
            self.actions.addObject(ModuleActionType.Done.rawValue)
        } else if isExpired {
            self.actions.addObject(ModuleActionType.Back.rawValue)
        } else if isFailed {
            let screenSize = UIScreen.mainScreen().bounds
            let screenHeight = screenSize.height
            
            if screenHeight <= 480 {
                self.actions.addObject(ModuleActionType.TryAgain.rawValue)
            } else {
                self.actions.addObject(ModuleActionType.Now.rawValue)
            }
            
            self.actions.addObject(ModuleActionType.Later.rawValue)
        } else if isTracking {
            self.actions.addObject(ModuleActionType.GiveUp.rawValue)
        } else {
            self.actions.addObject(ModuleActionType.Start.rawValue)
        }
        
        self.actionsCollectionView.reloadData()
    }
    
    private func showNavigationButtons() {
        if let controller = self.navigationController {
            controller.setNavigationBarHidden(false, animated: true)
        }
    }
    
    private func hideNavigationButtons() {
        if let controller = self.navigationController {
            controller.setNavigationBarHidden(true, animated: true)
        }
    }
    
    private func startUnplug() {
        if step == nil {
            return
        }
        
        if program == nil {
            return
        }
        
        let drive = program!["drive"] as? NSDictionary
        
        if drive == nil {
            return
        }
        
        let countMinutesPerStepNS = drive!["countMinutesPerStep"] as? NSNumber
        
        if countMinutesPerStepNS == nil {
            return
        }
        
        if let stepId = step!["id"] as? NSString, dueDate = Serv.getHardEndDate(step!) {
            ProgramStepUnplugCache().start(stepId as String, dueDate: dueDate)
            
            if !ProgramStepStartedCache().isStepStarted(stepId as String) {
                HabitlabRestAPI.putProgramStepStarted("Habitlab unplug tracker", stepId: stepId as String, handler: { (error, response) in
                    if error != nil {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        return
                    }
                    
                    ProgramStepStartedCache().setStepStarted(stepId as String)
                })
            }
        }
    }
    
    private func giveUpUnplug() {
        let destroyAlert = UIAlertController(title: "Give up on unplug", message: "By giving up on unplug, you lose all the time you unplugged, and you will have to start over. Do you want to still give up on unplugging?", preferredStyle: UIAlertControllerStyle.Alert)
        
        destroyAlert.addAction(UIAlertAction(title: "Yes, give up", style: .Default, handler: { (action: UIAlertAction!) in
            if self.step == nil {
                return
            }
            
            if let stepId = self.step!["id"] as? NSString, dueDate = Serv.getHardEndDate(self.step!) {
                ProgramStepUnplugCache().fail(stepId as String, dueDate: dueDate)
                self.refreshActions(false, isDone: false, isExpired: false, isFailed: true)
            }
        }))
        
        destroyAlert.addAction(UIAlertAction(title: "No, stay unplugged", style: .Cancel, handler: nil))
        
        presentViewController(destroyAlert, animated: true, completion: nil)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actions.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(90, 125) // This is the size for one collection item with button and label
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenWidth = Int(actionsCollectionView.frame.size.width)
        var contentWidth = Int(actions.count * 90) // This is the size for one collection item with button and label
        
        if screenWidth <= contentWidth {
            contentWidth = screenWidth / 90 * 90
        }
        
        let padding = CGFloat((screenWidth - contentWidth) / 2)
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if actions.count <= indexPath.item {
            return defaultCell
        }
        
        if let action = actions[indexPath.item] as? String {
            if let cell = actionsCollectionView.dequeueReusableCellWithReuseIdentifier("moduleActionCollectionCell", forIndexPath: indexPath) as? ModuleActionCollectionCell {
                if let moduleActionType = ModuleActionType(rawValue: action) {
                    cell.populate(actionType: moduleActionType, actionState: ModuleActionState.Enabled, controller: self)
                    return cell
                }
            }
        }
        
        return defaultCell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressDoneFromUnpluggingModule" {
            if let viewController = segue.destinationViewController as? ProgressDoneController {
                self.progressDoneController = viewController
                viewController.setDelegate(self)
                
                if let backgroundColor = self.progressDoneBackgroundColor {
                    viewController.setBackgroundColor(backgroundColor)
                }
                
                viewController.setDoneCaptions(friendlyMessages, doneStatus: doneStatus, suggestionsFirst: suggestionsFirst, suggestionSecond: suggestionSecond)
                viewController.markDone()
            }
        }
        
        if segue.identifier == "showCurrentHabitFromUnpluggingModule" {
            // self.showNavigationButtons()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    @IBAction func unpluggingModuleSentThoughtAboutYou(segue: UIStoryboardSegue) {
        // Nothing here.
    }
}
