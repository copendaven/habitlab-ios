//
//  ReconnectActionFeedCellWithComments.swift
//  Habitlab
//
//  Created by Vlad Manea on 26/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import RandomColorSwift

class ReconnectActionFeedCellWithComments: FeedCell {
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var cellMessage: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var firstComment: UILabel!
    @IBOutlet weak var missingComments: UILabel!
    @IBOutlet weak var secondToLastComment: UILabel!
    @IBOutlet weak var lastComment: UILabel!
    
    @IBOutlet weak var firstCommentTop: NSLayoutConstraint!
    @IBOutlet weak var firstCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var missingCommentTop: NSLayoutConstraint!
    @IBOutlet weak var missingCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var lastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var lastCommentBottom: NSLayoutConstraint!
    
    @IBOutlet weak var commentsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contactPhoto: UIImageView!
    @IBOutlet weak var initialLabel: UILabel!
    @IBOutlet weak var thoughtCloudImage: UIImageView!
    
    private func getCommenting() -> NSDictionary {
        let commenting = NSMutableDictionary()
        
        commenting.setObject(firstComment, forKey: "firstComment")
        commenting.setObject(missingComments, forKey: "missingComments")
        commenting.setObject(secondToLastComment, forKey: "secondToLastComment")
        commenting.setObject(lastComment, forKey: "lastComment")
        
        commenting.setObject(firstCommentTop, forKey: "firstCommentTop")
        commenting.setObject(firstCommentBottom, forKey: "firstCommentBottom")
        commenting.setObject(missingCommentTop, forKey: "missingCommentTop")
        commenting.setObject(missingCommentBottom, forKey: "missingCommentBottom")
        commenting.setObject(secondToLastCommentTop, forKey: "secondToLastCommentTop")
        commenting.setObject(secondToLastCommentBottom, forKey: "secondToLastCommentBottom")
        commenting.setObject(lastCommentTop, forKey: "lastCommentTop")
        commenting.setObject(lastCommentBottom, forKey: "lastCommentBottom")
        
        commenting.setObject(commentsView, forKey: "commentsView")
        commenting.setObject(commentsViewHeight, forKey: "commentsViewHeight")
        
        return commenting
    }
    
    @IBAction override func like(sender: AnyObject) {
        super.like(sender)
    }
    
    @IBAction override func comment(sender: AnyObject) {
        super.comment(sender)
    }
    
    @IBAction override func clickMore(sender: AnyObject) {
        super.clickMore(sender)
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickContact(sender: AnyObject) {
        if let realNotification = self.notification, realController = self.controller {
            if let userObjectId = realNotification["userObjectId"] as? NSString {
                if let feedController = realController as? NewFeedController {
                    feedController.performSegueWithIdentifier("showPublicProfileFromFeed", sender: userObjectId as String)
                    return
                }
                
                if let profileController = realController as? ProfileController {
                    profileController.performSegueWithIdentifier("showPublicProfileFromProfile", sender: userObjectId as String)
                    return
                }
            }
        }
    }
    
    func setLiking() {
        if let note = super.notification {
            super.setLiking(note, likingButton: self.likeButton, showLikeButton: true)
        }
    }
    
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, showButtons: Bool) {
        let commenting = getCommenting()
        
        super.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, commentingButton: commentButton, commenting: commenting, likingButton: likeButton, optionsButton: optionsButton, showButtons: showButtons)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.userPhoto.layer.borderWidth = 4.0
            self.userPhoto.layer.borderColor = UIColor.whiteColor().CGColor
            self.contactPhoto.layer.borderWidth = 4.0
            self.contactPhoto.layer.borderColor = UIColor.whiteColor().CGColor
            self.initialLabel.layer.borderWidth = 4.0
            self.initialLabel.layer.borderColor = UIColor.whiteColor().CGColor
            
            self.contactPhoto.layer.cornerRadius = 0.5 * self.contactPhoto.bounds.size.width
            self.initialLabel.layer.cornerRadius = 0.5 * self.initialLabel.bounds.size.width
        }
        
        self.setPhoto()
    }
    
    private func getFirstName() -> String? {
        var firstName: String? = nil
        
        if let realNotification = self.notification {
            if let realFirstName = realNotification["contactObjectFirstName"] as? NSString {
                firstName = realFirstName as String
            }
        }
        
        return firstName
    }
    
    private func getLastName() -> String? {
        var lastName: String? = nil
        
        if let realNotification = self.notification {
            if let realLastName = realNotification["contactObjectLastName"] as? NSString {
                lastName = realLastName as String
            }
        }
        
        return lastName
    }
    
    private func setPhoto() {
        var hasPhoto = false
        
        if notification == nil {
            return
        }
        
        if let userObjectFacebookPhoto = notification!["userObjectFacebookPhoto"] as? NSString {
            hasPhoto = true
            self.initialLabel.hidden = true
            
            Serv.loadImage(userObjectFacebookPhoto as String, handler: { (error, data) -> () in
                if error != nil {
                    print(error)
                }
                
                if data != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                            self.contactPhoto.hidden = false
                            self.contactPhoto!.image = UIImage(data: data!)
                            }, completion: nil)
                    })
                }
            })
        }
        
        if !hasPhoto {
            if let identifier = notification!["contactObjectIdentifier"] as? NSString {
                    
                // Read the image from the photo cache first, if possible.
                if let imageData = PhotoCache().getCache("contact\(identifier)") {
                    hasPhoto = true
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                            self.contactPhoto.hidden = false
                            self.contactPhoto!.image = UIImage(data: imageData)
                            }, completion: nil)
                    })
                }
            }
        }
        
        if !hasPhoto {
            if let contactHasPhoto = notification!["contactObjectHasPhoto"] as? NSNumber, identifier = notification!["contactObjectIdentifier"] as? NSString {
                if contactHasPhoto == 1 {
                    hasPhoto = true
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                        
                        // If the photo cache did not contain the image, I will download it from Amazon...
                        HabitlabS3API.downloadContactImage(identifier as String, handler: { (error, imageData) -> () in
                            if error != nil {
                                return
                            }
                            
                            if imageData != nil {
                                PhotoCache().setCache("contact\(identifier)", image: imageData!)
                                
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                                        self.contactPhoto.hidden = false
                                        self.contactPhoto!.image = UIImage(data: imageData!)
                                        }, completion: nil)
                                })
                            }
                            }, progress: nil)
                    })
                }
            }
        }
    
        if !hasPhoto {
            var initialLabelText = ""
            
            if let realFirstName = getFirstName() {
                if realFirstName.characters.count > 0 {
                    initialLabelText += realFirstName[realFirstName.startIndex.advancedBy(0)..<realFirstName.startIndex.advancedBy(1)]
                }
            }
            
            if let realLastName = getLastName() {
                if realLastName.characters.count > 0 {
                    initialLabelText += realLastName[realLastName.startIndex.advancedBy(0)..<realLastName.startIndex.advancedBy(1)]
                }
            }
            
            if initialLabelText == "" {
                initialLabelText = "?"
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.contactPhoto.image = nil
                self.contactPhoto.hidden = true
                self.initialLabel.hidden = false
                self.initialLabel.backgroundColor = randomColor(hue: .Red, luminosity: .Bright)
                self.initialLabel.text = initialLabelText.uppercaseString
            })
        }
    }
}
