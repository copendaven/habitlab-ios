//
//  ChooseNewHabitController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class ChooseNewLevelController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    var subcategory: NSDictionary?
    var category: NSDictionary?
    var drives = []
    var refresher: UIRefreshControl?
    var quitted = false
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var trashButton: UIBarButtonItem!
    @IBOutlet weak var levelsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ChooseNewLevelController)
        
        levelsTable.delegate = self
        levelsTable.dataSource = self
        levelsTable.estimatedRowHeight = 80
        levelsTable.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = levelsTable
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ChooseNewLevelController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        levelsTable.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        quitted = false
        trashButton.enabled = false
        
        refresh()
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realCategory = self.category {
            if let categ = realCategory["str"] as? NSString {
                dict["category"] = categ as String
            }
        }
        
        if let realSubcategory = self.subcategory {
            if let subcateg = realSubcategory["str"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ChooseNewLevelController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ChooseNewLevelController))
        }
    }
    
    @IBAction func signupLevel(sender: AnyObject) {
        for driveJson in drives {
            if let drive = driveJson as? NSDictionary {
                if let availabilityString = drive["availability"] as? NSString {
                    let availability = availabilityString as String
                    
                    if availability.compare("available") == NSComparisonResult.OrderedSame {
                        if let driveCategory = drive["driveCategory"] as? NSString {
                            if driveCategory.compare("reading") == NSComparisonResult.OrderedSame {
                                self.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                                return
                            }
                            
                            if driveCategory.compare("unplugging") == NSComparisonResult.OrderedSame {
                                self.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                                return
                            }
                            
                            if driveCategory.compare("running") == NSComparisonResult.OrderedSame {
                                self.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                                return
                            }
                            
                            if driveCategory.compare("reconnecting") == NSComparisonResult.OrderedSame {
                                self.performSegueWithIdentifier("showClarifyReconnectChooseFriendsFromChooseNewLevel", sender: drive)
                                return
                            }
                            
                            if driveCategory.compare("exercising") == NSComparisonResult.OrderedSame {
                                if let level = drive["level"] as? NSNumber {
                                    if level == 1 {
                                        self.performSegueWithIdentifier("showChooseProgressPermissionsFromChooseNewLevel", sender: drive)
                                    } else {
                                        self.performSegueWithIdentifier("showScheduleTimeFromChooseNewLevel", sender: drive)
                                    }
                                }
                                
                                return
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func quitProgram(sender: AnyObject) {
        if quitted {
            return
        }
        
        for drive in drives {
            if let driveId = drive["id"] as? NSString, availabilityString = drive["availability"] as? NSString {
                let availability = availabilityString as String
                
                if availability == "current" {
                    let alert = UIAlertController(title: "Quit level", message: "Are you sure you want to quit?", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let alertCancelAction = UIAlertAction(title: "Not now", style: UIAlertActionStyle.Cancel) { (UIAlertAction) -> Void in}
                    
                    let alertQuitAction = UIAlertAction(title: "Quit", style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
                        self.quitted = true
                        Serv.showSpinner()
                        
                        HabitlabRestAPI.putQuitProgram(driveId as String, handler: { (error, response) -> () in
                            Serv.hideSpinner()
                            
                            if error != nil {
                                self.quitted = false
                                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                return
                            }
                            
                            HabitsController.resetPrograms()
                            
                            self.performSegueWithIdentifier("habitsControllerRemovedHabit", sender: self)
                            
                            // Serv.showAlertPopup("Quit level", message: "You have quit the level.", okMessage: "Okay, got it.", controller: self, handler: nil)
                            // self.refresh()
                        })
                    }
                    
                    alert.addAction(alertCancelAction)
                    alert.addAction(alertQuitAction)
                    
                    self.presentViewController(alert, animated: true) { () -> Void in }
                    return
                }
            }
        }
        
        Serv.showAlertPopup("Quit error", message: "You are not registered in any level.", okMessage: "Okay, got it.", controller: self, handler: nil)
    }
    
    func filterDrives(drives: NSArray) -> NSArray {
        let result: NSMutableArray = NSMutableArray()
        
        // Add a new object for the first element!
        result.addObjectsFromArray(drives as [AnyObject]);
        
        return result
    }
    
    func refresh() {
        self.signupButton.hidden = true
        
        if category == nil {
            return
        }
        
        if subcategory == nil {
            return
        }
        
        if let categoryString = category!["str"] as? String {
            let handler = { (error: NSError?, drivesJson: AnyObject?) -> () in
                Serv.hideSpinner()
                
                if (error != nil) {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                if let drives = drivesJson as? NSArray {
                    self.drives = self.filterDrives(drives)
                    self.levelsTable.reloadData()
                    
                    if let refr = self.refresher {
                        refr.endRefreshing()
                    }
                    
                    for driveJson in self.drives {
                        if let drive = driveJson as? NSDictionary {
                            if let availabilityString = drive["availability"] as? NSString, level = drive["level"] as? NSNumber {
                                let availability = availabilityString as String
                                
                                if availability.compare("available") == NSComparisonResult.OrderedSame {
                                    self.signupButton.hidden = false
                                    self.signupButton.setTitle("SIGNUP FOR LEVEL \(level)", forState: UIControlState.Normal)
                                }
                                
                                if availability.compare("current") == NSComparisonResult.OrderedSame {
                                    self.trashButton.enabled = true
                                }
                            }
                        }
                    }
                }
            }
            
            if categoryString.compare("exercising") == NSComparisonResult.OrderedSame {
                print(subcategory!)
                
                if let physioProgram = subcategory!["physioProgram"] as? NSDictionary {
                    if let programId = physioProgram["id"] as? NSString {
                        Serv.showSpinner()
                        
                        HabitlabRestAPI.getDrivesByCategoryPhysioProgram(categoryString, physioProgram: programId as String, handler: handler)
                    }
                }
            } else {
                if let subcategoryString = subcategory!["str"] as? String {
                    Serv.showSpinner()
                    
                    HabitlabRestAPI.getDrivesByCategorySubcategory(categoryString, subcategory: subcategoryString, handler: handler)
                }
            }
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showScheduleTimeFromChooseNewLevel") {
            if let viewController = segue.destinationViewController as? ScheduleTimeController {
                viewController.drive = sender as? NSDictionary
            }
        }
        
        if (segue.identifier == "showClarifyReconnectChooseFriendsFromChooseNewLevel") {
            if let viewController = segue.destinationViewController as? ClarifyReconnectChooseFriendsController {
                viewController.drive = sender as? NSDictionary
            }
        }
        
        if (segue.identifier == "showCurrentHabitFromChooseNewLevel") {
            if let viewController = segue.destinationViewController as? CurrentHabitController {
                viewController.program = sender as? NSDictionary
            }
        }
        
        if (segue.identifier == "showChooseProgressPermissionsFromChooseNewLevel") {
            if let viewController = segue.destinationViewController as? ChooseProgressPermissionsController {
                viewController.category = self.category
                viewController.subcategory = self.subcategory
                viewController.drive = sender as? NSDictionary
            }
        }
    }
    
    private func loadBackground(drive: NSDictionary) {
        print(drive)
        
        if let photo = drive["cover"] as? NSString {
            let url = Configuration.getEndpoint() + (photo as String)
            
            Serv.loadImage(url, handler: { (error, data) -> () in
                if error != nil {
                    print(error)
                }
                
                if data != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        // self.backgroundImage.contentMode = UIViewContentMode.ScaleAspectFit
                        self.backgroundImage.image = UIImage(data: data!)
                    })
                }
            })
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return drives.count + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            var found = false
            
            // Finding that drive which is current or available and painting it on the wall :)
            for driveJson in drives {
                if let drive = driveJson as? NSDictionary {
                    if let availabilityString = drive["availability"] as? NSString {
                        let availability = availabilityString as String
                        
                        if availability.compare("current") == NSComparisonResult.OrderedSame || availability.compare("available") == NSComparisonResult.OrderedSame {
                            if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewLevelHeader", forIndexPath: indexPath) as? ChooseNewLevelHeader {
                                self.loadBackground(drive)
                                
                                found = true
                                cell.populate(drive, levels: drives.count)
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                return cell
                            }
                        }
                    }
                }
            }
            
            if (!found) {
                
                // If none is found, just paint the first photo that comes handy...
                for driveJson in drives {
                    if let drive = driveJson as? NSDictionary {
                        if let _ = drive["cover"] as? NSString {
                            if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewLevelHeader", forIndexPath: indexPath) as? ChooseNewLevelHeader {
                                self.loadBackground(drive)
                                
                                found = true
                                cell.populate(drive, levels: drives.count)
                                cell.selectionStyle = UITableViewCellSelectionStyle.None
                                return cell
                            }
                        }
                    }
                }
            }
            
            if let cell = tableView.dequeueReusableCellWithIdentifier("priorFeedCell", forIndexPath: indexPath) as? PriorFeedCell {
                cell.populate(self)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
        
            return defaultCell
        }
        
        if let drive = drives[indexPath.section - 1] as? NSDictionary {
            if let availabilityString = drive["availability"] as? NSString {
                let availability = availabilityString as String
                
                switch(availability) {
                case "complete":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewLevelCompleteCell", forIndexPath: indexPath) as? ChooseNewLevelCompleteCell {
                        cell.populate(drive, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "current":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewLevelCurrentCell", forIndexPath: indexPath) as? ChooseNewLevelCurrentCell {
                        cell.populate(drive, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "available":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewLevelAvailableCell", forIndexPath: indexPath) as? ChooseNewLevelAvailableCell {
                        cell.populate(drive, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "locked":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewLevelLockedCell", forIndexPath: indexPath) as? ChooseNewLevelLockedCell {
                        cell.populate(drive, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                default: break
                    // Do nothing.
                }
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Do nothing more...
    }
}
