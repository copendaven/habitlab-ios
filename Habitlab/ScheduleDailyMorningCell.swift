//
//  ScheduleDailyMorningCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/9/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import MGSwipeTableCell

class ScheduleDailyMorningCell: ScheduleDailyCell {
    @IBOutlet weak var countTimesLabel: UILabel!
    @IBOutlet weak var backLabel: UILabel!
    
    func populate(controller: UIViewController, countTimes: Int) {
        let handleMinus = {(sender: MGSwipeTableCell!) -> Bool in
            if let viewController = controller as? ScheduleDailyController {
                // viewController.clickMorningMinus()
            }
            
            return false
        }
        
        let handlePlus = {(sender: MGSwipeTableCell!) -> Bool in
            if let viewController = controller as? ScheduleDailyController {
                // viewController.clickMorningPlus()
            }
            
            return false
        }
        
        super.populate(countTimesLabel, backLabel: backLabel, countTimes: countTimes, handleMinus: handleMinus, handlePlus: handlePlus)
        self.setCountTimes(countTimes)
    }
    
    override func setCountTimes(countTimes: Int) {
        super.setCountTimes(countTimes)
        
        dispatch_async(dispatch_get_main_queue()) {
            if countTimes > 0 {
                self.countTimesLabel.text = "\(countTimes)"
            } else {
                self.countTimesLabel.text = "<"
            }
        }
    }
}
