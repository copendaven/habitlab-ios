//
//  ChooseNewProgramController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class ChooseNewProgramController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    var subcategories = []
    var category: NSDictionary?
    var refresher: UIRefreshControl?
    
    @IBOutlet weak var programsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ChooseNewProgramController)
        
        programsTableView.delegate = self
        programsTableView.dataSource = self
        programsTableView.estimatedRowHeight = 100
        programsTableView.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = programsTableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ChooseNewProgramController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        programsTableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        refresh()
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realCategory = self.category {
            if let categ = realCategory["str"] as? NSString {
                dict["category"] = categ as String
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ChooseNewProgramController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ChooseNewProgramController))
        }
    }
    
    func filterSubcategories(subcategories: NSArray) -> NSArray {
        let result: NSMutableArray = NSMutableArray()
        
        // Add a new object for the first element!
        result.addObjectsFromArray(subcategories as [AnyObject])
        
        return result
    }
    
    func refresh() {
        if category == nil {
            return
        }
        
        if let categoryString = category!["str"] as? NSString {
            SubcategoryCache.getDriveSubcategoriesByCategory(categoryString as String, handler: { (error, subcategoriesJson) -> () in
                if (error != nil) {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
            
                if let subcategories = subcategoriesJson {
                    self.subcategories = self.filterSubcategories(subcategories)
                    self.programsTableView.reloadData()
                    
                    if let refr = self.refresher {
                        refr.endRefreshing()
                    }
                }
            })
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showChooseNewLevelFromChooseNewProgram") {
            if category != nil {
                if let subcategory = sender as? NSDictionary {
                    if let viewController = segue.destinationViewController as? ChooseNewLevelController {
                        viewController.category = category
                        viewController.subcategory = subcategory
                    }
                }
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return subcategories.count + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewProgramHeader", forIndexPath: indexPath)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell
        }
        
        if let subcategory = subcategories[indexPath.section - 1] as? NSDictionary {
            if category != nil {
                if let cell = tableView.dequeueReusableCellWithIdentifier("chooseNewProgramCell", forIndexPath: indexPath) as? ChooseNewProgramCell {
                    cell.populate(category!, subcategory: subcategory, controller: self)
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    return cell
                }
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing else...
    }
}
