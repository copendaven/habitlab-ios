//
//  ScheduleDateWeekDayCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/20/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ScheduleDateWeekDayCell: UICollectionViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    
    func populate(weekDay: String) {
        self.dayLabel.text = weekDay
    }
}
