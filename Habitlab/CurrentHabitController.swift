//
//  CurrentHabitController.swift
//  Habitlab
//
//  Created by Vlad Manea on 27/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import AVFoundation
import Analytics

enum ProgramState: String {
    case Idle = "Idle"
    case Progress = "Progress"
    case Warning0 = "Warning0"
    case Warning1 = "Warning1"
    case Warning2 = "Warning2"
    case Closing = "Closing"
    case Complete = "Complete"
    case CompleteNext = "CompleteNext"
    case Forfeited = "Forfeited"
    case Quitted = "Quitted"
    case Capture = "Capture"
    case BetweenSoftHard = "BetweenSoftHard"
}

var currentHabitTimers: NSMutableArray = NSMutableArray()

class CurrentHabitController: GAITrackedViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var program: NSDictionary?
    var uuid: String?
    
    var circleTimer = NSTimer()
    var imagePicker: UIImagePickerController!
    
    var isOpenedFromPushNotification: Bool = false
    var progressDoneController: ProgressDoneController?
    var progressDoneBackgroundColor: ProgressDoneBackgroundColor?
    
    @IBOutlet weak var buttonImage: UIImageView!
    @IBOutlet weak var circleOverlay: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var todoLabel: UILabel!
    @IBOutlet weak var deadlineLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var circleButton: UIButton!
    @IBOutlet weak var totalPointsLabel: UILabel!
    @IBOutlet weak var levelPointsLabel: UILabel!
    @IBOutlet weak var levelCompletedLabel: UILabel!
    
    @IBOutlet weak var actionButton: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var circle: KDCircularProgress!
    
    @IBOutlet weak var currentHabitCaption: UINavigationItem!
    
    @IBAction func circleButtonClicked(sender: AnyObject) {
        buttonClicked(sender)
    }
    
    @IBAction func actionButtonClicked(sender: AnyObject) {
        buttonClicked(sender)
    }
    
    private func buttonClicked(sender: AnyObject) {
        if program == nil {
            return
        }
        
        if !isIrrelevantProgram(program!) {
            if let step = getCurrentStep() {
                    
                // We are in the progress situation.
                
                if let drive = program!["drive"] as? NSDictionary {
                    if let driveCategory = drive["driveCategory"] as? NSString {
                        
                        // For the running program, just point to the tracker.
                        if (driveCategory as String).compare("running") == NSComparisonResult.OrderedSame {
                            let object = NSMutableDictionary()
                            object.setObject(program!, forKey: "program")
                            object.setObject(step, forKey: "step")
                            self.performSegueWithIdentifier("showRunningModuleFromCurrentHabit", sender: object)
                        }
                        
                        // For the running program, just point to the tracker.
                        if (driveCategory as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                            let object = NSMutableDictionary()
                            object.setObject(program!, forKey: "program")
                            object.setObject(step, forKey: "step")
                            self.performSegueWithIdentifier("showReconnectingModuleChooseContactFromCurrentHabit", sender: object)
                        }
                        
                        // For the exercising program, just point to the tracker.
                        if (driveCategory as String).compare("exercising") == NSComparisonResult.OrderedSame {
                            let object = NSMutableDictionary()
                            object.setObject(program!, forKey: "program")
                            object.setObject(step, forKey: "step")
                            self.performSegueWithIdentifier("showChoosePhysioPainFromCurrentHabit", sender: object)
                        }
                        
                        // For the unplug program, just point to the unplugger.
                        if (driveCategory as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                            let object = NSMutableDictionary()
                            object.setObject(program!, forKey: "program")
                            object.setObject(step, forKey: "step")
                            
                            var shouldShowPlanUnplug = false
                            
                            if let stepId = step["id"] as? NSString {
                                if !PlanUnpluggingCache().isStepPlannedUnplugging(stepId as String) {
                                    shouldShowPlanUnplug = true
                                }
                            }
                            
                            if shouldShowPlanUnplug {
                                self.performSegueWithIdentifier("showPlanUnpluggingFromCurrentHabit", sender: object)
                            } else {
                                self.performSegueWithIdentifier("showUnpluggingModuleFromCurrentHabit", sender: object)
                            }
                        }
                        
                        // For the reading program, mark the step complete (for now).
                        if (driveCategory as String).compare("reading") == NSComparisonResult.OrderedSame {
                            if let stepId = step["id"] as? NSString {
                                if ProgramStepCompleteCache().isStepComplete(stepId as String) {
                                    return
                                }
                                
                                ProgramStepCompleteCache().setStepComplete(stepId as String)
                                
                                let cacheId = NSUUID().UUIDString
                                
                                if let owner = program!["owner"] as? NSDictionary {
                                    CurrentHabitController.setInspirationCache(cacheId, program: self.program!, step: step, owner: owner)
                                }
                                
                                // We are dealing with an internal loop in the level, so the level is not complete yet.
                                self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Red
                                
                                if let week = step["week"] as? NSNumber, index = step["index"] as? NSNumber, countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                                    let weekInt = Int(week)
                                    let countWeeksInt = Int(countWeeks)
                                    let indexInt = Int(index)
                                    let countStepsPerWeekInt = Int(countStepsPerWeek)
                                    
                                    if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt - 1 {
                                        
                                        // We are dealing with the last loop in the level, so the level is complete!
                                        self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Green
                                        
                                        if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                                            let levelInt = Int(level)
                                            let countLevels = Int(countLevels)
                                            
                                            if levelInt >= countLevels {
                                                
                                                // We are dealing with the last level in the program, so the program is complete!
                                                self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Blue
                                            }
                                        }
                                    }
                                }
                                
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.performSegueWithIdentifier("showProgressDoneFromCurrentHabit", sender: self)
                                })
                                
                                Serv.showSpinner()
                                
                                HabitlabRestAPI.putProgramStepComplete("Reading complete button", stepId: stepId as String, cacheId: cacheId, handler: { (error, stepJson) -> () in
                                    Serv.hideSpinner()
                                    
                                    if error != nil {
                                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                        ProgramStepCompleteCache().unsetStepComplete(stepId as String)
                                        
                                        dispatch_async(dispatch_get_main_queue(), {
                                            if let viewController = self.progressDoneController {
                                                viewController.hideSilently()
                                            }
                                        })
                                        
                                        return
                                    }
                                    
                                    HabitsController.resetPrograms()
                                    
                                    let friendlyMessages = ["Hooray!", "Well done!", "Rockstar!", "You rock!", "Way to go!", "Woohoo!", "You did it!", "One down!", "Alrighty!"]
                                    let suggestionsFirst = ["Make this count even more", "Double the impact", "Spread the good vibes", "Make sure to celebrate"]
                                    
                                    // We are dealing with an internal loop in the level, so the level is not complete yet.
                                    var doneStatus = "Loop complete"
                                    var suggestionSecond = "by inspiring your friends!"
                                    
                                    if let week = step["week"] as? NSNumber, index = step["index"] as? NSNumber, countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                                        let weekInt = Int(week)
                                        let countWeeksInt = Int(countWeeks)
                                        let indexInt = Int(index)
                                        let countStepsPerWeekInt = Int(countStepsPerWeek)
                                        
                                        if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt - 1 {
                                            
                                            // We are dealing with the last loop in the level, so the level is complete!
                                            doneStatus = "Level complete"
                                            suggestionSecond = "by inspiring your friends!"
                                            
                                            if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                                                let levelInt = Int(level)
                                                let countLevels = Int(countLevels)
                                                
                                                if levelInt >= countLevels {
                                                    
                                                    // We are dealing with the last level in the program, so the program is complete!
                                                    doneStatus = "Program complete"
                                                    suggestionSecond = "by inspiring your friends!"
                                                }
                                            }
                                        }
                                    }
                                    
                                    dispatch_async(dispatch_get_main_queue(), {
                                        if let viewController = self.progressDoneController {
                                            viewController.setDoneCaptions(friendlyMessages, doneStatus: doneStatus, suggestionsFirst: suggestionsFirst, suggestionSecond: suggestionSecond)
                                            viewController.markDone()
                                        }
                                        
                                        self.circleTimer.invalidate()
                                        
                                        self.circle.animateToAngle(360, duration: 0.1, relativeDuration: true, completion: { (success) -> Void in
                                            self.refresh()
                                        })
                                    })
                                })
                                
                                return
                            }
                        }
                    }
                }
                
                return
            }
        }
        
        if let step = getLastCompletedStep() {
            if isPostStepComplete(step) && !isPostStepCaptured(step) {
                if let stepId = step["id"] as? NSString {
                    if !ProgramStepInspireCache().isStepInspired(stepId as String) {
                        if let drive = program!["drive"] as? NSDictionary {
                            if let driveCategory = drive["driveCategory"] as? NSString {
                                
                                // For the running program, just point to the image picker.
                                if (driveCategory as String).compare("running") == NSComparisonResult.OrderedSame {
                                    presentViewController(imagePicker, animated: true, completion: nil)
                                    return
                                }
                                
                                // For the reading program, just point to the image picker.
                                if (driveCategory as String).compare("reading") == NSComparisonResult.OrderedSame {
                                    presentViewController(imagePicker, animated: true, completion: nil)
                                    return
                                }
                                
                                // For the unplugging program, point to the share emotions.
                                if (driveCategory as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                                    let dictionary = NSMutableDictionary()
                                    dictionary.setObject(stepId, forKey: "stepId")
                                    self.performSegueWithIdentifier("showSharingEmotionsChooseEmotionsFromCurrentHabit", sender: dictionary)
                                    return
                                }
                                
                                // For the reconnecting program, point to the share emotions.
                                if (driveCategory as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                                    let dictionary = NSMutableDictionary()
                                    dictionary.setObject(step, forKey: "step")
                                    self.performSegueWithIdentifier("showReconnectChooseActionFromCurrentHabit", sender: dictionary)
                                    return
                                }
                                
                                // For the exercising program, point to the share chart.
                                if (driveCategory as String).compare("exercising") == NSComparisonResult.OrderedSame {
                                    let dictionary = NSMutableDictionary()
                                    dictionary.setObject(step, forKey: "step")
                                    self.performSegueWithIdentifier("showPhysioInspireChartFromCurrentHabit", sender: dictionary)
                                    return
                                }
                            }
                        }
                    }
                }
                
                return
            }
        }
        
        if isCompleteNext(program!) {
            self.performSegueWithIdentifier("showChooseNewLevelFromCurrentHabit", sender: program)
            return
        }
    }
    
    static func setInspirationCache(cacheId: String, program: NSDictionary, step: NSDictionary, owner: NSDictionary) {
        
        // Owner
        
        var ownerId: String?
        
        if let ownerIdNs = owner["id"] as? NSString {
            ownerId = ownerIdNs as String
        }
        
        var ownerFirstName: String?
        
        if let ownerFirstNameNs = owner["firstName"] as? NSString {
            ownerFirstName = ownerFirstNameNs as String
        }
        
        var ownerLastName: String?
        
        if let ownerLastNameNs = owner["lastName"] as? NSString {
            ownerLastName = ownerLastNameNs as String
        }
        
        var ownerFacebookPhoto: String?
        
        if let ownerFacebookPhotoNs = owner["facebookPhoto"] as? NSString {
            ownerFacebookPhoto = ownerFacebookPhotoNs as String
        }
        
        // User subject
        
        var userSubjectId: String?
        
        if let userSubjectIdNs = owner["id"] as? NSString {
            userSubjectId = userSubjectIdNs as String
        }
        
        var userSubjectFirstName: String?
        
        if let userSubjectFirstNameNs = owner["firstName"] as? NSString {
            userSubjectFirstName = userSubjectFirstNameNs as String
        }
        
        var userSubjectLastName: String?
        
        if let userSubjectLastNameNs = owner["lastName"] as? NSString {
            userSubjectLastName = userSubjectLastNameNs as String
        }
        
        var userSubjectFacebookPhoto: String?
        
        if let userSubjectFacebookPhotoNs = owner["facebookPhoto"] as? NSString {
            userSubjectFacebookPhoto = userSubjectFacebookPhotoNs as String
        }
        
        var programStepObjectId: String?
        
        if let programStepObjectIdNs = step["id"] as? NSString {
            programStepObjectId = programStepObjectIdNs as String
        }
        
        var programStepObjectCaptured: Int?
        
        if let programStepObjectCapturedNs = step["captured"] as? NSNumber {
            programStepObjectCaptured = Int(programStepObjectCapturedNs)
        }
        
        var programObjectId: String?
        
        if let programObjectIdNs = program["id"] as? NSString {
            programObjectId = programObjectIdNs as String
        }
        
        var programObjectDriveId: String?
        var programObjectDriveCategoryName: String?
        var programObjectDriveSubcategoryName: String?
        var programObjectDriveTitle: String?
        var programObjectDriveStep: String?
        var programObjectDriveLevel: Int?
        
        if let drive = program["drive"] as? NSDictionary {
            if let programObjectDriveIdNs = drive["id"] as? NSString {
                programObjectDriveId = programObjectDriveIdNs as String
            }
            
            if let programObjectDriveCategoryNameNs = drive["categoryName"] as? NSString {
                programObjectDriveCategoryName = programObjectDriveCategoryNameNs as String
            }
            
            if let programObjectDriveSubcategoryNameNs = drive["subcategoryName"] as? NSString {
                programObjectDriveSubcategoryName = programObjectDriveSubcategoryNameNs as String
            }
            
            if let programObjectDriveTitleNs = drive["title"] as? NSString {
                programObjectDriveTitle = programObjectDriveTitleNs as String
            }
            
            if let programObjectDriveStepNs = drive["step"] as? NSString {
                programObjectDriveStep = programObjectDriveStepNs as String
            }
            
            if let programObjectDriveLevelNs = drive["level"] as? NSNumber {
                programObjectDriveLevel = Int(programObjectDriveLevelNs)
            }
        }
        
        if let stepId = step["id"] as? NSString {
            if let _ = InspirationCache().getCache(stepId as String) {
                return
            }
            
            InspirationCache().setCache(stepId as String, cacheId: cacheId, ownerId: ownerId, ownerFirstName: ownerFirstName, ownerLastName: ownerLastName, ownerFacebookPhoto: ownerFacebookPhoto, userSubjectId: userSubjectId, userSubjectFirstName: userSubjectFirstName, userSubjectLastName: userSubjectLastName, userSubjectFacebookPhoto: userSubjectFacebookPhoto, programStepObjectId: programStepObjectId, programStepObjectCaptured: programStepObjectCaptured, programObjectId: programObjectId, programObjectDriveId: programObjectDriveId, programObjectDriveCategoryName: programObjectDriveCategoryName, programObjectDriveSubcategoryName: programObjectDriveSubcategoryName, programObjectDriveTitle: programObjectDriveTitle, programObjectDriveStep: programObjectDriveStep, programObjectDriveLevel: programObjectDriveLevel)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if let step = self.getLastCompletedStep() {
            if let stepId = step["id"] as? NSString {
                if isPostStepComplete(step) && !isPostStepCaptured(step) {
                    if program != nil {
                        if let programId = program!["id"] as? NSString {
                            let dictionary = NSMutableDictionary()
                            dictionary.setObject(image, forKey: "image")
                            dictionary.setObject(stepId, forKey: "stepId")
                            dictionary.setObject(programId, forKey: "programId")
                            self.performSegueWithIdentifier("showCaptureMessageViewFromCurrentHabit", sender: dictionary)
                            return
                        }
                    }
                }
            }
        }
        
        Serv.showErrorPopup("Too late", message: "The image you were trying to post came too late.", okMessage: "Okay, got it.", controller: self, handler: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(CurrentHabitController)
        
        circleButton.layer.cornerRadius = 0.5 * circleButton.bounds.size.width
        actionButton.layer.cornerRadius = 0.5 * actionButton.bounds.size.width
        circleOverlay.layer.cornerRadius = 0.5 * circleOverlay.bounds.size.width
        buttonImage.layer.cornerRadius = 0.5 * buttonImage.bounds.size.width
        
        circle.progressThickness = 0.25
        circle.trackThickness = 0.25
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        todoLabel.preferredMaxLayoutWidth = todoLabel.frame.size.width
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
        }
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let viewController = self.progressDoneController {
            viewController.hideSilently()
        }
        
        for timerObject in currentHabitTimers {
            if let timer = timerObject as? NSTimer {
                timer.invalidate();
            }
        }
        
        currentHabitTimers = NSMutableArray()
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
        
        scheduleProgramSteps()
        refresh()
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(CurrentHabitController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(CurrentHabitController))
        }
    }
    
    func refresh() {
        if let controller = self.navigationController {
            controller.setNavigationBarHidden(false, animated: true)
        }
        
        if program == nil {
            return
        }
        
        if let programId = program!["id"] as? NSString {
            self.fetchPoints()
            
            Serv.showSpinner()
            
            HabitlabRestAPI.getProgram(programId as String, handler: { (error, programJson) -> () in
                Serv.hideSpinner()
                
                if let newProgram = programJson as? NSDictionary {
                    self.program = newProgram
                    
                    if let drive = self.program!["drive"] as? NSDictionary {
                        if let level = drive["level"] as? NSNumber {
                            self.fetchLevelPoints(level)
                        }
                        
                        if let title = drive["title"] as? NSString {
                            self.currentHabitCaption.title = (title as String).uppercaseString
                        }
                    }
                    
                    if let steps = self.program!["steps"] as? NSArray {
                        self.computeLevelCompleted(steps)
                    }
                    
                    if let drive = self.program!["drive"] as? NSDictionary {
                        if let cover = drive["cover"] as? NSString {
                            let url = Configuration.getEndpoint() + (cover as String)
                            
                            Serv.loadImage(url, handler: { (error, data) -> () in
                                if error != nil {
                                    print(error)
                                }
                                
                                if data != nil {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.backgroundImage.image = UIImage(data: data!)
                                    })
                                }
                            })
                        }
                    }
                    
                    self.handleProgramState()
                }
            })
        }
    }
    
    func getProgramStepStartTimer(uuid: String, step: NSDictionary) -> NSTimer? {
        let startDate: NSDate? = Serv.getStartDate(step)
        
        if startDate == nil {
            return nil
        }
        
        let now = NSDate()
        
        if startDate!.compare(now) == NSComparisonResult.OrderedAscending {
            return nil
        }
        
        return NSTimer(fireDate: startDate!, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramStepStart(_:)), userInfo: uuid, repeats: false)
    }
    
    func getProgramStepEndTimer(uuid: String, step: NSDictionary) -> NSTimer? {
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if endDate == nil {
            return nil
        }
            
        let now = NSDate()
            
        if endDate!.compare(now) == NSComparisonResult.OrderedAscending {
            return nil
        }
            
        return NSTimer(fireDate: endDate!, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramStepEnd(_:)), userInfo: uuid, repeats: false)
    }
    
    func getProgramStepHardEndTimer(uuid: String, step: NSDictionary) -> NSTimer? {
        let hardEndDate: NSDate? = Serv.getHardEndDate(step)
        
        if hardEndDate == nil {
            return nil
        }
        
        let now = NSDate()
        
        if hardEndDate!.compare(now) == NSComparisonResult.OrderedAscending {
            return nil
        }
        
        return NSTimer(fireDate: hardEndDate!, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramStepHardEnd(_:)), userInfo: uuid, repeats: false)
    }
    
    func getProgramStepWarning0Timer(uuid: String, step: NSDictionary) -> NSTimer? {
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return nil
        }
        
        let warning0Date = Serv.getWarning0Date(startDate!, endDate: endDate!)
        
        let now = NSDate()
        
        if warning0Date.compare(now) == NSComparisonResult.OrderedAscending {
            return nil
        }
        
        return NSTimer(fireDate: warning0Date, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramStepWarning0(_:)), userInfo: uuid, repeats: false)
    }
    
    func getProgramStepWarning1Timer(uuid: String, step: NSDictionary) -> NSTimer? {
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return nil
        }
        
        let warning1Date = Serv.getWarning1Date(startDate!, endDate: endDate!)
        
        let now = NSDate()
        
        if warning1Date.compare(now) == NSComparisonResult.OrderedAscending {
            return nil
        }
        
        return NSTimer(fireDate: warning1Date, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramStepWarning1(_:)), userInfo: uuid, repeats: false)
    }
    
    func getProgramStepWarning2Timer(uuid: String, step: NSDictionary) -> NSTimer? {
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return nil
        }
        
        let warning2Date = Serv.getWarning2Date(startDate!, endDate: endDate!)
        
        let now = NSDate()
        
        if warning2Date.compare(now) == NSComparisonResult.OrderedAscending {
            return nil
        }
        
        return NSTimer(fireDate: warning2Date, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramStepWarning2(_:)), userInfo: uuid, repeats: false)
    }
    
    func getProgramRefreshTimers(uuid: String, step: NSDictionary) -> NSArray {
        let arr: NSMutableArray = []
        
        if let doubleCheck = step["doubleCheck"] as? NSNumber {
            if doubleCheck == 1 {
                if let endDate = Serv.getEndDate(step) {
                    var seconds = 10
                    
                    while seconds <= 60 {
                        if let secondsEndDate = Serv.getDatePlusSeconds(endDate, seconds: seconds) {
                            let now = NSDate()
                        
                            if secondsEndDate.compare(now) == NSComparisonResult.OrderedAscending {
                                seconds += 10
                                continue
                            }
                            
                            let timer = NSTimer(fireDate: secondsEndDate, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramRefresh(_:)), userInfo: uuid, repeats: false)
                            
                            arr.addObject(timer)
                        }
                        
                        seconds += 10
                    }
                }
            }
        }
        
        if let hardEndDate = Serv.getHardEndDate(step) {
            var seconds = 10
            
            while seconds <= 60 {
                if let secondsHardEndDate = Serv.getDatePlusSeconds(hardEndDate, seconds: seconds) {
                    let now = NSDate()
                    
                    if secondsHardEndDate.compare(now) == NSComparisonResult.OrderedAscending {
                        seconds += 10
                        continue
                    }
                    
                    let timer = NSTimer(fireDate: secondsHardEndDate, interval: 0, target: self, selector: #selector(CurrentHabitController.handleProgramRefresh(_:)), userInfo: uuid, repeats: false)
                    
                    arr.addObject(timer)
                }
                
                seconds += 10
            }
        }
        
        return arr
    }
    
    func scheduleProgramSteps() {
        if program == nil {
            return
        }
        
        self.uuid = NSUUID().UUIDString
        
        if let steps = program!["steps"] as? NSArray {
            
            for i in 0 ..< steps.count {
                if let step = steps[i] as? NSDictionary {
                    self.scheduleStepTimers(uuid!, step: step)
                }
            }
        }
    }
    
    func scheduleStepTimers(uuid: String, step: NSDictionary) {
        if let startTimer = getProgramStepStartTimer(uuid, step: step) {
            currentHabitTimers.addObject(startTimer)
            NSRunLoop.currentRunLoop().addTimer(startTimer, forMode: NSDefaultRunLoopMode)
        }
        
        if let warning1Timer = getProgramStepWarning1Timer(uuid, step: step) {
            currentHabitTimers.addObject(warning1Timer)
            NSRunLoop.currentRunLoop().addTimer(warning1Timer, forMode: NSDefaultRunLoopMode)
        }
        
        if let warning2Timer = getProgramStepWarning2Timer(uuid, step: step) {
            currentHabitTimers.addObject(warning2Timer)
            NSRunLoop.currentRunLoop().addTimer(warning2Timer, forMode: NSDefaultRunLoopMode)
        }
        
        if let endTimer = getProgramStepEndTimer(uuid, step: step) {
            currentHabitTimers.addObject(endTimer)
            NSRunLoop.currentRunLoop().addTimer(endTimer, forMode: NSDefaultRunLoopMode)
        }
        
        for refreshTimer in getProgramRefreshTimers(uuid, step: step) {
            currentHabitTimers.addObject(refreshTimer)
            NSRunLoop.currentRunLoop().addTimer(refreshTimer as! NSTimer, forMode: NSDefaultRunLoopMode)
        }
    }
    
    func fetchPoints() {
        Serv.showSpinner()
        
        HabitlabRestAPI.getPoints({ (error, pointsJson) -> () in
            Serv.hideSpinner()
            
            if let points = pointsJson as? NSDictionary {
                if let total = points["total"] as? NSNumber {
                    self.totalPointsLabel.text = String(total)
                }
            }
        })
    }
    
    func fetchLevelPoints(level: NSNumber) {
        Serv.showSpinner()
        
        HabitlabRestAPI.getLevelPoints(level, handler: { (error, pointsJson) -> () in
            Serv.hideSpinner()
            
            if let points = pointsJson as? NSDictionary {
                if let total = points["total"] as? NSNumber {
                    self.levelPointsLabel.text = String(total)
                }
            }
        })
    }
    
    func computeLevelCompleted(steps: NSArray) {
        let total: Double = max(1, Double(steps.count))
        var completed: Double = 0
        
        for stepDict in steps {
            if let step = stepDict as? NSDictionary {
                if let complete = step["complete"] as? NSNumber {
                    completed += Double(complete)
                }
            }
        }
        
        let completedInt = Int(floor(100 * completed / total))
        self.levelCompletedLabel.text = "\(completedInt)%"
    }
    
    func isBetweenSoftAndHardEnd(step: NSDictionary) -> Bool {
        if isIrrelevantStep(step) {
            return false
        }
        
        let endDate: NSDate? = Serv.getEndDate(step)
        let hardEndDate: NSDate? = Serv.getHardEndDate(step)
        
        if endDate == nil || hardEndDate == nil {
            return false
        }
        
        let now = NSDate()
        return endDate!.compare(now) == NSComparisonResult.OrderedAscending && now.compare(hardEndDate!) == NSComparisonResult.OrderedAscending
    }
    
    func isWarning0(step: NSDictionary) -> Bool {
        if isIrrelevantStep(step) {
            return false
        }
        
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return false
        }
        
        let compareDate = Serv.getWarning0Date(startDate!, endDate: endDate!)
        let now = NSDate()
        
        return compareDate.compare(now) == NSComparisonResult.OrderedAscending
    }
    
    func isWarning1(step: NSDictionary) -> Bool {
        if isIrrelevantStep(step) {
            return false
        }
        
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return false
        }
        
        let compareDate = Serv.getWarning1Date(startDate!, endDate: endDate!)
        let now = NSDate()
        
        return compareDate.compare(now) == NSComparisonResult.OrderedAscending
    }

    func isWarning2(step: NSDictionary) -> Bool {
        if isIrrelevantStep(step) {
            return false
        }
        
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return false
        }
        
        let compareDate = Serv.getWarning2Date(startDate!, endDate: endDate!)
        let now = NSDate()
            
        return compareDate.compare(now) == NSComparisonResult.OrderedAscending
    }
    
    func isClosing(step: NSDictionary) -> Bool {
        if isIrrelevantStep(step) {
            return false
        }
        
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return false
        }
        
        let compareDate = Serv.getClosingDate(endDate!)
        let now = NSDate()
            
        return compareDate.compare(now) == NSComparisonResult.OrderedAscending
    }
    
    /*
    func isPostStepCheck(step: NSDictionary) -> Bool {
        var endDate: NSDate? = Serv.getEndDate(step)
     
        if endDate == nil {
            return false
        }
            
        let compareDate = Serv.getDatePlusSeconds(endDate!, seconds: 40)
            
        if compareDate == nil {
            return false
        }
            
        let now = NSDate()
        return compareDate!.compare(now) == NSComparisonResult.OrderedDescending
    }
    */
    
    func isPostStepComplete(step: NSDictionary) -> Bool {
        if let complete = step["complete"] as? NSNumber {
            if complete == 1 {
                var completeDate: NSDate? = nil
                
                if let completedAt = step["completedAt"] as? NSString {
                    completeDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
                }
                
                if let completedAt = step["completedAtTimeUTC"] as? NSNumber {
                    completeDate = Serv.getDateFromServerStringUTC(completedAt)
                }
                
                if completeDate == nil {
                    return false
                }
                
                let compareDate = Serv.getDatePlusSeconds(completeDate!, seconds: 120)
                
                if compareDate == nil {
                    return false
                }
                
                let now = NSDate()
                return now.compare(compareDate!) == NSComparisonResult.OrderedAscending
            }
        }
        
        return false
    }
    
    func isPostStepCaptured(step: NSDictionary) -> Bool {
        if let captured = step["captured"] as? NSNumber {
            if captured == 1 {
                return true
            }
        }
        
        return false
    }
    
    func isCompleteNext(program: NSDictionary) -> Bool {
        if let continued = program["continued"] as? NSNumber {
            if continued == 1 {
                return false
            }
            
            var completeDate: NSDate? = nil
            
            if let completedAt = program["completedAt"] as? NSString {
                completeDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
            }
            
            if let completedAt = program["completedAtTimeUTC"] as? NSNumber {
                completeDate = Serv.getDateFromServerStringUTC(completedAt)
            }
            
            if let discontinuedAt = program["discontinuedAt"] as? NSString {
                completeDate = Serv.getDateFromServerStringNonUTC(discontinuedAt as String)
            }
            
            if let discontinuedAt = program["discontinuedAtTimeUTC"] as? NSNumber {
                completeDate = Serv.getDateFromServerStringUTC(discontinuedAt)
            }
            
            if completeDate == nil {
                return false
            }
            
            let compareDate = Serv.getDatePlusHours(completeDate!, hours: 24)
            
            if compareDate == nil {
                return false
            }
            
            let now = NSDate()
            return now.compare(compareDate!) == NSComparisonResult.OrderedAscending
        }
        
        return false
    }
    
    func handleProgramState() {
        if program == nil {
            return
        }
        
        // Save the step complete if necessary
        if let step = getCurrentStep() {
            if let stepId = step["id"] as? NSString, stepComplete = step["complete"] as? NSNumber, drive = program!["drive"] as? NSDictionary {
                
                // If this step is marked as complete locally, but not on the server!
                if stepComplete == 0 && ProgramStepCompleteCache().isStepComplete(stepId as String) {
                    let cacheId = NSUUID().UUIDString
                    
                    if let owner = program!["owner"] as? NSDictionary {
                        CurrentHabitController.setInspirationCache(cacheId, program: self.program!, step: step, owner: owner)
                    }
                    
                    // We are dealing with an internal loop in the level, so the level is not complete yet.
                    self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Red
                    
                    if let week = step["week"] as? NSNumber, index = step["index"] as? NSNumber, countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                        let weekInt = Int(week)
                        let countWeeksInt = Int(countWeeks)
                        let indexInt = Int(index)
                        let countStepsPerWeekInt = Int(countStepsPerWeek)
                        
                        if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt - 1 {
                            
                            // We are dealing with the last loop in the level, so the level is complete!
                            self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Green
                            
                            if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                                let levelInt = Int(level)
                                let countLevels = Int(countLevels)
                                
                                if levelInt >= countLevels {
                                    
                                    // We are dealing with the last level in the program, so the program is complete!
                                    self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Blue
                                }
                            }
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.performSegueWithIdentifier("showProgressDoneFromCurrentHabit", sender: self)
                    })
                    
                    Serv.showSpinner()
                    
                    HabitlabRestAPI.putProgramStepComplete("Fallback step complete action", stepId: stepId as String, cacheId: cacheId, handler: { (error, stepJson) -> () in
                        Serv.hideSpinner()
                        
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                if let viewController = self.progressDoneController {
                                    viewController.hideSilently()
                                }
                            })
                            
                            return
                        }
                        
                        HabitsController.resetPrograms()
                        
                        self.circleTimer.invalidate()
                        
                        self.circle.animateToAngle(360, duration: 0.1, relativeDuration: true, completion: { (success) -> Void in
                            self.refresh()
                        })
                        
                        let friendlyMessages = ["Hooray!", "Well done!", "Rockstar!", "You rock!", "Way to go!", "Woohoo!", "You did it!", "One down!", "Alrighty!"]
                        let suggestionsFirst = ["Make this count even more", "Double the impact", "Spread the good vibes", "Make sure to celebrate"]
                        
                        // We are dealing with an internal loop in the level, so the level is not complete yet.
                        var doneStatus = "Loop complete"
                        var suggestionSecond = "by inspiring your friends!"
                        
                        if let week = step["week"] as? NSNumber, index = step["index"] as? NSNumber, countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                            let weekInt = Int(week)
                            let countWeeksInt = Int(countWeeks)
                            let indexInt = Int(index)
                            let countStepsPerWeekInt = Int(countStepsPerWeek)
                            
                            if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt - 1 {
                                
                                // We are dealing with the last loop in the level, so the level is complete!
                                doneStatus = "Level complete"
                                suggestionSecond = "by inspiring your friends!"
                                
                                if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                                    let levelInt = Int(level)
                                    let countLevels = Int(countLevels)
                                    
                                    if levelInt >= countLevels {
                                        
                                        // We are dealing with the last level in the program, so the program is complete!
                                        doneStatus = "Program complete"
                                        suggestionSecond = "by inspiring your friends!"
                                    }
                                }
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            if let viewController = self.progressDoneController {
                                viewController.setDoneCaptions(friendlyMessages, doneStatus: doneStatus, suggestionsFirst: suggestionsFirst, suggestionSecond: suggestionSecond)
                                viewController.markDone()
                            }
                            
                            self.circleTimer.invalidate()
                            
                            self.circle.animateToAngle(360, duration: 0.1, relativeDuration: true, completion: { (success) -> Void in
                                self.refresh()
                            })
                        })
                    })
                    
                    return
                }
            }
        }
        
        if let id = program!["id"] as? NSString {
            // Handle the volatile state(s) first.
            
            if let step = getLastCompletedStep() {
                if isPostStepComplete(step) && !isPostStepCaptured(step) {
                    if let stepId = step["id"] as? NSString {
                        if !ProgramStepInspireCache().isStepInspired(stepId as String) {
                            NSUserDefaults.standardUserDefaults().setObject(ProgramState.Capture.rawValue, forKey: "ProgramState\(id as String)")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            self.handleProgramCapture()
                            return
                        }
                    }
                }
            }
            
            if let complete = program!["complete"] as? NSNumber {
                if complete == 1 {
                    if isCompleteNext(program!) {
                        NSUserDefaults.standardUserDefaults().setObject(ProgramState.CompleteNext.rawValue, forKey: "ProgramState\(id as String)")
                    } else {
                        NSUserDefaults.standardUserDefaults().setObject(ProgramState.Complete.rawValue, forKey: "ProgramState\(id as String)")
                    }
                    
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            }
            
            if let quitted = program!["quitted"] as? NSNumber {
                if quitted == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(ProgramState.Quitted.rawValue, forKey: "ProgramState\(id as String)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            }
            
            if let forfeited = program!["forfeited"] as? NSNumber {
                if forfeited == 1 {
                    NSUserDefaults.standardUserDefaults().setObject(ProgramState.Forfeited.rawValue, forKey: "ProgramState\(id as String)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            }
            
            // Read the state
            
            if let stateString = NSUserDefaults.standardUserDefaults().objectForKey("ProgramState\(id as String)") as? NSString {
                if let state = ProgramState(rawValue: stateString as String) {
                    switch state {
                    case ProgramState.CompleteNext:
                        self.handleProgramCompleteNext()
                        return
                    case ProgramState.Complete:
                        self.handleProgramComplete()
                        return
                    case ProgramState.Forfeited:
                        self.handleProgramForfeited()
                        return
                    case ProgramState.Quitted:
                        self.handleProgramQuitted()
                        return
                    default: break
                        // Do nothing, this case will be handled below.
                    }
                }
            }
            
            if let step = getCurrentStep() {
                NSUserDefaults.standardUserDefaults().setObject(ProgramState.Progress.rawValue, forKey: "ProgramState\(id as String)")
                NSUserDefaults.standardUserDefaults().synchronize()
                
                if isWarning0(step) {
                    NSUserDefaults.standardUserDefaults().setObject(ProgramState.Warning0.rawValue, forKey: "ProgramState\(id as String)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
                
                if isWarning1(step) {
                    NSUserDefaults.standardUserDefaults().setObject(ProgramState.Warning1.rawValue, forKey: "ProgramState\(id as String)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
                
                if isWarning2(step) {
                    NSUserDefaults.standardUserDefaults().setObject(ProgramState.Warning2.rawValue, forKey: "ProgramState\(id as String)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
                
                if isClosing(step) {
                    NSUserDefaults.standardUserDefaults().setObject(ProgramState.Closing.rawValue, forKey: "ProgramState\(id as String)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
                
                if isBetweenSoftAndHardEnd(step) {
                    NSUserDefaults.standardUserDefaults().setObject(ProgramState.BetweenSoftHard.rawValue, forKey: "ProgramState\(id as String)")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }
            } else {
                NSUserDefaults.standardUserDefaults().setObject(ProgramState.Idle.rawValue, forKey: "ProgramState\(id as String)")
                NSUserDefaults.standardUserDefaults().synchronize()
                
                /*
                if let step = getPreviousStep() {
                    if isPostStepCheck(step) {
                        NSUserDefaults.standardUserDefaults().setObject(ProgramState.Processing.rawValue, forKey: "ProgramState\(id as String)")
                    }
                }
                */
            }
            
            if let stateString = NSUserDefaults.standardUserDefaults().objectForKey("ProgramState\(id as String)") as? NSString {
                if let state = ProgramState(rawValue: stateString as String) {
                    switch state {
                        
                    // Base states
                    case ProgramState.Idle:
                        self.handleProgramIdle()
                    case ProgramState.Progress:
                        self.handleProgramProgress()
                    case ProgramState.Warning0:
                        self.handleProgramWarning0()
                    case ProgramState.Warning1:
                        self.handleProgramWarning1()
                    case ProgramState.Warning2:
                        self.handleProgramWarning2()
                    case ProgramState.BetweenSoftHard:
                        self.handleProgramBetweenSoftHard()
                    case ProgramState.Closing:
                        self.handleProgramClosing()
                    // case ProgramState.Processing:
                    //    self.handleProgramProcessing()
                    
                    default: break
                        // Do nothing, this case has been handled above.
                    }
                }
            }
        }
    }

    // MARK: Step information getters
    
    func isIrrelevantStep(step: NSDictionary) -> Bool {
        if let complete = step["complete"] as? NSNumber {
            if complete == 1 {
                return true
            }
        }
        
        if let fuckup = step["fuckup"] as? NSNumber {
            if fuckup == 1 {
                return true
            }
        }
        
        if let quitted = step["quitted"] as? NSNumber {
            if quitted == 1 {
                return true
            }
        }
        
        return false
    }
    
    func isIrrelevantProgram(program: NSDictionary) -> Bool {
        if let complete = program["complete"] as? NSNumber {
            if complete == 1 {
                return true
            }
        }
        
        if let forfeited = program["forfeited"] as? NSNumber {
            if forfeited == 1 {
                return true
            }
        }
        
        if let quitted = program["quitted"] as? NSNumber {
            if quitted == 1 {
                return true
            }
        }
        
        return false
    }
    
    func getPreviousStep() -> NSDictionary? {
        if program == nil {
            return nil
        }
        
        var returnStep: NSDictionary? = nil
        
        if let steps = program!["steps"] as? NSArray {
            for stepObject in steps {
                if let step = stepObject as? NSDictionary {
                    if let _ = step["completedAt"] as? NSString {
                        var completeDate: NSDate? = nil
                        
                        if let completedAt = step["completedAt"] as? NSString {
                            completeDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
                        }
                        
                        if let completedAt = step["completedAtTimeUTC"] as? NSNumber {
                            completeDate = Serv.getDateFromServerStringUTC(completedAt)
                        }
                        
                        if completeDate == nil {
                            continue
                        }
                        
                        let now = NSDate()
                        
                        if completeDate!.compare(now) == NSComparisonResult.OrderedAscending {
                            returnStep = step
                        }
                    } else {
                        let hardEndDate: NSDate? = Serv.getHardEndDate(step)
        
                        if hardEndDate != nil {
                            let now = NSDate()
                            
                            if hardEndDate!.compare(now) == NSComparisonResult.OrderedAscending {
                                returnStep = step
                            }
                        }
                    }
                }
            }
        }
        
        return returnStep
    }
    
    func getCurrentStep() -> NSDictionary? {
        if program == nil {
            return nil
        }
        
        if let steps = program!["steps"] as? NSArray {
            for stepObject in steps {
                if let step = stepObject as? NSDictionary {
                    if isIrrelevantStep(step) {
                        continue
                    }
                    
                    let startDate: NSDate? = Serv.getStartDate(step)
                    let hardEndDate: NSDate? = Serv.getHardEndDate(step)
                    
                    if startDate == nil || hardEndDate == nil {
                        continue
                    }
                    
                    let now = NSDate()
                        
                    if startDate!.compare(now) == NSComparisonResult.OrderedAscending && now.compare(hardEndDate!) == NSComparisonResult.OrderedAscending {
                        return step
                    }
                }
            }
        }
        
        return nil
    }
    
    func getNextStep() -> NSDictionary? {
        if program == nil {
            return nil
        }
        
        if let steps = program!["steps"] as? NSArray {
            for stepObject in steps {
                if let step = stepObject as? NSDictionary {
                    if isIrrelevantStep(step) {
                        continue
                    }
                    
                    var startDate: NSDate? = nil
                    
                    if let start = step["start"] as? NSString {
                        startDate = Serv.getDateFromServerStringNonUTC(start as String)
                    }
                    
                    if let start = step["startTimeUTC"] as? NSNumber {
                        startDate = Serv.getDateFromServerStringUTC(start)
                    }
                    
                    if startDate == nil {
                        continue
                    }
                    
                    let now = NSDate()
                    
                    if now.compare(startDate!) == NSComparisonResult.OrderedAscending {
                        return step
                    }
                }
            }
        }
        
        return nil
    }
    
    func getLastCompletedStep() -> NSDictionary? {
        if program == nil {
            return nil
        }
        
        var returnStep: NSDictionary? = nil
        
        if let steps = program!["steps"] as? NSArray {
            for stepObject in steps {
                if let step = stepObject as? NSDictionary {
                    if let complete = step["complete"] as? NSNumber {
                        if complete != 1 {
                            // Do nothing
                        } else {
                            returnStep = step
                        }
                    }
                }
            }
        }
        
        return returnStep
    }
    
    func getTimeInterval(startDate: NSDate, endDate: NSDate) -> NSDateComponents? {
        if startDate.compare(endDate) == NSComparisonResult.OrderedDescending {
            return nil
        }
        
        if let calendar = Serv.calendar {
            return calendar.components(NSCalendarUnit.Second, fromDate: startDate, toDate: endDate, options: NSCalendarOptions.MatchFirst)
        }
        
        return nil
    }

    // MARK: Circle and timer progress setters
    
    func setCircleCompleteNext(program: NSDictionary) {
        var completeDate: NSDate? = nil
        
        if let completedAt = program["completedAt"] as? NSString {
            completeDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
        }
        
        if let completedAt = program["completedAtTimeUTC"] as? NSNumber {
            completeDate = Serv.getDateFromServerStringUTC(completedAt)
        }
        
        if completeDate == nil {
            return
        }
        
        let endDate = Serv.getDatePlusDays(completeDate!, days: 1)
        
        if endDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(endDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let totalInterval = max(1, abs(completeDate!.timeIntervalSinceDate(endDate!)))
        let remainingInterval = abs(now.timeIntervalSinceDate(endDate!))
        
        let progress = NSInteger(360 * remainingInterval / totalInterval)
        circle.animateToAngle(progress, duration: 0.1, relativeDuration: true, completion: nil)
    }
    
    func setCircleProgress(step: NSDictionary) {
        let startDate: NSDate? = Serv.getStartDate(step)
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if startDate == nil || endDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(endDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let totalInterval = max(1, abs(startDate!.timeIntervalSinceDate(endDate!)))
        let remainingInterval = abs(now.timeIntervalSinceDate(endDate!))
        
        let progress = NSInteger(360 * remainingInterval / totalInterval)
        circle.animateToAngle(progress, duration: 0.1, relativeDuration: true, completion: nil)
    }
    
    func setCircleProgressExtended(step: NSDictionary) {
        let startDate: NSDate? = Serv.getStartDate(step)
        let hardEndDate: NSDate? = Serv.getHardEndDate(step)
        
        if startDate == nil || hardEndDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(hardEndDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let totalInterval = max(1, abs(startDate!.timeIntervalSinceDate(hardEndDate!)))
        let remainingInterval = abs(now.timeIntervalSinceDate(hardEndDate!))
        
        let progress = NSInteger(360 * remainingInterval / totalInterval)
        circle.animateToAngle(progress, duration: 0.1, relativeDuration: true, completion: nil)
    }
    
    func setCircleCapture(step: NSDictionary) {
        var completeDate: NSDate? = nil
        
        if let completedAt = step["completedAt"] as? NSString {
            completeDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
        }
        
        if let completedAt = step["completedAtTimeUTC"] as? NSNumber {
            completeDate = Serv.getDateFromServerStringUTC(completedAt)
        }
        
        if completeDate == nil {
            return
        }
        
        let compareDate = Serv.getDatePlusSeconds(completeDate!, seconds: 120)
        
        if compareDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(compareDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let totalInterval = max(1, abs(completeDate!.timeIntervalSinceDate(compareDate!)))
        let remainingInterval = abs(now.timeIntervalSinceDate(compareDate!))
        
        let progress = NSInteger(360 * remainingInterval / totalInterval)
        circle.animateToAngle(progress, duration: 0.1, relativeDuration: true, completion: nil)
    }
    
    func setProgramCompleteNextCountdownValue(program: NSDictionary) {
        var completeDate: NSDate? = nil
        
        if let completedAt = program["completedAt"] as? NSString {
            completeDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
        }
        
        if let completedAt = program["completedAtTimeUTC"] as? NSNumber {
            completeDate = Serv.getDateFromServerStringUTC(completedAt)
        }
        
        if completeDate == nil {
            return
        }
        
        let endDate = Serv.getDatePlusHours(completeDate!, hours: 24)
        
        if endDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(endDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let remainingInterval = endDate!.timeIntervalSinceDate(now)
        timerLabel.text = Serv.getTimeLabelString(remainingInterval, enforceDays: false, enforceHours: true)
    }
    
    func setCurrentStepProgressCountdownValue(step: NSDictionary) {
        let endDate: NSDate? = Serv.getEndDate(step)
        
        if endDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(endDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let remainingInterval = endDate!.timeIntervalSinceDate(now)
        timerLabel.text = Serv.getTimeLabelString(remainingInterval, enforceDays: false, enforceHours: true)
    }
    
    func setCurrentStepProgressExtendedCountdownValue(step: NSDictionary) {
        let hardEndDate: NSDate? = Serv.getHardEndDate(step)
        
        if hardEndDate == nil {
            return
        }
        
        let now = NSDate()
        
        if now.compare(hardEndDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let remainingInterval = hardEndDate!.timeIntervalSinceDate(now)
        timerLabel.text = Serv.getTimeLabelString(remainingInterval, enforceDays: false, enforceHours: true)
    }
    
    func setCaptureStepCaptureCountdownValue(step: NSDictionary) {
        var completeDate: NSDate? = nil
        
        if let completedAt = step["completedAt"] as? NSString {
            completeDate = Serv.getDateFromServerStringNonUTC(completedAt as String)
        }
        
        if let completedAt = step["completedAtTimeUTC"] as? NSNumber {
            completeDate = Serv.getDateFromServerStringUTC(completedAt)
        }
        
        if completeDate == nil {
            return
        }
        
        let compareDate = Serv.getDatePlusSeconds(completeDate!, seconds: 120)
        
        if compareDate == nil {
            return
        }
        
        let now = NSDate()
        
        let remainingInterval = compareDate!.timeIntervalSinceDate(now)
        timerLabel.text = Serv.getTimeLabelString(remainingInterval, enforceDays: false, enforceHours: true)
    }
    
    func setNextStepCountdownValue(step: NSDictionary) {
        var startDate: NSDate? = nil
        
        if let start = step["start"] as? NSString {
            startDate = Serv.getDateFromServerStringNonUTC(start as String)
        }
        
        if let start = step["startTimeUTC"] as? NSNumber {
            startDate = Serv.getDateFromServerStringUTC(start)
        }
    
        if startDate == nil {
            return
        }
    
        let now = NSDate()
    
        if now.compare(startDate!) == NSComparisonResult.OrderedDescending {
            return
        }
        
        let remainingInterval = startDate!.timeIntervalSinceDate(now)
        timerLabel.text = Serv.getTimeLabelString(remainingInterval, enforceDays: false, enforceHours: true)
    }
    
    func updateProgramCompleteNextCountdown(timer: NSTimer) {
        if let program = timer.userInfo as? NSDictionary {
            setCircleCompleteNext(program)
            setProgramCompleteNextCountdownValue(program)
        }
    }

    func updateProgramProgressCountdown(timer: NSTimer) {
        if let step = timer.userInfo as? NSDictionary {
            setCircleProgress(step)
            setCurrentStepProgressCountdownValue(step)
        }
    }
    
    func updateProgramBetweenSoftHardCountdown(timer: NSTimer) {
        if let step = timer.userInfo as? NSDictionary {
            setCircleProgressExtended(step)
            setCurrentStepProgressExtendedCountdownValue(step)
        }
    }
    
    func updateProgramCaptureCountdown(timer: NSTimer) {
        if let step = timer.userInfo as? NSDictionary {
            setCircleCapture(step)
            setCaptureStepCaptureCountdownValue(step)
        }
    }

    // MARK: Program handlers inside
    
    func handleProgramIdle() {
        if program == nil {
            return
        }
        
        circleTimer.invalidate()
        circle.animateToAngle(0, duration: 0.1, relativeDuration: true, completion: nil)
        
        // Set no points
        self.pointsLabel.text = ""
        self.todoLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = ""
        self.timerLabel.text = ""
        
        circleButton.enabled = false
        actionButton.enabled = false
        
        var imageNamed = "CTACheckmarkRed"
        
        if let step = getNextStep() {
            if let drive = program!["drive"] as? NSDictionary {
                if let category = drive["driveCategory"] as? NSString {
                    if (category as String).compare("running") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                }
                
                if let instructionString = drive["instruction"] as? NSString {
                    self.todoLabel.text = instructionString as String
                } else if let activityString = drive["step"] as? NSString {
                    self.todoLabel.text = activityString as String
                }
                
                var deadlineText = ""
                
                if let timeOfDayString = step["timeOfDay"] as? NSString {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString) {
                        if let humanDate = Serv.getHumanDate(step, state: ProgramState.Idle, timeOfDay: timeOfDay) {
                            deadlineText = humanDate
                        }
                    }
                }
                
                if deadlineText == "" {
                    if let humanDate = Serv.getHumanDate(step, state: ProgramState.Idle, timeOfDay: nil) {
                        deadlineText = humanDate
                    }
                }
                
                self.deadlineLabel.text = deadlineText
            }
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 0.5
    }

    func handleProgramProgress() {
        if program == nil {
            return
        }
        
        // Set points
        self.pointsLabel.text = "1 POINT"
        self.todoLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = ""
        self.timerLabel.text = ""
        
        circleButton.enabled = true
        actionButton.enabled = true

        var imageNamed = "CTACheckmarkRed"
        
        if let step = getCurrentStep() {
            if let drive = program!["drive"] as? NSDictionary {
                if let category = drive["driveCategory"] as? NSString {
                    if (category as String).compare("running") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                }
                
                if let instructionString = drive["instruction"] as? NSString {
                    self.todoLabel.text = instructionString as String
                } else if let activityString = drive["step"] as? NSString {
                    self.todoLabel.text = activityString as String
                }
                
                var deadlineText = ""
                
                if let timeOfDayString = step["timeOfDay"] as? NSString {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString as String) {
                        if let humanDate = Serv.getHumanDate(step, state: ProgramState.Progress, timeOfDay: timeOfDay) {
                            deadlineText = humanDate
                        }
                    }
                }
                
                if deadlineText == "" {
                    if let humanDate = Serv.getHumanDate(step, state: ProgramState.Progress, timeOfDay: nil) {
                        deadlineText = humanDate
                    }
                }
                
                self.deadlineLabel.text = deadlineText
            }
            
            circleTimer.invalidate()
            circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramProgressCountdown(_:)), userInfo: step, repeats: true)
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 1.0
    }
    
    func handleProgramWarning0() {
        if program == nil {
            return
        }
        
        // Set points
        self.pointsLabel.text = "1 POINT"
        self.todoLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = ""
        self.timerLabel.text = ""
        
        circleButton.enabled = true
        actionButton.enabled = true
        
        var imageNamed = "CTACheckmarkRed"
        
        if let step = getCurrentStep() {
            if let drive = program!["drive"] as? NSDictionary {
                if let category = drive["driveCategory"] as? NSString {
                    if (category as String).compare("running") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                }
                
                if let instructionString = drive["instruction"] as? NSString {
                    self.todoLabel.text = instructionString as String
                } else if let activityString = drive["step"] as? NSString {
                    self.todoLabel.text = activityString as String
                }
                
                var deadlineText = ""
                
                if let timeOfDayString = step["timeOfDay"] as? NSString {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString as String) {
                        if let humanDate = Serv.getHumanDate(step, state: ProgramState.Warning0, timeOfDay: timeOfDay) {
                            deadlineText = humanDate
                        }
                    }
                }
                
                if deadlineText == "" {
                    if let humanDate = Serv.getHumanDate(step, state: ProgramState.Warning0, timeOfDay: nil) {
                        deadlineText = humanDate
                    }
                }
                
                self.deadlineLabel.text = deadlineText
            }
            
            circleTimer.invalidate()
            circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramProgressCountdown(_:)), userInfo: step, repeats: true)
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 1.0
    }
    
    func handleProgramWarning1() {
        if program == nil {
            return
        }
        
        // Set points
        self.pointsLabel.text = "1 POINT"
        self.todoLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = ""
        self.timerLabel.text = ""
        
        circleButton.enabled = true
        actionButton.enabled = true
        
        var imageNamed = "CTACheckmarkRed"
        
        if let step = getCurrentStep() {
            if let drive = program!["drive"] as? NSDictionary {
                if let category = drive["driveCategory"] as? NSString {
                    if (category as String).compare("running") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                }
                
                if let instructionString = drive["instruction"] as? NSString {
                    self.todoLabel.text = instructionString as String
                } else if let activityString = drive["step"] as? NSString {
                    self.todoLabel.text = activityString as String
                }
                
                var deadlineText = ""
                
                if let timeOfDayString = step["timeOfDay"] as? NSString {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString as String) {
                        if let humanDate = Serv.getHumanDate(step, state: ProgramState.Warning1, timeOfDay: timeOfDay) {
                            deadlineText = humanDate
                        }
                    }
                }
                
                if deadlineText == "" {
                    if let humanDate = Serv.getHumanDate(step, state: ProgramState.Warning1, timeOfDay: nil) {
                        deadlineText = humanDate
                    }
                }
                
                self.deadlineLabel.text = deadlineText
            }
            
            circleTimer.invalidate()
            circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramProgressCountdown(_:)), userInfo: step, repeats: true)
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 1.0
    }
    
    func handleProgramWarning2() {
        if program == nil {
            return
        }
        
        // Set points
        self.pointsLabel.text = "1 POINT"
        self.todoLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = ""
        self.timerLabel.text = ""
        
        circleButton.enabled = true
        actionButton.enabled = true
        
        var imageNamed = "CTACheckmarkRed"
        
        if let step = getCurrentStep() {
            if let drive = program!["drive"] as? NSDictionary {
                if let category = drive["driveCategory"] as? NSString {
                    if (category as String).compare("running") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                }
                
                if let instructionString = drive["instruction"] as? NSString {
                    self.todoLabel.text = instructionString as String
                } else if let activityString = drive["step"] as? NSString {
                    self.todoLabel.text = activityString as String
                }
                
                var deadlineText = ""
                
                if let timeOfDayString = step["timeOfDay"] as? NSString {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString as String) {
                        if let humanDate = Serv.getHumanDate(step, state: ProgramState.Warning2, timeOfDay: timeOfDay) {
                            deadlineText = humanDate
                        }
                    }
                }
                
                if deadlineText == "" {
                    if let humanDate = Serv.getHumanDate(step, state: ProgramState.Warning2, timeOfDay: nil) {
                        deadlineText = humanDate
                    }
                }
                
                self.deadlineLabel.text = deadlineText
            }
            
            circleTimer.invalidate()
            circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramProgressCountdown(_:)), userInfo: step, repeats: true)
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 1.0
    }
    
    func handleProgramClosing() {
        if program == nil {
            return
        }
        
        // Set points
        self.pointsLabel.text = "1 POINT"
        self.todoLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = ""
        self.timerLabel.text = ""
        
        circleButton.enabled = true
        actionButton.enabled = true
        
        var imageNamed = "CTACheckmarkRed"
        
        if let step = getCurrentStep() {
            if let drive = program!["drive"] as? NSDictionary {
                if let category = drive["driveCategory"] as? NSString {
                    if (category as String).compare("running") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                }
                
                if let instructionString = drive["instruction"] as? NSString {
                    self.todoLabel.text = instructionString as String
                } else if let activityString = drive["step"] as? NSString {
                    self.todoLabel.text = activityString as String
                }
                
                var deadlineText = ""
                
                if let timeOfDayString = step["timeOfDay"] as? NSString {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString as String) {
                        if let humanDate = Serv.getHumanDate(step, state: ProgramState.Closing, timeOfDay: timeOfDay) {
                            deadlineText = humanDate
                        }
                    }
                }
                
                if deadlineText == "" {
                    if let humanDate = Serv.getHumanDate(step, state: ProgramState.Closing, timeOfDay: nil) {
                        deadlineText = humanDate
                    }
                }
                
                self.deadlineLabel.text = deadlineText
            }
            
            circleTimer.invalidate()
            circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramProgressCountdown(_:)), userInfo: step, repeats: true)
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 1.0
    }
    
    func handleProgramBetweenSoftHard() {
        if program == nil {
            return
        }
        
        // Set points
        self.pointsLabel.text = "1 POINT"
        self.todoLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = ""
        self.timerLabel.text = ""
        
        circleButton.enabled = true
        actionButton.enabled = true
        
        var imageNamed = "CTACheckmarkRed"
        
        if let step = getCurrentStep() {
            if let drive = program!["drive"] as? NSDictionary {
                if let category = drive["driveCategory"] as? NSString {
                    if (category as String).compare("running") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }

                    if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                    
                    if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                        imageNamed = "CTAHabitSwitchRed"
                    }
                }
                
                if let instructionString = drive["instruction"] as? NSString {
                    self.todoLabel.text = instructionString as String
                } else if let activityString = drive["step"] as? NSString {
                    self.todoLabel.text = activityString as String
                }
                
                var deadlineText = ""
                
                if let timeOfDayString = step["timeOfDay"] as? NSString {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString as String) {
                        if let humanDate = Serv.getHumanDate(step, state: ProgramState.BetweenSoftHard, timeOfDay: timeOfDay) {
                            deadlineText = humanDate
                        }
                    }
                }
                
                if deadlineText == "" {
                    if let humanDate = Serv.getHumanDate(step, state: ProgramState.BetweenSoftHard, timeOfDay: nil) {
                        deadlineText = humanDate
                    }
                }
                
                self.deadlineLabel.text = deadlineText
            }
            
            circleTimer.invalidate()
            circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramBetweenSoftHardCountdown(_:)), userInfo: step, repeats: true)
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 1.0
    }
    
    func handleProgramProcessing() {
        if program == nil {
            return
        }
        
        circleButton.enabled = false
        actionButton.enabled = false

        buttonImage.image = UIImage(named: "CTACheckmarkRed")
        buttonImage.alpha = 0.5
        
        // Set points
        self.pointsLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = "Processing"
        self.timerLabel.text = ""
        self.todoLabel.text = ""
        
        circleTimer.invalidate()
        circle.animateToAngle(0, duration: 0.1, relativeDuration: true, completion: nil)
    }
    
    func handleProgramCompleteNext() {
        if program == nil {
            return
        }
        
        HabitsController.resetPrograms()
        
        self.pointsLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = "Schedule Next Level"
        self.timerLabel.text = ""
        self.todoLabel.text = ""
        
        circle.animateToAngle(0, duration: 0.1, relativeDuration: true, completion: nil)
        
        circleButton.enabled = true
        actionButton.enabled = true
        
        buttonImage.image = UIImage(named: "CTACalendarRed")
        buttonImage.alpha = 1.0
        
        circleTimer.invalidate()
        circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramCompleteNextCountdown(_:)), userInfo: program, repeats: true)
    }
    
    func handleProgramComplete() {
        if program == nil {
            return
        }
        
        HabitsController.resetPrograms()
        
        self.pointsLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = "Level Complete"
        self.timerLabel.text = ""
        self.todoLabel.text = ""
        
        circle.animateToAngle(0, duration: 0.1, relativeDuration: true, completion: nil)
        
        actionButton.enabled = false
        circleButton.enabled = false
        
        buttonImage.image = UIImage(named: "CTACheckmarkRed")
        buttonImage.alpha = 0.5
        
        circleTimer.invalidate()
    }
    
    func handleProgramForfeited() {
        if program == nil {
            return
        }
        
        HabitsController.resetPrograms()
        
        circle.animateToAngle(0, duration: 0.1, relativeDuration: true, completion: nil)
        
        self.pointsLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = "Level Failed"
        self.timerLabel.text = ""
        self.todoLabel.text = ""
        
        actionButton.enabled = false
        circleButton.enabled = false
        
        buttonImage.image = UIImage(named: "CTACheckmarkRed")
        buttonImage.alpha = 0.5
        
        circleTimer.invalidate()
    }
    
    func handleProgramQuitted() {
        if program == nil {
            return
        }
        
        HabitsController.resetPrograms()
        
        circle.animateToAngle(0, duration: 0.1, relativeDuration: true, completion: nil)
        
        self.pointsLabel.text = ""
        self.deadlineLabel.text = ""
        self.infoLabel.text = "Level Quit"
        self.timerLabel.text = ""
        self.todoLabel.text = ""
        
        actionButton.enabled = false
        circleButton.enabled = false
        
        buttonImage.image = UIImage(named: "CTACheckmarkRed")
        buttonImage.alpha = 0.5
        
        circleTimer.invalidate()
    }
    
    func handleProgramCapture() {
        if program == nil {
            return
        }
        
        circleTimer.invalidate()
        
        if let step = getPreviousStep() {
            circleTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(CurrentHabitController.updateProgramCaptureCountdown(_:)), userInfo: step, repeats: true)
        }
        
        var infoLabelText = "Inspire Your Friends"
        var pointsLabelText = "3 POINTS"
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let driveCategory = drive["category"] as? NSDictionary, driveSubcategory = drive["subcategory"] as? NSDictionary {
                    if let driveCategoryStr = driveCategory["str"] as? NSString, driveSubcategoryStr = driveSubcategory["str"] as? NSString {
                        if driveCategoryStr.compare("exercising") == NSComparisonResult.OrderedSame && driveSubcategoryStr.compare("physio") == NSComparisonResult.OrderedSame {
                            infoLabelText = "See your progress"
                            pointsLabelText = ""
                        }
                    }
                }
            }
        }
        
        self.infoLabel.text = infoLabelText
        self.pointsLabel.text = pointsLabelText
        self.deadlineLabel.text = ""
        self.timerLabel.text = ""
        self.todoLabel.text = ""
        
        actionButton.enabled = true
        circleButton.enabled = true
        
        var imageNamed = "CTACameraRed"
        
        if let drive = program!["drive"] as? NSDictionary {
            if let category = drive["driveCategory"] as? NSString {
                if (category as String).compare("unplugging") == NSComparisonResult.OrderedSame {
                    imageNamed = "CTAEmojiRed"
                }

                if (category as String).compare("reconnecting") == NSComparisonResult.OrderedSame {
                    imageNamed = "CTAEmojiRed"
                }
                
                if (category as String).compare("exercising") == NSComparisonResult.OrderedSame {
                    imageNamed = "CTAEmojiRed"
                }
            }
        }
        
        buttonImage.image = UIImage(named: imageNamed)
        buttonImage.alpha = 1.0
    }

    // Handle states coming from timers

    func handleProgramStepStart(timer: NSTimer) {
        if uuid == nil {
            return
        }
        
        if let timerUuid = timer.userInfo as? NSString {
            if (timerUuid as String).compare(uuid!) != NSComparisonResult.OrderedSame {
                return
            }
        }
        
        refresh()
    }
    
    func handleProgramStepHardEnd(timer: NSTimer) {
        if uuid == nil {
            return
        }
        
        if let timerUuid = timer.userInfo as? NSString {
            if (timerUuid as String).compare(uuid!) != NSComparisonResult.OrderedSame {
                return
            }
        }
        
        refresh()
    }
    
    func handleProgramStepWarning0(timer: NSTimer) {
        if uuid == nil {
            return
        }
        
        if let timerUuid = timer.userInfo as? NSString {
            if (timerUuid as String).compare(uuid!) != NSComparisonResult.OrderedSame {
                return
            }
        }
        
        refresh()
    }
    
    func handleProgramStepWarning1(timer: NSTimer) {
        if uuid == nil {
            return
        }
        
        if let timerUuid = timer.userInfo as? NSString {
            if (timerUuid as String).compare(uuid!) != NSComparisonResult.OrderedSame {
                return
            }
        }
        
        refresh()
    }
    
    func handleProgramStepWarning2(timer: NSTimer) {
        if uuid == nil {
            return
        }
        
        if let timerUuid = timer.userInfo as? NSString {
            if (timerUuid as String).compare(uuid!) != NSComparisonResult.OrderedSame {
                return
            }
        }
        
        refresh()
    }
    
    func handleProgramStepEnd(timer: NSTimer) {
        if uuid == nil {
            return
        }
        
        if let timerUuid = timer.userInfo as? NSString {
            if (timerUuid as String).compare(uuid!) != NSComparisonResult.OrderedSame {
                return
            }
        }
        
        refresh()
    }
    
    func handleProgramRefresh(timer: NSTimer) {
        if uuid == nil {
            return
        }
        
        if let timerUuid = timer.userInfo as? NSString {
            if (timerUuid as String).compare(uuid!) != NSComparisonResult.OrderedSame {
                return
            }
        }
        
        refresh()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showCaptureMessageViewFromCurrentHabit") {
            if let viewController = segue.destinationViewController as? CaptureMessageViewController {
                if let object = sender as? NSDictionary {
                    if let image = object["image"] as? UIImage, stepId = object["stepId"] as? NSString, programId = object["programId"] as? NSString {
                        viewController.image = image
                        viewController.stepId = stepId as String
                        viewController.programId = programId as String
                    }
                }
            }
        }
        
        if (segue.identifier == "showSharingEmotionsChooseEmotionsFromCurrentHabit") {
            if let viewController = segue.destinationViewController as? SharingEmotionsChooseEmotionsController {
                if let object = sender as? NSDictionary {
                    if let stepId = object["stepId"] as? NSString {
                        viewController.stepId = stepId as String
                    }
                }
            }
        }
        
        if (segue.identifier == "showReconnectChooseActionFromCurrentHabit") {
            if let viewController = segue.destinationViewController as? ReconnectChooseActionController {
                if let object = sender as? NSDictionary {
                    if let step = object["step"] as? NSDictionary {
                        if let stepId = step["id"] as? NSString {
                            if let contact = ProgramStepCompleteReconnectContactCache().getStepCompleteReconnectContact(stepId as String) {
                                viewController.step = step
                                viewController.program = program
                                viewController.contact = contact
                            }
                        }
                    }
                }
            }
        }
        
        if segue.identifier == "showChooseNewLevelFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? ChooseNewLevelController {
                if program != nil {
                    if let drive = program!["drive"] as? NSDictionary {
                        if let category = drive["category"] as? NSDictionary, subcategory = drive["subcategory"] as? NSDictionary {
                            viewController.category = category
                            viewController.subcategory = subcategory
                        }
                    }
                }
            }
        }
        
        if segue.identifier == "showRunningModuleFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? RunningModuleController {
                if let object = sender as? NSDictionary {
                    if let program = object["program"] as? NSDictionary, step = object["step"] as? NSDictionary {
                        viewController.program = program
                        viewController.step = step
                    }
                }
            }
        }
        
        if segue.identifier == "showChoosePhysioPainFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? ChoosePhysioPainController {
                if let object = sender as? NSDictionary {
                    if let program = object["program"] as? NSDictionary, step = object["step"] as? NSDictionary {
                        viewController.program = program
                        viewController.step = step
                        viewController.painTiming = ChoosePhysioPainTiming.BeforeExercising
                    }
                }
            }
        }
        
        if segue.identifier == "showPhysioInspireChartFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? PhysioInspireChartController {
                if let realProgram = self.program {
                    viewController.program = realProgram
                }
            }
        }
        
        if segue.identifier == "showReconnectingModuleChooseContactFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? ReconnectModuleChooseContactController {
                if let object = sender as? NSDictionary {
                    if let program = object["program"] as? NSDictionary, step = object["step"] as? NSDictionary {
                        viewController.program = program
                        viewController.step = step
                    }
                }
            }
        }
        
        if segue.identifier == "showUnpluggingModuleFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? UnpluggingModuleController {
                if let object = sender as? NSDictionary {
                    if let program = object["program"] as? NSDictionary, step = object["step"] as? NSDictionary {
                        viewController.program = program
                        viewController.step = step
                    }
                }
            }
        }
        
        if segue.identifier == "showPlanUnpluggingFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? PlanUnpluggingController {
                if let object = sender as? NSDictionary {
                    if let program = object["program"] as? NSDictionary, step = object["step"] as? NSDictionary {
                        viewController.program = program
                        viewController.step = step
                    }
                }
            }
        }
        
        if segue.identifier == "showProgressDoneFromCurrentHabit" {
            if let viewController = segue.destinationViewController as? ProgressDoneController {
                self.progressDoneController = viewController
                viewController.setDelegate(self)
                
                if let backgroundColor = self.progressDoneBackgroundColor {
                    viewController.setBackgroundColor(backgroundColor)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func handleAppReview() {
        HabitlabRestAPI.getShouldReviewIosApp { (error, reviewOutcome) in
            if error != nil {
                return
            }
            
            if let outcome = reviewOutcome as? NSDictionary {
                if let shouldReview = outcome["shouldReview"] as? NSNumber {
                    if shouldReview == 1 {
                        let shouldFeedbackClickedYes = {(dictionary: NSDictionary) -> () in
                            if let checkURL = NSURL(string: "mailto://info@habitlab.me") {
                                UIApplication.sharedApplication().openURL(checkURL)
                            }
                            
                            Serv.showSpinner()
                            
                            HabitlabRestAPI.postAcceptIosAppReview { (error, result) in
                                Serv.hideSpinner()
                            }
                        };
                        
                        let shouldFeedbackClickedNo = {(dictionary: NSDictionary) -> () in
                            Serv.showSpinner()
                            
                            HabitlabRestAPI.postRejectIosAppReview({ (error, response) in
                                Serv.hideSpinner()
                                
                                print("Sent the post reject app review")
                            })
                        };
                        
                        let shouldReviewClickedYes = {(dictionary: NSDictionary) -> () in
                            if let reviewAppController = self.storyboard?.instantiateViewControllerWithIdentifier("reviewAppController") as? ReviewAppController {
                                self.presentViewController(reviewAppController, animated: true, completion: nil)
                            }
                        };
                        
                        let shouldReviewClickedNo = {(dictionary: NSDictionary) -> () in
                            if let feedbackPopup = self.storyboard?.instantiateViewControllerWithIdentifier("fullScreenOverlayPopup") as? FullScreenOverlayPopup {
                                let dictionary = NSDictionary()
                                var buttons: [FullScreenOverlayPopupButton] = []
                                buttons.append(FullScreenOverlayPopupButton(type: FullScreenOverlayPopupButtonType.Green, title: "Yes!".uppercaseString, dictionary: dictionary, action: shouldFeedbackClickedYes))
                                buttons.append(FullScreenOverlayPopupButton(type: FullScreenOverlayPopupButtonType.White, title: "No thanks".uppercaseString, dictionary: dictionary, action: shouldFeedbackClickedNo))
                                feedbackPopup.setContent("We are sorry to hear that!", description: "We'd love to improve your Habitlab experience. Would you like to give us feedback?", photoNamed: "AppIconRound", buttons: buttons)
                                self.presentViewController(feedbackPopup, animated: true, completion: nil)
                            }
                        };
                        
                        if let reviewPopup = self.storyboard?.instantiateViewControllerWithIdentifier("fullScreenOverlayPopup") as? FullScreenOverlayPopup {
                            let dictionary = NSDictionary()
                            var buttons: [FullScreenOverlayPopupButton] = []
                            buttons.append(FullScreenOverlayPopupButton(type: FullScreenOverlayPopupButtonType.Green, title: "Yes!".uppercaseString, dictionary: dictionary, action: shouldReviewClickedYes))
                            buttons.append(FullScreenOverlayPopupButton(type: FullScreenOverlayPopupButtonType.White, title: "Not so much".uppercaseString, dictionary: dictionary, action: shouldReviewClickedNo))
                            
                            var title = "Hi there"
                            
                            if let user = UserCache().getUser() {
                                if let firstName = user["firstName"] as? NSString {
                                    title = "Hi \(firstName)"
                                }
                            }
                            
                            reviewPopup.setContent(title, description: "Do you enjoy the Habitlab experience?", photoNamed: "AppIconRound", buttons: buttons)
                            self.presentViewController(reviewPopup, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: Unwind segues
    
    @IBAction func currentHabitCompletedCapture(segue: UIStoryboardSegue) {
        circle.animateToAngle(360, duration: 0.1, completion: nil)
        self.refresh()
        self.handleAppReview()
    }
    
    @IBAction func currentHabitCancelledCapture(segue: UIStoryboardSegue) {
        self.refresh()
    }
    
    @IBAction func currentHabitCompletedStep(segue: UIStoryboardSegue) {
        self.refresh()
    }
}
