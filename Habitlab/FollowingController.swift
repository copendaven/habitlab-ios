//
//  FollowingController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class FollowingController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var noFollowingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var followings = []
    var refresher: UIRefreshControl?
    
    var isOpenedFromPushNotification: Bool = false
    var pushNotificationPayload: NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(FollowingController)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = self.tableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(FollowingController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if isOpenedFromPushNotification {
            self.trackUser()
        }
        
        refresh()
        segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(FollowingController))
    }
    
    private func trackUser() {
        if let pushNotificationPayloadDictionary = self.pushNotificationPayload {
            if let userTrackingId = pushNotificationPayloadDictionary["userTrackingId"] as? NSString {
                HabitlabRestAPI.postTrackUser(userTrackingId as String, handler: { (error, object) in
                    // Do nothing.
                })
            }
        }
    }
    
    func filterBlockedOut(followings: NSArray) -> NSArray {
        return followings.filter { (followingJson) -> Bool in
            if let following = followingJson as? NSDictionary {
                if let status = following["status"] as? NSString {
                    return (status as String).compare("blocked") != NSComparisonResult.OrderedSame
                }
                
                return false
            }
            
            return false
        }
    }
    
    func refresh() {
        Serv.showSpinner()
        
        HabitlabRestAPI.getFollowing({ (error, followingsJson) -> () in
            Serv.hideSpinner()
            
            if error != nil {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let followings = followingsJson as? NSArray {
                self.followings = self.filterBlockedOut(followings)
                self.tableView.reloadData()
                
                if let refr = self.refresher {
                    refr.endRefreshing()
                }
                
                self.noFollowingLabel.hidden = (self.followings.count > 0)
            } else {
                self.noFollowingLabel.hidden = false
            }
        })
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showPublicProfileFromFollowing") {
            if let viewController = segue.destinationViewController as? PublicProfileController {
                if let userId = sender as? String {
                    viewController.userId = userId
                }
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return followings.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if let following = followings[indexPath.section] as? NSDictionary {
            if let status = following["status"] as? NSString {
                switch (status) {
                case "unfollowed":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followingUnfollowedCell", forIndexPath: indexPath) as? FollowingUnfollowedCell {
                        cell.populate(following, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "waiting":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followingWaitingCell", forIndexPath: indexPath) as? FollowingWaitingCell {
                        cell.populate(following, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "approved":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followingApprovedCell", forIndexPath: indexPath) as? FollowingApprovedCell {
                        cell.populate(following, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                default:
                    // Do nothing.
                    break
                }
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
