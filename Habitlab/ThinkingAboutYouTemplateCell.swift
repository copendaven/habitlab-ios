//
//  ThinkingAboutYouTemplateCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ThinkingAboutYouTemplateCell: UITableViewCell {
    private var template: NSDictionary?
    private var controller: UIViewController?
    
    @IBOutlet weak var button: UIButton!
    
    @IBAction func clickButton(sender: AnyObject) {
        if controller == nil {
            return
        }
        
        if template == nil {
            return
        }
        
        if let templateController = controller as? ThinkingAboutYouChooseMessageController {
            templateController.setSelected(template!)
        }
    }
    
    private func setSelected(selected: Bool) {
        
        // Mark the button as selected
        button.selected = selected
    }
    
    func populate(controller: UIViewController, template: NSDictionary, selected: Bool) {
        self.template = template
        self.controller = controller
        
        if let titleLabel = self.button.titleLabel {
            titleLabel.numberOfLines = 0
            titleLabel.textAlignment = NSTextAlignment.Center
        }
        
        if let message = template["message"] as? NSString {
            self.button.setTitle("\"\(message as String)\"", forState: UIControlState.Normal)
            self.button.setTitle("\"\(message as String)\"", forState: UIControlState.Selected)
        }
        
        setSelected(selected)
    }
}
