//
//  DynamicCollectionView.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/4/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class DynamicCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if !CGSizeEqualToSize(self.bounds.size, self.intrinsicContentSize()) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func intrinsicContentSize() -> CGSize {
        return self.contentSize
    }
}
