//
//  MobileNotificationCommentCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/25/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData

class MobileNotificationCommentCache: NSObject {
    private static var object: NSObject = NSObject()
    
    private static var cacheCommentsByMobileNotificationId: NSMutableDictionary = NSMutableDictionary()
    private static var cacheCommentsByMobileNotificationCacheId: NSMutableDictionary = NSMutableDictionary()
    private static var cacheInspiresByMobileNotificationId: NSMutableDictionary = NSMutableDictionary()
    private static var cacheInspiresByMobileNotificationCacheId: NSMutableDictionary = NSMutableDictionary()
    
    private func cleanupMobileNotificationId() {
        if let calendar = Serv.calendar {
            if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                var mobileNotificationIds = NSMutableArray()
                
                for (mobileNotificationId, cacheObject) in MobileNotificationCommentCache.cacheCommentsByMobileNotificationId {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                mobileNotificationIds.addObject(mobileNotificationId)
                            }
                        }
                    }
                }
                
                for mobileNotificationId in mobileNotificationIds {
                    MobileNotificationCommentCache.cacheCommentsByMobileNotificationId.removeObjectForKey(mobileNotificationId)
                }
                
                mobileNotificationIds = NSMutableArray()
                
                for (mobileNotificationId, cacheObject) in MobileNotificationCommentCache.cacheInspiresByMobileNotificationId {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                mobileNotificationIds.addObject(mobileNotificationId)
                            }
                        }
                    }
                }
                
                for mobileNotificationId in mobileNotificationIds {
                    MobileNotificationCommentCache.cacheInspiresByMobileNotificationId.removeObjectForKey(mobileNotificationId)
                }
            }
        }
    }
    
    private func cleanupMobileNotificationCacheId() {
        if let calendar = Serv.calendar {
            if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                var mobileNotificationCacheIds = NSMutableArray()
                
                for (mobileNotificationCacheId, cacheObject) in MobileNotificationCommentCache.cacheCommentsByMobileNotificationCacheId {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                mobileNotificationCacheIds.addObject(mobileNotificationCacheId)
                            }
                        }
                    }
                }
                
                for mobileNotificationCacheId in mobileNotificationCacheIds {
                    MobileNotificationCommentCache.cacheCommentsByMobileNotificationCacheId.removeObjectForKey(mobileNotificationCacheId)
                }
                
                mobileNotificationCacheIds = NSMutableArray()
                
                for (mobileNotificationCacheId, cacheObject) in MobileNotificationCommentCache.cacheInspiresByMobileNotificationCacheId {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                mobileNotificationCacheIds.addObject(mobileNotificationCacheId)
                            }
                        }
                    }
                }
                
                for mobileNotificationCacheId in mobileNotificationCacheIds {
                    MobileNotificationCommentCache.cacheInspiresByMobileNotificationCacheId.removeObjectForKey(mobileNotificationCacheId)
                }
            }
        }
    }
    
    private func addComment(cache: NSMutableDictionary, comment: NSMutableDictionary) {
        cache.setObject(NSDate(), forKey: "cachedAt")
        
        if cache.objectForKey("comments") == nil {
            cache.setObject(NSMutableArray(), forKey: "comments")
        }
        
        let finalComments = NSMutableArray()
        
        if let commentsJson = cache.objectForKey("comments") {
            if let comments = commentsJson as? NSMutableArray {
                finalComments.addObjectsFromArray(comments as [AnyObject])
            }
        }
        
        // Add the latest at the end
        finalComments.addObject(comment)
        
        cache.setObject(finalComments, forKey: "comments")
    }
    
    private func addCommentMobileNotificationId(mobileNotificationId: String, comment: NSMutableDictionary) {
        if MobileNotificationCommentCache.cacheCommentsByMobileNotificationId.objectForKey(mobileNotificationId) == nil {
            MobileNotificationCommentCache.cacheCommentsByMobileNotificationId.setObject(NSMutableDictionary(), forKey: mobileNotificationId)
        }
        
        if let cacheJson = MobileNotificationCommentCache.cacheCommentsByMobileNotificationId.objectForKey(mobileNotificationId) {
            if let cache = cacheJson as? NSMutableDictionary {
                addComment(cache, comment: comment)
            }
        }
    }
    
    private func addCommentMobileNotificationCacheId(mobileNotificationCacheId: String, comment: NSMutableDictionary) {
        if MobileNotificationCommentCache.cacheCommentsByMobileNotificationCacheId.objectForKey(mobileNotificationCacheId) == nil {
            MobileNotificationCommentCache.cacheCommentsByMobileNotificationCacheId.setObject(NSMutableDictionary(), forKey: mobileNotificationCacheId)
        }
        
        if let cacheJson = MobileNotificationCommentCache.cacheCommentsByMobileNotificationCacheId.objectForKey(mobileNotificationCacheId) {
            if let cache = cacheJson as? NSMutableDictionary {
                addComment(cache, comment: comment)
            }
        }
    }
    
    private func addInspireMobileNotificationId(mobileNotificationId: String, comment: NSMutableDictionary) {
        if MobileNotificationCommentCache.cacheInspiresByMobileNotificationId.objectForKey(mobileNotificationId) == nil {
            MobileNotificationCommentCache.cacheInspiresByMobileNotificationId.setObject(NSMutableDictionary(), forKey: mobileNotificationId)
        }
        
        if let cacheJson = MobileNotificationCommentCache.cacheInspiresByMobileNotificationId.objectForKey(mobileNotificationId) {
            if let cache = cacheJson as? NSMutableDictionary {
                addComment(cache, comment: comment)
            }
        }
    }
    
    private func addInspireMobileNotificationCacheId(mobileNotificationCacheId: String, comment: NSMutableDictionary) {
        if MobileNotificationCommentCache.cacheInspiresByMobileNotificationCacheId.objectForKey(mobileNotificationCacheId) == nil {
            MobileNotificationCommentCache.cacheInspiresByMobileNotificationCacheId.setObject(NSMutableDictionary(), forKey: mobileNotificationCacheId)
        }
        
        if let cacheJson = MobileNotificationCommentCache.cacheInspiresByMobileNotificationCacheId.objectForKey(mobileNotificationCacheId) {
            if let cache = cacheJson as? NSMutableDictionary {
                addComment(cache, comment: comment)
            }
        }
    }
    
    static func erase() {
        objc_sync_enter(MobileNotificationCommentCache.object)
        MobileNotificationCommentCache.cacheCommentsByMobileNotificationId.removeAllObjects()
        MobileNotificationCommentCache.cacheCommentsByMobileNotificationCacheId.removeAllObjects()
        MobileNotificationCommentCache.cacheInspiresByMobileNotificationId.removeAllObjects()
        MobileNotificationCommentCache.cacheInspiresByMobileNotificationCacheId.removeAllObjects()
        objc_sync_exit(MobileNotificationCommentCache.object)
    }

    func addComment(cacheId: String, owner: NSDictionary, mobileNotificationId: String, mobileNotificationBaseId: String, mobileNotificationCacheId: String, message: String) {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        cleanupMobileNotificationId()
        
        let newMobileNotificationComment = NSMutableDictionary()
        
        newMobileNotificationComment.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationComment.setObject(owner, forKey: "owner")
        newMobileNotificationComment.setObject(mobileNotificationId, forKey: "mobileNotification")
        newMobileNotificationComment.setObject(mobileNotificationBaseId, forKey: "mobileNotificationBase")
        newMobileNotificationComment.setObject(mobileNotificationCacheId, forKey: "mobileNotificationCache")
        newMobileNotificationComment.setObject(message, forKey: "message")
        
        addCommentMobileNotificationId(mobileNotificationId, comment: newMobileNotificationComment)
        addCommentMobileNotificationCacheId(mobileNotificationCacheId, comment: newMobileNotificationComment)
        
        objc_sync_exit(MobileNotificationCommentCache.object)
    }
    
    func addComment(cacheId: String, owner: NSDictionary, mobileNotificationId: String, mobileNotificationBaseId: String, message: String) {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        cleanupMobileNotificationId()
        
        let newMobileNotificationComment = NSMutableDictionary()
        
        newMobileNotificationComment.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationComment.setObject(owner, forKey: "owner")
        newMobileNotificationComment.setObject(mobileNotificationId, forKey: "mobileNotification")
        newMobileNotificationComment.setObject(mobileNotificationBaseId, forKey: "mobileNotificationBase")
        newMobileNotificationComment.setObject(message, forKey: "message")
        
        addCommentMobileNotificationId(mobileNotificationId, comment: newMobileNotificationComment)
        
        objc_sync_exit(MobileNotificationCommentCache.object)
    }
    
    func addComment(cacheId: String, owner: NSDictionary, mobileNotificationCacheId: String, message: String) {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        cleanupMobileNotificationCacheId()
        
        let newMobileNotificationComment = NSMutableDictionary()
        
        newMobileNotificationComment.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationComment.setObject(owner, forKey: "owner")
        newMobileNotificationComment.setObject(mobileNotificationCacheId, forKey: "mobileNotificationCache")
        newMobileNotificationComment.setObject(message, forKey: "message")
        
        addCommentMobileNotificationCacheId(mobileNotificationCacheId, comment: newMobileNotificationComment)
        
        objc_sync_exit(MobileNotificationCommentCache.object)
    }
    
    func addInspiration(cacheId: String, owner: NSDictionary, mobileNotificationId: String, mobileNotificationBaseId: String, mobileNotificationCacheId: String, message: String) {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        cleanupMobileNotificationId()
        
        let newMobileNotificationComment = NSMutableDictionary()
        newMobileNotificationComment.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationComment.setObject(owner, forKey: "owner")
        newMobileNotificationComment.setObject(mobileNotificationId, forKey: "mobileNotification")
        newMobileNotificationComment.setObject(mobileNotificationBaseId, forKey: "mobileNotificationBase")
        newMobileNotificationComment.setObject(mobileNotificationCacheId, forKey: "mobileNotificationCache")
        newMobileNotificationComment.setObject(message, forKey: "message")
        newMobileNotificationComment.setObject(true, forKey: "initial")
        
        addInspireMobileNotificationId(mobileNotificationId, comment: newMobileNotificationComment)
        addInspireMobileNotificationCacheId(mobileNotificationCacheId, comment: newMobileNotificationComment)
        
        objc_sync_exit(MobileNotificationCommentCache.object)
    }
    
    func addInspiration(cacheId: String, owner: NSDictionary, mobileNotificationId: String, mobileNotificationBaseId: String, message: String) {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        cleanupMobileNotificationId()
        
        let newMobileNotificationComment = NSMutableDictionary()
        newMobileNotificationComment.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationComment.setObject(owner, forKey: "owner")
        newMobileNotificationComment.setObject(mobileNotificationId, forKey: "mobileNotification")
        newMobileNotificationComment.setObject(mobileNotificationBaseId, forKey: "mobileNotificationBase")
        newMobileNotificationComment.setObject(message, forKey: "message")
        newMobileNotificationComment.setObject(true, forKey: "initial")
        
        addInspireMobileNotificationId(mobileNotificationId, comment: newMobileNotificationComment)
        
        objc_sync_exit(MobileNotificationCommentCache.object)
    }
    
    func addInspiration(cacheId: String, owner: NSDictionary, mobileNotificationCacheId: String, message: String) {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        cleanupMobileNotificationCacheId()
        
        let newMobileNotificationComment = NSMutableDictionary()
        newMobileNotificationComment.setObject(cacheId, forKey: "cacheId")
        newMobileNotificationComment.setObject(owner, forKey: "owner")
        newMobileNotificationComment.setObject(mobileNotificationCacheId, forKey: "mobileNotificationCache")
        newMobileNotificationComment.setObject(message, forKey: "message")
        newMobileNotificationComment.setObject(true, forKey: "initial")
        
        addInspireMobileNotificationCacheId(mobileNotificationCacheId, comment: newMobileNotificationComment)
        
        objc_sync_exit(MobileNotificationCommentCache.object)
    }
    
    func getByMobileNotificationId(mobileNotificationId: String) -> NSArray {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        let results = NSMutableArray()
        
        if let cacheObject = MobileNotificationCommentCache.cacheInspiresByMobileNotificationId.objectForKey(mobileNotificationId) {
            if let cache = cacheObject as? NSDictionary {
                if let comments = cache["comments"] as? NSArray {
                    results.addObjectsFromArray(comments as [AnyObject])
                }
            }
        }
        
        if let cacheObject = MobileNotificationCommentCache.cacheCommentsByMobileNotificationId.objectForKey(mobileNotificationId) {
            if let cache = cacheObject as? NSDictionary {
                if let comments = cache["comments"] as? NSArray {
                    results.addObjectsFromArray(comments as [AnyObject])
                }
            }
        }
        
        objc_sync_exit(MobileNotificationCommentCache.object)
        return results
    }
    
    func getByMobileNotificationCacheId(mobileNotificationCacheId: String) -> NSArray {
        objc_sync_enter(MobileNotificationCommentCache.object)
        
        let results = NSMutableArray()
        
        if let cacheObject = MobileNotificationCommentCache.cacheInspiresByMobileNotificationCacheId.objectForKey(mobileNotificationCacheId) {
            if let cache = cacheObject as? NSDictionary {
                if let comments = cache["comments"] as? NSArray {
                    results.addObjectsFromArray(comments as [AnyObject])
                }
            }
        }
        
        if let cacheObject = MobileNotificationCommentCache.cacheCommentsByMobileNotificationCacheId.objectForKey(mobileNotificationCacheId) {
            if let cache = cacheObject as? NSDictionary {
                if let comments = cache["comments"] as? NSArray {
                    results.addObjectsFromArray(comments as [AnyObject])
                }
            }
        }
        
        objc_sync_exit(MobileNotificationCommentCache.object)
        return results
    }
}
