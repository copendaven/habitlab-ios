//
//  ThinkingAboutYouChooseFriendsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

enum ThinkingAboutYouState: String {
    case BeforeRun = "before-run"
    case DuringRun = "during-run"
    case AfterRun = "after-run"
}

class ThinkingAboutYouChooseFriendsController: GAITrackedViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private static var object = NSObject()
    
    private var sentMessage = false
    
    var progressLoadingController: ProgressLoadingController?
    var refresher: UIRefreshControl?
    
    @IBOutlet weak var friendsCollectionView: UICollectionView!
    @IBOutlet weak var phraseLabel: UILabel!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var sendMessageImage: UIImageView!
    
    var selectedTemplate: NSDictionary?
    var thinkingAboutYouState: ThinkingAboutYouState?
    
    private var friends = []
    private var serverFriends = []
    private var selectedFriendIds = Set<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ThinkingAboutYouChooseFriendsController)
        
        friendsCollectionView.delegate = self
        friendsCollectionView.dataSource = self
        
        sendMessageImage.layer.cornerRadius = 0.5 * sendMessageImage.bounds.size.width
        sendMessageImage.alpha = 0.5
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ThinkingAboutYouChooseFriendsController.refreshPullToRefresh), forControlEvents: UIControlEvents.ValueChanged)
        friendsCollectionView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let viewController = self.progressLoadingController {
            viewController.hideSilently()
        }
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        if self.selectedTemplate != nil {
            if let message = self.selectedTemplate!["message"] as? NSString {
                self.phraseLabel.text = "\"\(message as String)\""
            }
        }
    
        sentMessage = false
        
        objc_sync_enter(ThinkingAboutYouChooseFriendsController.object)
        
        if self.serverFriends.count <= 0 {
            objc_sync_exit(ThinkingAboutYouChooseFriendsController.object)
            self.refreshFromServer()
        } else {
            objc_sync_exit(ThinkingAboutYouChooseFriendsController.object)
            self.refreshFromClient()
        }
        
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realState = self.thinkingAboutYouState {
            dict["thinkingAboutYouState"] = realState.rawValue
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ThinkingAboutYouChooseFriendsController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ThinkingAboutYouChooseFriendsController))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func refreshPullToRefresh() {
        self.refreshFromServer()
    }
    
    func filterFriends(receivedFriends: NSArray) -> NSArray {
        let processedFriends: NSMutableArray = NSMutableArray()
        processedFriends.addObjectsFromArray(receivedFriends as [AnyObject])
        return processedFriends
    }
    
    func refreshFromClient() {
        objc_sync_enter(ThinkingAboutYouChooseFriendsController.object)
        
        self.friends = self.filterFriends(self.serverFriends)
        
        dispatch_async(dispatch_get_main_queue()) {
            self.friendsCollectionView.reloadData()
        }
        
        objc_sync_exit(ThinkingAboutYouChooseFriendsController.object)
        
        if let refr = self.refresher {
            refr.endRefreshing()
        }
    }
    
    func refreshFromServer() {
        Serv.showSpinner()
        
        HabitlabRestAPI.getFriends({ (error, friendsJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let friendsArray = friendsJson as? NSArray {
                objc_sync_enter(ThinkingAboutYouChooseFriendsController.object)
                
                self.serverFriends = NSArray(array: friendsArray)
                
                objc_sync_exit(ThinkingAboutYouChooseFriendsController.object)
                
                self.refreshFromClient()
            }
        })
    }
    
    func setSelectedFriend(friendId: String, selected: Bool) {
        if selected {
            selectedFriendIds.insert(friendId)
        } else {
            selectedFriendIds.remove(friendId)
        }
        
        if selectedFriendIds.count > 0 {
            self.sendMessageImage.alpha = 1.0
            self.sendMessageButton.enabled = true
        } else {
            self.sendMessageImage.alpha = 0.5
            self.sendMessageButton.enabled = false
        }
    }

    @IBAction func sendMessage(sender: AnyObject) {
        if sentMessage {
            return
        }
        
        if selectedTemplate == nil {
            return
        }
        
        if thinkingAboutYouState == nil {
            return
        }
        
        if selectedFriendIds.count <= 0 {
            return
        }
        
        if let templateId = selectedTemplate!["id"] as? NSString, message = selectedTemplate!["message"] as? NSString {
            
            self.sentMessage = true
            
            dispatch_async(dispatch_get_main_queue(), {
                self.performSegueWithIdentifier("showProgressLoadingFromThinkingAboutYouFriends", sender: self)
            })
                
            Serv.showSpinner()
            let cacheId = NSUUID().UUIDString
            
            HabitlabRestAPI.postThinkingAboutYouMessage(Array(selectedFriendIds), cacheId: cacheId, templateId: templateId as String, message: message as String, state: thinkingAboutYouState!, handler: { (error, object) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    self.sentMessage = false
                
                    dispatch_async(dispatch_get_main_queue(), {
                        if let viewController = self.progressLoadingController {
                            viewController.hideSilently()
                        }
                    })
                    
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    if let viewController = self.progressLoadingController {
                        viewController.markDone("Sent", hideInSeconds: 1)
                    }
                })
            })
        }
    }
    
    func doneLoading() {
        self.performSegueWithIdentifier("showRunningModuleFromThinkingAboutYouFriends", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressLoadingFromThinkingAboutYouFriends" {
            if let viewController = segue.destinationViewController as? ProgressLoadingController {
                self.progressLoadingController = viewController
                viewController.setDelegate(self)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friends.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let horizontalDimension = floor(self.friendsCollectionView.frame.size.width / 3.0)
        let verticalDimension = CGFloat(120)
        return CGSizeMake(horizontalDimension, verticalDimension)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenHeight = Int(friendsCollectionView.frame.size.height)
        var contentHeight = Int(friends.count * 120) // This is the size for one collection item with button and label
        
        if screenHeight <= contentHeight {
            contentHeight = screenHeight / 120 * 120
        }
        
        let padding = CGFloat((screenHeight - contentHeight) / 2)
        return UIEdgeInsets(top: padding, left: 0, bottom: padding, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        objc_sync_enter(ThinkingAboutYouChooseFriendsController.object)
        
        if friends.count <= indexPath.item {
            objc_sync_exit(ThinkingAboutYouChooseFriendsController.object)
            return defaultCell
        }
        
        if let friend = friends[indexPath.item] as? NSDictionary {
            if let friendId = friend["id"] as? NSString {
                if let cell = friendsCollectionView.dequeueReusableCellWithReuseIdentifier("thinkingAboutYouFriendCell", forIndexPath: indexPath) as? ThinkingAboutYouFriendCell {
                    
                    if selectedFriendIds.contains(friendId as String) {
                        cell.populate(self, friend: friend, setSelected: true)
                    } else {
                        cell.populate(self, friend: friend, setSelected: false)
                    }
                    
                    objc_sync_exit(ThinkingAboutYouChooseFriendsController.object)
                    return cell
                }
            }
        }
        
        objc_sync_exit(ThinkingAboutYouChooseFriendsController.object)
        return defaultCell
    }
}
