//
//  ThinkingAboutYouFriendCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ThinkingAboutYouFriendCell: UICollectionViewCell {
    private var controller: UIViewController?
    private var friend: NSDictionary?
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    
    @IBAction func onClick(sender: AnyObject) {
        self.selected = !self.selected
        self.borderView.hidden = !self.selected
        
        if controller == nil {
            return
        }
        
        if friend == nil {
            return
        }
        
        if let thinkingController = controller as? ThinkingAboutYouChooseFriendsController {
            if let id = friend!["id"] as? NSString {
                thinkingController.setSelectedFriend(id as String, selected: self.selected)
            }
        }
    }
    
    func populate(controller: UIViewController, friend: NSDictionary, setSelected: Bool) {
        self.controller = controller
        self.friend = friend
        
        self.selected = setSelected
        self.borderView.hidden = !self.selected
        
        self.userPhoto!.layer.cornerRadius = self.userPhoto!.frame.size.width / 2
        self.borderView.layer.cornerRadius = self.borderView.frame.size.width / 2
        self.userPhoto!.clipsToBounds = true
        
        if let firstName = friend["firstName"] as? NSString {
            self.userNameLabel.text = firstName as String
        }
        
        if let photo = friend["facebookPhoto"] as? NSString {
            Serv.loadImage(photo as String, handler: { (error, data) -> () in
                if error != nil {
                    print(error)
                }
                
                if data != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.userPhoto!.image = UIImage(data: data!)
                    })
                }
            })
        }
    }
}