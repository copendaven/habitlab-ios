//
//  ProgramStepStartedCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ProgramStepStartedCache: NSObject {
    private static var caches = Set<String>()
    private static var object = NSObject()
    
    static func erase() {
        objc_sync_enter(ProgramStepStartedCache.object)
        ProgramStepStartedCache.caches.removeAll()
        objc_sync_exit(ProgramStepStartedCache.object)
    }
    
    func setStepStarted(stepId: String) {
        objc_sync_enter(ProgramStepStartedCache.object)
        ProgramStepStartedCache.caches.insert(stepId)
        objc_sync_exit(ProgramStepStartedCache.object)
    }
    
    func isStepStarted(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepStartedCache.object)
        let result = ProgramStepStartedCache.caches.contains(stepId)
        objc_sync_exit(ProgramStepStartedCache.object)
        return result
    }
}