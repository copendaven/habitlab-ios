//
//  PhysioExerciseCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData

class PhysioExerciseCache: NSObject {
    private static var object: NSObject = NSObject()
    private let entityName = "PhysioExerciseCache"
    
    private func cleanup() {
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            if let calendar = Serv.calendar {
                if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Year, value: -1, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                    let request = NSFetchRequest(entityName: entityName)
                    request.returnsObjectsAsFaults = false
                    request.predicate = NSPredicate(format: "cachedAt < %@", earlyDate)
                    
                    var results: NSArray?
                    
                    do {
                        results = try context.executeFetchRequest(request)
                    } catch {
                        return
                    }
                    
                    if results == nil {
                        return
                    }
                    
                    for result in results! {
                        if let physioTracking = result as? NSManagedObject {
                            context.deleteObject(physioTracking)
                        }
                    }
                    
                    do {
                        try context.save()
                    } catch {
                        // Do nothing.
                    }
                }
            }
        }
    }
    
    func getComplete(stepId: String, exerciseId: String) -> Bool {
        objc_sync_enter(PhysioExerciseCache.object)
        
        if let cache = getCache(stepId, exerciseId: exerciseId) {
            if let complete = cache.valueForKey("complete") as? Bool {
                objc_sync_exit(PhysioExerciseCache.object)
                return complete
            }
        }
        
        objc_sync_exit(PhysioExerciseCache.object)
        return false
    }
    
    func getDontShow(stepId: String, exerciseId: String) -> Bool {
        objc_sync_enter(PhysioExerciseCache.object)
        
        if let cache = getCache(stepId, exerciseId: exerciseId) {
            if let dontShow = cache.valueForKey("dontShow") as? Bool {
                objc_sync_exit(PhysioExerciseCache.object)
                return dontShow
            }
        }
        
        objc_sync_exit(PhysioExerciseCache.object)
        return false
    }
    
    func setCache(stepId: String, exerciseId: String, index: Int) {
        objc_sync_enter(PhysioExerciseCache.object)
        
        if let _ = getCache(stepId, exerciseId: exerciseId) {
            // Do nothing
        } else {
            addCache(stepId, exerciseId: exerciseId, index: index)
        }
        
        objc_sync_exit(PhysioExerciseCache.object)
    }
    
    func setComplete(stepId: String, exerciseId: String, index: Int) {
        objc_sync_enter(PhysioExerciseCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId, exerciseId: exerciseId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, exerciseId: exerciseId, index: index)
        }
        
        if cache == nil {
            objc_sync_exit(PhysioExerciseCache.object)
            return
        }
        
        cache!.setValue(true, forKey: "complete")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(PhysioExerciseCache.object)
                return
            }
        }
        
        objc_sync_exit(PhysioExerciseCache.object)
    }

    func setDontShow(stepId: String, exerciseId: String, index: Int) {
        objc_sync_enter(PhysioExerciseCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId, exerciseId: exerciseId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, exerciseId: exerciseId, index: index)
        }
        
        if cache == nil {
            objc_sync_exit(PhysioExerciseCache.object)
            return
        }
        
        cache!.setValue(true, forKey: "dontShow")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(PhysioExerciseCache.object)
                return
            }
        }
        
        objc_sync_exit(PhysioExerciseCache.object)
    }
    
    func unsetDontShow(stepId: String, exerciseId: String, index: Int) {
        objc_sync_enter(PhysioExerciseCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId, exerciseId: exerciseId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, exerciseId: exerciseId, index: index)
        }
        
        if cache == nil {
            objc_sync_exit(PhysioExerciseCache.object)
            return
        }
        
        cache!.setValue(false, forKey: "dontShow")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(PhysioExerciseCache.object)
                return
            }
        }
        
        objc_sync_exit(PhysioExerciseCache.object)
    }

    func getLastCompleteExerciseIndex(stepId: String) -> Int {
        objc_sync_enter(PhysioExerciseCache.object)
        
        var maxIndex = 0
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let request = NSFetchRequest(entityName: entityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "(complete = %@) AND (stepId = %@)", true, stepId)
                    
            var results: NSArray?
            
            do {
                results = try context.executeFetchRequest(request)
            } catch {
                objc_sync_exit(PhysioExerciseCache.object)
                return maxIndex
            }
            
            if results == nil {
                objc_sync_exit(PhysioExerciseCache.object)
                return maxIndex
            }
            
            for result in results! {
                if let cache = result as? NSManagedObject {
                    if let index = cache.valueForKey("index") as? Int {
                        maxIndex = max(maxIndex, index)
                    }
                }
            }
        }
        
        objc_sync_exit(PhysioExerciseCache.object)
        return maxIndex
    }

    private func getCache(stepId: String, exerciseId: String) -> NSManagedObject? {
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let request = NSFetchRequest(entityName: entityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "(stepId = %@) AND (exerciseId = %@)", stepId, exerciseId)
            
            var results: NSArray?
            
            do {
                results = try context.executeFetchRequest(request)
            } catch {
                return nil
            }
            
            if results!.count <= 0 {
                return nil
            }
            
            if let PhysioExerciseCache = results![0] as? NSManagedObject {
                return PhysioExerciseCache
            }
        }
        
        return nil
    }
    
    private func addCache(stepId: String, exerciseId: String, index: Int) -> NSManagedObject? {
        cleanup()
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let newPhysioCache = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context)
            newPhysioCache.setValue(stepId, forKey: "stepId")
            newPhysioCache.setValue(exerciseId, forKey: "exerciseId")
            newPhysioCache.setValue(index, forKey: "index")
            newPhysioCache.setValue(false, forKey: "dontShow")
            newPhysioCache.setValue(false, forKey: "complete")
            newPhysioCache.setValue(NSDate(), forKey: "cachedAt")
            
            do {
                try context.save()
                return newPhysioCache
            } catch {
                return nil
            }
        }
        
        return nil
    }
}