//
//  ReconnectModuleChooseContactContactCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ReconnectModuleChooseContactContactCell: UITableViewCell {
    private var controller: UIViewController?
    private var contact: NSDictionary?
    private var color: UIColor?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var friendImage: UIImageView!
    @IBOutlet weak var initialLabel: UILabel!
    @IBOutlet weak var imageBorder: UIView!
    
    private func getFirstName() -> String? {
        var firstName: String? = nil
        
        if let realContact = self.contact {
            if let realFirstName = realContact["firstName"] as? NSString {
                firstName = realFirstName as String
            }
            
            if let user = realContact["user"] as? NSDictionary {
                if let realFirstName = user["firstName"] as? NSString {
                    firstName = realFirstName as String
                }
            }
        }
        
        return firstName
    }
    
    private func getLastName() -> String? {
        var lastName: String? = nil
        
        if let realContact = self.contact {
            if let realLastName = realContact["lastName"] as? NSString {
                lastName = realLastName as String
            }
            
            if let user = realContact["user"] as? NSDictionary {
                if let realLastName = user["lastName"] as? NSString {
                    lastName = realLastName as String
                }
            }
        }
        
        return lastName
    }
    
    private func getReason() -> String? {
        var reason: String? = nil
        
        if let realContact = self.contact {
            if let realReason = realContact["reason"] as? NSString {
                reason = realReason as String
            }
        }
        
        return reason
    }
    
    private func setPhoto() {
        var hasPhoto = false
        
        if let realContact = self.contact {
            if let user = realContact["user"] as? NSDictionary {
                if let photo = user["facebookPhoto"] as? NSString {
                    hasPhoto = true
                    self.initialLabel.hidden = true
                    
                    Serv.loadImage(photo as String, handler: { (error, data) -> () in
                        if error != nil {
                            print(error)
                        }
                        
                        if data != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.friendImage.hidden = false
                                self.friendImage.image = UIImage(data: data!)
                            })
                        }
                    })
                }
            }
            
            if !hasPhoto {
                if let identifier = realContact["identifier"] as? NSString {
                    
                    // Read the image from the photo cache first, if possible.
                    if let imageData = PhotoCache().getCache("contact\(identifier)") {
                        hasPhoto = true
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.friendImage.hidden = false
                            self.friendImage.image = UIImage(data: imageData)
                        })
                    }
                }
            }
            
            if !hasPhoto {
                if let contactHasPhoto = realContact["hasPhoto"] as? NSNumber, identifier = realContact["identifier"] as? NSString {
                    if contactHasPhoto == 1 {
                        hasPhoto = true
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                            
                            // If the photo cache did not contain the image, I will download it from Amazon...
                            HabitlabS3API.downloadContactImage(identifier as String, handler: { (error, imageData) -> () in
                                if error != nil {
                                    return
                                }
                                
                                if imageData != nil {
                                    PhotoCache().setCache("contact\(identifier)", image: imageData!)
                                    
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.friendImage.hidden = false
                                        self.friendImage.image = UIImage(data: imageData!)
                                    })
                                }
                                }, progress: nil)
                        })
                    }
                }
            }
        }
        
        if !hasPhoto {
            self.friendImage.hidden = true
            self.initialLabel.hidden = false
            self.initialLabel.backgroundColor = color
            
            var initialLabelText = ""
            
            if let realFirstName = getFirstName() {
                if realFirstName.characters.count > 0 {
                    initialLabelText += realFirstName[realFirstName.startIndex.advancedBy(0)..<realFirstName.startIndex.advancedBy(1)]
                }
            }
            
            if let realLastName = getLastName() {
                if realLastName.characters.count > 0 {
                    initialLabelText += realLastName[realLastName.startIndex.advancedBy(0)..<realLastName.startIndex.advancedBy(1)]
                }
            }
            
            if initialLabelText == "" {
                initialLabelText = "?"
            }
            
            self.initialLabel.text = initialLabelText.uppercaseString
        }
    }
    
    func populate(controller: UIViewController, contact: NSDictionary, color: UIColor) {
        self.controller = controller
        self.contact = contact
        self.color = color
        
        dispatch_async(dispatch_get_main_queue()) {
            self.friendImage.layer.cornerRadius = 0.5 * self.friendImage.bounds.size.width
            self.initialLabel.layer.cornerRadius = 0.5 * self.initialLabel.bounds.size.width
            self.imageBorder.layer.cornerRadius = 0.5 * self.imageBorder.bounds.size.width
            
            if let realFirstName = self.getFirstName() {
                self.titleLabel.text = realFirstName
            }
            
            if let realReason = self.getReason() {
                self.descriptionLabel.text = realReason
            }
            
            self.setPhoto()
        }
    }
    
    @IBAction func selectContact(sender: AnyObject) {
        if let viewController = self.controller as? ReconnectModuleChooseContactController, realContact = self.contact, realColor = self.color {
            viewController.selectContact(realContact, color: realColor)
        }
    }
}
