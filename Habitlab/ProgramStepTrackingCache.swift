//
//  ProgramStepTrackingCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/25/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData

class ProgramStepTrackingCache: NSObject {
    private static var object: NSObject = NSObject()
    private let entityName = "ProgramStepTrackingCache"
    
    private func cleanup() {
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            if let calendar = Serv.calendar {
                if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Day, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                    let request = NSFetchRequest(entityName: entityName)
                    request.returnsObjectsAsFaults = false
                    request.predicate = NSPredicate(format: "cachedAt < %@", earlyDate)
                    
                    var results: NSArray?
                    
                    do {
                        results = try context.executeFetchRequest(request)
                    } catch {
                        return
                    }
                    
                    if results == nil {
                        return
                    }
                    
                    for result in results! {
                        if let programStepTracking = result as? NSManagedObject {
                            context.deleteObject(programStepTracking)
                        }
                    }
                    
                    do {
                        try context.save()
                    } catch {
                        // Do nothing.
                    }
                }
            }
        }
    }
    
    func addDistance(stepId: String, distance: Double, dueDate: NSDate) {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return
        }
        
        if let existingDistance = cache!.valueForKey("distance") as? Double {
            let newDistance: Double = existingDistance + distance
            cache!.setValue(newDistance, forKey: "distance")
            
            if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
                let context: NSManagedObjectContext = appDelegate.managedObjectContext
             
                do {
                    try context.save()
                } catch {
                    objc_sync_exit(ProgramStepTrackingCache.object)
                    return
                }
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
    }
    
    func resetDistance(stepId: String, dueDate: NSDate) {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return
        }
        
        cache!.setValue(0.0, forKey: "distance")
            
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return
            }
        }
    
        objc_sync_exit(ProgramStepTrackingCache.object)
    }
    
    func setHalfway(stepId: String, halfway: Int, dueDate: NSDate) {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return
        }

        if let existingHalfway = cache!.valueForKey("halfway") as? Int {
            if (halfway <= existingHalfway) {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return
            }
        }
        
        cache!.setValue(halfway, forKey: "halfway")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return
            }
        }
    
        objc_sync_exit(ProgramStepTrackingCache.object)
    }
    
    func setDone(stepId: String, dueDate: NSDate) {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return
        }

        cache!.setValue(true, forKey: "done")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
    }
    
    func trySetRunDistanceUnits(stepId: String, distanceUnits: Int, dueDate: NSDate) -> Bool {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return false
        }
        
        if let distanceUnitsRecorded = cache!.valueForKey("runDistanceUnits") as? NSNumber {
            if Int(distanceUnitsRecorded) >= distanceUnits {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return false
            }
        }
        
        cache!.setValue(distanceUnits, forKey: "runDistanceUnits")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return false
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return true
    }
    
    func trySetVibratedDistanceUnits(stepId: String, distanceUnits: Int, dueDate: NSDate) -> Bool {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return false
        }
        
        if let distanceUnitsRecorded = cache!.valueForKey("vibratedDistanceUnits") as? NSNumber {
            if Int(distanceUnitsRecorded) >= distanceUnits {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return false
            }
        }
        
        cache!.setValue(distanceUnits, forKey: "vibratedDistanceUnits")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return false
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return true
    }
    
    func setVibratedHalfway(stepId: String, vibratedHalfway: Bool, dueDate: NSDate) {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return
        }

        cache!.setValue(vibratedHalfway, forKey: "vibratedHalfway")
            
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
    }
    
    func setVibratedDone(stepId: String, vibratedDone: Bool, dueDate: NSDate) {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return
        }
        
        cache!.setValue(vibratedDone, forKey: "vibratedDone")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
    }
    
    func setTracking(stepId: String, tracking: Bool, dueDate: NSDate) {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepTrackingCache.object)
            return
        }
        
        cache!.setValue(tracking, forKey: "tracking")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return
            }
        }
    
        objc_sync_exit(ProgramStepTrackingCache.object)
    }
    
    func getExpired(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        if let cache = getCache(stepId) {
            if let dueDate = cache.valueForKey("dueDate") as? NSDate {
                let dateNow = NSDate()
                
                if dueDate.compare(dateNow) == NSComparisonResult.OrderedAscending {
                    objc_sync_exit(ProgramStepTrackingCache.object)
                    return true
                }
                
                objc_sync_exit(ProgramStepTrackingCache.object)
                return false
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return true
    }
    
    func getTracking(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        if let cache = getCache(stepId) {
            if let tracking = cache.valueForKey("tracking") as? Bool {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return tracking
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return false
    }

    func getDistance(stepId: String) -> Double {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        if let cache = getCache(stepId) {
            if let distance = cache.valueForKey("distance") as? Double {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return distance
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return 0.0
    }

    func getHalfway(stepId: String) -> Int? {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        if let cache = getCache(stepId) {
            if let halfway = cache.valueForKey("halfway") as? Int {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return halfway
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return nil
    }

    func getDone(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        if let cache = getCache(stepId) {
            if let done = cache.valueForKey("done") as? Bool {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return done
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return false
    }
    
    func getVibratedHalfway(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        if let cache = getCache(stepId) {
            if let vibrated = cache.valueForKey("vibratedHalfway") as? Bool {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return vibrated
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return false
    }
    
    func getVibratedDone(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepTrackingCache.object)
        
        if let cache = getCache(stepId) {
            if let vibrated = cache.valueForKey("vibratedDone") as? Bool {
                objc_sync_exit(ProgramStepTrackingCache.object)
                return vibrated
            }
        }
        
        objc_sync_exit(ProgramStepTrackingCache.object)
        return false
    }

    private func getCache(stepId: String) -> NSManagedObject? {
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let request = NSFetchRequest(entityName: entityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "stepId = %@", stepId)
            
            var results: NSArray?
            
            do {
                results = try context.executeFetchRequest(request)
            } catch {
                return nil
            }
            
            if results!.count <= 0 {
                return nil
            }
            
            if let programStepTrackingCache = results![0] as? NSManagedObject {
                return programStepTrackingCache
            }
        }
        
        return nil
    }
    
    private func addCache(stepId: String, dueDate: NSDate) -> NSManagedObject? {
        cleanup()
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let newProgramStepCache = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context)
            newProgramStepCache.setValue(stepId, forKey: "stepId")
            newProgramStepCache.setValue(0.0, forKey: "distance")
            newProgramStepCache.setValue(false, forKey: "tracking")
            newProgramStepCache.setValue(0, forKey: "halfway")
            newProgramStepCache.setValue(false, forKey: "done")
            newProgramStepCache.setValue(false, forKey: "vibratedHalfway")
            newProgramStepCache.setValue(false, forKey: "vibratedDone")
            newProgramStepCache.setValue(0, forKey: "runDistanceUnits")
            newProgramStepCache.setValue(0, forKey: "vibratedDistanceUnits")
            newProgramStepCache.setValue(NSDate(), forKey: "cachedAt")
            newProgramStepCache.setValue(dueDate, forKey: "dueDate")
            
            do {
                try context.save()
                return newProgramStepCache
            } catch {
                return nil
            }
        }
        
        return nil
    }
}
