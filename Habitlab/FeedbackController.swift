//
//  FeedbackController.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/19/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class FeedbackController: GAITrackedViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(FeedbackController)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(FeedbackController))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
