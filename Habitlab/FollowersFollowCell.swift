//
//  FollowersFollowCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowersFollowCell: FollowersCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var followed = false
    
    func populate(follower: NSDictionary, controller: UIViewController) {
        super.populate(follower, controller: controller, userName: userName, userImage: userImage)
        followed = false
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickFollowButton(sender: AnyObject) {
        if followed {
            return
        }
        
        if follower != nil && controller != nil {
            if let user = follower!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    followed = true
                    Serv.showSpinner()
                    
                    HabitlabRestAPI.postFollow(id as String, handler: { (error, response) -> () in
                        Serv.hideSpinner()
                        
                        if error != nil {
                            self.followed = false
                            Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                            return
                        }
                        
                        if let followersController = self.controller! as? FollowersController {
                            followersController.refresh()
                        }
                    })
                }
            }
        }
    }
}
