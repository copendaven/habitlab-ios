//
//  ScheduleDateController.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Contacts
import Analytics

class ScheduleDateController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource {
    private static let serverNotificationsQueue = dispatch_queue_create("me.habitlab.mobile.scheduleDateController.serverNotificationsQueue", DISPATCH_QUEUE_SERIAL)
    
    var drive: NSDictionary?
    var contacts: [CNContact] = []
    var timesOfDayDistribution: [Int] = []
    var contactMessages = NSMutableDictionary()
    
    private var signedUp = false
    private var months: [(year: Int, month: Int)] = []
    private var scheduleCalendar: ScheduleCalendar?
    private var initialDate: NSDate?
    private var lastSelectedMonth: (year: Int, month: Int)?
    
    var response: AnyObject? = nil
    var progressLoadingController: ProgressLoadingController?
    
    @IBOutlet weak var monthsTable: UITableView!
    @IBOutlet weak var showNextStepLabel: UILabel!
    @IBOutlet weak var showNextStepView: UIView!
    @IBOutlet weak var signupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ScheduleDateController)
        
        monthsTable.delegate = self
        monthsTable.dataSource = self
        monthsTable.estimatedRowHeight = 300
        monthsTable.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = monthsTable
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let viewController = self.progressLoadingController {
            viewController.hideSilently()
        }
        
        self.signedUp = false
        self.lastSelectedMonth = nil
        
        if self.drive == nil {
            return
        }
        
        if self.timesOfDayDistribution.count <= 0 {
            return
        }
        
        if let countWeeks = drive!["countWeeks"] as? NSNumber {
            var valid = false
            var countDays = 0
            
            if let countStepsPerWeek = drive!["countStepsPerWeek"] as? NSNumber {
                valid = true
                countDays = Int(countStepsPerWeek)
            }
            
            if let _ = drive!["countStepsPerDay"] as? NSNumber {
                valid = true
                countDays = 7 * Int(countWeeks)
            }
            
            if (valid) {
                self.scheduleCalendar = ScheduleCalendar(countWeeks: Int(countWeeks), countDays: countDays)
                refresh()
            }
        }
        
        if let title = drive!["title"] as? NSString, level = drive!["level"] as? NSNumber {
            self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
        }
        
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let drive = self.drive {
            if let categ = drive["driveCategory"] as? NSString {
                dict["category"] = categ as String
            }
            
            if let subcateg = drive["driveSubcategory"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
            
            if let level = drive["level"] as? NSNumber {
                dict["level"] = String(level)
            }
        }
        
        if timesOfDayDistribution.count >= 3 {
            if timesOfDayDistribution[0] + timesOfDayDistribution[1] + timesOfDayDistribution[2] <= 1 {
                if timesOfDayDistribution[0] > 0 {
                    dict["timeOfDay"] = ScheduledTimeOfDay.Morning.rawValue
                }
                
                if timesOfDayDistribution[1] > 0 {
                    dict["timeOfDay"] = ScheduledTimeOfDay.Day.rawValue
                }
                
                if timesOfDayDistribution[2] > 0 {
                    dict["timeOfDay"] = ScheduledTimeOfDay.Evening.rawValue
                }
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ScheduleDateController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ScheduleDateController))
        }
    }
    
    @IBAction func clickedSignup(sender: AnyObject) {
        if signedUp {
            return
        }
        
        if timesOfDayDistribution.count < 3 {
            Serv.showErrorPopup("Time not found", message: "The time of day to start the habit was not found. Go one page back and select one.", okMessage: "Okay, got it.", controller: self, handler: nil)
            return
        }
        
        if drive == nil {
            Serv.showErrorPopup("Program not found", message: "The program was not found. Try to reload the page.", okMessage: "Okay, got it.", controller: self, handler: nil)
            return
        }
        
        if scheduleCalendar == nil {
            return
        }
        
        let state = scheduleCalendar!.getState()
        
        if state != ScheduleCalendarState.SuccessAllWeeksSelected {
            return
        }
        
        if let doesStartFiveMinutesBeforeSoft = doesStartFiveMinutesBeforeSoftEnd() {
            if !doesStartFiveMinutesBeforeSoft {
                Serv.showErrorPopup("Time frame already past", message: "The level cannot start now. Choose a future start date in the calendar, or switch to a later time of day in the previous screen.", okMessage: "Okay, got it.", controller: self, handler: nil)
                return
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.performSegueWithIdentifier("showProgressLoadingFromScheduleDateHabit", sender: self)
        })
        
        if self.contacts.count > 0 {
            handleSignupContacts()
        } else {
            handleSignupFinal()
        }
    }
    
    private func handleSignupContacts() {
        let group = dispatch_group_create()
        
        for contact in contacts {
            if let realImageData = contact.imageData {
                dispatch_group_enter(group)
                
                PhotoCache().setCache("contact\(contact.identifier)", image: realImageData)
                
                HabitlabS3API.uploadContactImage(contact.identifier, data: realImageData, handler: { (error) in
                    dispatch_group_leave(group)
                    }, progress: nil)
            }
        }
        
        dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.handleSignupFinal()
        }
    }
    
    private func handleSignupFinal() {
        let body = createBody()
        
        Serv.showSpinner()
        self.signedUp = true
        
        HabitlabRestAPI.postProgramSignup(body, handler: { (error, response) -> () in
            Serv.hideSpinner()
            
            if error != nil {
                self.signedUp = false
                
                dispatch_async(dispatch_get_main_queue(), {
                    if let viewController = self.progressLoadingController {
                        viewController.hideSilently()
                    }
                })
                
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            self.response = response
            
            if let user = UserCache().getUser() {
                if let userId = user["id"] as? NSString {
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "\(userId as String)-signed-up-level")
                }
            }
            
            NSUserDefaults.standardUserDefaults()
            
            dispatch_async(dispatch_get_main_queue(), {
                if let viewController = self.progressLoadingController {
                    viewController.markDone("Scheduled", hideInSeconds: 1)
                }
            })
        })
    }
    
    func refresh() {
        if drive == nil {
            return
        }
        
        if self.timesOfDayDistribution.count < 3 {
            return
        }
        
        if scheduleCalendar == nil {
            return
        }
        
        let months = scheduleCalendar!.getMonths()
        
        // Get the months for when the initial date is selected
        if months != nil {
            self.months = months!
            self.monthsTable.reloadData()
        }
        
        let state = scheduleCalendar!.getState()
        
        if state == ScheduleCalendarState.SuccessAllWeeksSelected {
            self.showNextStepView.hidden = true
            self.signupButton.hidden = false
        } else {
            if let countStepsPerWeek = drive!["countStepsPerWeek"] as? NSNumber {
                let countSelectedDaysInCurrentWeek = scheduleCalendar!.countSelectedDaysInCurrentWeek()
                
                self.setNextStepLabel(state, countStepsSelectedInWeek: countSelectedDaysInCurrentWeek, countStepsPerWeek: Int(countStepsPerWeek))
            } else if let _ = drive!["countStepsPerDay"] as? NSNumber {
                let countSelectedDaysInCurrentWeek = scheduleCalendar!.countSelectedDaysInCurrentWeek()
                
                self.setNextStepLabel(state, countStepsSelectedInWeek: countSelectedDaysInCurrentWeek, countStepsPerWeek: 7)
            }
            
            self.showNextStepView.hidden = false
            self.signupButton.hidden = true
        }
        
        if let monthOfCurrentWeek = scheduleCalendar!.getMonthOfCurrentWeek() {
            if let lastSelectMonth = self.lastSelectedMonth {
                if lastSelectMonth.month == monthOfCurrentWeek.month && lastSelectMonth.year == monthOfCurrentWeek.year {
                    return
                }
            }
            
            lastSelectedMonth = monthOfCurrentWeek
            
            var index = 0
            var selectedIndex: Int?
            
            for month in self.months {
                if month.year == monthOfCurrentWeek.year && month.month == monthOfCurrentWeek.month {
                    selectedIndex = index
                }
                
                index = index + 1
            }
            
            if selectedIndex != nil {
                let indexPath = NSIndexPath(forRow: 0, inSection: 1 + selectedIndex!)
                self.monthsTable.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
            }
        }
    }
    
    private func doesStartFiveMinutesBeforeSoftEnd() -> Bool? {
        if scheduleCalendar == nil {
            return nil
        }
        
        if let fiveMinutesFromNow = Serv.getDatePlusMinutes(NSDate(), minutes: 5) {
            let selectedDays = scheduleCalendar!.getSelectedDays()
            
            for day in selectedDays {
                
                // Do the morning.
                var timesPerDayMorning = 0
                
                if self.timesOfDayDistribution.count >= 3 {
                    timesPerDayMorning = self.timesOfDayDistribution[0]
                }
                
                for timePerDay in 0..<timesPerDayMorning {
                    if let dateComponents = getEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayMorning, timeOfDay: ScheduledTimeOfDay.Morning) {
                        if let calendar = Serv.calendar {
                            if let date = calendar.dateFromComponents(dateComponents) {
                                if date.timeIntervalSinceDate(fiveMinutesFromNow) < 0 {
                                    return false
                                }
                            }
                        }
                    }
                }
                
                // Do the day.
                var timesPerDayDay = 0
                
                if self.timesOfDayDistribution.count >= 3 {
                    timesPerDayDay = self.timesOfDayDistribution[1]
                }
                
                for timePerDay in 0..<timesPerDayDay {
                    if let dateComponents = getEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayDay, timeOfDay: ScheduledTimeOfDay.Day) {
                        if let calendar = Serv.calendar {
                            if let date = calendar.dateFromComponents(dateComponents) {
                                if date.timeIntervalSinceDate(fiveMinutesFromNow) < 0 {
                                    return false
                                }
                            }
                        }
                    }
                }
                
                // Do the evening.
                var timesPerDayEvening = 0
                
                if self.timesOfDayDistribution.count >= 3 {
                    timesPerDayEvening = self.timesOfDayDistribution[2]
                }
                
                for timePerDay in 0..<timesPerDayEvening {
                    if let dateComponents = getEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayEvening, timeOfDay: ScheduledTimeOfDay.Evening) {
                        if let calendar = Serv.calendar {
                            if let date = calendar.dateFromComponents(dateComponents) {
                                if date.timeIntervalSinceDate(fiveMinutesFromNow) < 0 {
                                    return false
                                }
                            }
                        }
                    }
                }
            }
            
            return true
        }
    
        return nil
    }
    
    private func getStartDateComponents(date: (year: Int, month: Int, day: Int), timePerDay: Int, timesPerDay: Int, timeOfDay: ScheduledTimeOfDay) -> NSDateComponents? {
        let components = NSDateComponents()
        components.year = date.year
        components.month = date.month
        components.day = date.day
        
        if timesPerDay <= 1 {
            switch timeOfDay {
            case .Morning:
                components.hour = 5
            case .Day:
                components.hour = 11
            case .Evening:
                components.hour = 17
            }
            
            components.minute = 0
            components.second = 0
            components.nanosecond = 0
        } else {
            switch timeOfDay {
            case .Morning:
                let hour: Float = (8 + 3 * Float(timePerDay) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            case .Day:
                let hour: Float = (11 + 6 * Float(timePerDay) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            case .Evening:
                let hour: Float = (17 + 7 * Float(timePerDay) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            }
            
            components.second = 0
            components.nanosecond = 0
        }
        
        return components
    }

    private func getEndDateComponents(date: (year: Int, month: Int, day: Int), timePerDay: Int, timesPerDay: Int, timeOfDay: ScheduledTimeOfDay) -> NSDateComponents? {
        let components = NSDateComponents()
        
        components.year = date.year
        components.month = date.month
        components.day = date.day
        
        if timesPerDay <= 1 {
            switch timeOfDay {
            case .Morning:
                components.hour = 11
            case .Day:
                components.hour = 17
            case .Evening:
                let nextDate = scheduleCalendar!.getNextDate(date)
                components.year = nextDate.year
                components.month = nextDate.month
                components.day = nextDate.day
                components.hour = 0
            }
            
            components.minute = 0
            components.second = 0
            components.nanosecond = 0
        } else {
            switch timeOfDay {
            case .Morning:
                let hour: Float = (8 + 3 * Float(timePerDay + 1) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            case .Day:
                let hour: Float = (11 + 6 * Float(timePerDay + 1) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            case .Evening:
                let hour: Float = (17 + 7 * Float(timePerDay + 1) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            }
            
            components.second = 0
            components.nanosecond = 0
        }
        
        return components
    }

    private func getHardEndDateComponents(date: (year: Int, month: Int, day: Int), timePerDay: Int, timesPerDay: Int, timeOfDay: ScheduledTimeOfDay) -> NSDateComponents? {
        let components = NSDateComponents()
        
        components.year = date.year
        components.month = date.month
        components.day = date.day
        
        switch timeOfDay {
        case .Morning:
            components.hour = 16
            components.minute = 50
        case .Day:
            components.hour = 23
            components.minute = 50
        case .Evening:
            let nextDate = scheduleCalendar!.getNextDate(date)
            components.year = nextDate.year
            components.month = nextDate.month
            components.day = nextDate.day
            components.hour = 4
            components.minute = 50
        }
        
        components.second = 0
        components.nanosecond = 0
        
        return components
    }
    
    private func getNotifyStartDateComponents(date: (year: Int, month: Int, day: Int), timePerDay: Int, timesPerDay: Int, timeOfDay: ScheduledTimeOfDay) -> NSDateComponents? {
        let components = NSDateComponents()
        components.year = date.year
        components.month = date.month
        components.day = date.day
        
        if timesPerDay <= 1 {
            switch timeOfDay {
            case .Morning:
                components.hour = 8
            case .Day:
                components.hour = 11
            case .Evening:
                components.hour = 17
            }
            
            components.minute = 0
            components.second = 0
            components.nanosecond = 0
        } else {
            switch timeOfDay {
            case .Morning:
                let hour: Float = (8 + 3 * Float(timePerDay) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            case .Day:
                let hour: Float = (11 + 6 * Float(timePerDay) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            case .Evening:
                let hour: Float = (17 + 7 * Float(timePerDay) / Float(timesPerDay))
                components.hour = Int(hour)
                components.minute = Int(60 * (hour - floor(hour)))
            }
            
            components.second = 0
            components.nanosecond = 0
        }
        
        return components
    }

    private func getContactEmail(contact: CNContact) -> String? {
        var emailString: String? = nil
        
        for email in contact.emailAddresses {
            if emailString != nil {
                continue
            }
            
            if email.label == CNLabelHome {
                if let emailValue = email.value as? String {
                    emailString = emailValue
                }
            }
        }
        
        for email in contact.emailAddresses {
            if emailString != nil {
                continue
            }
            
            if email.label == CNLabelWork {
                if let emailValue = email.value as? String {
                    emailString = emailValue
                }
            }
        }
        
        for email in contact.emailAddresses {
            if emailString != nil {
                continue
            }
            
            if email.label == CNLabelOther {
                if let emailValue = email.value as? String {
                    emailString = emailValue
                }
            }
        }
        
        for email in contact.emailAddresses {
            if emailString != nil {
                continue
            }
            
            if let emailValue = email.value as? String {
                emailString = emailValue
            }
        }

        return emailString
    }
    
    private func getContactPhone(contact: CNContact) -> String? {
        var phoneString: String? = nil
        
        for phone in contact.phoneNumbers {
            if phoneString != nil {
                continue
            }
            
            if phone.label == CNLabelPhoneNumberMain {
                phoneString = phone.value as? String
            }
        }
        
        for phone in contact.phoneNumbers {
            if phoneString != nil {
                continue
            }
            
            if phone.label == CNLabelPhoneNumberMobile {
                phoneString = phone.value as? String
            }
        }
        
        for phone in contact.phoneNumbers {
            if phoneString != nil {
                continue
            }
            
            if phone.label == CNLabelPhoneNumberiPhone {
                phoneString = phone.value as? String
            }
        }
        
        for phone in contact.phoneNumbers {
            if phoneString != nil {
                continue
            }
            
            if phone.label == CNLabelPhoneNumberPager {
                phoneString = phone.value as? String
            }
        }
        
        for phone in contact.phoneNumbers {
            if phoneString != nil {
                continue
            }
            
            phoneString = phone.value as? String
        }
        
        return phoneString
    }
    
    private func createContacts() -> NSArray {
        var returnedContacts: [NSDictionary] = []
        
        for contact in contacts {
            let addedContact = NSMutableDictionary()
            
            addedContact.setObject(contact.identifier, forKey: "identifier")
            addedContact.setObject(contact.givenName, forKey: "firstName")
            addedContact.setObject(contact.familyName, forKey: "lastName")
            
            if let _ = contact.imageData {
                addedContact.setObject(true, forKey: "hasPhoto")
            }
            
            if let realMessage = contactMessages.objectForKey(contact.identifier) {
                addedContact.setObject(realMessage, forKey: "reason")
            }
            
            if let realEmailString = getContactEmail(contact) {
                addedContact.setObject(realEmailString, forKey: "email")
            }
            
            if let realPhoneString = getContactPhone(contact) {
                addedContact.setObject(realPhoneString, forKey: "phone")
            }
            
            returnedContacts.append(addedContact)
        }
        
        return returnedContacts
    }
    
    private func createSteps() -> NSArray {
        var steps: [NSDictionary] = []
        let emptySteps: [NSDictionary] = []
        
        let dateFormat: NSDateFormatter = NSDateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormat.timeZone = NSTimeZone.localTimeZone()
        dateFormat.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        
        if drive == nil {
            return emptySteps
        }
        
        if scheduleCalendar == nil {
            return emptySteps
        }
        
        if timesOfDayDistribution.count < 3 {
            return emptySteps
        }
        
        if let description = drive!["step"] as? NSString, countWeeks = drive!["countWeeks"] as? NSNumber {
            if let calendar = Serv.calendar {
                let daysInEachWeek = NSMutableDictionary()
                let selectedDays = scheduleCalendar!.getSelectedDays()
                
                for i in 0..<Int(countWeeks) {
                    daysInEachWeek.setObject(0, forKey: i)
                }
                
                // Do the morning ones first.
                for day in selectedDays {
                    let timesPerDayMorning = self.timesOfDayDistribution.count >= 3 ? self.timesOfDayDistribution[0] : 0
                    
                    for timePerDay in 0..<timesPerDayMorning {
                        if let startDateComponents = getStartDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayMorning, timeOfDay: ScheduledTimeOfDay.Morning), endDateComponents = getEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayMorning, timeOfDay: ScheduledTimeOfDay.Morning), notifyStartDateComponents = getNotifyStartDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayMorning, timeOfDay: ScheduledTimeOfDay.Morning), hardEndDateComponents = getHardEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayMorning, timeOfDay: ScheduledTimeOfDay.Morning) {
                            if let startDate = calendar.dateFromComponents(startDateComponents), endDate = calendar.dateFromComponents(endDateComponents), notifyStartDate = calendar.dateFromComponents(notifyStartDateComponents), hardEndDate = calendar.dateFromComponents(hardEndDateComponents) {
                                
                                let step: NSMutableDictionary = NSMutableDictionary()
                                
                                let dateStartString = dateFormat.stringFromDate(startDate)
                                let dateEndString = dateFormat.stringFromDate(endDate)
                                let dateNotifyStartString = dateFormat.stringFromDate(notifyStartDate)
                                let dateHardEndString = dateFormat.stringFromDate(hardEndDate)
                                
                                step.setObject(dateStartString, forKey: "start")
                                step.setObject(dateEndString, forKey: "end")
                                step.setObject(dateHardEndString, forKey: "hardEnd")
                                step.setObject(dateNotifyStartString, forKey: "notifyStart")
                                
                                step.setObject(1000 * startDate.timeIntervalSince1970, forKey: "startTimeUTC")
                                step.setObject(1000 * endDate.timeIntervalSince1970, forKey: "endTimeUTC")
                                step.setObject(1000 * hardEndDate.timeIntervalSince1970, forKey: "hardEndTimeUTC")
                                step.setObject(1000 * notifyStartDate.timeIntervalSince1970, forKey: "notifyStartTimeUTC")
                                
                                step.setObject(description as String, forKey: "description")
                                step.setObject(true, forKey: "doubleCheck")
                                step.setObject(ScheduledTimeOfDay.Morning.rawValue, forKey: "timeOfDay")
                                
                                if let week = day.weekNumber {
                                    if let index = daysInEachWeek.objectForKey(week) as? Int {
                                        step.setObject(week, forKey: "week")
                                        step.setObject(index, forKey: "index")
                                        daysInEachWeek.setObject(1 + index, forKey: week)
                                    } else {
                                        return emptySteps
                                    }
                                } else {
                                    return emptySteps
                                }
                                
                                steps.append(step)
                            }
                        }
                    }
                    
                    let timesPerDayDay = self.timesOfDayDistribution.count >= 3 ? self.timesOfDayDistribution[1] : 0
                    
                    for timePerDay in 0..<timesPerDayDay {
                        if let startDateComponents = getStartDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayDay, timeOfDay: ScheduledTimeOfDay.Day), endDateComponents = getEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayDay, timeOfDay: ScheduledTimeOfDay.Day), notifyStartDateComponents = getNotifyStartDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayDay, timeOfDay: ScheduledTimeOfDay.Day), hardEndDateComponents = getHardEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayDay, timeOfDay: ScheduledTimeOfDay.Day) {
                            if let startDate = calendar.dateFromComponents(startDateComponents), endDate = calendar.dateFromComponents(endDateComponents), notifyStartDate = calendar.dateFromComponents(notifyStartDateComponents), hardEndDate = calendar.dateFromComponents(hardEndDateComponents) {
                                
                                let step: NSMutableDictionary = NSMutableDictionary()
                                
                                let dateStartString = dateFormat.stringFromDate(startDate)
                                let dateEndString = dateFormat.stringFromDate(endDate)
                                let dateNotifyStartString = dateFormat.stringFromDate(notifyStartDate)
                                let dateHardEndString = dateFormat.stringFromDate(hardEndDate)
                                
                                step.setObject(dateStartString, forKey: "start")
                                step.setObject(dateEndString, forKey: "end")
                                step.setObject(dateHardEndString, forKey: "hardEnd")
                                step.setObject(dateNotifyStartString, forKey: "notifyStart")
                                
                                step.setObject(1000 * startDate.timeIntervalSince1970, forKey: "startTimeUTC")
                                step.setObject(1000 * endDate.timeIntervalSince1970, forKey: "endTimeUTC")
                                step.setObject(1000 * hardEndDate.timeIntervalSince1970, forKey: "hardEndTimeUTC")
                                step.setObject(1000 * notifyStartDate.timeIntervalSince1970, forKey: "notifyStartTimeUTC")
                                
                                step.setObject(description as String, forKey: "description")
                                step.setObject(true, forKey: "doubleCheck")
                                step.setObject(ScheduledTimeOfDay.Day.rawValue, forKey: "timeOfDay")
                                
                                if let week = day.weekNumber {
                                    if let index = daysInEachWeek.objectForKey(week) as? Int {
                                        step.setObject(week, forKey: "week")
                                        step.setObject(index, forKey: "index")
                                        daysInEachWeek.setObject(1 + index, forKey: week)
                                    } else {
                                        return emptySteps
                                    }
                                } else {
                                    return emptySteps
                                }
                                
                                steps.append(step)
                            }
                        }
                    }
                    
                    let timesPerDayEvening = self.timesOfDayDistribution.count >= 3 ? self.timesOfDayDistribution[2] : 0
                    
                    for timePerDay in 0..<timesPerDayEvening {
                        if let startDateComponents = getStartDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayEvening, timeOfDay: ScheduledTimeOfDay.Evening), endDateComponents = getEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayEvening, timeOfDay: ScheduledTimeOfDay.Evening), notifyStartDateComponents = getNotifyStartDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayEvening, timeOfDay: ScheduledTimeOfDay.Evening), hardEndDateComponents = getHardEndDateComponents(day.date, timePerDay: timePerDay, timesPerDay: timesPerDayEvening, timeOfDay: ScheduledTimeOfDay.Evening) {
                            if let startDate = calendar.dateFromComponents(startDateComponents), endDate = calendar.dateFromComponents(endDateComponents), notifyStartDate = calendar.dateFromComponents(notifyStartDateComponents), hardEndDate = calendar.dateFromComponents(hardEndDateComponents) {
                                
                                let step: NSMutableDictionary = NSMutableDictionary()
                                
                                let dateStartString = dateFormat.stringFromDate(startDate)
                                let dateEndString = dateFormat.stringFromDate(endDate)
                                let dateNotifyStartString = dateFormat.stringFromDate(notifyStartDate)
                                let dateHardEndString = dateFormat.stringFromDate(hardEndDate)
                                
                                step.setObject(dateStartString, forKey: "start")
                                step.setObject(dateEndString, forKey: "end")
                                step.setObject(dateHardEndString, forKey: "hardEnd")
                                step.setObject(dateNotifyStartString, forKey: "notifyStart")
                                
                                step.setObject(1000 * startDate.timeIntervalSince1970, forKey: "startTimeUTC")
                                step.setObject(1000 * endDate.timeIntervalSince1970, forKey: "endTimeUTC")
                                step.setObject(1000 * hardEndDate.timeIntervalSince1970, forKey: "hardEndTimeUTC")
                                step.setObject(1000 * notifyStartDate.timeIntervalSince1970, forKey: "notifyStartTimeUTC")
                                
                                step.setObject(description as String, forKey: "description")
                                step.setObject(true, forKey: "doubleCheck")
                                step.setObject(ScheduledTimeOfDay.Evening.rawValue, forKey: "timeOfDay")
                                
                                if let week = day.weekNumber {
                                    if let index = daysInEachWeek.objectForKey(week) as? Int {
                                        step.setObject(week, forKey: "week")
                                        step.setObject(index, forKey: "index")
                                        daysInEachWeek.setObject(1 + index, forKey: week)
                                    } else {
                                        return emptySteps
                                    }
                                } else {
                                    return emptySteps
                                }
                                
                                steps.append(step)
                            }
                        }
                    }
                }
            }
        }
    
        print(steps)
        return steps
    }

    private func createBody() -> NSMutableDictionary {
        let body: NSMutableDictionary = NSMutableDictionary()
        
        if drive == nil {
            return body
        }
        
        if let driveId = drive!["id"] as? NSString {
            body.setObject(driveId as String, forKey: "drive")
        }
        
        let steps = createSteps()
        body.setObject(steps, forKey: "steps")
        
        let reconnectContacts = createContacts()
        body.setObject(reconnectContacts, forKey: "reconnectContacts")
        
        return body
    }
    
    private func setNextStepLabel(state: ScheduleCalendarState, countStepsSelectedInWeek: Int, countStepsPerWeek: Int) {
        switch state {
        case ScheduleCalendarState.SuccessNoDaySelected:
            self.showNextStepLabel.text = "Choose the first day."
        case ScheduleCalendarState.SuccessFirstDayOfFirstWeekSelected:
            let difference = countStepsPerWeek - countStepsSelectedInWeek
            self.showNextStepLabel.text = "Choose \(difference) more day\(difference == 1 ? "" : "s") in this week."
        case ScheduleCalendarState.SuccessFirstWeekSelected, ScheduleCalendarState.SuccessAFewWeeksSelected:
            if countStepsSelectedInWeek > 0 {
                let difference = countStepsPerWeek - countStepsSelectedInWeek
                self.showNextStepLabel.text = "Choose \(difference) more day\(difference == 1 ? "" : "s") in this week."
            } else {
                self.showNextStepLabel.text = "Choose the first day for the next week."
            }
        case ScheduleCalendarState.SuccessAllWeeksSelected:
            self.showNextStepLabel.text = "You are done."
        case ScheduleCalendarState.ErrorTooManyDaysInWeek, ScheduleCalendarState.ErrorTooManyWeeks, ScheduleCalendarState.ErrorNotPrefix, ScheduleCalendarState.ErrorDateBeforeToday:
            break
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressLoadingFromScheduleDateHabit" {
            if let viewController = segue.destinationViewController as? ProgressLoadingController {
                self.progressLoadingController = viewController
                viewController.setDelegate(self)
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1 + self.months.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if (self.drive == nil) {
            return defaultCell
        }
        
        if self.scheduleCalendar == nil {
            return defaultCell
        }
        
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("scheduleDateHeaderCell", forIndexPath: indexPath) as? ScheduleDateHeaderCell {
                cell.populate(self.drive!)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
            
            return defaultCell
        }
        
        if indexPath.section > self.months.count {
            return defaultCell
        }
        
        let month = self.months[indexPath.section - 1]
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("scheduleDateMonthCell", forIndexPath: indexPath) as? ScheduleDateMonthCell {
            cell.populate(month, controller: self, scheduleCalendar: self.scheduleCalendar!, initialDate: self.initialDate)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell
        }
        
        return defaultCell
    }
    
    func clickedDate(date: (year: Int, month: Int, day: Int)) {
        if let scheduleCalendar = self.scheduleCalendar {
            var bulkCount = 1
            
            if self.timesOfDayDistribution.count >= 3 {
                if self.timesOfDayDistribution[0] + self.timesOfDayDistribution[1] + self.timesOfDayDistribution[2] > 1 {
                    if let realDrive = self.drive {
                        if let countWeeks = realDrive["countWeeks"] as? NSNumber {
                            bulkCount = 7 * Int(countWeeks)
                        }
                    }
                }
            }
            
            let state = scheduleCalendar.toggleDate(date, bulkCount: bulkCount)
            let enabled = scheduleCalendar.isEnabled(date)
            
            switch state {
            case ScheduleCalendarState.ErrorDateBeforeToday:
                if enabled {
                    Serv.showErrorPopup("Schedule level", message: "Could not unselect this date, because the first date must be today the earliest.", okMessage: "Okay, got it.", controller: self, handler: nil)
                } else {
                    Serv.showErrorPopup("Schedule level", message: "Could not select this date, because the first date must be today the earliest.", okMessage: "Okay, got it.", controller: self, handler: nil)
                }
                
                return
            case ScheduleCalendarState.ErrorNotPrefix:
                if enabled {
                    Serv.showErrorPopup("Schedule level", message: "Could not unselect this date, because the schedule must be done in all previous weeks first.", okMessage: "Okay, got it.", controller: self, handler: nil)
                } else {
                    Serv.showErrorPopup("Schedule level", message: "Could not select this date, because the schedule must be done in all previous weeks first.", okMessage: "Okay, got it.", controller: self, handler: nil)
                }
                
                return
            case ScheduleCalendarState.ErrorTooManyWeeks:
                if enabled {
                    Serv.showErrorPopup("Schedule level", message: "Could not unselect this date, because there would be too many scheduled weeks.", okMessage: "Okay, got it.", controller: self, handler: nil)
                } else {
                    Serv.showErrorPopup("Schedule level", message: "Could not select this date, because there would be too many scheduled weeks.", okMessage: "Okay, got it.", controller: self, handler: nil)
                }
                
                return
            case ScheduleCalendarState.ErrorTooManyDaysInWeek:
                if enabled {
                    Serv.showErrorPopup("Schedule level", message: "Could not unselect this date, because there would be too many scheduled days in a week.", okMessage: "Okay, got it.", controller: self, handler: nil)
                } else {
                    Serv.showErrorPopup("Schedule level", message: "Could not select this date, because there would be too many scheduled days in a week.", okMessage: "Okay, got it.", controller: self, handler: nil)
                }
                
                return
            case ScheduleCalendarState.SuccessNoDaySelected, ScheduleCalendarState.SuccessAllWeeksSelected, ScheduleCalendarState.SuccessAFewWeeksSelected, ScheduleCalendarState.SuccessFirstWeekSelected, ScheduleCalendarState.SuccessFirstDayOfFirstWeekSelected:
                self.refresh()
                
                if let doesStartFiveMinutesBeforeSoft = doesStartFiveMinutesBeforeSoftEnd() {
                    if !doesStartFiveMinutesBeforeSoft && scheduleCalendar.countSelectedDays() == 1 {
                        Serv.showErrorPopup("Time frame already past", message: "The level cannot start now. Choose a future start date in the calendar, or switch to a later time of day in the previous screen.", okMessage: "Okay, got it.", controller: self, handler: nil)
                    }
                }
            }
        }
    }
    
    func doneLoading() {
        if let selfResponse = self.response {
            HabitsController.resetPrograms()
            self.performSegueWithIdentifier("habitsControllerCreatedDateHabit", sender: selfResponse)
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Do nothing more...
    }
}
