//
//  ReconnectModuleChooseContactController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Contacts
import RandomColorSwift
import Analytics

class ReconnectModuleChooseContactController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    private static var object = NSObject()
    
    @IBOutlet weak var contactsTableView: UITableView!
    
    var serverContacts = []
    var contacts = []
    var colors = [UIColor]()
    
    var selectedContact: NSDictionary?
    var selectedColor: UIColor?
    
    var program: NSDictionary?
    var step: NSDictionary?
    
    var refresher: UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ReconnectModuleChooseContactController)
        
        contactsTableView.estimatedRowHeight = contactsTableView.rowHeight
        contactsTableView.rowHeight = UITableViewAutomaticDimension
        
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
        self.shyNavBarManager.scrollView = contactsTableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ReconnectModuleChooseContactController.refreshPullToRefresh), forControlEvents: UIControlEvents.ValueChanged)
        contactsTableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let realProgram = self.program {
            if let realDrive = realProgram["drive"] as? NSDictionary {
                if let title = realDrive["title"] as? NSString, level = realDrive["level"] as? NSNumber {
                    self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
                }
            }
        }
        
        objc_sync_enter(ReconnectModuleChooseContactController.object)
        
        if self.serverContacts.count <= 0 {
            objc_sync_exit(ReconnectModuleChooseContactController.object)
            self.refreshFromServer()
        } else {
            objc_sync_exit(ReconnectModuleChooseContactController.object)
            self.refreshFromClient()
        }
        
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if let realStep = self.step {
            if let index = realStep["index"] as? NSNumber {
                dict["index"] = String(index)
            }
            
            if let week = realStep["week"] as? NSNumber {
                dict["week"] = String(week)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ReconnectModuleChooseContactController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ReconnectModuleChooseContactController))
        }
    }
    
    func refreshPullToRefresh() {
        self.refreshFromServer()
    }
    
    func refreshFromClient() {
        objc_sync_enter(ReconnectModuleChooseContactController.object)
        
        self.contacts = NSArray(array: self.serverContacts)
        
        self.colors.removeAll()
        
        for _ in self.contacts {
            self.colors.append(randomColor(hue: .Red, luminosity: .Bright))
        }
        
        self.contactsTableView.reloadData()
        
        objc_sync_exit(ReconnectModuleChooseContactController.object)
        
        if let refr = self.refresher {
            refr.endRefreshing()
        }
    }
    
    func refreshFromServer() {
        if let realProgram = self.program, realStep = self.step {
            if let programId = realProgram["id"] as? NSString, stepId = realStep["id"] as? NSString {
                Serv.showSpinner()
                
                HabitlabRestAPI.getReconnectContacts(programId as String, programStepId: stepId as String, handler: { (error, templatesJson) -> () in
                    Serv.hideSpinner()
                    
                    if (error != nil) {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        return
                    }
                    
                    if let templatesArray = templatesJson as? NSArray {
                        objc_sync_enter(ReconnectModuleChooseContactController.object)
                        self.serverContacts = templatesArray
                        objc_sync_exit(ReconnectModuleChooseContactController.object)
                        
                        self.refreshFromClient()
                    }
                })
            }
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showReconnectModuleChooseMethodFromReconnectModuleChooseContact") {
            if let viewController = segue.destinationViewController as? ReconnectModuleChooseMethodController {
                if let realSelectedContact = self.selectedContact, realSelectedColor = self.selectedColor {
                    viewController.program = self.program
                    viewController.step = self.step
                    viewController.contact = realSelectedContact
                    viewController.color = realSelectedColor
                }
            }
        }
    }
    
    func selectContact(contact: NSDictionary, color: UIColor) {
        self.selectedContact = contact
        self.selectedColor = color
        self.performSegueWithIdentifier("showReconnectModuleChooseMethodFromReconnectModuleChooseContact", sender: self)
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("reconnectModuleChooseContactHeaderCell", forIndexPath: indexPath) as? ReconnectModuleChooseContactHeaderCell {
                if let user = UserCache().getUser() {
                    cell.populate(user)
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    return cell
                }
            }
            
            return defaultCell
        }
        
        objc_sync_enter(ReconnectModuleChooseContactController.object)
        
        let color = colors[indexPath.row - 1]
        
        if let contact = contacts[indexPath.row - 1] as? NSDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("reconnectModuleChooseContactContactCell", forIndexPath: indexPath) as? ReconnectModuleChooseContactContactCell {
                cell.populate(self, contact: contact, color: color)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                objc_sync_exit(ReconnectModuleChooseContactController.object)
                return cell
            }
        }
        
        objc_sync_exit(ReconnectModuleChooseContactController.object)
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}