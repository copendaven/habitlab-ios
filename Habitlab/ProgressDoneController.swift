//
//  ProgressDoneController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/29/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

enum ProgressDoneBackgroundColor: String {
    case Red = "red"
    case Green = "green"
    case Blue = "blue"
}

class ProgressDoneController: UIViewController {
    private let serialQueue = dispatch_queue_create("me.habitlab.mobile.progressDoneController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    private static var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    
    private var friendlyMessagesDone: [String]?
    private var doneStatusDone: String?
    private var suggestionsFirstDone: [String]?
    private var suggestionSecondDone: String?
    
    private var circleTimer = NSTimer()
    private var delegate: UIViewController?
    private var backgroundColor: ProgressDoneBackgroundColor?
    
    @IBOutlet weak var circle: KDCircularProgress!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var friendlyMessageLabel: UILabel!
    @IBOutlet weak var doneStatusLabel: UILabel!
    
    @IBOutlet weak var suggestionFirstLabel: UILabel!
    @IBOutlet weak var suggestionSecondLabel: UILabel!
    
    @IBOutlet weak var backgroundView: UIView!
    
    func setDelegate(delegate: UIViewController?) {
        self.delegate = delegate
    }
    
    func setBackgroundColor(backgroundColor: ProgressDoneBackgroundColor) {
        self.backgroundColor = backgroundColor
    }
    
    func setDoneCaptions(friendlyMessages: [String]?, doneStatus: String?, suggestionsFirst: [String]?, suggestionSecond: String?) {
        self.friendlyMessagesDone = friendlyMessages
        self.doneStatusDone = doneStatus
        self.suggestionsFirstDone = suggestionsFirst
        self.suggestionSecondDone = suggestionSecond
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        circle.progressThickness = 0.25
        circle.trackThickness = 0.25
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        friendlyMessageLabel.alpha = 0.0
        doneStatusLabel.alpha = 0.0
        suggestionFirstLabel.alpha = 0.0
        suggestionSecondLabel.alpha = 0.0
        circle.alpha = 0.0
        
        if let color = self.backgroundColor {
            switch color {
            case ProgressDoneBackgroundColor.Green:
                self.backgroundView.backgroundColor = UIColor(red: 0.21, green: 0.84, blue: 0.6, alpha: 0.9)
            case ProgressDoneBackgroundColor.Red:
                self.backgroundView.backgroundColor = UIColor(red: 0.94, green: 0.32, blue: 0.37, alpha: 0.9)
            case ProgressDoneBackgroundColor.Blue:
                self.backgroundView.backgroundColor = UIColor(red: 0.55, green: 0.75, blue: 0.76, alpha: 0.9)
            }
        }
        
        circleTimer.invalidate()
        circle.angle = 0
        
        activityIndicator.startAnimating()
        
        UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.activityIndicator.alpha = 1
        }, completion: nil)
    }
    
    func setCircleProgressLoading(timer: NSTimer) {
        dispatch_async(dispatch_get_main_queue()) {
            
            // Set the circle to load exponentially based on its current angle.
            let angle = NSInteger((360 + 3 * self.circle.angle) / 4)
            self.circle.animateToAngle(angle, duration: 0.1, relativeDuration: true, completion: nil)
        }
    }
    
    func markDone() {
        dispatch_async(dispatch_get_main_queue(), {
            self.circleTimer.invalidate()
        
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                if let realActivityIndicator = self.activityIndicator {
                    realActivityIndicator.alpha = 0.0
                }
                
                if let realCircle = self.circle {
                    realCircle.alpha = 1.0
                }
                }, completion:
                { (success) in
                    dispatch_async(self.serialQueue, {
                        if let friendlyMessagesExisting = self.friendlyMessagesDone, doneStatusExisting = self.doneStatusDone {
                            if friendlyMessagesExisting.count > 0 {
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.friendlyMessageLabel.text = friendlyMessagesExisting[Int(arc4random_uniform(UInt32(friendlyMessagesExisting.count)))]
                                    self.doneStatusLabel.text = doneStatusExisting
                                    
                                    UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                                        self.friendlyMessageLabel.alpha = 1.0
                                        self.doneStatusLabel.alpha = 1.0
                                        }, completion: nil)
                                })
                            }
                        }
                        
                        self.playDoneSound()
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            self.circle.animateToAngle(360, duration: 0.5, relativeDuration: true) { (animated) in
                                if let suggestionsFirstExisting = self.suggestionsFirstDone, suggestionSecondExisting = self.suggestionSecondDone {
                                    dispatch_async(dispatch_get_main_queue(), {
                                        self.suggestionFirstLabel.text = suggestionsFirstExisting[Int(arc4random_uniform(UInt32(suggestionsFirstExisting.count)))]
                                        self.suggestionSecondLabel.text = suggestionSecondExisting
                                        
                                        UIView.animateWithDuration(0.5, delay: 0.5, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                                            self.suggestionFirstLabel.alpha = 1.0
                                            self.suggestionSecondLabel.alpha = 1.0
                                        }) { (success) in
                                            NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(ProgressDoneController.hideWithCallback), userInfo: nil, repeats: false)
                                        }
                                    })
                                } else {
                                    NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: #selector(ProgressDoneController.hideWithCallback), userInfo: nil, repeats: false)
                                }
                            }
                        })
                    })
            })
        })
    }
    
    private func playDoneSound() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: AVAudioSessionCategoryOptions.DuckOthers)
        } catch let error as NSError {
            print(error)
        }
        
        if let audioPath = NSBundle.mainBundle().pathForResource("done", ofType: "mp3") {
            do {
                ProgressDoneController.audioPlayer = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: audioPath))
                ProgressDoneController.audioPlayer.play() // Play the sound.
                
                NSTimer.scheduledTimerWithTimeInterval(ProgressDoneController.audioPlayer.duration, target: self, selector: #selector(ProgressDoneController.stopDoneSound), userInfo: nil, repeats: false)
            } catch {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate) // At least vibrate.
            }
        }
    }
    
    func stopDoneSound() {
        ProgressDoneController.audioPlayer.stop()
        
        do {
            try AVAudioSession.sharedInstance().setActive(false, withOptions: AVAudioSessionSetActiveOptions.NotifyOthersOnDeactivation)
        } catch {
            // Do nothing.
        }
    }
    
    func hideSilently() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func hideWithCallback() {
        if let _ = self.delegate as? CurrentHabitController {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        if let viewController = self.delegate as? RunningModuleController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.progressDoneFinished()
            })
        }
        
        if let viewController = self.delegate as? UnpluggingModuleController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.progressDoneFinished()
            })
        }
        
        if let viewController = self.delegate as? ReconnectModuleChooseMethodController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.progressDoneFinished()
            })
        }
        
        if let viewController = self.delegate as? ChoosePhysioPainController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.progressDoneFinished()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
