//
//  SharingEmotionsChooseEmotionsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

enum EmotionType : String {
    case Happy = "Happy"
    case Cool = "Cool"
    case Inspired = "Inspired"
    case Energized = "Energized"
    case InLove = "In love"
    case Zen = "Zen"
    case Sleepy = "Sleepy"
    case Funny = "Funny"
    case Smart = "Smart"
    // TODO: add any emotion to the emotions list below too!
    // TODO: keep these emotions in sync with the server emotions!
    
    static let allValues = [Happy, Cool, Inspired, Energized, InLove, Zen, Sleepy, Funny, Smart]
}

class SharingEmotionsChooseEmotionsController: GAITrackedViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    static func getSelectImage(emotion: EmotionType) -> UIImage {
        switch emotion {
        case .Cool:
            return UIImage(named: "SharingEmotionCool")!
        case .Energized:
            return UIImage(named: "SharingEmotionEnergized")!
        case .Funny:
            return UIImage(named: "SharingEmotionFunny")!
        case .Happy:
            return UIImage(named: "SharingEmotionHappy")!
        case .InLove:
            return UIImage(named: "SharingEmotionInLove")!
        case .Inspired:
            return UIImage(named: "SharingEmotionInspired")!
        case .Sleepy:
            return UIImage(named: "SharingEmotionSleepy")!
        case .Smart:
            return UIImage(named: "SharingEmotionSmart")!
        case .Zen:
            return UIImage(named: "SharingEmotionZen")!
        }
    }
    
    static func getFeedImage(emotion: EmotionType) -> UIImage {
        switch emotion {
        case .Cool:
            return UIImage(named: "SharingEmotionCoolFeed")!
        case .Energized:
            return UIImage(named: "SharingEmotionEnergizedFeed")!
        case .Funny:
            return UIImage(named: "SharingEmotionFunnyFeed")!
        case .Happy:
            return UIImage(named: "SharingEmotionHappyFeed")!
        case .InLove:
            return UIImage(named: "SharingEmotionInLoveFeed")!
        case .Inspired:
            return UIImage(named: "SharingEmotionInspiredFeed")!
        case .Sleepy:
            return UIImage(named: "SharingEmotionSleepyFeed")!
        case .Smart:
            return UIImage(named: "SharingEmotionSmartFeed")!
        case .Zen:
            return UIImage(named: "SharingEmotionZenFeed")!
        }
    }
    
    var stepId: String?
    var programId: String?
    
    private var sentMessage = false
    
    var progressLoadingController: ProgressLoadingController?
    
    @IBOutlet weak var emotionsCollectionView: UICollectionView!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var sendMessageImage: UIImageView!
    
    private var selectedEmotions = Set<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(SharingEmotionsChooseEmotionsController)
        
        emotionsCollectionView.delegate = self
        emotionsCollectionView.dataSource = self
        
        sendMessageImage.layer.cornerRadius = 0.5 * sendMessageImage.bounds.size.width
        sendMessageImage.alpha = 0.5
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let viewController = self.progressLoadingController {
            viewController.hideSilently()
        }
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        sentMessage = false
        self.refreshFromClient()
        self.segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(SharingEmotionsChooseEmotionsController))
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func filterFriends(receivedFriends: NSArray) -> NSArray {
        let processedFriends: NSMutableArray = NSMutableArray()
        processedFriends.addObjectsFromArray(receivedFriends as [AnyObject])
        return processedFriends
    }
    
    func refreshFromClient() {
        dispatch_async(dispatch_get_main_queue()) {
            self.emotionsCollectionView.reloadData()
        }
    }
    
    func setSelectedEmotion(emotion: EmotionType, selected: Bool) -> Bool {
        var success = true
        
        if selected {
            if selectedEmotions.count >= 4 {
                Serv.showErrorPopup("Too many emotions", message: "You can select at most four emotions.", okMessage: "Okay, got it", controller: self, handler: nil)
                success = false
            } else {
                selectedEmotions.insert(emotion.rawValue)
            }
        } else {
            selectedEmotions.remove(emotion.rawValue)
        }
        
        if selectedEmotions.count > 0 {
            self.sendMessageImage.alpha = 1.0
            self.sendMessageButton.enabled = true
        } else {
            self.sendMessageImage.alpha = 0.5
            self.sendMessageButton.enabled = false
        }
        
        return success
    }

    @IBAction func sendMessage(sender: AnyObject) {
        if sentMessage {
            return
        }
        
        if selectedEmotions.count <= 0 {
            return
        }
        
        if stepId == nil {
            return
        }
        
        self.sentMessage = true
            
        dispatch_async(dispatch_get_main_queue(), {
            self.performSegueWithIdentifier("showProgressLoadingFromSharingEmotionsChooseEmotions", sender: self)
        })
        
        Serv.showSpinner()
        let cacheId = NSUUID().UUIDString
        
        ProgramStepInspireCache().setStepInspired(self.stepId!)
        
        HabitlabRestAPI.postSendingEmotionsMessage(Array(selectedEmotions), stepId: self.stepId!, cacheId: cacheId, handler: { (error, object) -> () in
            Serv.hideSpinner()
            
            if error != nil {
                self.sentMessage = false
                
                if self.stepId != nil {
                    ProgramStepInspireCache().removeStepInspired(self.stepId!)
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    if let viewController = self.progressLoadingController {
                        viewController.hideSilently()
                    }
                })
                
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                if let viewController = self.progressLoadingController {
                    viewController.markDone("Shared", hideInSeconds: 1)
                }
            })
        })
    }

    @IBAction func clickedExit(sender: AnyObject) {
        doneLoading()
    }
    
    func doneLoading() {
        self.performSegueWithIdentifier("showCurrentHabitFromSharingEmotionsChooseEmotions", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressLoadingFromSharingEmotionsChooseEmotions" {
            if let viewController = segue.destinationViewController as? ProgressLoadingController {
                self.progressLoadingController = viewController
                viewController.setDelegate(self)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return EmotionType.allValues.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let horizontalDimension = floor(self.emotionsCollectionView.frame.size.width / 3.0)
        let verticalDimension = CGFloat(120)
        return CGSizeMake(horizontalDimension, verticalDimension)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenHeight = Int(emotionsCollectionView.frame.size.height)
        var contentHeight = Int(EmotionType.allValues.count * 120) // This is the size for one collection item with button and label
        
        if screenHeight <= contentHeight {
            contentHeight = screenHeight / 120 * 120
        }
        
        let padding = CGFloat((screenHeight - contentHeight) / 2)
        return UIEdgeInsets(top: padding, left: 0, bottom: padding, right: 0)
        
        
        /*
        
        let screenWidth = Int(emotionsCollectionView.frame.size.width)
        let screenHeight = Int(emotionsCollectionView.frame.size.height)
        
        var contentWidth = Int(EmotionType.allValues.count * 120)
        var contentHeight = Int(EmotionType.allValues.count * 120)
        
        if screenWidth <= contentWidth {
            contentWidth = screenWidth / 120 * 120
        }
        
        if screenHeight <= contentHeight {
            contentHeight = screenHeight / 120 * 120
        }
        
        let paddingWidth = CGFloat((screenWidth - contentWidth) / 2)
        let paddingHeight = CGFloat((screenHeight - contentHeight) / 2)
        
        return UIEdgeInsets(top: paddingHeight, left: paddingWidth, bottom: paddingHeight, right: paddingWidth)
 
        */
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        let emotion = EmotionType.allValues[indexPath.item]
        
        if let cell = emotionsCollectionView.dequeueReusableCellWithReuseIdentifier("sharingEmotionsEmotionCell", forIndexPath: indexPath) as? SharingEmotionsEmotionCell {
            cell.populate(self, emotion: emotion, setSelected: selectedEmotions.contains(emotion.rawValue))
            return cell
        }
        
        return defaultCell
    }
}
