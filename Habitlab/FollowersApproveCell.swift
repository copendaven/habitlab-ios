//
//  FollowersApproveCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowersApproveCell: FollowersCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var followApproved = false
    var followUnapproved = false
    
    func populate(follower: NSDictionary, controller: UIViewController) {
        super.populate(follower, controller: controller, userName: userName, userImage: userImage)
        followApproved = false
        followUnapproved = false
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickApproveButton(sender: AnyObject) {
        if self.followApproved {
            return
        }
        
        if follower != nil && controller != nil {
            if let user = follower!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    let approveAlert = UIAlertController(title: "Approve follow", message: "By approving the follow, your friend will start following.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    approveAlert.addAction(UIAlertAction(title: "Approve", style: .Default, handler: { (action: UIAlertAction!) in
                        self.followApproved = true
                        Serv.showSpinner()
                        
                        // Go girl, approve that user.
                        HabitlabRestAPI.putFollowApprove(id as String, handler: { (error, followed) -> () in
                            Serv.hideSpinner()
                            
                            if error != nil {
                                self.followApproved = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                            
                            if let followersController = self.controller! as? FollowersController {
                                followersController.refresh()
                            }
                        })
                    }))
                
                    approveAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
                    self.controller!.presentViewController(approveAlert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func clickUnapproveButton(sender: AnyObject) {
        if self.followUnapproved {
            return
        }
        
        if follower != nil && controller != nil {
            if let user = follower!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    let unapproveAlert = UIAlertController(title: "Unapprove follow", message: "By unapproving the follow, your friend will not follow you anymore.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    unapproveAlert.addAction(UIAlertAction(title: "Unapprove", style: .Default, handler: { (action: UIAlertAction!) in
                        self.followUnapproved = true
                        Serv.showSpinner()
                        
                        HabitlabRestAPI.putFollowUnapprove(id as String, handler: { (error, followed) -> () in
                            Serv.hideSpinner()
                            
                            if error != nil {
                                self.followUnapproved = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                            
                            if let followersController = self.controller! as? FollowersController {
                                followersController.refresh()
                            }
                        })
                    }))
                
                    unapproveAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
                    self.controller!.presentViewController(unapproveAlert, animated: true, completion: nil)
                }
            }
        }
    }
}