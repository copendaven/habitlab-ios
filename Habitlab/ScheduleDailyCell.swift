//
//  ScheduleDailyCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/9/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import MGSwipeTableCell

public class ScheduleDailyCell: MGSwipeTableCell, MGSwipeTableCellDelegate {
    private var minusRightButton: MGSwipeButton?
    private var numberRightButton: MGSwipeButton?
    private var plusRightButton: MGSwipeButton?
    
    private var countTimesLabel: UILabel?
    private var backLabel: UILabel?
    
    public func swipeTableCell(cell: MGSwipeTableCell!, canSwipe direction: MGSwipeDirection) -> Bool {
        print("Called can swipe")
        print(direction)
        
        switch direction {
        case .LeftToRight:
            print("Left to right")
        case .RightToLeft:
            print("Right to left")
        }
        
        return true
    }
    
    public func swipeTableCell(cell: MGSwipeTableCell!, didChangeSwipeState state: MGSwipeState, gestureIsActive: Bool) {
        print("Called did change swipe state")
        print(state)
        print(gestureIsActive)
        
        switch state {
        case .ExpandingLeftToRight:
            print("Expanding left to right")
        case .ExpandingRightToLeft:
            print("Expanding right to left")
        case .None:
            print("Expanding none")
            
            if let realCountTimesLabel = self.countTimesLabel {
                UIView.animateWithDuration(0.35, animations: {
                    realCountTimesLabel.alpha = 1.0
                })
            }
            
            if let realBackLabel = self.backLabel {
                UIView.animateWithDuration(0.35, animations: {
                    realBackLabel.alpha = 0.0
                })
            }
        case .SwipingLeftToRight:
            print("Swiping left to right")
        case .SwipingRightToLeft:
            print("Swiping right to left")
        }
    }
    
    public func swipeTableCell(cell: MGSwipeTableCell!, tappedButtonAtIndex index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        print("Tapped button at index")
        print(index)
        print(direction)
        print(fromExpansion)
        
        switch direction {
        case .LeftToRight:
            print("Left to right")
        case .RightToLeft:
            print("Right to left")
        }
        
        return false
    }
    
    public func swipeTableCell(cell: MGSwipeTableCell!, swipeButtonsForDirection direction: MGSwipeDirection, swipeSettings: MGSwipeSettings!, expansionSettings: MGSwipeExpansionSettings!) -> [AnyObject]! {
        print("Called swipe buttons for direction")
        print(direction)
        print(swipeSettings)
        print(expansionSettings)
        
        switch direction {
        case .LeftToRight:
            print("Left to right")
        case .RightToLeft:
            print("Right to left")
        }
        
        let response: [AnyObject] = []
        return response
    }
    
    public func swipeTableCell(cell: MGSwipeTableCell!, shouldHideSwipeOnTap point: CGPoint) -> Bool {
        print("Should hide swipe on tap")
        print(point)
        return true
    }
    
    public func swipeTableCellWillBeginSwiping(cell: MGSwipeTableCell!) {
        print("Swipe table will begin swiping")
        
        if let realCountTimesLabel = self.countTimesLabel {
            realCountTimesLabel.alpha = 0.0
        }
        
        if let realBackLabel = self.backLabel {
            realBackLabel.alpha = 1.0
        }
    }
    
    public func swipeTableCellWillEndSwiping(cell: MGSwipeTableCell!) {
        print("Swipe table will end swiping")
    }
    
    func populate(countTimesLabel: UILabel, backLabel: UILabel, countTimes: Int, handleMinus: ((sender: MGSwipeTableCell!) -> Bool), handlePlus: ((sender: MGSwipeTableCell!) -> Bool)) {
        self.countTimesLabel = countTimesLabel
        self.backLabel = backLabel
        
        if self.minusRightButton == nil {
            self.minusRightButton = MGSwipeButton(title: "-", backgroundColor: UIColor.redColor(), callback: handleMinus)
            self.plusRightButton?.buttonWidth = 70
        }
        
        if self.numberRightButton == nil {
            self.numberRightButton = MGSwipeButton(title: "\(countTimes)", backgroundColor: UIColor.lightGrayColor())
            self.plusRightButton?.buttonWidth = 70
        }
        
        if self.plusRightButton == nil {
            self.plusRightButton = MGSwipeButton(title: "+", backgroundColor: UIColor.greenColor(), callback: handlePlus)
            self.plusRightButton?.buttonWidth = 70
        }
        
        if let realMinusButton = self.minusRightButton, realNumberButton = self.numberRightButton, realPlusButton = self.plusRightButton {
            self.rightButtons = [realPlusButton, realNumberButton, realMinusButton]
        }
        
        self.rightSwipeSettings.transition = MGSwipeTransition.Static
        self.delegate = self
    }
    
    func setCountTimes(countTimes: Int) {
        dispatch_async(dispatch_get_main_queue(), {
            if let realNumberRightButton = self.numberRightButton {
                realNumberRightButton.titleLabel?.text = "\(countTimes)"
            }
        })
    }
}
