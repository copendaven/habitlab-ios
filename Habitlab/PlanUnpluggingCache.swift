//
//  PlanUnpluggingCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class PlanUnpluggingCache: NSObject {
    private static var caches = Set<String>()
    private static var object = NSObject()
    
    static func erase() {
        objc_sync_enter(PlanUnpluggingCache.object)
        PlanUnpluggingCache.caches.removeAll()
        objc_sync_exit(PlanUnpluggingCache.object)
    }
    
    func setStepPlannedUnplugging(stepId: String) {
        objc_sync_enter(PlanUnpluggingCache.object)
        PlanUnpluggingCache.caches.insert(stepId)
        objc_sync_exit(PlanUnpluggingCache.object)
    }
    
    func removeStepPlannedUnplugging(stepId: String) {
        objc_sync_enter(PlanUnpluggingCache.object)
        PlanUnpluggingCache.caches.remove(stepId)
        objc_sync_exit(PlanUnpluggingCache.object)
    }
    
    func isStepPlannedUnplugging(stepId: String) -> Bool {
        objc_sync_enter(PlanUnpluggingCache.object)
        let result = PlanUnpluggingCache.caches.contains(stepId)
        objc_sync_exit(PlanUnpluggingCache.object)
        return result
    }
}