//
//  SharingEmotionsEmotionCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class SharingEmotionsEmotionCell: UICollectionViewCell {
    private var controller: UIViewController?
    private var emotion: EmotionType?
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var emotionLabel: UILabel!
    @IBOutlet weak var emotionPhoto: UIImageView!
    
    @IBAction func onClick(sender: AnyObject) {
        if controller == nil {
            return
        }
        
        if emotion == nil {
            return
        }
        
        if let sharingEmotionsController = controller as? SharingEmotionsChooseEmotionsController {
            if sharingEmotionsController.setSelectedEmotion(self.emotion!, selected: !self.selected) {
                self.selected = !self.selected
                self.borderView.hidden = !self.selected
            }
        }
    }
    
    func populate(controller: UIViewController, emotion: EmotionType, setSelected: Bool) {
        self.controller = controller
        self.emotion = emotion
        
        self.selected = setSelected
        self.borderView.hidden = !self.selected
        
        self.borderView.layer.cornerRadius = self.borderView.frame.size.width / 2
        self.emotionPhoto!.clipsToBounds = true
        
        dispatch_async(dispatch_get_main_queue()) {
            self.emotionLabel.text = emotion.rawValue
            self.emotionPhoto.image = SharingEmotionsChooseEmotionsController.getSelectImage(emotion)
        }
    }
}