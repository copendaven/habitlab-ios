//
//  ComparatorDoubleExtension.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/12/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

extension Double {
    func compare(other: Double) -> NSComparisonResult {
        let epsilon: Double = 0.00001
        
        if self < other - epsilon {
            return NSComparisonResult.OrderedAscending
        }
        
        if self > other + epsilon {
            return NSComparisonResult.OrderedDescending
        }
        
        return NSComparisonResult.OrderedSame
    }
}