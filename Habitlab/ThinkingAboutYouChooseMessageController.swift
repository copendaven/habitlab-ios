//
//  ThinkingAboutYouChooseMessageController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class ThinkingAboutYouChooseMessageController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    private static var object = NSObject()
    
    @IBOutlet weak var templatesTableView: UITableView!
    @IBOutlet weak var friendsButton: UIButton!
    @IBOutlet weak var friendsImage: UIImageView!
    
    var serverTemplates = []
    var templates = []
    
    var selectedTemplate: NSDictionary?
    var thinkingAboutYouState: ThinkingAboutYouState?
    
    var refresher: UIRefreshControl?
    
    var downloadedCount = 0
    var downloadedWindow = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ThinkingAboutYouChooseMessageController)
        
        templatesTableView.estimatedRowHeight = templatesTableView.rowHeight
        templatesTableView.rowHeight = UITableViewAutomaticDimension
        
        friendsImage.layer.cornerRadius = 0.5 * friendsImage.bounds.size.width
        friendsImage.alpha = 0.5
        
        templatesTableView.delegate = self
        templatesTableView.dataSource = self
        self.shyNavBarManager.scrollView = templatesTableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(ThinkingAboutYouChooseMessageController.refreshPullToRefresh), forControlEvents: UIControlEvents.ValueChanged)
        templatesTableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        templatesTableView.infiniteScrollIndicatorMargin = 40
        
        templatesTableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            self.refreshFromServer(true)
        }
        
        objc_sync_enter(ThinkingAboutYouChooseMessageController.object)
        
        if self.serverTemplates.count <= 0 {
            objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
            self.refreshFromServer(false)
        } else {
            objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
            self.refreshFromClient(false)
        }
        
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realState = self.thinkingAboutYouState {
            dict["thinkingAboutYouState"] = realState.rawValue
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ThinkingAboutYouChooseMessageController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ThinkingAboutYouChooseMessageController))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    func refreshPullToRefresh() {
        self.refreshFromServer(false)
    }
    
    func refreshFromClient(isInfiniteScroll: Bool) {
        objc_sync_enter(ThinkingAboutYouChooseMessageController.object)
        
        self.templates = NSArray(array: self.serverTemplates)
        self.templatesTableView.reloadData()
        
        if isInfiniteScroll {
            self.templatesTableView.finishInfiniteScroll()
        }
        
        objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
        
        if let refr = self.refresher {
            refr.endRefreshing()
        }
    }
    
    func refreshFromServer(isInfiniteScroll: Bool) {
        if (!isInfiniteScroll) {
            objc_sync_enter(ThinkingAboutYouChooseMessageController.object)
            downloadedCount = 0
            objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
        }
        
        objc_sync_enter(ThinkingAboutYouChooseMessageController.object)
        let skip = downloadedCount
        let limit = downloadedWindow
        objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
        
        Serv.showSpinner()
        
        HabitlabRestAPI.getThinkingAboutYouTemplates(skip, limit: limit, handler: { (error, templatesJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let templatesArray = templatesJson as? NSArray {
                objc_sync_enter(ThinkingAboutYouChooseMessageController.object)
                
                if !isInfiniteScroll {
                    self.serverTemplates = templatesArray
                } else {
                    var set = Set<String>()
                    let array = NSMutableArray()
                    
                    for templateJson in self.serverTemplates {
                        if let template = templateJson as? NSDictionary {
                            if let id = template["id"] as? NSString {
                                if !set.contains(id as String) {
                                    set.insert(id as String)
                                    array.addObject(templateJson)
                                }
                            }
                        }
                    }
                    
                    for templateJson in templatesArray {
                        if let template = templateJson as? NSDictionary {
                            if let id = template["id"] as? NSString {
                                if !set.contains(id as String) {
                                    set.insert(id as String)
                                    array.addObject(templateJson)
                                }
                            }
                        }
                    }
                    
                    self.serverTemplates = array
                }
                
                self.downloadedCount = self.serverTemplates.count
                
                objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
                
                self.refreshFromClient(isInfiniteScroll)
            }
        })
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showThinkingAboutYouFriendsFromThinkingAboutYouMessage") {
            if let viewController = segue.destinationViewController as? ThinkingAboutYouChooseFriendsController {
                if self.selectedTemplate != nil {
                    viewController.selectedTemplate = self.selectedTemplate!
                }
                
                if self.thinkingAboutYouState != nil {
                    viewController.thinkingAboutYouState = self.thinkingAboutYouState!
                }
            }
        }
    }
    
    func setSelected(template: NSDictionary) {
        self.selectedTemplate = template
        templatesTableView.reloadData()
        self.friendsButton.enabled = true
        self.friendsImage.alpha = 1.0
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return templates.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("thinkingAboutYouHeaderCell", forIndexPath: indexPath)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            return cell
        }
        
        objc_sync_enter(ThinkingAboutYouChooseMessageController.object)
        
        if templates.count <= indexPath.row {
            objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
            
            if let cell = tableView.dequeueReusableCellWithIdentifier("priorFeedCell", forIndexPath: indexPath) as? PriorFeedCell {
                cell.populate(self)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return cell
            }
            
            return defaultCell
        }
        
        if let template = templates[indexPath.row - 1] as? NSMutableDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("thinkingAboutYouTemplateCell", forIndexPath: indexPath) as? ThinkingAboutYouTemplateCell {
                
                var selected = false
                
                if let thisTemplateId = template["id"] as? NSString {
                    if selectedTemplate != nil {
                        if let selectedTemplateId = selectedTemplate!["id"] as? NSString {
                            if (thisTemplateId as String).compare(selectedTemplateId as String) == NSComparisonResult.OrderedSame {
                                selected = true
                            }
                        }
                    }
                }
                
                cell.populate(self, template: template, selected: selected)
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
                return cell
            }
        }
        
        objc_sync_exit(ThinkingAboutYouChooseMessageController.object)
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    
}
