//
//  UnpluggingPlanFeedCellWithComments.swift
//  Habitlab
//
//  Created by Vlad Manea on 26/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class UnpluggingPlanFeedCellWithComments: FeedCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var cellMessage: UILabel!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var firstComment: UILabel!
    @IBOutlet weak var missingComments: UILabel!
    @IBOutlet weak var secondToLastComment: UILabel!
    @IBOutlet weak var lastComment: UILabel!
    
    @IBOutlet weak var friendsCollectionView: UICollectionView!
    
    @IBOutlet weak var firstCommentTop: NSLayoutConstraint!
    @IBOutlet weak var firstCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var missingCommentTop: NSLayoutConstraint!
    @IBOutlet weak var missingCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var lastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var lastCommentBottom: NSLayoutConstraint!
    
    @IBOutlet weak var commentsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var thoughtCloudImage: UIImageView!
    
    private var photos: [String] = []
    
    private func getCommenting() -> NSDictionary {
        let commenting = NSMutableDictionary()
        
        commenting.setObject(firstComment, forKey: "firstComment")
        commenting.setObject(missingComments, forKey: "missingComments")
        commenting.setObject(secondToLastComment, forKey: "secondToLastComment")
        commenting.setObject(lastComment, forKey: "lastComment")
        
        commenting.setObject(firstCommentTop, forKey: "firstCommentTop")
        commenting.setObject(firstCommentBottom, forKey: "firstCommentBottom")
        commenting.setObject(missingCommentTop, forKey: "missingCommentTop")
        commenting.setObject(missingCommentBottom, forKey: "missingCommentBottom")
        commenting.setObject(secondToLastCommentTop, forKey: "secondToLastCommentTop")
        commenting.setObject(secondToLastCommentBottom, forKey: "secondToLastCommentBottom")
        commenting.setObject(lastCommentTop, forKey: "lastCommentTop")
        commenting.setObject(lastCommentBottom, forKey: "lastCommentBottom")
        
        commenting.setObject(commentsView, forKey: "commentsView")
        commenting.setObject(commentsViewHeight, forKey: "commentsViewHeight")
        
        return commenting
    }
    
    @IBAction override func like(sender: AnyObject) {
        super.like(sender)
    }
    
    @IBAction override func comment(sender: AnyObject) {
        super.comment(sender)
    }
    
    @IBAction override func clickMore(sender: AnyObject) {
        super.clickMore(sender)
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    func setLiking() {
        if let note = super.notification {
            super.setLiking(note, likingButton: self.likeButton, showLikeButton: true)
        }
    }
    
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, showButtons: Bool) {
        let commenting = getCommenting()
        
        self.friendsCollectionView.delegate = self
        self.friendsCollectionView.dataSource = self
        
        super.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, commentingButton: commentButton, commenting: commenting, likingButton: likeButton, optionsButton: optionsButton, showButtons: showButtons)

        dispatch_async(dispatch_get_main_queue()) {
            self.photos.removeAll()
            
            if let photo = notification["userSubjectFacebookPhoto"] as? NSString {
                self.photos.append(photo as String)
            }
            
            if let photos = notification["userObjectsFacebookPhotos"] as? NSArray {
                for photo in photos {
                    if let photoString = photo as? NSString {
                        self.photos.append(photoString as String)
                    }
                }
            }
            
            self.friendsCollectionView.reloadData()
            
            if let message = notification["message"] as? NSString {
                self.messageLabel.text = "\"\(message as String)\""
            }
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photos.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(70, 70)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenWidth = Int(friendsCollectionView.frame.size.width)
        var contentWidth = Int(photos.count * 70) // This is the size for one collection item with button and label
        
        if screenWidth <= contentWidth {
            contentWidth = screenWidth / 70 * 70
        }
        
        let padding = CGFloat((screenWidth - contentWidth) / 2)
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if photos.count <= indexPath.item {
            return defaultCell
        }
        
        if self.controller == nil {
            return defaultCell
        }
        
        if let cell = friendsCollectionView.dequeueReusableCellWithReuseIdentifier("unpluggingPlanFriendCell", forIndexPath: indexPath) as? UnpluggingPlanFriendCell {
            cell.populate(self.controller!, photo: photos[indexPath.item])
            return cell
        }
    
        return defaultCell
    }
}
