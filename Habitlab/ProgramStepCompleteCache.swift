//
//  ProgramStepCompleteCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ProgramStepCompleteCache: NSObject {
    private static var caches = Set<String>()
    private static var object = NSObject()
    
    static func erase() {
        objc_sync_enter(ProgramStepCompleteCache.object)
        ProgramStepCompleteCache.caches.removeAll()
        objc_sync_exit(ProgramStepCompleteCache.object)
    }
    
    func setStepComplete(stepId: String) {
        objc_sync_enter(ProgramStepCompleteCache.object)
        ProgramStepCompleteCache.caches.insert(stepId)
        objc_sync_exit(ProgramStepCompleteCache.object)
    }
    
    func unsetStepComplete(stepId: String) {
        objc_sync_enter(ProgramStepCompleteCache.object)
        ProgramStepCompleteCache.caches.remove(stepId)
        objc_sync_exit(ProgramStepCompleteCache.object)
    }
    
    func isStepComplete(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepCompleteCache.object)
        let result = ProgramStepCompleteCache.caches.contains(stepId)
        objc_sync_exit(ProgramStepCompleteCache.object)
        return result
    }
}