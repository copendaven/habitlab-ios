//
//  ReconnectModuleChooseContactHeaderCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ReconnectModuleChooseContactHeaderCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func populate(user: NSDictionary) {
        if let firstName = user["firstName"] as? NSString {
            self.titleLabel.text = "Hi \(firstName)"
        } else {
            self.titleLabel.text = "Reconnect with a friend"
        }
    }
}