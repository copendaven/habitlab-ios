//
//  ChooseNewLevelLockedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class ChooseNewLevelLockedCell: ChooseNewLevelCell {
    var controller: UIViewController?
    
    @IBOutlet weak var levelName: UILabel!
    @IBOutlet weak var levelLevel: UILabel!
    
    func populate(drive: NSDictionary, controller: UIViewController) {
        self.controller = controller
        super.populate(drive, levelName: levelName, levelLevel: levelLevel)
    }
}