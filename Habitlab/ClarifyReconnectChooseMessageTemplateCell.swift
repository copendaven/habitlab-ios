//
//  ClarifyReconnectChooseMessageTemplateCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ClarifyReconnectChooseMessageTemplateCell: UITableViewCell {
    private var template: NSDictionary?
    private var controller: UIViewController?
    
    @IBOutlet weak var button: UIButton!
    
    @IBAction func clickButton(sender: AnyObject) {
        if controller == nil {
            return
        }
        
        if template == nil {
            return
        }
        
        if let templateController = controller as? ClarifyReconnectChooseMessageController {
            templateController.setSelected(template!)
        }
    }
    
    private func setSelected(selected: Bool) {
        
        // Mark the button as selected
        button.selected = selected
        
        if selected {
            button.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.75)
        } else {
            button.backgroundColor = UIColor.clearColor()
        }
    }
    
    func populate(controller: UIViewController, template: NSDictionary, selected: Bool) {
        self.template = template
        self.controller = controller
        
        if let titleLabel = self.button.titleLabel {
            titleLabel.numberOfLines = 0
            titleLabel.textAlignment = NSTextAlignment.Center
        }
        
        if let reason = template["reason"] as? NSString {
            self.button.setTitle("\(reason as String)", forState: UIControlState.Normal)
            self.button.setTitle("\(reason as String)", forState: UIControlState.Selected)
        }
        
        setSelected(selected)
    }
}
