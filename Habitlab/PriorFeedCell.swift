//
//  PriorFeedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/10/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class PriorFeedCell: UITableViewCell {
    var controller: UIViewController?
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    func populate(controller: UIViewController) {
        self.controller = controller
        
        dispatch_async(dispatch_get_main_queue()) {
            if let navigationController = controller.navigationController {
                self.heightConstraint.constant = navigationController.navigationBar.frame.size.height + UIApplication.sharedApplication().statusBarFrame.size.height
            }
        }
    }
}
