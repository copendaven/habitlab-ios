//
//  ClarifyReconnectChooseContactCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Contacts

class ClarifyReconnectChooseContactCell: UITableViewCell {
    private var controller: UIViewController?
    private var contact: CNContact?
    private var color: UIColor?
    
    @IBOutlet weak var imageBorder: UIView!
    @IBOutlet weak var initialLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    func populate(controller: UIViewController, contact: CNContact, color: UIColor) {
        self.controller = controller
        self.contact = contact
        self.color = color
        
        dispatch_async(dispatch_get_main_queue()) {
            self.userImage.layer.cornerRadius = 0.5 * self.userImage.bounds.size.width
            self.initialLabel.layer.cornerRadius = 0.5 * self.initialLabel.bounds.size.width
            self.imageBorder.layer.cornerRadius = 0.5 * self.imageBorder.bounds.size.width
            self.userImage.clipsToBounds = true
            
            self.userName.text = "\(contact.givenName) \(contact.familyName)"
            
            if let imageData = contact.imageData {
                self.userImage.hidden = false
                self.initialLabel.hidden = true
                self.userImage.image = UIImage(data: imageData)
            } else {
                self.userImage.image = nil
                self.userImage.hidden = true
                self.initialLabel.hidden = false
                
                self.initialLabel.backgroundColor = color
                
                var initialLabelText = ""
                
                if contact.givenName.characters.count > 0 {
                    initialLabelText += contact.givenName[contact.givenName.startIndex.advancedBy(0)..<contact.givenName.startIndex.advancedBy(1)]
                }
                
                if contact.familyName.characters.count > 0 {
                    initialLabelText += contact.familyName[contact.familyName.startIndex.advancedBy(0)..<contact.familyName.startIndex.advancedBy(1)]
                }
                
                if initialLabelText == "" {
                    initialLabelText = "?"
                }
                
                self.initialLabel.text = initialLabelText.uppercaseString
            }
        }
    }
    
    @IBAction func clickedSelect(sender: AnyObject) {
        if let viewController = self.controller as? ClarifyReconnectChooseContactController, realContact = self.contact, realColor = self.color {
            viewController.clickSelectedContact(realContact, color: realColor)
        }
    }
}