//
//  ReconnectChooseActionController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

enum ReconnectActionType: String {
    case TalkAgainSoon = "TalkAgainSoon"
    case InvitedNameOver = "InvitedNameOver"
    case HaveDinner = "HaveDinner"
    case VisitName = "VisitName"
    case MeetForCoffee = "MeetForCoffee"
    case GoOnATrip = "GoOnATrip"
    case GoShopping = "GoShopping"
    case GoToMovies = "GoToMovies"
    case GoOut = "GoOut"
}

class ReconnectChooseActionController: GAITrackedViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private var sentMessage = false
    
    var progressLoadingController: ProgressLoadingController?
    
    @IBOutlet weak var actionsCollectionView: UICollectionView!
    @IBOutlet weak var phraseLabel: UILabel!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var sendMessageImage: UIImageView!
    
    var selectedAction: ReconnectActionType?
    var contact: NSDictionary?
    var step: NSDictionary?
    var program: NSDictionary?
    
    private var actions = [ReconnectActionType.TalkAgainSoon, ReconnectActionType.InvitedNameOver, ReconnectActionType.HaveDinner, ReconnectActionType.VisitName, ReconnectActionType.MeetForCoffee, ReconnectActionType.GoOnATrip, ReconnectActionType.GoShopping, ReconnectActionType.GoToMovies, ReconnectActionType.GoOut]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ReconnectChooseActionController)
        
        actionsCollectionView.delegate = self
        actionsCollectionView.dataSource = self
        
        sendMessageImage.layer.cornerRadius = 0.5 * sendMessageImage.bounds.size.width
        sendMessageImage.alpha = 0.5
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let viewController = self.progressLoadingController {
            viewController.hideSilently()
        }
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        if let realContact = self.contact {
            if let firstName = realContact["firstName"] as? NSString {
                self.phraseLabel.text = "What's next after reconnecting with \(firstName)?"
            }
        }
        
        self.sendMessageImage.alpha = 0.5
        self.sendMessageButton.enabled = false
        sentMessage = false
        
        self.refresh()
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if let realStep = self.step {
            if let index = realStep["index"] as? NSNumber {
                dict["index"] = String(index)
            }
            
            if let week = realStep["week"] as? NSNumber {
                dict["week"] = String(week)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ReconnectChooseActionController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ReconnectChooseActionController))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func refresh() {
        dispatch_async(dispatch_get_main_queue()) {
            self.actionsCollectionView.reloadData()
        }
    }
    
    func setSelectedAction(action: ReconnectActionType, selected: Bool) {
        if selected {
            self.selectedAction = action
            self.sendMessageImage.alpha = 1.0
            self.sendMessageButton.enabled = true
        } else {
            self.selectedAction = nil
            self.sendMessageImage.alpha = 0.5
            self.sendMessageButton.enabled = false
        }
        
        self.refresh()
    }
    
    @IBAction func sendMessage(sender: AnyObject) {
        if sentMessage {
            return
        }
        
        if selectedAction == nil {
            return
        }
        
        if contact == nil {
            return
        }
        
        if step == nil {
            return
        }
        
        if let contactId = self.contact!["id"] as? NSString, stepId = self.step!["id"] as? NSString {
            if ProgramStepInspireCache().isStepInspired(stepId as String) {
                return
            }
            
            ProgramStepInspireCache().setStepInspired(stepId as String)
            
            self.sentMessage = true
            
            dispatch_async(dispatch_get_main_queue(), {
                self.performSegueWithIdentifier("showProgressLoadingFromReconnectChooseAction", sender: self)
            })
            
            let cacheId = NSUUID().UUIDString
            Serv.showSpinner()
            
            HabitlabRestAPI.postReconnectAction(selectedAction!.rawValue, contactId: contactId as String, stepId: stepId as String, cacheId: cacheId, handler: { (error, object) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    self.sentMessage = false
                    
                    ProgramStepInspireCache().removeStepInspired(stepId as String)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        if let viewController = self.progressLoadingController {
                            viewController.hideSilently()
                        }
                    })
                    
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    if let viewController = self.progressLoadingController {
                        viewController.markDone("Shared", hideInSeconds: 1)
                    }
                })
            })
        }
    }
    
    func doneLoading() {
        self.performSegueWithIdentifier("showCurrentHabitFromReconnectChooseAction", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showProgressLoadingFromReconnectChooseAction" {
            if let viewController = segue.destinationViewController as? ProgressLoadingController {
                self.progressLoadingController = viewController
                viewController.setDelegate(self)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actions.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let horizontalDimension = floor(self.actionsCollectionView.frame.size.width / 3.0)
        let verticalDimension = CGFloat(120)
        return CGSizeMake(horizontalDimension, verticalDimension)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenHeight = Int(actionsCollectionView.frame.size.height)
        var contentHeight = Int(actions.count * 120) // This is the size for one collection item with button and label
        
        if screenHeight <= contentHeight {
            contentHeight = screenHeight / 120 * 120
        }
        
        let padding = CGFloat((screenHeight - contentHeight) / 2)
        return UIEdgeInsets(top: padding, left: 0, bottom: padding, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if actions.count <= indexPath.item {
            return defaultCell
        }
        
        let action = actions[indexPath.item]
        
        if let cell = actionsCollectionView.dequeueReusableCellWithReuseIdentifier("reconnectChooseActionActionCell", forIndexPath: indexPath) as? ReconnectChooseActionActionCell, realContact = self.contact {
            if selectedAction != nil && selectedAction!.rawValue.compare(action.rawValue) == NSComparisonResult.OrderedSame {
                cell.populate(self, action: action, contact: realContact, setSelected: true)
            } else {
                cell.populate(self, action: action, contact: realContact, setSelected: false)
            }
            
            return cell
        }
    
        return defaultCell
    }
}
