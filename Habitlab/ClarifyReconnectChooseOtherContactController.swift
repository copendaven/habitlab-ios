//
//  ClarifyReconnectChooseOtherContactController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Contacts
import Analytics
import RandomColorSwift

class ClarifyReconnectChooseOtherContactController: GAITrackedViewController, UITextFieldDelegate {
    var drive: NSDictionary?
    
    @IBOutlet weak var contactNameTextField: UITextField!
    @IBOutlet weak var contactNameTextFieldPlaceholder: UILabel!
    
    @IBOutlet weak var saveLabel: UILabel!
    @IBOutlet weak var saveButtonImage: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ClarifyReconnectChooseOtherContactController)
        
        contactNameTextField.delegate = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ClarifyReconnectChooseOtherContactController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        
        if let realText = contactNameTextField.text {
            self.refresh(realText)
        } else {
            self.refresh("")
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
        
        if let realText = contactNameTextField.text {
            self.refresh(realText)
        } else {
            self.refresh("")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        self.saveButtonImage.layer.cornerRadius = 0.5 * self.saveButtonImage.bounds.size.width
        self.saveButton.layer.cornerRadius = 0.5 * self.saveButton.bounds.size.width
        
        refresh("")
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realDrive = self.drive {
            if let categ = realDrive["driveCategory"] as? NSString {
                dict["category"] = categ as String
            }
            
            if let subcateg = realDrive["driveSubcategory"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
            
            if let level = realDrive["level"] as? NSNumber {
                dict["level"] = String(level)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseOtherContactController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseOtherContactController))
        }
    }
    
    private func refresh(text: String) {
        if text.characters.count > 0 {
            self.saveButton.enabled = true
            self.saveLabel.alpha = 1.0
            self.saveButtonImage.alpha = 1.0
            self.contactNameTextFieldPlaceholder.hidden = true
            return
        }
    
        self.saveButton.enabled = false
        self.saveLabel.alpha = 0.5
        self.saveButtonImage.alpha = 0.5
        self.contactNameTextFieldPlaceholder.hidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.contactNameTextFieldPlaceholder.hidden = true
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if let realText = self.contactNameTextField.text {
            self.refresh(realText)
        } else {
            self.refresh("")
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var finalText = ""
        
        if let textInside = textField.text {
            finalText = textInside
            
            let stringLength = string.characters.count
            let resultLength = finalText.characters.count
            
            if stringLength > 0 {
                finalText = finalText + string
            } else {
                if resultLength == 1 {
                    finalText = ""
                } else if resultLength > 1 {
                    finalText = finalText.substringToIndex(finalText.endIndex.predecessor())
                }
            }
        } else {
            finalText = string
        }
        
        self.refresh(finalText)
        return true
    }
    
    @IBAction func clickedSave(sender: AnyObject) {
        self.performSegueWithIdentifier("showClarifyReconnectChooseMessageFromClarifyReconnectChooseOtherContact", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showClarifyReconnectChooseMessageFromClarifyReconnectChooseOtherContact" {
            if let viewController = segue.destinationViewController as? ClarifyReconnectChooseMessageController {
                let contact = CNMutableContact()
                
                if let realText = contactNameTextField.text {
                    contact.givenName = realText
                } else {
                    contact.givenName = ""
                }
                
                contact.familyName = ""
                
                viewController.contact = contact
                viewController.color = randomColor(hue: .Red, luminosity: .Bright)
                viewController.drive = self.drive
            }
        }
    }
}
