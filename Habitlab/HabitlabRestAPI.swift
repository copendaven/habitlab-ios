//
//  HabitlabRestAPI.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class HabitlabRestAPI: NSObject {
    static let Endpoint: String = Configuration.getEndpoint() + "/rest/ios"
    
    private static func getVersion() -> Int {
        var version = 1
        
        if let dictionary = NSBundle.mainBundle().infoDictionary {
            if let realVersion = dictionary["CFBundleVersion"] as? String {
                version = Int(realVersion)!
            }
        }
        
        return version
    }

    static func getShouldReviewIosApp(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/shouldreviewiosapp")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func postAcceptIosAppReview(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/acceptiosappreview")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postRejectIosAppReview(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/rejectiosappreview")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func getNotificationsFeed(skip: Int, limit: Int, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        let version = HabitlabRestAPI.getVersion()
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/notifications-feed?skip=\(skip)&limit=\(limit)&iosappversion=\(version)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getNotificationsProfile(skip: Int, limit: Int, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/notifications-profile?skip=\(skip)&limit=\(limit)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getFakeNotification(mobileNotificationCacheId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/fakenotification/\(mobileNotificationCacheId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getFilteredNotification(owner: String, programStepObject: String, mobileNotificationType: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/filterednotification/?owner=\(owner)&programStepObject=\(programStepObject)&mobileNotificationType=\(mobileNotificationType)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getFilteredNotification(owner: String, mobileNotificationBase: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/filterednotification?owner=\(owner)&mobileNotificationBase=\(mobileNotificationBase)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getNotification(id: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/notification/\(id)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getPhysioInspireChartData(programId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/physioinspirechartdata/\(programId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getFollowers(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/followers")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getFollowing(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/following")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getLikings(mobileNotificationBaseId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/mobilenotificationlikers/\(mobileNotificationBaseId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getMe(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/user/me")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getUser(userId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/user/\(userId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getActiveProgramsByUser(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getProgram(id: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program/\(id)")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getPoints(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/points")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }

    static func getLevelPoints(level: NSNumber, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/level/\(level)")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getFriends(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/user/me/friends")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getThinkingAboutYouTemplates(skip: Int, limit: Int, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/thinkingaboutyoutemplate?skip=\(skip)&limit=\(limit)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getReconnectContacts(programId: String, programStepId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/reconnectcontact/\(programId)/\(programStepId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func postReconnectReasonTemplates(firstName: String, skip: Int, limit: Int, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/reconnectreasontemplate?skip=\(skip)&limit=\(limit)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        let body = ["firstName": firstName] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func postSharePhysioProgressWithPhysiotherapist(toggle: Bool, physioProgram: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/sharephysioprogresswithphysiotherapist")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        let body = ["physioProgram": physioProgram, "toggle": String(toggle)] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func postSharePhysioProgressWithFollowers(toggle: Bool, physioProgram: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/sharephysioprogresswithfollowers")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        let body = ["physioProgram": physioProgram, "toggle": String(toggle)] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getDriveCategories(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        let version = HabitlabRestAPI.getVersion()
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/category?iosappversion=\(version)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }

    static func getDriveSubcategoriesByCategory(category: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/subcategory?category=\(category)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }

    static func getDrivesByCategorySubcategory(category: String, subcategory: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/drive?driveCategory=\(category)&driveSubcategory=\(subcategory)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getDrivesByCategoryPhysioProgram(category: String, physioProgram: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/drive?driveCategory=\(category)&physioProgram=\(physioProgram)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getDriveById(id: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/drive/\(id)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func postFlagComment(commentId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/comment/\(commentId)/flag")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }

    static func putFollowApprove(userId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/approve/user/\(userId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putFollowUnapprove(userId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/unapprove/user/\(userId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putFollowBlock(userId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/block/user/\(userId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putFollowUnblock(userId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/unblock/user/\(userId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postFollow(userId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/follow/user/\(userId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func deleteUnfollow(userId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/unfollow/user/\(userId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "DELETE"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }

    static func postLike(notificationId: String, notificationBaseId: String, cacheId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/like/notification/\(notificationId)/\(notificationBaseId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["cacheId": cacheId] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func deleteUnlike(notificationId: String, notificationBaseId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/unlike/notification/\(notificationId)/\(notificationBaseId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "DELETE"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putWelcomeUser(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/welcome/user")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }

    static func postComment(notificationId: String, notificationBaseId: String, cacheId: String, message: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/comment/notification/\(notificationId)/\(notificationBaseId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["message": message, "cacheId": cacheId] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postSendingEmotionsMessage(emotions: [String], stepId: String, cacheId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/sharingemotionsmessage")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["emotions": emotions, "stepId": stepId, "cacheId": cacheId] as Dictionary<String, NSObject>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postThinkingAboutYouMessage(friends: [String], cacheId: String, templateId: String, message: String, state: ThinkingAboutYouState, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/thinkingaboutyoumessage")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["friends": friends, "thinkingAboutYouState": state.rawValue, "cacheId": cacheId, "message": message, "template": templateId] as Dictionary<String, NSObject>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postUnpluggingPlan(friends: [String], stepId: String, cacheId: String, message: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/unpluggingplan")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["friends": friends, "stepId": stepId, "cacheId": cacheId, "message": message] as Dictionary<String, NSObject>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putQuitProgram(driveId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program/quit/\(driveId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postIosAppToken(iosAppToken: String, indicator: UIActivityIndicatorView?, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/token")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["iosAppToken": iosAppToken] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postProgramSignup(body: NSDictionary, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postReconnectAction(action: String, contactId: String, stepId: String, cacheId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/reconnectaction")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["action": action, "contact": contactId, "programStep": stepId, "cacheId": cacheId] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putProgramStepComplete(source: String, stepId: String, cacheId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program/step/complete/\(stepId)")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        
        let body = ["cacheId": cacheId] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }

    static func putProgramStepCompleteWithPainLevels(source: String, stepId: String, cacheId: String, beforePainLevel: Int, afterPainLevel: Int, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program/step/complete/\(stepId)")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        
        let body = ["cacheId": cacheId, "beforePainLevel": String(beforePainLevel), "afterPainLevel": String(afterPainLevel)] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putProgramStepReconnectContact(contactId: String, programId: String, stepId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program/step/reconnect")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        
        let body = ["programId": programId, "programStepId": stepId, "contactId": contactId] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    
    
    
    
    
    
    static func putProgramStepStarted(source: String, stepId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program/step/started/\(stepId)")
        } catch let error as NSError {
            print(error)
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putProgramStepCapture(stepId: String, cacheId: String, message: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/program/step/capture/\(stepId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        
        let body = ["message": message, "cacheId": cacheId] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postTrackUser(userTrackingId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/trackuser/\(userTrackingId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
}
