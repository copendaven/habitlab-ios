//
//  ChooseNewProgramCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class ChooseNewProgramCell: UITableViewCell {
    var category: NSDictionary?
    var subcategory: NSDictionary?
    var controller: UIViewController?
    
    @IBOutlet weak var programPhoto: UIImageView!
    @IBOutlet weak var programName: UILabel!
    
    @IBAction func clickedNext(sender: AnyObject) {
        if let programController = controller as? ChooseNewProgramController {
            if category != nil && subcategory != nil {
                programController.performSegueWithIdentifier("showChooseNewLevelFromChooseNewProgram", sender: subcategory)
            }
        }
    }
    
    func populate(category: NSDictionary, subcategory: NSDictionary, controller: UIViewController) {
        self.category = category
        self.subcategory = subcategory
        self.controller = controller
        
        Serv.setChooseNewProgramText(category, subcategory: subcategory, label: programName)
        
        if let photo = subcategory["photo"] as? NSString {
            let url = Configuration.getEndpoint() + (photo as String)
            
            Serv.loadImage(url, handler: { (error, data) -> () in
                if error != nil {
                    print(error)
                }
                
                if data != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.programPhoto.image = UIImage(data: data!)
                    })
                }
            })
        }
    }
}
