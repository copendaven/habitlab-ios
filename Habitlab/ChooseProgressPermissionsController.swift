//
//  ChooseProgressPermissionsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class ChooseProgressPermissionsController: GAITrackedViewController, UITableViewDelegate, UITableViewDataSource {
    var subcategory: NSDictionary?
    var category: NSDictionary?
    var drive: NSDictionary?
    
    @IBOutlet weak var sendImage: UIImageView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var permissionsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ChooseProgressPermissionsController)
        
        self.permissionsTableView.dataSource = self
        self.permissionsTableView.delegate = self
        self.permissionsTableView.estimatedRowHeight = 80
        self.permissionsTableView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        self.sendImage.layer.cornerRadius = 0.5 * self.sendImage.bounds.size.width
        
        refresh()
        segment()
    }
    
    @IBAction func clickedSend(sender: AnyObject) {
        if let realDrive = self.drive {
            if let _ = realDrive["countStepsPerDay"] as? NSNumber {
                self.performSegueWithIdentifier("showScheduleDailyFromChooseProgressPermissions", sender: self.drive)
            } else if let _ = realDrive["countStepsPerWeek"] as? NSNumber {
                self.performSegueWithIdentifier("showScheduleTimeFromChooseProgressPermissions", sender: self.drive)
            }
        }
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realCategory = self.category {
            if let categ = realCategory["str"] as? NSString {
                dict["category"] = categ as String
            }
        }
        
        if let realSubcategory = self.subcategory {
            if let subcateg = realSubcategory["str"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ChooseProgressPermissionsController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ChooseProgressPermissionsController))
        }
    }
    
    func refresh() {
        if category == nil {
            return
        }
        
        if subcategory == nil {
            return
        }
        
        if drive == nil {
            return
        }
    }
    
    func switchChangedFollowers(value: Bool) {
        if let realSubcategory = self.subcategory {
            if let physioProgram = realSubcategory["physioProgram"] as? NSDictionary {
                if let physioProgramId = physioProgram["id"] as? NSString {
                    HabitlabRestAPI.postSharePhysioProgressWithFollowers(value, physioProgram: physioProgramId as String, handler: { (error, result) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        }
                    })
                }
            }
        }
    }
    
    func switchChangedPhysiotherapist(value: Bool) {
        if let realSubcategory = self.subcategory {
            if let physioProgram = realSubcategory["physioProgram"] as? NSDictionary {
                if let physioProgramId = physioProgram["id"] as? NSString {
                    HabitlabRestAPI.postSharePhysioProgressWithPhysiotherapist(value, physioProgram: physioProgramId as String, handler: { (error, result) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        }
                    })
                }
            }
        }
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showScheduleDailyFromChooseProgressPermissions") {
            if let viewController = segue.destinationViewController as? ScheduleDailyController {
                viewController.drive = sender as? NSDictionary
            }
        }
        
        if (segue.identifier == "showScheduleTimeFromChooseProgressPermissions") {
            if let viewController = segue.destinationViewController as? ScheduleTimeController {
                viewController.drive = sender as? NSDictionary
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("chooseProgressPermissionsPhysiotherapistCell", forIndexPath: indexPath) as? ChooseProgressPermissionsPhysiotherapistCell {
                return cell
            }
            
            return defaultCell
        }
        
        if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("chooseProgressPermissionsFollowersCell", forIndexPath: indexPath) as? ChooseProgressPermissionsFollowersCell {
                return cell
            }
            
            return defaultCell
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            if let realCell = cell as? ChooseProgressPermissionsPhysiotherapistCell {
                realCell.populate(self, subcategory: self.subcategory)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                
                let topLineView = UIView(frame: CGRectMake(0, 0, self.view.bounds.size.width, 1))
                topLineView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
                realCell.contentView.addSubview(topLineView)
                
                return
            }
            
            return
        }
        
        if indexPath.section == 1 {
            if let realCell = cell as? ChooseProgressPermissionsFollowersCell {
                realCell.populate(self, subcategory: self.subcategory)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                
                let topLineView = UIView(frame: CGRectMake(0, 0, self.view.bounds.size.width, 1))
                topLineView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
                realCell.contentView.addSubview(topLineView)
                
                let bottomLineView = UIView(frame: CGRectMake(0, realCell.bounds.size.height - 1, self.view.bounds.size.width, 1))
                bottomLineView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
                realCell.contentView.addSubview(bottomLineView)
                
                return
            }
            
            return
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Do nothing more...
    }
}