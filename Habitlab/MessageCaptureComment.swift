//
//  MessageCaptureComment.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/5/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class MessageCaptureComment: UITableViewCell, UITextViewDelegate {
    var controller: UIViewController?
    var stepId: String?
    var programId: String?
    var capturedImage: UIImage?
    let placeholderText = "Write a comment here"
    var captured = false
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var captureImage: UIImageView!
    @IBOutlet weak var messageTextView: UITextView!
    
    @IBAction func clickCancel(sender: AnyObject) {
        if controller == nil {
            return
        }
        
        self.controller!.performSegueWithIdentifier("currentHabitCancelledCapture", sender: self.controller!)
    }
    
    @IBAction func clickSend(sender: AnyObject) {
        if captured {
            return
        }
        
        if controller == nil {
            return
        }
        
        if stepId == nil {
            return
        }
        
        if programId == nil {
            return
        }
        
        if capturedImage == nil {
            return
        }
        
        if ProgramStepInspireCache().isStepInspired(stepId!) {
            return
        }
        
        ProgramStepInspireCache().setStepInspired(stepId!)
        
        var sentText = messageTextView.text
        
        if sentText == placeholderText {
            sentText = ""
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            self.progressView.hidden = false
            self.progressView.setProgress(0.1, animated: false)
            self.captureImage.alpha = 0.75
        }
        
        let resizedImage = Serv.resizeImageForFeedCapture(capturedImage!)
        
        let data = UIImageJPEGRepresentation(resizedImage, 0.5)
        
        if data == nil {
            dispatch_async(dispatch_get_main_queue()) {
                self.progressView.hidden = true
                self.captureImage.alpha = 1.0
            }
            
            return
        }
        
        self.captured = true
        let commentCacheId = NSUUID().UUIDString
        
        HabitlabS3API.uploadStepImage(self.stepId!, data: data!, handler: { (error) in
            if error != nil {
                print(error)
                
                if self.controller != nil {
                    Serv.showErrorPopupFromError(error!, controller: self.controller, handler: nil)
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.progressView.hidden = true
                    self.captureImage.alpha = 1.0
                }
                
                return
            }
            
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                Serv.showSpinner()
                
                HabitlabRestAPI.putProgramStepCapture(self.stepId!, cacheId: commentCacheId, message: sentText, handler: { (error, response) -> () in
                    Serv.hideSpinner()
                    
                    if error != nil {
                        self.captured = false
                        
                        if self.stepId != nil {
                            ProgramStepInspireCache().removeStepInspired(self.stepId!)
                        }
                        
                        if self.controller != nil {
                            Serv.showErrorPopupFromError(error, controller: self.controller!, handler: nil)
                        }
                        
                        return
                    }
                    
                    PhotoCache().setCache("programstep\(self.stepId!)", image: data!)
                    
                    if let inspirationCache = InspirationCache().getCache(self.stepId!) {
                        if let ownerId = inspirationCache.objectForKey("ownerId") as? String, mobileNotificationCacheId = inspirationCache.objectForKey("cacheId") as? String {
                            let owner = NSMutableDictionary()
                            owner.setObject(ownerId, forKey: "id")
                            
                            if let ownerFirstName = inspirationCache.objectForKey("ownerFirstName") as? String {
                                owner.setObject(ownerFirstName, forKey: "firstName")
                            }
                            
                            if let ownerLastName = inspirationCache.objectForKey("ownerLastName") as? String {
                                owner.setObject(ownerLastName, forKey: "lastName")
                            }
                            
                            if let ownerFacebookPhoto = inspirationCache.objectForKey("ownerFacebookPhoto") as? String {
                                owner.setObject(ownerFacebookPhoto, forKey: "facebookPhoto")
                            }
                            
                            MobileNotificationCommentCache().addInspiration(commentCacheId, owner: owner, mobileNotificationCacheId: mobileNotificationCacheId, message: sentText)
                        }
                    }
                    
                    self.controller!.performSegueWithIdentifier("currentHabitCompletedCapture", sender: self.controller!)
                })
            }
            }) { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                dispatch_async(dispatch_get_main_queue(), {
                    var progress = Float(0.0)
                    
                    if totalBytesExpectedToSend >= 1 {
                        progress = Float(totalBytesSent / totalBytesExpectedToSend)
                    }
                    
                    progress = 0.1 + 0.9 * progress
                    
                    self.progressView.setProgress(progress, animated: false)
                })
        }
    }

    func populate(controller: UIViewController, stepId: String, programId: String, image: UIImage) {
        self.controller = controller
        self.stepId = stepId
        self.programId = programId
        self.capturedImage = image

        messageTextView.delegate = self
        messageTextView.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5);
        messageTextView.text = placeholderText
        messageTextView.textColor = UIColor.lightGrayColor()

        captureImage.alpha = 1.0
        captureImage.image = self.capturedImage
        captured = false
    }

    func registerForKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageCaptureComment.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MessageCaptureComment.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if self.controller == nil {
            return
        }
        
        if let captureMessageViewController = self.controller! as? CaptureMessageViewController {
            if let userInfo = notification.userInfo {
                if let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                    var tabBarHeight = CGFloat(0.0)
                    
                    if let tabBarController = captureMessageViewController.tabBarController {
                        let tabbarSize = tabBarController.tabBar.frame
                        tabBarHeight = tabbarSize.height
                    }
                    
                    let keyboardSize = keyboardFrame.CGRectValue().size
                    let contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
                    
                    captureMessageViewController.commentsTable.contentInset = contentInset
                    captureMessageViewController.commentsTable.scrollIndicatorInsets = contentInset
                    
                    let tableRect = captureMessageViewController.commentsTable.frame
                    let compareRect = CGRectMake(0.0, 0.0, tableRect.size.width, tableRect.size.height - keyboardSize.height + tabBarHeight)
                    let offsetHeight = CGFloat(captureImage.frame.height)
                    let textboxFrame = CGRectMake(0.0, offsetHeight - tabBarHeight, messageTextView.frame.size.width, messageTextView.frame.size.height)
                    let textboxPoint = CGPointMake(0.0, offsetHeight + messageTextView.frame.size.height)
                    
                    if !CGRectContainsPoint(compareRect, textboxPoint) {
                        captureMessageViewController.commentsTable.scrollRectToVisible(textboxFrame, animated: false)
                    }
                }
            }
        }
    }

    func keyboardWillHide(notification: NSNotification) {
        if self.controller == nil {
            return
        }
        
        if let captureMessageViewController = self.controller! as? CaptureMessageViewController {
            if let userInfo = notification.userInfo {
                if let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                    let keyboardSize = keyboardFrame.CGRectValue().size
                    let contentInset = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize.height, 0.0)
                    captureMessageViewController.commentsTable.contentInset = contentInset
                    captureMessageViewController.commentsTable.scrollIndicatorInsets = contentInset
                    captureMessageViewController.view.endEditing(true)
                }
            }
        }
    }
    
    // MARK: UITextViewDelegate methods.
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == placeholderText {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
}
