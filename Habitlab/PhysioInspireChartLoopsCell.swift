//
//  PhysioInspireChartLoopsCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/19/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Charts

class PhysioInspireChartLoopsCell: UICollectionViewCell, ChartViewDelegate, ChartXAxisValueFormatter {
    private let serialQueue = dispatch_queue_create("me.habitlab.mobile.physioInspireChartPainCell.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    @IBOutlet weak var lineChartView: LineChartView!
    
    func populate(program: NSDictionary?, controller: UIViewController?) {
        self.lineChartView.delegate = self
        self.setChartStyle()
        
        dispatch_async(self.serialQueue) {
            if let realProgram = program {
                if let programId = realProgram["id"] as? NSString {
                    HabitlabRestAPI.getPhysioInspireChartData(programId as String, handler: { (error, stepsJson) in
                        if let steps = stepsJson as? NSArray {
                            self.setChartData(steps)
                        }
                    })
                }
            }
        }
    }
    
    func setChartStyle() {
        dispatch_async(dispatch_get_main_queue()) {
            self.lineChartView.drawGridBackgroundEnabled = false
            self.lineChartView.drawBordersEnabled = false
            self.lineChartView.multipleTouchEnabled = false
            self.lineChartView.dragEnabled = false
            self.lineChartView.setScaleEnabled(false)
            self.lineChartView.pinchZoomEnabled = false
            self.lineChartView.doubleTapToZoomEnabled = false
            self.lineChartView.highlightPerTapEnabled = false
            self.lineChartView.highlightPerDragEnabled = false
            self.lineChartView.descriptionText = ""
            self.lineChartView.animate(xAxisDuration: 1.0)
            self.lineChartView.legend.textColor = UIColor.whiteColor()
            self.lineChartView.infoTextColor = UIColor.whiteColor()
            
            let xAxis = self.lineChartView.xAxis
            xAxis.labelPosition = ChartXAxis.LabelPosition.Bottom
            xAxis.drawGridLinesEnabled = false
            xAxis.drawAxisLineEnabled = false
            xAxis.labelTextColor = UIColor.whiteColor()
            xAxis.avoidFirstLastClippingEnabled = true
            xAxis.valueFormatter = self
            xAxis.setLabelsToSkip(1)
            
            let leftAxis = self.lineChartView.leftAxis
            leftAxis.gridColor = UIColor.whiteColor()
            leftAxis.drawAxisLineEnabled = false
            leftAxis.drawZeroLineEnabled = false
            leftAxis.labelTextColor = UIColor.whiteColor()
            leftAxis.spaceTop = 25.0
            leftAxis.spaceBottom = 25.0
            leftAxis.axisMinValue = 0
            
            let rightAxis = self.lineChartView.rightAxis
            rightAxis.enabled = false
        }
    }
    
    func stringForXValue(index: Int, original: String, viewPortHandler: ChartViewPortHandler) -> String {
        return original
    }
    
    func setChartData(programSteps: NSArray) {
        
        // Create arrays of data entries
        var loops = [String]()
        var levels = [ChartDataEntry]()
        
        var weekIndex: Int? = nil
        var lastWeek: Int? = nil
        var lastProgram: String? = nil
        var weekQuantity: Int = 0
        var maxWeekQuantity: Int = 0
        var weekIndexShown = 1
        
        for i in 0..<programSteps.count {
            if let programStep = programSteps[i] as? NSDictionary {
                if let programStepProgram = programStep["program"] as? NSString, programStepWeek = programStep["week"] as? NSNumber, programStepComplete = programStep["complete"] as? NSNumber {
                    let isCompleteSignum = Int(programStepComplete)
                    
                    if lastWeek == programStepWeek && lastProgram == (programStepProgram as String) {
                        weekQuantity = weekQuantity + isCompleteSignum
                    } else {
                        if let realWeekIndex = weekIndex {
                            // It is shifting now.
                            loops.append("w\(weekIndexShown)")
                            weekIndexShown += 1
                            
                            levels.append(ChartDataEntry(value: Double(weekQuantity), xIndex: realWeekIndex))
                            maxWeekQuantity = max(maxWeekQuantity, weekQuantity)
                            weekIndex = realWeekIndex + 1
                        } else {
                            // It has just started now.
                            weekIndex = 1
                        }
                        
                        weekQuantity = isCompleteSignum
                    }
                    
                    lastWeek = Int(programStepWeek)
                    lastProgram = programStepProgram as String
                }
            }
        }
        
        if let realWeekIndex = weekIndex {
            loops.append("w\(weekIndexShown)")
            levels.append(ChartDataEntry(value: Double(weekQuantity), xIndex: realWeekIndex))
            maxWeekQuantity = max(maxWeekQuantity, weekQuantity)
        }
        
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = NSNumberFormatterStyle.NoStyle
        
        maxWeekQuantity = max(1, maxWeekQuantity)
        self.lineChartView.leftAxis.axisMaxValue = Double(maxWeekQuantity)
        
        // Create data set before with our array
        let setLoops = LineChartDataSet(yVals: levels, label: "Number of loops per week")
        setLoops.axisDependency = .Left
        setLoops.setColor(UIColor.cyanColor().colorWithAlphaComponent(0.5)) // our line's opacity is 50%
        setLoops.setCircleColor(UIColor.cyanColor()) // our circle will be dark red
        setLoops.lineWidth = 2.0
        setLoops.circleRadius = 3.0 // the radius of the node circle
        setLoops.fillAlpha = 65 / 255.0
        setLoops.fillColor = UIColor.cyanColor()
        setLoops.highlightColor = UIColor.whiteColor()
        setLoops.valueFormatter = numberFormatter
        setLoops.drawValuesEnabled = false
        setLoops.mode = LineChartDataSet.Mode.CubicBezier
        setLoops.drawCircleHoleEnabled = false
        
        // Create an array to store our LineChartDataSets
        var dataSets = [LineChartDataSet]()
        dataSets.append(setLoops)
        
        // Pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData = LineChartData(xVals: loops, dataSets: dataSets)
        data.setValueTextColor(UIColor.whiteColor())
        
        dispatch_async(dispatch_get_main_queue()) {
            self.lineChartView.data = data
        }
    }
}
