//
//  LockNotifierCallback.h
//  Habitlab
//
//  Created by Vlad Manea on 4/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

#ifndef LockNotifierCallback_h
#define LockNotifierCallback_h

@interface LockNotifierCallback : NSObject

+ (void(*)(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo))notifierProc;

@end

#endif /* LockNotifierCallback_h */
