//
//  HabitsCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class HabitsCell: UITableViewCell {
    var program: NSDictionary?
    var controller: UIViewController?
    
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelPhoto: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    @IBAction func clickedNext(sender: AnyObject) {
        if program != nil {
            if let habitsController = controller as? HabitsController {
                habitsController.performSegueWithIdentifier("showCurrentHabitFromHabits", sender: program)
            }
        }
    }
    
    func populate(program: NSDictionary, controller: UIViewController) {
        self.program = program
        self.controller = controller
        
        Serv.setMyHabitText(program, label: levelLabel)
        
        if let drive = program["drive"] as? NSDictionary {
            if let photo = drive["photo"] as? NSString {
                let url = Configuration.getEndpoint() + (photo as String)
                
                Serv.loadImage(url, handler: { (error, data) -> () in
                    if error != nil {
                        print(error)
                    }
                    
                    if data != nil {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.levelPhoto.image = UIImage(data: data!)
                        })
                    }
                })
            }
        }
    }
}