//
//  ScheduleTimeController.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Contacts
import Analytics

enum ScheduledTimeOfDay: NSString {
    case Morning = "morning"
    case Day = "day"
    case Evening = "evening"
}

class ScheduleTimeController: GAITrackedViewController {
    var drive: NSDictionary?
    var contacts: [CNContact] = []
    var contactMessages = NSMutableDictionary()
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ScheduleTimeController)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()

        if let realDrive = self.drive {
            if let title = realDrive["title"] as? NSString, level = realDrive["level"] as? NSNumber {
                self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
            }
        }
        
        refresh()
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realDrive = self.drive {
            if let categ = realDrive["driveCategory"] as? NSString {
                dict["category"] = categ as String
            }
            
            if let subcateg = realDrive["driveSubcategory"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
            
            if let level = realDrive["level"] as? NSNumber {
                dict["level"] = String(level)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ScheduleTimeController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ScheduleTimeController))
        }
    }
    
    func refresh() {
        if drive == nil {
            return
        }
        
        if let verb = drive!["verb"] as? NSString {
            self.questionLabel.text = "What time of day would you like to \(verb as String)?"
        }
        
        Serv.setDriveStatusText(drive!, label: self.detailsLabel)
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showScheduleDateFromScheduleTime") {
            if let dict = sender as? NSDictionary {
                if let timeOfDayString = dict["timeOfDay"] as? String, drive = dict["drive"] as? NSDictionary {
                    if let timeOfDay = ScheduledTimeOfDay(rawValue: timeOfDayString) {
                        if let viewController = segue.destinationViewController as? ScheduleDateController {
                            viewController.drive = drive
                            
                            switch timeOfDay {
                            case .Morning:
                                viewController.timesOfDayDistribution = [1, 0, 0]
                            case .Day:
                                viewController.timesOfDayDistribution = [0, 1, 0]
                            case .Evening:
                                viewController.timesOfDayDistribution = [0, 0, 1]
                            }
                            
                            viewController.contacts = self.contacts
                            viewController.contactMessages = self.contactMessages
                        }
                    }
                }
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    @IBAction func clickedMorning() {
        self.clickedTime(ScheduledTimeOfDay.Morning)
    }
    
    @IBAction func clickedDay() {
        self.clickedTime(ScheduledTimeOfDay.Day)
    }
    
    @IBAction func clickedEvening() {
        self.clickedTime(ScheduledTimeOfDay.Evening)
    }
    
    func clickedTime(timeOfDay: ScheduledTimeOfDay) {
        if drive == nil {
            return
        }
        
        let sender = NSMutableDictionary()
        sender.setObject(timeOfDay.rawValue, forKey: "timeOfDay")
        sender.setObject(self.drive!, forKey: "drive")
        
        self.performSegueWithIdentifier("showScheduleDateFromScheduleTime", sender: sender)
    }
}
