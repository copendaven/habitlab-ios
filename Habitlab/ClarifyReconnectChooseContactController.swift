//
//  ClarifyReconnectChooseContactController.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/15/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Contacts
import RandomColorSwift
import Analytics

class ClarifyReconnectChooseContactController: GAITrackedViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    var prohibitedContactIds: [String] = []
    
    var drive: NSDictionary?
    var contacts = [CNContact]()
    var colors = [UIColor]()
    var selectedContact: CNContact?
    var selectedColor: UIColor?
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ClarifyReconnectChooseContactController)
        
        self.searchTextField.delegate = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = self.tableView
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let realDrive = self.drive {
            if let title = realDrive["title"] as? NSString, level = realDrive["level"] as? NSNumber {
                self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
            }
        }
        
        self.refresh("")
        self.segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realDrive = self.drive {
            if let categ = realDrive["driveCategory"] as? NSString {
                dict["category"] = categ as String
            }
            
            if let subcateg = realDrive["driveSubcategory"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
            
            if let level = realDrive["level"] as? NSNumber {
                dict["level"] = String(level)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseContactController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseContactController))
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if let realText = textField.text {
            self.refresh(realText)
        } else {
            self.refresh("")
        }
        
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        self.refresh("")
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func refresh(query: String) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            AppDelegate.getAppDelegate().requestForAccessContacts { (accessGranted) in
                if accessGranted {
                    let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactIdentifierKey]
                    
                    self.contacts.removeAll()
                    self.colors.removeAll()
                    
                    do {
                        let contactsStore = AppDelegate.getAppDelegate().contactStore
                        
                        try contactsStore.enumerateContactsWithFetchRequest(CNContactFetchRequest(keysToFetch: keys)) { (contact, pointer) -> Void in
                            if self.prohibitedContactIds.contains(contact.identifier) {
                                return
                            }
                            
                            if contact.givenName == "" && contact.familyName == "" {
                                return
                            }
                            
                            if query != "" && contact.givenName.lowercaseString.rangeOfString(query.lowercaseString) == nil && contact.familyName.lowercaseString.rangeOfString(query.lowercaseString) == nil {
                                return
                            }
                            
                            self.contacts.append(contact)
                            self.colors.append(randomColor(hue: .Red, luminosity: .Bright))
                        }
                    } catch {
                        // Do nothing
                    }
                    
                    /*self.contacts.sortInPlace({ (contact1, contact2) -> Bool in
                        return contact1.givenName.compare(contact2.givenName) == NSComparisonResult.OrderedAscending
                    })*/
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.tableView.reloadData()
                    })
                }
            }
        }
    }
    
    @IBAction func clickedAddOtherContact(sender: AnyObject) {
        self.performSegueWithIdentifier("showClarifyReconnectChooseOtherContactFromClarifyReconnectChooseContact", sender: self)
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showClarifyReconnectChooseMessageFromClarifyReconnectChooseContact") {
            if let viewController = segue.destinationViewController as? ClarifyReconnectChooseMessageController {
                if let contact = self.selectedContact, color = self.selectedColor {
                    viewController.contact = contact
                    viewController.color = color
                    viewController.drive = drive
                }
            }
        }
        
        if segue.identifier == "showClarifyReconnectChooseOtherContactFromClarifyReconnectChooseContact" {
            if let viewController = segue.destinationViewController as? ClarifyReconnectChooseOtherContactController {
                viewController.drive = self.drive
            }
        }
    }
    
    func clickSelectedContact(contact: CNContact, color: UIColor) {
        self.selectedContact = contact
        self.selectedColor = color
        self.performSegueWithIdentifier("showClarifyReconnectChooseMessageFromClarifyReconnectChooseContact", sender: self)
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.contacts.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("clarifyReconnectChooseContactCell", forIndexPath: indexPath) as? ClarifyReconnectChooseContactCell {
            return cell
        }
    
        return UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section < self.contacts.count {
            let contact = self.contacts[indexPath.section]
            let color = self.colors[indexPath.section]
            
            if let realCell = cell as? ClarifyReconnectChooseContactCell {
                realCell.populate(self, contact: contact, color: color)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
            }
        }
    }

    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
