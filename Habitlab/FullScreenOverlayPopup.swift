//
//  FullScreenOverlayPopup.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/29/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

enum FullScreenOverlayPopupButtonType {
    case Green
    case White
}

class FullScreenOverlayPopupButton {
    var type: FullScreenOverlayPopupButtonType = FullScreenOverlayPopupButtonType.Green
    var title: String?
    var action: ((NSDictionary) -> ())?
    var dictionary: NSDictionary?
    
    init(type: FullScreenOverlayPopupButtonType, title: String?, dictionary: NSDictionary?, action: ((NSDictionary) -> ())?) {
        self.type = type
        self.title = title
        self.action = action
        self.dictionary = dictionary
    }
}

class FullScreenOverlayPopup: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    private var photoURL: String?
    private var photoNamed: String?
    private var titleValue: String?
    private var descriptionValue: String?
    private var buttons: [FullScreenOverlayPopupButton] = []
    
    private var controller: UIViewController?
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionButtonsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actionButtonsCollectionView.delegate = self
        actionButtonsCollectionView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.photoImage.layer.cornerRadius = 0.5 * self.photoImage.bounds.size.width
        self.photoImage.layer.borderWidth = 4.0
        self.photoImage.layer.borderColor = UIColor.whiteColor().CGColor
        self.photoImage.hidden = true
        
        if let realTitle = self.titleValue {
            self.titleLabel.text = realTitle
        }
        
        if let realDescription = self.descriptionValue {
            self.descriptionLabel.text = realDescription
        }
        
        if let realPhotoNamed = self.photoNamed {
            dispatch_async(dispatch_get_main_queue(), {
                self.photoImage.image = UIImage(named: realPhotoNamed)
                self.photoImage.hidden = false
            })
        } else if let realPhotoURL = self.photoURL {
            Serv.loadImage(realPhotoURL as String, handler: { (error, data) -> () in
                if error != nil {
                    print(error)
                }
                
                if data != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.photoImage.image = UIImage(data: data!)
                        self.photoImage.hidden = false
                    })
                }
            })
        }
        
        self.refresh()
    }
    
    private func refresh() {
        //
    }
    
    @IBAction func clickedOverlay(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func setController(controller: UIViewController) {
        self.controller = controller
    }
    
    func setContent(title: String?, description: String?, buttons: [FullScreenOverlayPopupButton]) {
        self.titleValue = title
        self.descriptionValue = description
        self.buttons = buttons
    }
    
    func setContent(title: String?, description: String?, photoURL: String?, buttons: [FullScreenOverlayPopupButton]) {
        self.setContent(title, description: description, buttons: buttons)
        self.photoURL = photoURL
        self.refresh()
    }
    
    func setContent(title: String?, description: String?, photoNamed: String?, buttons: [FullScreenOverlayPopupButton]) {
        self.setContent(title, description: description, buttons: buttons)
        self.photoNamed = photoNamed
        self.refresh()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return buttons.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(140, 46) // This is the size for one collection item with button and label
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenWidth = Int(actionButtonsCollectionView.frame.size.width)
        var contentWidth = Int(buttons.count * 140) // This is the size for one collection item with button and label
        
        if screenWidth <= contentWidth {
            contentWidth = screenWidth / 140 * 140
        }
        
        let padding = CGFloat((screenWidth - contentWidth) / 2)
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if buttons.count <= indexPath.item {
            return defaultCell
        }
        
        let button = buttons[indexPath.item]
        
        switch button.type {
        case .Green:
            if let cell = actionButtonsCollectionView.dequeueReusableCellWithReuseIdentifier("fullScreenOverlayPopupGreenButtonCell", forIndexPath: indexPath) as? FullScreenOverlayPopupGreenButtonCell {
                cell.populate(self, buttonRepresentation: button)
                return cell
            }
        case .White:
            if let cell = actionButtonsCollectionView.dequeueReusableCellWithReuseIdentifier("fullScreenOverlayPopupWhiteButtonCell", forIndexPath: indexPath) as? FullScreenOverlayPopupWhiteButtonCell {
                cell.populate(self, buttonRepresentation: button)
                return cell
            }
        }
    
        return defaultCell
    }
}
