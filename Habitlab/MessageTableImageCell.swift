//
//  MessageTableImageCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/13/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class MessageTableImageCell: UITableViewCell {
    @IBOutlet weak var captionImage: UIImageView!
    var notification: NSDictionary?
    
    func populate(notification: NSDictionary) {
        self.notification = notification
        
        if let stepId = notification["programStepObjectId"] as? NSString {
            
            // Read the image from the photo cache first, if possible.
            if let imageData = PhotoCache().getCache("programstep\(stepId)") {
                let image = UIImage(data: imageData)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    UIView.transitionWithView(self.captionImage, duration: 0.4, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { self.captionImage.image = image }, completion: nil)
                })
                
                return
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                
                // If the photo cache did not contain the image, I will download it from Amazon...
                HabitlabS3API.downloadStepImage(stepId as String, handler: { (error, imageData) -> () in
                    if error != nil {
                        return
                    }
                    
                    if imageData != nil {
                        PhotoCache().setCache("programstep\(stepId as String)", image: imageData!)
                        let image = UIImage(data: imageData!)
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            if notification != self.notification {
                                return
                            }
                            
                            UIView.transitionWithView(self.captionImage, duration: 0.4, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { self.captionImage.image = image }, completion: nil)
                        })
                    }
                }, progress: nil)
            })
        }
    }
}
