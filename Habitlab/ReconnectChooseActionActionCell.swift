//
//  ReconnectChooseActionActionCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/17/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ReconnectChooseActionActionCell: UICollectionViewCell {
    private var controller: UIViewController?
    private var action: ReconnectActionType?
    
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var actionPhoto: UIImageView!
    
    @IBAction func onClick(sender: AnyObject) {
        self.selected = !self.selected
        
        if let realController = self.controller as? ReconnectChooseActionController, realAction = action {
            realController.setSelectedAction(realAction, selected: self.selected)
        }
    }
    
    func populate(controller: UIViewController, action: ReconnectActionType, contact: NSDictionary, setSelected: Bool) {
        self.controller = controller
        self.action = action
        
        self.selected = setSelected
        
        if self.selected {
            self.actionPhoto.layer.borderWidth = 2.0
            self.actionPhoto.layer.borderColor = UIColor.whiteColor().CGColor
        } else {
            self.actionPhoto.layer.borderWidth = 2.0
            self.actionPhoto.layer.borderColor = UIColor.clearColor().CGColor
        }
        
        self.actionPhoto!.layer.cornerRadius = self.actionPhoto!.frame.size.width / 2
        self.actionPhoto!.clipsToBounds = true
        self.actionPhoto.image = UIImage(named: "ReconnectAction\(action.rawValue)")
        
        switch action {
        case .GoOnATrip:
            self.actionLabel.text = "We will go on a trip"
        case .GoOut:
            self.actionLabel.text = "We go out"
        case .GoShopping:
            self.actionLabel.text = "We'll go shopping"
        case .GoToMovies:
            self.actionLabel.text = "We'll go to the movies"
        case .HaveDinner:
            self.actionLabel.text = "We'll have dinner"
        case .InvitedNameOver:
            if let firstName = contact["firstName"] as? NSString {
                self.actionLabel.text = "I invited \(firstName) over"
            } else {
                self.actionLabel.text = "I invited them over"
            }
        case .MeetForCoffee:
            self.actionLabel.text = "We'll meet for coffee"
        case .TalkAgainSoon:
            self.actionLabel.text = "We'll talk again soon"
        case .VisitName:
            if let firstName = contact["firstName"] as? NSString {
                self.actionLabel.text = "I'll visit \(firstName)"
            } else {
                self.actionLabel.text = "I'll visit them"
            }
        }
    }
}
