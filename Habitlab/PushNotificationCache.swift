//
//  PushNotificationCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/13/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class PushNotificationCache: NSObject {
    private var keyPushNotificationObject = "startupPushNotification"
    private var keyPushNotificationOrigin = "startupPushNotificationOrigin"
    
    func setPushNotification(userInfo: [NSObject: AnyObject], origin: PushNotificationOrigin, overwrite: Bool) {
        if let _ = NSUserDefaults.standardUserDefaults().objectForKey(keyPushNotificationOrigin), _ = NSUserDefaults.standardUserDefaults().objectForKey(keyPushNotificationObject) {
            if !overwrite {
                
                // Do nothing, this is not to be set.
                return
            }
        }
        
        let userData = NSKeyedArchiver.archivedDataWithRootObject(userInfo)
        NSUserDefaults.standardUserDefaults().setObject(userData, forKey: keyPushNotificationObject)
        NSUserDefaults.standardUserDefaults().setObject(origin.rawValue, forKey: keyPushNotificationOrigin)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getPushNotification(origin: PushNotificationOrigin) -> [NSObject: AnyObject]? {
        if let userData = NSUserDefaults.standardUserDefaults().objectForKey(keyPushNotificationObject) as? NSData, originString = NSUserDefaults.standardUserDefaults().objectForKey(keyPushNotificationOrigin) as? String {
            if let existingOrigin = PushNotificationOrigin(rawValue: originString) {
                if existingOrigin != origin {
                    return nil
                }
                
                NSUserDefaults.standardUserDefaults().removeObjectForKey(keyPushNotificationObject)
                NSUserDefaults.standardUserDefaults().removeObjectForKey(keyPushNotificationOrigin)
                NSUserDefaults.standardUserDefaults().synchronize()
                return NSKeyedUnarchiver.unarchiveObjectWithData(userData) as? [NSObject: AnyObject]
            }
        }
        
        return nil
    }
}
