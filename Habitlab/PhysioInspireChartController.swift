//
//  PhysioInspireChartController.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/19/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Charts
import Analytics
import UIKit
import MessageUI

class PhysioInspireChartController: GAITrackedViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ChartViewDelegate, MFMailComposeViewControllerDelegate {
    private let serialQueue = dispatch_queue_create("me.habitlab.mobile.physioInspireChartController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    var program: NSDictionary?
    
    @IBOutlet weak var chartsCollectionView: UICollectionView!
    @IBOutlet weak var chartsPageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(PhysioInspireChartController)
        
        self.chartsCollectionView.delegate = self
        self.chartsCollectionView.dataSource = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if program == nil {
            return
        }
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let title = drive["title"] as? NSString, level = drive["level"] as? NSNumber {
                    self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
                }
            }
        }
        
        refresh()
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(PhysioInspireChartController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(PhysioInspireChartController))
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if error != nil {
            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
        }
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func sendEmailToPhysiotherapist(sender: AnyObject) {
        // Send the email to the physiotherapist.
        
        if MFMailComposeViewController.canSendMail() {
            let composeViewController = MFMailComposeViewController()
            composeViewController.mailComposeDelegate = self
            
            print(program)
            
            if let realProgram = self.program {
                if let drive = realProgram["drive"] as? NSDictionary {
                    if let physioProgram = drive["physioProgram"] as? NSDictionary {
                        if let physioTherapist = physioProgram["physioTherapist"] as? NSDictionary {
                            if let facebookEmail = physioTherapist["facebookEmail"] as? NSString {
                                composeViewController.setToRecipients([facebookEmail as String])
                            }
                        }
                    }
                }
            }
            
            if let user = UserCache().getUser() {
                if let firstName = user["firstName"] as? NSString, lastName = user["lastName"] as? NSString {
                    var subject = "New message from \(firstName) \(lastName)"
                    
                    if let realProgram = self.program {
                        if let drive = realProgram["drive"] as? NSDictionary {
                            if let physioProgram = drive["physioProgram"] as? NSDictionary {
                                if let title = physioProgram["title"] as? NSString {
                                    subject = subject + " - \(title)"
                                }
                            }
                        }
                    }
                    
                    composeViewController.setSubject(subject)
                }
            }
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending email", message: "This device cannot send emails.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if scrollView == self.chartsCollectionView {
            let pageWidth = scrollView.frame.size.width
            let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
            
            print("page = \(page)")
            self.chartsPageControl.currentPage = page
        }
    }
    
    @IBAction func changePage(sender: AnyObject) {
        let offsetX = CGFloat(chartsPageControl.currentPage) * chartsCollectionView.frame.size.width
        chartsCollectionView.setContentOffset(CGPointMake(offsetX, 0), animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        /*if segue.identifier == "showChoosePhysioPainFromPhysioModule" {
            if let viewController = segue.destinationViewController as? ChoosePhysioPainController {
                viewController.program = self.program
                viewController.step = self.step
                viewController.beforePainLevel = self.beforePainLevel
                viewController.painTiming = ChoosePhysioPainTiming.AfterExercising
            }
        }*/
    }
    
    func refresh() {
        self.chartsPageControl.numberOfPages = 2
        self.chartsCollectionView.reloadData()
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if collectionView == chartsCollectionView {
            return 1
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == chartsCollectionView {
            return 2
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if collectionView == chartsCollectionView {
            let screenWidth = CGFloat(chartsCollectionView.frame.size.width)
            return CGSizeMake(screenWidth, 240)
        }
        
        return CGSizeMake(0, 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if collectionView == chartsCollectionView {
            if indexPath.item == 0 {
                if let cell = chartsCollectionView.dequeueReusableCellWithReuseIdentifier("physioInspireChartPainCell", forIndexPath: indexPath) as? PhysioInspireChartPainCell {
                    cell.populate(self.program, controller: self)
                    return cell
                }
            }
            
            if indexPath.item == 1 {
                if let cell = chartsCollectionView.dequeueReusableCellWithReuseIdentifier("physioInspireChartLoopsCell", forIndexPath: indexPath) as? PhysioInspireChartLoopsCell {
                    cell.populate(self.program, controller: self)
                    return cell
                }
            }
        }
        
        return defaultCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
}
