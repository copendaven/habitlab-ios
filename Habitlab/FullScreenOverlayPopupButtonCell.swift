//
//  FullScreenOverlayPopupButtonCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class FullScreenOverlayPopupButtonCell: UICollectionViewCell {
    private var controller: UIViewController?
    private var buttonRepresentation: FullScreenOverlayPopupButton?
    
    func populate(controller: UIViewController, buttonRepresentation: FullScreenOverlayPopupButton, button: UIButton!) {
        self.controller = controller
        self.buttonRepresentation = buttonRepresentation
        
        button.setTitle(buttonRepresentation.title, forState: UIControlState.Normal)
        button.setTitle(buttonRepresentation.title, forState: UIControlState.Selected)
    }
    
    func clickedButton() {
        if let viewController = self.controller as? FullScreenOverlayPopup {
            viewController.dismissViewControllerAnimated(true, completion: {
                if let representation = self.buttonRepresentation {
                    if let action = representation.action, dictionary = representation.dictionary {
                        action(dictionary)
                    }
                }
            })
        }
    }
}