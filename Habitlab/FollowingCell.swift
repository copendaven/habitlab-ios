//
//  FollowingCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowingCell: UITableViewCell {
    var following: NSMutableDictionary?
    var controller: UIViewController?
    
    func refresh(userName: UILabel, userImage: UIImageView) {
        userImage.layer.cornerRadius = userImage.frame.size.width / 2
        userImage.clipsToBounds = true
        
        if following != nil {
            if let user = following!["user"] as? NSDictionary {
                if let photo = user["facebookPhoto"] as? NSString {
                    Serv.loadImage(photo as String, handler: { (error, data) -> () in
                        if error != nil {
                            print(error)
                        }
                        
                        if data != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                userImage.image = UIImage(data: data!)
                            })
                        }
                    })
                }
                
                if let firstName = user["firstName"] as? NSString, lastName = user["lastName"] as? NSString {
                    userName.text = "\(firstName) \(lastName)"
                }
            }
        }
    }
    
    func populate(following: NSDictionary, controller: UIViewController, userName: UILabel, userImage: UIImageView) {
        self.following = NSMutableDictionary(dictionary: following)
        self.controller = controller
        
        self.refresh(userName, userImage: userImage)
    }
    
    func clickProfile(sender: AnyObject) {
        if following == nil {
            return
        }
        
        if controller == nil {
            return
        }
        
        if let user = following!["user"] as? NSDictionary {
            if let id = user["id"] as? NSString {
                if let controller = controller as? FollowingController {
                    controller.performSegueWithIdentifier("showPublicProfileFromFollowing", sender: id as String)
                }
                
                if let controller = controller as? LikersController {
                    controller.performSegueWithIdentifier("showPublicProfileFromLikers", sender: id as String)
                }
            }
        }
    }
}
