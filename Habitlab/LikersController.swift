//
//  LikersController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Analytics

class LikersController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var mobileNotificationBaseId: String?
    
    var likings = []
    var refresher: UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(LikersController)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.shyNavBarManager.scrollView = self.tableView
        
        refresher = UIRefreshControl()
        refresher!.attributedTitle = NSAttributedString(string: "Pull to refresh.")
        
        if let navigationController = self.navigationController {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - navigationController.navigationBar.frame.size.height - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        } else {
            refresher!.bounds = CGRectMake(refresher!.bounds.origin.x, refresher!.bounds.origin.y - UIApplication.sharedApplication().statusBarFrame.size.height, refresher!.bounds.size.width, refresher!.bounds.size.height)
        }
        
        refresher!.addTarget(self, action: #selector(FollowingController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refresher!)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hasExitedUnplugCheck()
        refresh()
        segment()
    }
    
    private func segment() {
        SEGAnalytics.sharedAnalytics().screen(String(LikersController))
    }
    
    func filterBlockedOut(likings: NSArray) -> NSArray {
        return likings.filter { (likingJson) -> Bool in
            if let liking = likingJson as? NSDictionary {
                if let status = liking["status"] as? NSString {
                    return (status as String).compare("blocked") != NSComparisonResult.OrderedSame
                }
                
                return false
            }
            
            return false
        }
    }
    
    func refresh() {
        if mobileNotificationBaseId == nil {
            return
        }
        
        Serv.showSpinner()
        
        HabitlabRestAPI.getLikings(mobileNotificationBaseId!, handler: { (error, likingsJson) -> () in
            Serv.hideSpinner()
            
            if error != nil {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                return
            }
            
            if let likings = likingsJson as? NSArray {
                self.likings = self.filterBlockedOut(likings)
                self.tableView.reloadData()
                
                if let refr = self.refresher {
                    refr.endRefreshing()
                }
            }
        })
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showPublicProfileFromLikers") {
            if let viewController = segue.destinationViewController as? PublicProfileController {
                if let userId = sender as? String {
                    viewController.userId = userId
                }
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return likings.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if let liking = likings[indexPath.section] as? NSDictionary {
            if let status = liking["status"] as? NSString {
                switch (status) {
                case "unfollowed":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followingUnfollowedCell", forIndexPath: indexPath) as? FollowingUnfollowedCell {
                        cell.populate(liking, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "waiting":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followingWaitingCell", forIndexPath: indexPath) as? FollowingWaitingCell {
                        cell.populate(liking, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "approved":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followingApprovedCell", forIndexPath: indexPath) as? FollowingApprovedCell {
                        cell.populate(liking, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                case "yourself":
                    if let cell = tableView.dequeueReusableCellWithIdentifier("followingYourselfCell", forIndexPath: indexPath) as? FollowingYourselfCell {
                        cell.populate(liking, controller: self)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                default:
                    // Do nothing.
                    break
                }
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
