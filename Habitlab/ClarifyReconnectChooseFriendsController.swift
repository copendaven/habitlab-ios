//
//  ClarifyReconnectChooseFriendsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import Contacts
import Analytics

class ClarifyReconnectChooseFriendsController: GAITrackedViewController, UITableViewDataSource, UITableViewDelegate {
    var selectedContacts: [CNContact] = []
    var drive: NSDictionary?
    var contactMessages = NSMutableDictionary()
    var contactColors = NSMutableDictionary()
    
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var scheduleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ClarifyReconnectChooseFriendsController)
        
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
        contactsTableView.estimatedRowHeight = 100
        contactsTableView.rowHeight = UITableViewAutomaticDimension
        // self.shyNavBarManager.scrollView = contactsTableView
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        //self.addContactEmil()
        //self.addContactPhaedra()
        //self.addContactChristina()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.hasExitedUnplugCheck()
        
        if let realDrive = self.drive {
            if let title = realDrive["title"] as? NSString, level = realDrive["level"] as? NSNumber {
                self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
            }
        }
        
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realDrive = self.drive {
            if let categ = realDrive["driveCategory"] as? NSString {
                dict["category"] = categ as String
            }
            
            if let subcateg = realDrive["driveSubcategory"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
            
            if let level = realDrive["level"] as? NSNumber {
                dict["level"] = String(level)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseFriendsController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ClarifyReconnectChooseFriendsController))
        }
    }
    
    func refresh() {
        if let realDrive = self.drive {
            if let countWeeks = realDrive["countWeeks"] as? NSNumber, countStepsPerWeek = realDrive["countStepsPerWeek"] as? NSNumber {
                let countContacts = Int(countWeeks) * Int(countStepsPerWeek)
                
                dispatch_async(dispatch_get_main_queue(), {
                    if self.selectedContacts.count < countContacts {
                        self.scheduleButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.75)
                        self.scheduleButton.enabled = false
                        self.scheduleButton.alpha = 0.5
                    } else {
                        self.scheduleButton.backgroundColor = UIColor(red: 0.21, green: 0.84, blue: 0.59, alpha: 1.0)
                        self.scheduleButton.enabled = true
                        self.scheduleButton.alpha = 1.0
                    }
                    
                    self.contactsTableView.reloadData()
                })
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    func addFriend(friend: CNContact, message: String, color: UIColor) {
        if let realDrive = self.drive {
            if let countWeeks = realDrive["countWeeks"] as? NSNumber, countStepsPerWeek = realDrive["countStepsPerWeek"] as? NSNumber {
                let countContacts = Int(countWeeks) * Int(countStepsPerWeek)
                
                if self.selectedContacts.count >= countContacts {
                    Serv.showErrorPopup("Too many friends", message: "You can choose at most five friends.", okMessage: "Okay, got it", controller: self, handler: nil)
                    return
                }
                
                var found = false
                
                for contact in selectedContacts {
                    if contact.identifier == friend.identifier {
                        found = true
                        break
                    }
                }
                
                if found {
                    Serv.showErrorPopup("Friend already chosen", message: "You can choose a friend only once.", okMessage: "Okay, got it", controller: self, handler: nil)
                    return
                }
                
                self.selectedContacts.append(friend)
                contactMessages.setObject(message, forKey: friend.identifier)
                contactColors.setObject(color, forKey: friend.identifier)
                self.refresh()
            }
        }
    }
    
    @IBAction func clarifyReconnectChooseFriendsControllerSelectedContact(segue: UIStoryboardSegue) {
        self.refresh()
    }
    
    @IBAction func clickSchedule(sender: AnyObject) {
        self.performSegueWithIdentifier("showScheduleTimeFromClarifyReconnectChooseFriends", sender: self)
    }
    
    func clickAddFriend() {
        self.performSegueWithIdentifier("showClarifyReconnectChooseContactFromClarifyReconnectChooseFriends", sender: self)
    }
    
    func removeFriend(friend: CNContact) {
        var removed = false
        
        for i in 0..<selectedContacts.count {
            if selectedContacts[i].identifier == friend.identifier {
                selectedContacts.removeAtIndex(i)
                contactMessages.removeObjectForKey(friend.identifier)
                contactColors.removeObjectForKey(friend.identifier)
                removed = true
                break
            }
        }
        
        if removed {
            self.refresh()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showClarifyReconnectChooseContactFromClarifyReconnectChooseFriends" {
            if let viewController = segue.destinationViewController as? ClarifyReconnectChooseContactController {
                var contactIds = [String]()
                
                for contact in self.selectedContacts {
                    contactIds.append(contact.identifier)
                }
                
                viewController.prohibitedContactIds = contactIds
                viewController.drive = drive
            }
        }
        
        if segue.identifier == "showScheduleTimeFromClarifyReconnectChooseFriends" {
            if let viewController = segue.destinationViewController as? ScheduleTimeController {
                viewController.drive = self.drive
                viewController.contacts = self.selectedContacts
                viewController.contactMessages = self.contactMessages
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let realDrive = self.drive {
            if let countWeeks = realDrive["countWeeks"] as? NSNumber, countStepsPerWeek = realDrive["countStepsPerWeek"] as? NSNumber {
                let countContacts = Int(countWeeks) * Int(countStepsPerWeek)
                return 2 + countContacts
            }
        }
        
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if let realDrive = self.drive {
            if let countWeeks = realDrive["countWeeks"] as? NSNumber, countStepsPerWeek = realDrive["countStepsPerWeek"] as? NSNumber {
                if indexPath.section == 0 {
                    if let cell = tableView.dequeueReusableCellWithIdentifier("clarifyReconnectChooseFriendsHeader", forIndexPath: indexPath) as? ClarifyReconnectChooseFriendsHeader {
                        cell.populate(self, countWeeks: Int(countWeeks), countFriends: Int(countStepsPerWeek) * Int(countWeeks))
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                    
                    return defaultCell
                }
                
                if indexPath.section >= 1 + Int(countStepsPerWeek) * Int(countWeeks) {
                    let cell = contactsTableView.dequeueReusableCellWithIdentifier("clarifyReconnectChooseFriendsFooter", forIndexPath: indexPath)
                    cell.selectionStyle = UITableViewCellSelectionStyle.None
                    return cell
                }
                
                if indexPath.section - 1 < selectedContacts.count {
                    let selectedContact = selectedContacts[indexPath.section - 1]
                    
                    if let cell = contactsTableView.dequeueReusableCellWithIdentifier("clarifyReconnectChooseFriendsSelectedFriendCell", forIndexPath: indexPath) as? ClarifyReconnectChooseFriendsSelectedFriendCell {
                        
                        var message = "I want to reconnect"
                        var color = UIColor.redColor()
                        
                        if let realMessage = contactMessages.objectForKey(selectedContact.identifier) as? String {
                            message = realMessage
                        }
                        
                        if let realColor = contactColors.objectForKey(selectedContact.identifier) as? UIColor {
                            color = realColor
                        }
                        
                        cell.populate(self, friend: selectedContact, reconnectAction: message, color: color)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                    
                    return defaultCell
                } else {
                    if let cell = contactsTableView.dequeueReusableCellWithIdentifier("clarifyReconnectChooseFriendsEmptyCell", forIndexPath: indexPath) as? ClarifyReconnectChooseFriendsEmptyCell {
                        cell.populate(self, nextToSelect: indexPath.section - 1 == selectedContacts.count)
                        cell.selectionStyle = UITableViewCellSelectionStyle.None
                        return cell
                    }
                    
                    return defaultCell
                }
            }
        }
        
        return defaultCell
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing else...
    }
    
    // MARK: Helper functions
    
    private func addContactEmil() {
        let emil = CNMutableContact()
        emil.givenName = "Marc Emil"
        emil.familyName = "Domar"
        
        if let image = UIImage(named: "AppIconRound") {
            if let imageData = UIImagePNGRepresentation(image) {
                emil.imageData = imageData
            }
        }
        
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: "52619552"))
        let workPhone = CNLabeledValue(label: CNLabelWork, value: CNPhoneNumber(stringValue: "52619552"))
        emil.phoneNumbers = [homePhone, workPhone]
        
        let homeEmail = CNLabeledValue(label: CNLabelHome, value: "vlad.c.manea@gmail.com")
        let workEmail = CNLabeledValue(label: CNLabelWork, value: "vlad@habitlab.me")
        emil.emailAddresses = [homeEmail, workEmail]
        
        let request = CNSaveRequest()
        request.addContact(emil, toContainerWithIdentifier: nil)
        let contactsStore = AppDelegate.getAppDelegate().contactStore
        
        do {
            try contactsStore.executeSaveRequest(request)
        } catch let err {
            print("An error has occurred: \(err)")
        }
    }
    
    private func addContactChristina() {
        let christina = CNMutableContact()
        christina.givenName = "Christina"
        christina.familyName = "Lauer"
        
        if let image = UIImage(named: "AppIconRound") {
            if let imageData = UIImagePNGRepresentation(image) {
                christina.imageData = imageData
            }
        }
        
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue: "52619552"))
        let workPhone = CNLabeledValue(label: CNLabelWork, value: CNPhoneNumber(stringValue: "52619552"))
        christina.phoneNumbers = [homePhone, workPhone]
        
        let workEmail = CNLabeledValue(label: CNLabelWork, value: "vlad@habitlab.me")
        christina.emailAddresses = [workEmail]
        
        let request = CNSaveRequest()
        request.addContact(christina, toContainerWithIdentifier: nil)
        let contactsStore = AppDelegate.getAppDelegate().contactStore
        
        do {
            try contactsStore.executeSaveRequest(request)
        } catch let err {
            print("An error has occurred: \(err)")
        }
    }
    
    private func addContactPhaedra() {
        let phaedra = CNMutableContact()
        phaedra.givenName = "Phaedra"
        phaedra.familyName = "Papanikolaou"
        
        if let image = UIImage(named: "AppIconRound") {
            if let imageData = UIImagePNGRepresentation(image) {
                phaedra.imageData = imageData
            }
        }
        
        let workPhone = CNLabeledValue(label: CNLabelWork, value: CNPhoneNumber(stringValue: "52619552"))
        phaedra.phoneNumbers = [workPhone]
        
        let homeEmail = CNLabeledValue(label: CNLabelHome, value: "vlad.c.manea@gmail.com")
        phaedra.emailAddresses = [homeEmail]
        
        let request = CNSaveRequest()
        request.addContact(phaedra, toContainerWithIdentifier: nil)
        let contactsStore = AppDelegate.getAppDelegate().contactStore
        
        do {
            try contactsStore.executeSaveRequest(request)
        } catch let err {
            print("An error has occurred: \(err)")
        }
    }
}
