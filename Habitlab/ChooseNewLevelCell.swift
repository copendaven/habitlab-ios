//
//  ChooseNewLevelCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class ChooseNewLevelCell: UITableViewCell {
    var drive: NSDictionary?
    
    func populate(drive: NSDictionary, levelName: UILabel, levelLevel: UILabel) {
        self.drive = drive
        
        if let level = drive["level"] as? NSNumber {
            let levelInt = level as Int
            
            if levelInt < 10 {
                levelLevel.text = "0\(levelInt)"
            } else {
                levelLevel.text = String(levelInt)
            }
        }
        
        Serv.setLevelText(drive, label: levelName)
    }
}