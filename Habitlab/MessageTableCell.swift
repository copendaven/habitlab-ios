//
//  MessageTableCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 13/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class MessageTableCell: UITableViewCell {
    @IBOutlet weak var userMessage: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    
    func populate(comment: NSDictionary) {
        userPhoto.layer.cornerRadius = userPhoto.frame.size.width / 2
        userPhoto.clipsToBounds = true
        
        if let user = comment["owner"] as? NSDictionary {
            if let photo = user["facebookPhoto"] as? NSString {
                Serv.loadImage(photo as String, handler: { (error, data) -> () in
                    if error != nil {
                        print(error)
                    }
                    
                    if data != nil {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.userPhoto.image = UIImage(data: data!)
                        })
                    }
                })
            }
        }

        if let message = comment["message"] as? NSString {
            userMessage.textAlignment = .Center
            let htmlString = Serv.renderComment(comment, newLine: false)
            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: message as String, lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
            userMessage.attributedText = attributedString
            userMessage.sizeToFit()
            userMessage.setNeedsDisplay()
        }
    }
}
