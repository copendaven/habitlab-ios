//
//  FeedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    var notification: NSDictionary?
    var controller: UIViewController?
    var liked = false
    
    @IBAction func comment(sender: AnyObject) {
        if notification != nil {
            if let feedController = controller as? NewFeedController {
                feedController.performSegueWithIdentifier("showWriteMessageFromFeed", sender: notification)
            }
            
            if let profileController = controller as? ProfileController {
                profileController.performSegueWithIdentifier("showWriteMessageFromProfile", sender: notification)
            }
        }
    }
    
    @IBAction func like(sender: AnyObject) {
        if liked {
            return
        }
        
        if notification == nil {
            return
        }
        
        if let mobileNotificationId = notification!["id"] as? NSString, mobileNotificationBaseId = notification!["mobileNotificationBase"] as? NSString, mobileNotificationCacheId = notification!["cacheId"] as? NSString {
            self.likeInternal(mobileNotificationId as String, mobileNotificationBaseId: mobileNotificationBaseId as String, mobileNotificationCacheId: mobileNotificationCacheId as String)
            return
        }
        
        if let mobileNotificationId = notification!["id"] as? NSString, mobileNotificationBaseId = notification!["mobileNotificationBase"] as? NSString {
            self.likeInternal(mobileNotificationId as String, mobileNotificationBaseId: mobileNotificationBaseId as String)
            return
        }
        
        if let mobileNotificationCacheId = notification!["cacheId"] as? NSString {
            self.likeInternal(mobileNotificationCacheId as String)
            return
        }
    }

    // Must be called from the main thread.
    private func likeInternal(mobileNotificationId: String, mobileNotificationBaseId: String, mobileNotificationCacheId: String) {
        self.liked = true
        
        if let user = UserCache().getUser() {
            Serv.showSpinner()
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                let cacheId = NSUUID().UUIDString
        
                MobileNotificationLikeCache().setCache(cacheId, owner: user, mobileNotificationId: mobileNotificationId, mobileNotificationBaseId: mobileNotificationBaseId, mobileNotificationCacheId: mobileNotificationCacheId)
                
                dispatch_async(dispatch_get_main_queue(), {
                    if self.notification == nil {
                        return
                    }
                    
                    if let id = self.notification!["id"] as? NSString {
                        if (id as String).compare(mobileNotificationId) != NSComparisonResult.OrderedSame {
                            return
                        }
                    } else {
                        return
                    }
                    
                    if let feedController = self.controller as? NewFeedController {
                        feedController.cacheLikedMobileNotificationId = mobileNotificationId as String
                        feedController.refreshFromClientLike()
                    }
                })
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    HabitlabRestAPI.postLike(mobileNotificationId, notificationBaseId: mobileNotificationBaseId, cacheId: cacheId, handler: { (error, response) -> () in
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            Serv.hideSpinner()
                            
                            if self.notification == nil {
                                return
                            }
                            
                            if let id = self.notification!["id"] as? NSString {
                                if (id as String).compare(mobileNotificationId) != NSComparisonResult.OrderedSame {
                                    return
                                }
                            } else {
                                return
                            }
                            
                            if (error != nil) {
                                self.liked = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                        }
                    })
                })
            })
        }
    }
    
    // Must be called from the main thread.
    private func likeInternal(mobileNotificationId: String, mobileNotificationBaseId: String) {
        self.liked = true
        
        if let user = UserCache().getUser() {
            Serv.showSpinner()
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                let cacheId = NSUUID().UUIDString
            
                MobileNotificationLikeCache().setCache(cacheId, owner: user, mobileNotificationId: mobileNotificationId, mobileNotificationBaseId: mobileNotificationBaseId)
                
                dispatch_async(dispatch_get_main_queue(), {
                    if self.notification == nil {
                        return
                    }
                    
                    if let id = self.notification!["id"] as? NSString {
                        if (id as String).compare(mobileNotificationId) != NSComparisonResult.OrderedSame {
                            return
                        }
                    } else {
                        return
                    }
                    
                    if let feedController = self.controller as? NewFeedController {
                        dispatch_async(dispatch_get_main_queue()) {
                            if self.notification == nil {
                                return
                            }
                            
                            if let id = self.notification!["id"] as? NSString {
                                if (id as String).compare(mobileNotificationId) != NSComparisonResult.OrderedSame {
                                    return
                                }
                            } else {
                                return
                            }
                            
                            feedController.cacheLikedMobileNotificationId = mobileNotificationId as String
                            feedController.refreshFromClientLike()
                        }
                    }
                })
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    HabitlabRestAPI.postLike(mobileNotificationId, notificationBaseId: mobileNotificationBaseId, cacheId: cacheId, handler: { (error, response) -> () in
                        dispatch_async(dispatch_get_main_queue()) {
                            Serv.hideSpinner()
                            
                            if self.notification == nil {
                                return
                            }
                            
                            if let id = self.notification!["id"] as? NSString {
                                if (id as String).compare(mobileNotificationId) != NSComparisonResult.OrderedSame {
                                    return
                                }
                            } else {
                                return
                            }
                            
                            if (error != nil) {
                                self.liked = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                        }
                    })
                })
            })
        }
    }
    
    // Must be called from the main thread.
    private func likeInternal(mobileNotificationCacheId: String) {
        // We do not set the liked here, we will call another likeInternal where we set it...
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            HabitlabRestAPI.getFakeNotification(mobileNotificationCacheId as String, handler: { (error, fakeNotificationJson) -> () in
                dispatch_async(dispatch_get_main_queue(), {
                    if self.notification == nil {
                        return
                    }
                    
                    if let fakeNotification = fakeNotificationJson as? NSDictionary {
                        if let fakeNotificationCacheId = fakeNotification["cacheId"] as? NSString {
                            if let cacheId = self.notification!["cacheId"] as? NSString {
                                if (cacheId as String).compare(fakeNotificationCacheId as String) != NSComparisonResult.OrderedSame {
                                    return
                                }
                            } else {
                                return
                            }
                        }
                    }
                    
                    if (error != nil) {
                        Serv.showErrorPopup("Love message error", message: "The post you want to love is not yet ready. Try again in a few seconds.", okMessage: "Okay, got it.", controller: self.controller!, handler: nil)
                        return
                    }
                    
                    if let fakeNotification = fakeNotificationJson as? NSDictionary {
                        if let mobileNotificationId = fakeNotification["id"] as? NSString, notificationBaseId = fakeNotification["mobileNotificationBase"] as? NSString {
                            self.likeInternal(mobileNotificationId as String, mobileNotificationBaseId: notificationBaseId as String, mobileNotificationCacheId: mobileNotificationCacheId)
                            return
                        }
                        
                        Serv.showErrorPopup("Love message error", message: "The post you want to love is not yet ready. Try again in a few seconds.", okMessage: "Okay, got it.", controller: self.controller!, handler: nil)
                    }
                })
            })
        }
    }
    
    func setLiking(notification: NSDictionary, likingButton: UIButton?, showLikeButton: Bool) {
        dispatch_async(dispatch_get_main_queue(), {
            if self.notification != notification {
                return
            }
            
            self.liked = false
            
            if likingButton != nil {
                likingButton!.hidden = !showLikeButton
            }
            
            if let likes = notification["likes"] as? NSArray {
                if likingButton != nil {
                    likingButton!.setTitle(String(likes.count), forState: UIControlState.Normal)
                }
                
                if let user = UserCache().getUser() {
                    if let userId = user["id"] as? NSString {
                        for likeJson in likes {
                            if let like = likeJson as? NSDictionary {
                                if let owner = like["owner"] as? NSDictionary {
                                    if let ownerId = owner["id"] as? NSString {
                                        if (ownerId as String).compare(userId as String) == NSComparisonResult.OrderedSame {
                                            self.liked = true
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if likingButton != nil {
                        likingButton!.selected = self.liked
                    }
                }
            }
        })
    }
    
    private func setCommenting(notification: NSDictionary, commenting: NSDictionary) {
        let firstComment = commenting.objectForKey("firstComment") as? UILabel
        let missingComments = commenting.objectForKey("missingComments") as? UILabel
        let secondToLastComment = commenting.objectForKey("secondToLastComment") as? UILabel
        let lastComment = commenting.objectForKey("lastComment") as? UILabel
        
        let firstCommentTop = commenting.objectForKey("firstCommentTop") as? NSLayoutConstraint
        let firstCommentBottom = commenting.objectForKey("firstCommentBottom") as? NSLayoutConstraint
        
        let missingCommentTop = commenting.objectForKey("missingCommentTop") as? NSLayoutConstraint
        let missingCommentBottom = commenting.objectForKey("missingCommentBottom") as? NSLayoutConstraint
        
        let secondToLastCommentTop = commenting.objectForKey("secondToLastCommentTop") as? NSLayoutConstraint
        let secondToLastCommentBottom = commenting.objectForKey("secondToLastCommentBottom") as? NSLayoutConstraint
        
        let lastCommentTop = commenting.objectForKey("lastCommentTop") as? NSLayoutConstraint
        let lastCommentBottom = commenting.objectForKey("lastCommentBottom") as? NSLayoutConstraint
        
        let commentsView = commenting.objectForKey("commentsView") as? UIView
        let commentsViewHeight = commenting.objectForKey("commentsViewHeight") as? NSLayoutConstraint
        
        if let comments = notification["comments"] as? NSArray {
            if comments.count > 0 {
                dispatch_async(dispatch_get_main_queue()) {
                    if self.notification != notification {
                        return
                    }
                    
                    if comments.count == 1 {
                        missingComments?.hidden = true
                        secondToLastComment?.hidden = true
                        lastComment?.hidden = true
                        
                        firstComment?.hidden = false
                        firstCommentTop?.constant = 8
                        firstCommentBottom?.constant = 8
                        
                        if let comment = comments[0] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            firstComment?.attributedText = attributedString
                        }
                        
                        commentsViewHeight?.constant = 47
                        commentsView?.hidden = false
                        return
                    }
                    
                    if comments.count == 2 {
                        missingComments?.hidden = true
                        secondToLastComment?.hidden = true
                        
                        firstComment?.hidden = false
                        firstCommentTop?.constant = 8
                        firstCommentBottom?.constant = 29
                        
                        lastComment?.hidden = false
                        lastCommentTop?.constant = 29
                        lastCommentBottom?.constant = 8
                        
                        if let comment = comments[0] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            firstComment?.attributedText = attributedString
                        }
                        
                        
                        if let comment = comments[comments.count - 1] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            lastComment?.attributedText = attributedString
                        }
                        
                        commentsViewHeight?.constant = 58
                        commentsView?.hidden = false
                        return
                    }
                    
                    if comments.count == 3 {
                        missingComments?.hidden = true
                        
                        firstComment?.hidden = false
                        firstCommentTop?.constant = 8
                        firstCommentBottom?.constant = 50
                        
                        secondToLastComment?.hidden = false
                        secondToLastCommentTop?.constant = 29
                        secondToLastCommentBottom?.constant = 29
                        
                        lastComment?.hidden = false
                        lastCommentTop?.constant = 50
                        lastCommentBottom?.constant = 8
                        
                        if let comment = comments[0] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            firstComment?.attributedText = attributedString
                        }
                        
                        if let comment = comments[comments.count - 2] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            secondToLastComment?.attributedText = attributedString
                        }
                        
                        if let comment = comments[comments.count - 1] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            lastComment?.attributedText = attributedString
                        }
                        
                        commentsViewHeight?.constant = 79
                        commentsView?.hidden = false
                        return
                    }
                    
                    if comments.count >= 4 {
                        firstComment?.hidden = false
                        firstCommentTop?.constant = 8
                        firstCommentBottom?.constant = 71
                        
                        missingComments?.hidden = false
                        missingCommentTop?.constant = 29
                        missingCommentBottom?.constant = 50
                        
                        secondToLastComment?.hidden = false
                        secondToLastCommentTop?.constant = 50
                        secondToLastCommentBottom?.constant = 29
                        
                        lastComment?.hidden = false
                        lastCommentTop?.constant = 71
                        lastCommentBottom?.constant = 8
                        
                        if let comment = comments[0] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            firstComment?.attributedText = attributedString
                        }
                        
                        if comments.count - 3 == 1 {
                            missingComments?.text = "See one more comment"
                        } else {
                            missingComments?.text = "See \(comments.count - 3) more comments"
                        }
                        
                        if let comment = comments[comments.count - 2] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            secondToLastComment?.attributedText = attributedString
                        }
                        
                        if let comment = comments[comments.count - 1] as? NSDictionary {
                            let htmlString = Serv.renderComment(comment, newLine: false)
                            let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: "No comment yet.", lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                            lastComment?.attributedText = attributedString
                        }
                        
                        commentsViewHeight?.constant = 100
                        commentsView?.hidden = false
                        return
                    }
                }
                
                return
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            commentsViewHeight?.constant = 0
            commentsView?.hidden = true
        })
    }

    private func setCountComments(notification: NSDictionary, commentingButton: UIButton?, showCommentButton: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            if self.notification != notification {
                return
            }
            
            if commentingButton != nil {
                commentingButton!.hidden = !showCommentButton
            }
            
            if let comments = notification["comments"] as? NSArray {
                if commentingButton != nil {
                    commentingButton!.setTitle(String(comments.count), forState: UIControlState.Normal)
                }
            } else {
                if commentingButton != nil {
                    commentingButton!.setTitle(String(0), forState: UIControlState.Normal)
                }
            }
        }
    }
    
    private func setOptions(optionsButton: UIButton?, showOptionsButton: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            if optionsButton != nil {
                optionsButton!.hidden = !showOptionsButton
            }
        }
    }
    
    // Call this from thread DISPATCH_QUEUE_PRIORITY_HIGH.
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, cellTime: UILabel?, cellMessage: UILabel?, userPhoto: UIImageView?, commentingButton: UIButton?, commenting: NSDictionary, likingButton: UIButton?, optionsButton: UIButton?, showButtons: Bool) {
        self.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, showButtons: showButtons)
        self.setLiking(notification, likingButton: likingButton, showLikeButton: showButtons)
        self.setCommenting(notification, commenting: commenting)
        self.setCountComments(notification, commentingButton: commentingButton, showCommentButton: showButtons)
        self.setOptions(optionsButton, showOptionsButton: showButtons)
    }
    
    // Call this from thread DISPATCH_QUEUE_PRIORITY_HIGH.
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, cellTime: UILabel?, cellMessage: UILabel?, userPhoto: UIImageView?, commentingButton: UIButton?, commenting: NSDictionary, optionsButton: UIButton?, showButtons: Bool) {
        self.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, showButtons: showButtons)
        self.setCommenting(notification, commenting: commenting)
        self.setCountComments(notification, commentingButton: commentingButton, showCommentButton: showButtons)
        self.setOptions(optionsButton, showOptionsButton: showButtons)
    }
    
    // Call this from thread DISPATCH_QUEUE_PRIORITY_HIGH.
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, cellTime: UILabel?, cellMessage: UILabel?, userPhoto: UIImageView?, commentingButton: UIButton?, likingButton: UIButton?, optionsButton: UIButton?, showButtons: Bool) {
        self.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, showButtons: showButtons)
        self.setLiking(notification, likingButton: likingButton, showLikeButton: showButtons)
        self.setCountComments(notification, commentingButton: commentingButton, showCommentButton: showButtons)
        self.setOptions(optionsButton, showOptionsButton: showButtons)
    }
    
    // Call this from thread DISPATCH_QUEUE_PRIORITY_HIGH.
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, cellTime: UILabel?, cellMessage: UILabel?, userPhoto: UIImageView?, commentingButton: UIButton?, optionsButton: UIButton?, showButtons: Bool) {
        self.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, showButtons: showButtons)
        self.setCountComments(notification, commentingButton: commentingButton, showCommentButton: showButtons)
        self.setOptions(optionsButton, showOptionsButton: showButtons)
    }
    
    // Call this from thread DISPATCH_QUEUE_PRIORITY_HIGH.
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, cellTime: UILabel?, cellMessage: UILabel?, userPhoto: UIImageView?, likingButton: UIButton?, optionsButton: UIButton?, showButtons: Bool) {
        self.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, showButtons: showButtons)
        self.setLiking(notification, likingButton: likingButton, showLikeButton: showButtons)
        self.setOptions(optionsButton, showOptionsButton: showButtons)
    }
    
    // Call this from thread DISPATCH_QUEUE_PRIORITY_HIGH.
    private func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, cellTime: UILabel?, cellMessage: UILabel?, userPhoto: UIImageView?, showButtons: Bool) {
        self.notification = notification
        self.controller = controller
        
        dispatch_async(dispatch_get_main_queue()) {
            if userPhoto != nil {
                userPhoto!.layer.cornerRadius = userPhoto!.frame.size.width / 2
                userPhoto!.clipsToBounds = true
            }
            
            if let humanTime = notification["humanTime"] as? NSString {
                if cellTime != nil {
                    cellTime!.text = humanTime as String
                }
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                Serv.setNotificationMessage(notification, label: cellMessage)
            })
            
            if let userSubjectFacebookPhoto = notification["userSubjectFacebookPhoto"] as? NSString {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    Serv.loadImage(userSubjectFacebookPhoto as String, handler: { (error, data) -> () in
                        if error != nil {
                            print(error)
                        }
                        
                        if data != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                if self.notification != notification {
                                    return
                                }
                                
                                if userPhoto != nil {
                                    userPhoto!.image = UIImage(data: data!)
                                }
                            })
                        }
                    })
                })
            }
        }
    }
    
    // This must be called from the main thread.
    func clickMore(sender: AnyObject) {
        if controller == nil {
            return
        }
        
        if notification == nil {
            return
        }
        
        if let user = UserCache().getUser() {
            if let notificationId = self.notification!["id"] as? NSString, firstName = user["firstName"] as? NSString, lastName = user["lastName"] as? NSString, userId = user["id"] as? NSString {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                    let optionMenu = UIAlertController(title: "More actions", message: "Choose an action", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let flagAction = UIAlertAction(title: "Flag post or comments", style: UIAlertActionStyle.Default) { (action) -> Void in
                        let urlString = "mailto:vlad@habitlab.me?subject=Flag%20notification%20id%20\(notificationId)%20body%20or%20messages&body=Hi%20Vlad.%20I%20am%20\(firstName)%20\(lastName)%20(id%20\(userId)),%20and%20I%20want%20to%20flag%20post%20(id%20\(notificationId))%20for%20inappropriate%20content%20in%20its%20body%20or%20comments.%20Thank%20you."
                        
                        if let url = NSURL(string: urlString) {
                            UIApplication.sharedApplication().openURL(url)
                        }
                    }
                    
                    let cancelAction = UIAlertAction(title: "Not now", style: UIAlertActionStyle.Cancel, handler: nil)
                    
                    optionMenu.addAction(flagAction)
                    optionMenu.addAction(cancelAction)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.controller?.presentViewController(optionMenu, animated: true, completion: nil)
                    })
                })
            }
        }
    }

    // This must be called from the main thread.
    func clickProfile(sender: AnyObject) {
        if notification == nil {
            return
        }
        
        if controller == nil {
            return
        }
        
        if let userSubjectId = notification!["userSubjectId"] as? NSString {
            if let controller = controller as? NewFeedController {
                controller.performSegueWithIdentifier("showPublicProfileFromFeed", sender: userSubjectId as String)
            }
            
            if let controller = controller as? ProfileController {
                controller.performSegueWithIdentifier("showPublicProfileFromProfile", sender: userSubjectId as String)
            }
        }
    }
}
