//
//  ChooseNewLevelCurrentCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class ChooseNewLevelCurrentCell: ChooseNewLevelCell {
    var controller: UIViewController?
    
    @IBOutlet weak var percentCompleted: UILabel!
    @IBOutlet weak var levelName: UILabel!
    @IBOutlet weak var levelLevel: UILabel!
    
    @IBAction func clickedButton(sender: AnyObject) {
        if controller == nil {
            return
        }
        
        if let levelController = controller as? ChooseNewLevelController {
            if let drive = super.drive {
                if let program = drive["program"] as? NSDictionary {
                    levelController.performSegueWithIdentifier("showCurrentHabitFromChooseNewLevel", sender: program)
                }
            }
        }
    }
    
    func populate(drive: NSDictionary, controller: UIViewController) {
        self.controller = controller
        super.populate(drive, levelName: levelName, levelLevel: levelLevel)
        
        if let program = drive["program"] as? NSDictionary {
            if let success = program["success"] as? NSNumber {
                self.percentCompleted.text = "\(success)%"
            }
        }
    }
}