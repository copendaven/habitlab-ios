//
//  FadeInAnimator.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/13/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class FadeInAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.1
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        if let containerView = transitionContext.containerView(), toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) {
            
            containerView.addSubview(toViewController.view)
            toViewController.view.alpha = 0.0
            
            let duration = transitionDuration(transitionContext)
            
            UIView.animateWithDuration(duration, animations: {
                toViewController.view.alpha = 1.0
            }) { (finished) in
                let cancelled = transitionContext.transitionWasCancelled()
                transitionContext.completeTransition(!cancelled)
            }
        }
    }
}
