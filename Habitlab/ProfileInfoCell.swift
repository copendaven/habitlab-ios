//
//  ProfileInfoCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 1/21/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ProfileInfoCell: UITableViewCell {
    var controller: UIViewController?
    
    @IBOutlet weak var userPhoto: UIImageView!
    
    @IBOutlet weak var countHabits: UIButton!
    @IBOutlet weak var countFollowing: UIButton!
    @IBOutlet weak var countFollowers: UIButton!
    
    @IBAction func clickFollowing(sender: AnyObject) {
        if controller != nil {
            controller!.performSegueWithIdentifier("showFollowingFromProfile", sender: controller!)
        }
    }
    
    @IBAction func clickFollowers(sender: AnyObject) {
        if controller != nil {
            controller!.performSegueWithIdentifier("showFollowersFromProfile", sender: controller!)
        }
    }
    
    @IBAction func clickHabits(sender: AnyObject) {
        if controller != nil {
            controller!.performSegueWithIdentifier("showHabitsFromProfile", sender: controller!)
        }
    }
    
    func populate(controller: UIViewController) {
        self.controller = controller
        
        dispatch_async(dispatch_get_main_queue()) {
            self.userPhoto.layer.cornerRadius = self.userPhoto.frame.size.width / 2
            self.userPhoto.layer.borderWidth = 4.0
            self.userPhoto.layer.borderColor = UIColor.whiteColor().CGColor
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            if let user = UserCache().getUser() {
                if let photo = user["facebookPhoto"] as? NSString {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                        Serv.loadImage(photo as String, handler: { (error, data) -> () in
                            if error != nil {
                                print(error)
                            }
                            
                            if data != nil {
                                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                    self.userPhoto.image = UIImage(data: data!)
                                })
                            }
                        })
                    }
                }
                
                if let countFollowers = user["countFollowers"] as? NSNumber {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.countFollowers.setTitle(String(countFollowers), forState: UIControlState.Normal)
                    }
                }
                
                if let countFollowing = user["countFollowing"] as? NSNumber {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.countFollowing.setTitle(String(countFollowing), forState: UIControlState.Normal)
                    }
                }
                
                if let countHabits = user["countHabits"] as? NSNumber {
                    dispatch_async(dispatch_get_main_queue()) {
                        self.countHabits.setTitle(String(countHabits), forState: UIControlState.Normal)
                    }
                }
            }
        }
    }
}
