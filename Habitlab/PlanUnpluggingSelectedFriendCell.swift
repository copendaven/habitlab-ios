//
//  PlanUnpluggingSelectedFriendCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/7/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class PlanUnpluggingSelectedFriendCell: UITableViewCell {
    private var controller: UIViewController?
    private var friend: NSDictionary?
    
    @IBOutlet weak var userName: UILabel!
    
    func populate(controller: UIViewController, friend: NSDictionary) {
        self.controller = controller
        self.friend = friend
        
        if let firstName = friend["firstName"] as? NSString, lastName = friend["lastName"] as? NSString {
            self.userName.text = "\(firstName) \(lastName)"
        }
    }
    
    @IBAction func deselectedFriend(sender: AnyObject) {
        if let realFriend = self.friend {
            if let id = realFriend["id"] as? NSString {
                if let viewController = self.controller as? PlanUnpluggingController {
                    viewController.deselectFriend(id as String)
                }
            }
        }
    }
}
