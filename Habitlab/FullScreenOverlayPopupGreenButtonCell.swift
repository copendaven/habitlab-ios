//
//  FullScreenOverlayPopupGreenButtonCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 5/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class FullScreenOverlayPopupGreenButtonCell: FullScreenOverlayPopupButtonCell {
    @IBOutlet weak var button: UIButton!
    
    func populate(controller: UIViewController, buttonRepresentation: FullScreenOverlayPopupButton) {
        super.populate(controller, buttonRepresentation: buttonRepresentation, button: button)
        
        self.button.layer.borderWidth = 2.0
        self.button.layer.borderColor = UIColor(red: 0.21, green: 0.84, blue: 0.59, alpha: 1.0).CGColor
        self.button.layer.cornerRadius = 0.5 * (button.bounds.size.height + self.button.layer.borderWidth)
    }
    
    @IBAction func clickedButton(sender: AnyObject) {
        super.clickedButton()
    }
}
