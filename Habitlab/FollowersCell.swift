//
//  FollowersCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowersCell: UITableViewCell {
    var follower: NSMutableDictionary?
    var controller: UIViewController?
    
    func refresh(userName: UILabel, userImage: UIImageView) {
        userImage.layer.cornerRadius = userImage.frame.size.width / 2
        userImage.clipsToBounds = true
        
        if follower != nil {
            if let user = follower!["user"] as? NSDictionary {
                if let photo = user["facebookPhoto"] as? NSString {
                    Serv.loadImage(photo as String, handler: { (error, data) -> () in
                        if error != nil {
                            print(error)
                        }
                        
                        if data != nil {
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                userImage.image = UIImage(data: data!)
                            })
                        }
                    })
                }
                
                if let firstName = user["firstName"] as? NSString, lastName = user["lastName"] as? NSString {
                    userName.text = "\(firstName) \(lastName)"
                }
            }
        }
    }
    
    func populate(follower: NSDictionary, controller: UIViewController, userName: UILabel, userImage: UIImageView) {
        self.follower = NSMutableDictionary(dictionary: follower)
        self.controller = controller
        self.refresh(userName, userImage: userImage)
    }
    
    func clickProfile(sender: AnyObject) {
        if follower == nil {
            return
        }
        
        if controller == nil {
            return
        }
        
        if let user = follower!["user"] as? NSDictionary {
            if let id = user["id"] as? NSString {
                if let controller = controller as? FollowersController {
                    controller.performSegueWithIdentifier("showPublicProfileFromFollowers", sender: id as String)
                }
            }
        }
    }
}
