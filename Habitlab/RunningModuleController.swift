//
//  RunningModuleController.swift
//  Habitlab
//
//  Created by Vlad Manea on 2/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Analytics

class RunningModuleController: GAITrackedViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private static var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    
    var program: NSDictionary?
    var step: NSDictionary?
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var goalLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var circle: KDCircularProgress!
    
    private var friendlyMessages: [String]?
    private var suggestionsFirst: [String]?
    private var doneStatus: String?
    private var suggestionSecond: String?
    
    private var timer = NSTimer()
    private var halfwayTimer = NSTimer()
    private var actions: NSMutableArray = NSMutableArray(array: [ModuleActionType.ThinkingAboutYou.rawValue])
    
    var progressDoneController: ProgressDoneController?
    var progressDoneBackgroundColor: ProgressDoneBackgroundColor?
    
    @IBOutlet weak var actionsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(RunningModuleController)
        
        actionsCollectionView.dataSource = self
        actionsCollectionView.delegate = self
        
        circle.progressThickness = 0.25
        circle.trackThickness = 0.25
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if step == nil {
            return
        }
        
        if program == nil {
            return
        }
        
        if let viewController = self.progressDoneController {
            viewController.hideSilently()
        }
        
        timer.invalidate()
        timer = NSTimer.scheduledTimerWithTimeInterval(0.25, target: self, selector: #selector(RunningModuleController.refresh), userInfo: nil, repeats: true)
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = true
        }
        
        // Initialize the running tracker.
        RunningTracker.runningTracker = RunningTracker(program: program!, step: step!, controller: self)
        
        if let stepId = step!["id"] as? NSString {
            if ProgramStepTrackingCache().getTracking(stepId as String) {
                self.startTracking(false)
            }
        }
        
        refreshActions(false, isDone: false)
        segment()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realProgram = self.program {
            if let drive = realProgram["drive"] as? NSDictionary {
                if let categ = drive["driveCategory"] as? NSString {
                    dict["category"] = categ as String
                }
                
                if let subcateg = drive["driveSubcategory"] as? NSString {
                    dict["subcategory"] = subcateg as String
                }
                
                if let level = drive["level"] as? NSNumber {
                    dict["level"] = String(level)
                }
            }
        }
        
        if let realStep = self.step {
            if let index = realStep["index"] as? NSNumber {
                dict["index"] = String(index)
            }
            
            if let week = realStep["week"] as? NSNumber {
                dict["week"] = String(week)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(RunningModuleController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(RunningModuleController))
        }
    }
    
    private func refreshActions(isTracking: Bool, isDone: Bool) {
        self.actions = NSMutableArray()
        
        if !isDone {
            if !isTracking {
                self.actions.addObject(ModuleActionType.Start.rawValue)
            } else {
                self.actions.addObject(ModuleActionType.Stop.rawValue)
            }
        } else {
            self.actions.addObject(ModuleActionType.Done.rawValue)
        }
        
        self.actions.addObject(ModuleActionType.ThinkingAboutYou.rawValue)
        
        self.actionsCollectionView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
        halfwayTimer.invalidate()
        
        if let tabBarController = self.tabBarController {
            tabBarController.tabBar.hidden = false
        }
    }
    
    func startTracking(byButtonClick: Bool) {
        if step == nil {
            return
        }
        
        if let stepId = step!["id"] as? NSString, endDate = Serv.getEndDate(step!), hardEndDate = Serv.getHardEndDate(step!) {
            
            // Do not track if past the hard deadline, no matter what.
            if hardEndDate.compare(NSDate()) == NSComparisonResult.OrderedAscending {
                ProgramStepTrackingCache().setTracking(stepId as String, tracking: false, dueDate: hardEndDate)
                RunningTracker.stop()
                return
            }
            
            // Do not track if past the soft deadline, if the user triggered the track by clicking.
            if byButtonClick && endDate.compare(NSDate()) == NSComparisonResult.OrderedAscending && !ProgramStepStartedCache().isStepStarted(stepId as String) {
                ProgramStepTrackingCache().setTracking(stepId as String, tracking: false, dueDate: hardEndDate)
                RunningTracker.stop()
                return
            }
            
            ProgramStepTrackingCache().setTracking(stepId as String, tracking: true, dueDate: hardEndDate)
            
            if ProgramStepTrackingCache().getExpired(stepId as String) {
                ProgramStepTrackingCache().setTracking(stepId as String, tracking: false, dueDate: hardEndDate)
                RunningTracker.stop()
                return
            }
            
            if ProgramStepTrackingCache().getDone(stepId as String) {
                ProgramStepTrackingCache().setTracking(stepId as String, tracking: false, dueDate: hardEndDate)
                RunningTracker.stop()
                return
            }
            
            RunningTracker.start()
            
            if !ProgramStepStartedCache().isStepStarted(stepId as String) {
                HabitlabRestAPI.putProgramStepStarted("Habitlab running tracker", stepId: stepId as String, handler: { (error, response) in
                    if error != nil {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        return
                    }
                    
                    ProgramStepStartedCache().setStepStarted(stepId as String)
                })
            }
        }
    }
    
    func stopTracking() {
        let destroyAlert = UIAlertController(title: "Opt out of tracking", message: "By opting out of tracking, you lose all kilometers run, and you will have to start over. Do you want to still opt out of tracking?", preferredStyle: UIAlertControllerStyle.Alert)
        
        destroyAlert.addAction(UIAlertAction(title: "Yes, opt out", style: .Default, handler: { (action: UIAlertAction!) in
            if self.step == nil {
                return
            }
            
            if let stepId = self.step!["id"] as? NSString, hardEndDate = Serv.getHardEndDate(self.step!) {
                ProgramStepTrackingCache().setTracking(stepId as String, tracking: false, dueDate: hardEndDate)
                ProgramStepTrackingCache().resetDistance(stepId as String, dueDate: hardEndDate)
                ProgramStepTrackingCache().setVibratedHalfway(stepId as String, vibratedHalfway: false, dueDate: hardEndDate)
                ProgramStepTrackingCache().setVibratedDone(stepId as String, vibratedDone: false, dueDate: hardEndDate)
            }
            
            RunningTracker.stop()
            self.performSegueWithIdentifier("showCurrentHabitFromRunningModule", sender: nil)
        }))
        
        destroyAlert.addAction(UIAlertAction(title: "No, stay in", style: .Cancel, handler: nil))
        
        presentViewController(destroyAlert, animated: true, completion: nil)
    }
    
    func buttonClicked(actionType: ModuleActionType) {
        switch (actionType) {
        case .Done:
            self.progressDoneStarted()
            break
        case .Start:
            self.startTracking(true)
            break
        case .Stop:
            self.stopTracking()
            break
        case .ThinkingAboutYou:
            var state = ThinkingAboutYouState.BeforeRun
            
            if step != nil {
                if let stepId = step!["id"] as? NSString {
                    if ProgramStepTrackingCache().getTracking(stepId as String) {
                        state = ThinkingAboutYouState.DuringRun
                    }
                    
                    if ProgramStepTrackingCache().getDone(stepId as String) {
                        state = ThinkingAboutYouState.AfterRun
                    }
                }
            }
            
            self.performSegueWithIdentifier("showThinkingAboutYouFromRunningModule", sender: state.rawValue)
            break
        default:
            break
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showThinkingAboutYouFromRunningModule") {
            if let viewController = segue.destinationViewController as? ThinkingAboutYouChooseMessageController, stateString = sender as? String {
                if let state = ThinkingAboutYouState(rawValue: stateString) {
                    viewController.thinkingAboutYouState = state
                }
            }
        }
        
        if segue.identifier == "showProgressDoneFromRunningModule" {
            if let viewController = segue.destinationViewController as? ProgressDoneController {
                self.progressDoneController = viewController
                viewController.setDelegate(self)
                
                if let backgroundColor = self.progressDoneBackgroundColor {
                    viewController.setBackgroundColor(backgroundColor)
                }
                
                viewController.setDoneCaptions(friendlyMessages, doneStatus: doneStatus, suggestionsFirst: suggestionsFirst, suggestionSecond: suggestionSecond)
                viewController.markDone()
            }
        }
    }
    
    func refresh() {
        if step == nil || program == nil {
            self.statusLabel.text = ""
            self.distanceLabel.text = ""
            self.goalLabel.text = ""
            
            return
        }
        
        if let stepId = step!["id"] as? NSString {
            var isTracking = false
            var isDone = false
            
            if ProgramStepTrackingCache().getTracking(stepId as String) {
                isTracking = true
                
                self.statusLabel.text = "YOU'VE RUN"
                
                if let drive = program!["drive"] as? NSDictionary {
                    if let countKilometersPerStep = drive["countKilometersPerStep"] as? NSNumber {
                        self.goalLabel.text = "OUT OF \(countKilometersPerStep) KM"
                    }
                }
                
                var dist = ProgramStepTrackingCache().getDistance(stepId as String)
                
                if dist <= 0.0 {
                    dist = 0.0
                }
                
                self.distanceLabel.text = "\(dist < 10.0 ? "0": "")\(String(format: "%2.2f", dist))"
            
                if let halfway = ProgramStepTrackingCache().getHalfway(stepId as String) {
                    if halfway == 1 {
                        self.statusLabel.text = "YOU ARE HALFWAY"
                    }
                }
            }
        
            if ProgramStepTrackingCache().getDone(stepId as String) {
                isDone = true
                
                self.statusLabel.text = "YOU MADE IT!"
                
                if let drive = program!["drive"] as? NSDictionary {
                    if let countKilometersPerStep = drive["countKilometersPerStep"] as? NSNumber {
                        self.distanceLabel.text = "\(Double(countKilometersPerStep) < 10.0 ? "0": "")\(String(format: "%2.2f", Double(countKilometersPerStep)))"
                    }
                }
            }
        
            self.refreshActions(isTracking, isDone: isDone)
        }
    }
    
    func addDelta(delta: Double) {
        if step == nil {
            return
        }
        
        if program == nil {
            return
        }
        
        if let stepId = step!["id"] as? NSString, dueDate = Serv.getHardEndDate(step!) {
            if ProgramStepTrackingCache().getExpired(stepId as String) {
                RunningTracker.stop()
                return
            }
            
            ProgramStepTrackingCache().addDistance(stepId as String, distance: delta / 1000, dueDate: dueDate) // Distance in kilometers.
            
            if let drive = program!["drive"] as? NSDictionary {
                if let countKilometersPerStepNumber = drive["countKilometersPerStep"] as? NSNumber {
                    let doubleCountKilometersPerStep = Double(countKilometersPerStepNumber)
                    let distance = ProgramStepTrackingCache().getDistance(stepId as String)
                    
                    if distance >= doubleCountKilometersPerStep {
                        self.handleDone(stepId as String)
                        return
                    }
                    
                    let halfNecessaryDistance = doubleCountKilometersPerStep / 2
                    
                    if distance >= halfNecessaryDistance {
                        self.handleHalfway()
                    }
                    
                    let intDistance = Int(distance)
                    let doubleIntDistance = Double(intDistance)
                    
                    // Play the sound only if the halfway and new distance milestone do not overlap too much.
                    if abs(doubleIntDistance - halfNecessaryDistance) > 0.1 && abs(doubleIntDistance - doubleCountKilometersPerStep) > 0.1 {
                        if ProgramStepTrackingCache().trySetRunDistanceUnits(stepId as String, distanceUnits: intDistance, dueDate: dueDate) {
                            if ProgramStepTrackingCache().trySetVibratedDistanceUnits(stepId as String, distanceUnits: intDistance, dueDate: dueDate) {
                                self.playRunDistanceSound()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func handleHalfway() {
        if step == nil {
            return
        }
        
        if let stepId = step!["id"] as? NSString, hardEndDate = Serv.getHardEndDate(step!) {
            if let halfway = ProgramStepTrackingCache().getHalfway(stepId as String) {
                if halfway == 0 {
                    ProgramStepTrackingCache().setHalfway(stepId as String, halfway: 1, dueDate: hardEndDate)
                    halfwayTimer.invalidate()
                    halfwayTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(RunningModuleController.hideHalfway), userInfo: nil, repeats: false)
                    return
                }
                
                if halfway == 1 {
                    let vibrate = { () -> () in
                        self.playAlertSound()
                        ProgramStepTrackingCache().setVibratedHalfway(stepId as String, vibratedHalfway: true, dueDate: hardEndDate)
                    }
                    
                    if !ProgramStepTrackingCache().getVibratedHalfway(stepId as String) {
                        vibrate()
                    }
                }
            }
        }
    }
    
    func hideHalfway() {
        if step == nil {
            return
        }
        
        if let stepId = step!["id"] as? NSString, hardEndDate = Serv.getHardEndDate(step!) {
            ProgramStepTrackingCache().setHalfway(stepId as String, halfway: 2, dueDate: hardEndDate)
        }
    }
    
    func playRunDistanceSound() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: AVAudioSessionCategoryOptions.DuckOthers)
        } catch let error as NSError {
            print(error)
        }
        
        if let audioPath = NSBundle.mainBundle().pathForResource("alert", ofType: "mp3") {
            do {
                RunningModuleController.audioPlayer = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: audioPath))
                RunningModuleController.audioPlayer.play() // Play the sound.
                
                NSTimer.scheduledTimerWithTimeInterval(RunningModuleController.audioPlayer.duration, target: self, selector: #selector(RunningModuleController.stopRunDistanceSound), userInfo: nil, repeats: false)
            } catch {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate) // At least vibrate.
            }
        }
    }
    
    func stopRunDistanceSound() {
        RunningModuleController.audioPlayer.stop()
        
        do {
            try AVAudioSession.sharedInstance().setActive(false, withOptions: AVAudioSessionSetActiveOptions.NotifyOthersOnDeactivation)
        } catch {
            // Do nothing.
        }
    }
    
    func playAlertSound() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, withOptions: AVAudioSessionCategoryOptions.DuckOthers)
        } catch let error as NSError {
            print(error)
        }
        
        if let audioPath = NSBundle.mainBundle().pathForResource("alert", ofType: "mp3") {
            do {
                RunningModuleController.audioPlayer = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: audioPath))
                RunningModuleController.audioPlayer.play() // Play the sound.
                
                NSTimer.scheduledTimerWithTimeInterval(RunningModuleController.audioPlayer.duration, target: self, selector: #selector(RunningModuleController.stopAlertSound), userInfo: nil, repeats: false)
            } catch {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate) // At least vibrate.
            }
        }
    }

    func stopAlertSound() {
        RunningModuleController.audioPlayer.stop()
        
        do {
            try AVAudioSession.sharedInstance().setActive(false, withOptions: AVAudioSessionSetActiveOptions.NotifyOthersOnDeactivation)
        } catch {
            // Do nothing.
        }
    }
    
    private func handleDone(stepId: String) {
        if step == nil {
            return
        }
        
        if program == nil {
            return
        }
        
        if let owner = program!["owner"] as? NSDictionary, hardEndDate = Serv.getHardEndDate(step!) {
            let vibrate = { () -> () in
                self.playAlertSound()
                ProgramStepTrackingCache().setVibratedDone(stepId as String, vibratedDone: true, dueDate: hardEndDate)
            }
            
            if !ProgramStepTrackingCache().getVibratedDone(stepId as String) {
                vibrate()
            }
            
            RunningTracker.stop()
            ProgramStepTrackingCache().setTracking(stepId as String, tracking: false, dueDate: hardEndDate)
            ProgramStepTrackingCache().setDone(stepId as String, dueDate: hardEndDate)
            ProgramStepTrackingCache().setHalfway(stepId as String, halfway: 2, dueDate: hardEndDate)
            
            let cacheId = NSUUID().UUIDString
            
            if ProgramStepCompleteCache().isStepComplete(stepId) {
                return
            }
            
            ProgramStepCompleteCache().setStepComplete(stepId)
            
            CurrentHabitController.setInspirationCache(cacheId, program: self.program!, step: self.step!, owner: owner)
            
            Serv.showSpinner()
            
            HabitlabRestAPI.putProgramStepComplete("Habitlab running tracker", stepId: stepId as String, cacheId: cacheId, handler: { (error, stepJson) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    ProgramStepCompleteCache().unsetStepComplete(stepId as String)
                    return
                }
                
                HabitsController.resetPrograms()
                self.refresh()
            })
        }
    }
    
    private func progressDoneStarted() {
        
        if step == nil || program == nil {
            return
        }
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Red
        
        friendlyMessages = ["Hooray!", "Well done!", "Rockstar!", "You rock!", "Way to go!", "Woohoo!", "You did it!", "One down!", "Alrighty!"]
        suggestionsFirst = ["Make this count even more", "Double the impact", "Spread the good vibes", "Make sure to celebrate"]
        
        // We are dealing with an internal loop in the level, so the level is not complete yet.
        doneStatus = "Loop complete"
        suggestionSecond = "by inspiring your friends!"
        
        if let drive = self.program!["drive"] as? NSDictionary, week = self.step!["week"] as? NSNumber, index = self.step!["index"] as? NSNumber {
            if let countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
                let weekInt = Int(week)
                let countWeeksInt = Int(countWeeks)
                let indexInt = Int(index)
                let countStepsPerWeekInt = Int(countStepsPerWeek)
                
                if weekInt == countWeeksInt - 1 && indexInt == countStepsPerWeekInt - 1 {
                    
                    // We are dealing with the last loop in the level, so the level is complete!
                    self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Green
                    
                    // We are dealing with the last loop in the level, so the level is complete!
                    doneStatus = "Level complete"
                    suggestionSecond = "by inspiring your friends!"
                    
                    if let level = drive["level"] as? NSNumber, countLevels = drive["countLevels"] as? NSNumber {
                        let levelInt = Int(level)
                        let countLevels = Int(countLevels)
                        
                        if levelInt >= countLevels {
                            
                            // We are dealing with the last level in the program, so the program is complete!
                            doneStatus = "Program complete"
                            suggestionSecond = "by inspiring your friends!"
                            
                            // We are dealing with the last level in the program, so the program is complete!
                            self.progressDoneBackgroundColor = ProgressDoneBackgroundColor.Blue
                        }
                    }
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.performSegueWithIdentifier("showProgressDoneFromRunningModule", sender: self)
        })
    }
    
    func progressDoneFinished() {
        self.performSegueWithIdentifier("showCurrentHabitFromRunningModule", sender: nil)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actions.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(90, 125) // This is the size for one collection item with button and label
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenWidth = Int(actionsCollectionView.frame.size.width)
        var contentWidth = Int(actions.count * 90) // This is the size for one collection item with button and label
        
        if screenWidth <= contentWidth {
            contentWidth = screenWidth / 90 * 90
        }
        
        let padding = CGFloat((screenWidth - contentWidth) / 2)
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if actions.count <= indexPath.item {
            return defaultCell
        }
        
        if let action = actions[indexPath.item] as? String {
            if let cell = actionsCollectionView.dequeueReusableCellWithReuseIdentifier("moduleActionCollectionCell", forIndexPath: indexPath) as? ModuleActionCollectionCell {
                if let moduleActionType = ModuleActionType(rawValue: action) {
                    cell.populate(actionType: moduleActionType, actionState: ModuleActionState.Enabled, controller: self)
                    return cell
                }
            }
        }
        
        return defaultCell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }

    @IBAction func runningModuleSentThoughtAboutYou(segue: UIStoryboardSegue) {
        // Nothing here.
    }
}
