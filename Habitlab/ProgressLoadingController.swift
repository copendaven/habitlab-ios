//
//  ProgressLoadingController.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/29/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ProgressLoadingController: UIViewController {
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var pictogram: UIImageView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    private var delegate: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        activityIndicator.startAnimating()
        
        caption.alpha = 0.0
        pictogram.alpha = 0.0
        
        print("Setting the image to LOADING")
    }
    
    func setDelegate(delegate: UIViewController) {
        self.delegate = delegate
    }
    
    func markDone(captionText: String, hideInSeconds: NSTimeInterval) {
        caption.text = captionText
        
        activityIndicator.stopAnimating()
        
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.activityIndicator.alpha = 0.0
            self.pictogram.alpha = 1.0
            self.caption.alpha = 1.0
        }) { (success) in
            print("Setting the image to DONE")
            
            NSTimer.scheduledTimerWithTimeInterval(hideInSeconds, target: self, selector: #selector(ProgressDoneController.hideWithCallback), userInfo: nil, repeats: false)
        }
    }
    
    func hideSilently() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func hideWithCallback() {
        if let viewController = self.delegate as? ThinkingAboutYouChooseFriendsController {
            self.dismissViewControllerAnimated(true) {
                viewController.doneLoading()
            }
        }
        
        if let viewController = self.delegate as? ScheduleDateController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.doneLoading()
            })
        }
        
        if let viewController = self.delegate as? SharingEmotionsChooseEmotionsController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.doneLoading()
            })
        }
        
        if let viewController = self.delegate as? PlanUnpluggingController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.doneLoading()
            })
        }
        
        if let viewController = self.delegate as? ReconnectChooseActionController {
            self.dismissViewControllerAnimated(true, completion: {
                viewController.doneLoading()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
}
