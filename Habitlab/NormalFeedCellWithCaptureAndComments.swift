//
//  NormalFeedCellWithCaptureAndComments.swift
//  Habitlab
//
//  Created by Vlad Manea on 26/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class NormalFeedCellWithCaptureAndComments: FeedCell {
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var cellTime: UILabel!
    @IBOutlet weak var cellMessage: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var captureImage: UIImageView!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var optionsView: UIView!
        
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var firstComment: UILabel!
    @IBOutlet weak var missingComments: UILabel!
    @IBOutlet weak var secondToLastComment: UILabel!
    @IBOutlet weak var lastComment: UILabel!
    
    @IBOutlet weak var firstCommentTop: NSLayoutConstraint!
    @IBOutlet weak var firstCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var missingCommentTop: NSLayoutConstraint!
    @IBOutlet weak var missingCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var secondToLastCommentBottom: NSLayoutConstraint!
    @IBOutlet weak var lastCommentTop: NSLayoutConstraint!
    @IBOutlet weak var lastCommentBottom: NSLayoutConstraint!

    @IBOutlet weak var commentsViewHeight: NSLayoutConstraint!
    
    @IBAction override func clickMore(sender: AnyObject) {
        super.clickMore(sender)
    }
    
    @IBAction override func comment(sender: AnyObject) {
        super.comment(sender)
    }
    
    @IBAction override func like(sender: AnyObject) {
        super.like(sender)
    }
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    private func getCommenting() -> NSDictionary {
        let commenting = NSMutableDictionary()
        
        commenting.setObject(firstComment, forKey: "firstComment")
        commenting.setObject(missingComments, forKey: "missingComments")
        commenting.setObject(secondToLastComment, forKey: "secondToLastComment")
        commenting.setObject(lastComment, forKey: "lastComment")
        
        commenting.setObject(firstCommentTop, forKey: "firstCommentTop")
        commenting.setObject(firstCommentBottom, forKey: "firstCommentBottom")
        commenting.setObject(missingCommentTop, forKey: "missingCommentTop")
        commenting.setObject(missingCommentBottom, forKey: "missingCommentBottom")
        commenting.setObject(secondToLastCommentTop, forKey: "secondToLastCommentTop")
        commenting.setObject(secondToLastCommentBottom, forKey: "secondToLastCommentBottom")
        commenting.setObject(lastCommentTop, forKey: "lastCommentTop")
        commenting.setObject(lastCommentBottom, forKey: "lastCommentBottom")
        
        commenting.setObject(commentsView, forKey: "commentsView")
        commenting.setObject(commentsViewHeight, forKey: "commentsViewHeight")
        
        return commenting
    }
    
    func setLiking() {
        if let note = super.notification {
            super.setLiking(note, likingButton: self.likeButton, showLikeButton: true)
        }
    }
    
    func populate(aboutMe: Bool, notification: NSDictionary, controller: UIViewController, showButtons: Bool) {
        let commenting = getCommenting()
        
        super.populate(aboutMe, notification: notification, controller: controller, cellTime: cellTime, cellMessage: cellMessage, userPhoto: userPhoto, commentingButton: commentButton, commenting: commenting, likingButton: likeButton, optionsButton: optionsButton, showButtons: showButtons)
        
        dispatch_async(dispatch_get_main_queue()) {
            if self.optionsView != nil {
                self.optionsView!.hidden = !showButtons
            }
        }
        
        if let programStepObjectId = notification["programStepObjectId"] as? NSString {
            // Read the image from the photo cache first, if possible.
            if let imageData = PhotoCache().getCache("programstep\(programStepObjectId)") {
                let image = UIImage(data: imageData)
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if self.captureImage != nil {
                        UIView.transitionWithView(self.captureImage, duration: 0.4, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { self.captureImage.image = image }, completion: nil)
                    }
                })
                
                return
            }
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                
                // If the photo cache did not contain the image, I will download it from Amazon...
                HabitlabS3API.downloadStepImage(programStepObjectId as String, handler: { (error, imageData) -> () in
                    if error != nil {
                        return
                    }
                    
                    if imageData != nil {
                        PhotoCache().setCache("programstep\(programStepObjectId as String)", image: imageData!)
                        let image = UIImage(data: imageData!)
                        
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            if notification != super.notification {
                                return
                            }
                            
                            if self.captureImage != nil {
                                UIView.transitionWithView(self.captureImage, duration: 0.4, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { self.captureImage.image = image }, completion: nil)
                            }
                        })
                    }
                }, progress: nil)
            })
        }
    }
}
