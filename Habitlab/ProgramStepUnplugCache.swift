//
//  ProgramStepUnplugCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/10/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData

class ProgramStepUnplugCache: NSObject {
    private static var object: NSObject = NSObject()
    private let entityName = "ProgramStepUnplugCache"
    
    // MARK: Public API
    
    func start(stepId: String, dueDate: NSDate) -> Bool {
        
        objc_sync_enter(ProgramStepUnplugCache.object)
        
        if self.isDoneInternal(stepId) {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return false
        }
        
        if self.isExpiredInternal(stepId) {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return false
        }
        
        self.setFailed(stepId, failed: false, dueDate: dueDate)
        self.setStarted(stepId, started: true, dueDate: dueDate)
        self.setTracking(stepId, tracking: true, dueDate: dueDate)
        self.setFirstStarted(stepId, dueDate: dueDate)
        self.setLastUpdated(stepId, dueDate: dueDate)
        objc_sync_exit(ProgramStepUnplugCache.object)
        
        return true
    }
    
    func resume(delayInSeconds: Double) {
        objc_sync_enter(ProgramStepUnplugCache.object)
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let request = NSFetchRequest(entityName: entityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "(started = %@) AND (done = %@) AND (failed = %@)", true, false, false)
            
            var results: NSArray?
            
            do {
                results = try context.executeFetchRequest(request)
            } catch {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return
            }
            
            if results == nil {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return
            }
            
            for result in results! {
                if let cache = result as? NSManagedObject {
                    if let stepId = cache.valueForKey("stepId") as? String, dueDate = cache.valueForKey("dueDate") as? NSDate {
                        if !self.isDelayedInternal(stepId, delayInSeconds: delayInSeconds) {
                            self.resumeInternal(stepId, dueDate: dueDate)
                        } else {
                            print("Not resuming because it is delayed")
                        }
                    }
                }
            }
        }
        
        objc_sync_exit(ProgramStepUnplugCache.object)
    }
    
    func update(stepId: String, dueDate: NSDate) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        
        if self.isDoneInternal(stepId) {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return false
        }
        
        if self.isExpiredInternal(stepId) {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return false
        }
        
        if self.isFailedInternal(stepId) {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return false
        }
        
        if !self.isTrackingInternal(stepId) {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return false
        }
        
        if !self.isStartedInternal(stepId) {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return false
        }
        
        self.setLastUpdated(stepId, dueDate: dueDate)
        objc_sync_exit(ProgramStepUnplugCache.object)
        
        return true
    }
    
    func stop(stepId: String, dueDate: NSDate) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        self.setStarted(stepId, started: false, dueDate: dueDate)
        self.setTracking(stepId, tracking: false, dueDate: dueDate)
        objc_sync_exit(ProgramStepUnplugCache.object)
        
        return true
    }
    
    func fail(stepId: String, dueDate: NSDate) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        self.setStarted(stepId, started: false, dueDate: dueDate)
        self.setTracking(stepId, tracking: false, dueDate: dueDate)
        self.setFailed(stepId, failed: true, dueDate: dueDate)
        objc_sync_exit(ProgramStepUnplugCache.object)
        
        return true
    }
    
    func hasExitedUnplugPrepareAlert() -> Bool {
        let count = hasExitedUnplug()
        
        if count > 0 {
            setShowUnplugAlert()
            return true
        }
        
        return false
    }
    
    func hasExitedUnplugSendLocalNotification() -> Bool {
        let count = hasExitedUnplug()
        
        if count > 0 {
            self.sendLocalNotification()
            return true
        }
        
        return false
    }
    
    func hasExitedUnplugShowAlert(controller: UIViewController) -> Bool {
        if getShowUnplugAlert(true) {
            Serv.showAlertPopup("Unplug interrupted", message: "You are interrupting your unplug. Go back to unplug!", okMessage: "Okay, got it.", controller: controller, handler: nil)
            
            return true
        }
        
        return false
    }
    
    func complete(stepId: String, dueDate: NSDate) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        self.setDone(stepId, dueDate: dueDate)
        objc_sync_exit(ProgramStepUnplugCache.object)
        
        return true
    }
    
    // MARK: Public state
    
    func isExpired(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        let result = isExpiredInternal(stepId)
        objc_sync_exit(ProgramStepUnplugCache.object)
        return result
    }
    
    func isDelayed(stepId: String, delayInSeconds: Double) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        let result = isDelayedInternal(stepId, delayInSeconds: delayInSeconds)
        objc_sync_exit(ProgramStepUnplugCache.object)
        return result
    }

    func isTracking(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        let result = isTrackingInternal(stepId)
        objc_sync_exit(ProgramStepUnplugCache.object)
        return result
    }
    
    func isDone(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        let result = isDoneInternal(stepId)
        objc_sync_exit(ProgramStepUnplugCache.object)
        return result
    }
    
    func isStarted(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        let result = isStartedInternal(stepId)
        objc_sync_exit(ProgramStepUnplugCache.object)
        return result
    }

    func isFailed(stepId: String) -> Bool {
        objc_sync_enter(ProgramStepUnplugCache.object)
        let result = isFailedInternal(stepId)
        objc_sync_exit(ProgramStepUnplugCache.object)
        return result
    }

    // MARK: Public getters
    
    func getTime(stepId: String) -> Double? {
        objc_sync_enter(ProgramStepUnplugCache.object)
        
        if let cache = getCache(stepId) {
            if let started = cache.valueForKey("started") as? Bool {
                if !started {
                    objc_sync_exit(ProgramStepUnplugCache.object)
                    return nil
                }
            }
            
            if let firstStarted = cache.valueForKey("firstStarted") as? NSDate {
                let interval = NSDate().timeIntervalSinceDate(firstStarted)
                
                if interval >= 0 {
                    objc_sync_exit(ProgramStepUnplugCache.object)
                    return interval
                }
            }
        }
        
        objc_sync_exit(ProgramStepUnplugCache.object)
        return nil
    }
    
    func getLastUpdated(stepId: String) -> NSDate? {
        objc_sync_enter(ProgramStepUnplugCache.object)
        
        if let cache = getCache(stepId) {
            if let lastUpdated = cache.valueForKey("lastUpdated") as? NSDate {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return lastUpdated
            }
        }
        
        objc_sync_exit(ProgramStepUnplugCache.object)
        return nil
    }
    
    // MARK: Private
    
    private func isExpiredInternal(stepId: String) -> Bool {
        if let cache = getCache(stepId) {
            if let dueDate = cache.valueForKey("dueDate") as? NSDate {
                let dateNow = NSDate()
                
                if dueDate.compare(dateNow) == NSComparisonResult.OrderedAscending {
                    return true
                }
                
                return false
            }
        }
        
        return false
    }
    
    private func isDelayedInternal(stepId: String, delayInSeconds: Double) -> Bool {
        if let cache = getCache(stepId) {
            if let started = cache.valueForKey("started") as? Bool {
                if !started {
                    return false
                }
            }
            
            if let lastUpdated = cache.valueForKey("lastUpdated") as? NSDate {
                if let calendar = Serv.calendar {
                    if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Second, value: -Int(delayInSeconds), toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                        
                        if lastUpdated.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                            return true
                        }
                    }
                }
            }
        }
        
        return false
    }
    
    private func isTrackingInternal(stepId: String) -> Bool {
        if let cache = getCache(stepId) {
            if let tracking = cache.valueForKey("tracking") as? Bool {
                return tracking
            }
        }
        
        return false
    }
    
    private func isDoneInternal(stepId: String) -> Bool {
        if let cache = getCache(stepId) {
            if let done = cache.valueForKey("done") as? Bool {
                return done
            }
        }
        
        return false
    }
    
    private func isStartedInternal(stepId: String) -> Bool {
        if let cache = getCache(stepId) {
            if let started = cache.valueForKey("started") as? Bool {
                return started
            }
        }
        
        return false
    }
    
    private func isFailedInternal(stepId: String) -> Bool {
        if let cache = getCache(stepId) {
            if let failed = cache.valueForKey("failed") as? Bool {
                return failed
            }
        }
        
        return false
    }
    
    private func setFirstStarted(stepId: String, dueDate: NSDate) {
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return
        }
        
        cache!.setValue(NSDate(), forKey: "firstStarted")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return
            }
        }
    }
    
    private func resetTime(stepId: String, dueDate: NSDate) {
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return
        }
        
        cache!.setValue(NSDate(), forKey: "firstStarted")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return
            }
        }
    }
    
    private func setDone(stepId: String, dueDate: NSDate) {
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return
        }
        
        cache!.setValue(true, forKey: "done")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return
            }
        }
    }
    
    private func setLastUpdated(stepId: String, dueDate: NSDate) {
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            return
        }
        
        cache!.setValue(NSDate(), forKey: "lastUpdated")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                return
            }
        }
    }
    
    private func setTracking(stepId: String, tracking: Bool, dueDate: NSDate) {
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return
        }
        
        cache!.setValue(tracking, forKey: "tracking")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                return
            }
        }
    }
    
    private func setFailed(stepId: String, failed: Bool, dueDate: NSDate) {
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return
        }
        
        cache!.setValue(failed, forKey: "failed")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                return
            }
        }
    }
    
    private func setStarted(stepId: String, started: Bool, dueDate: NSDate) {
        var cache: NSManagedObject?
        
        if let existingCache = getCache(stepId) {
            cache = existingCache
        } else {
            cache = addCache(stepId, dueDate: dueDate)
        }
        
        if cache == nil {
            objc_sync_exit(ProgramStepUnplugCache.object)
            return
        }
        
        cache!.setValue(started, forKey: "started")
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            do {
                try context.save()
            } catch {
                return
            }
        }
    }
    
    func resumeInternal(stepId: String, dueDate: NSDate) {
        if self.isDoneInternal(stepId) {
            return
        }
        
        if self.isExpiredInternal(stepId) {
            return
        }
        
        if self.isFailedInternal(stepId) {
            return
        }
        
        if !self.isStartedInternal(stepId) {
            return
        }
        
        print("Resuming internal for one tracker")
        self.setTracking(stepId, tracking: true, dueDate: dueDate)
    }
    
    private func getCache(stepId: String) -> NSManagedObject? {
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let request = NSFetchRequest(entityName: entityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "stepId = %@", stepId)
            
            var results: NSArray?
            
            do {
                results = try context.executeFetchRequest(request)
            } catch {
                return nil
            }
            
            if results!.count <= 0 {
                return nil
            }
            
            if let programStepTrackingCache = results![0] as? NSManagedObject {
                return programStepTrackingCache
            }
        }
        
        return nil
    }
    
    private func addCache(stepId: String, dueDate: NSDate) -> NSManagedObject? {
        cleanup()
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let newProgramStepCache = NSEntityDescription.insertNewObjectForEntityForName(entityName, inManagedObjectContext: context)
            newProgramStepCache.setValue(stepId, forKey: "stepId")
            newProgramStepCache.setValue(false, forKey: "tracking")
            newProgramStepCache.setValue(false, forKey: "done")
            newProgramStepCache.setValue(NSDate(), forKey: "cachedAt")
            newProgramStepCache.setValue(NSDate(), forKey: "lastUpdated")
            newProgramStepCache.setValue(dueDate, forKey: "dueDate")
            newProgramStepCache.setValue(false, forKey: "started")
            newProgramStepCache.setValue(false, forKey: "failed")
            
            do {
                try context.save()
                return newProgramStepCache
            } catch {
                return nil
            }
        }
        
        return nil
    }
    
    private func sendLocalNotification() {
        let notification = UILocalNotification()
        notification.alertTitle = "Unplug interrupted"
        notification.alertBody = "You are interrupting your unplug. Go back to unplug!"
        notification.alertAction = "Unplug"
        notification.fireDate = NSDate()
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = [:]
        notification.applicationIconBadgeNumber = 1
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
    
    private func hasExitedUnplug() -> Int {
        objc_sync_enter(ProgramStepUnplugCache.object)
        
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            let request = NSFetchRequest(entityName: entityName)
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format: "(started = %@) AND (tracking = %@)", true, true)
            
            var results: NSArray?
            
            do {
                results = try context.executeFetchRequest(request)
            } catch {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return 0
            }
            
            if results == nil {
                objc_sync_exit(ProgramStepUnplugCache.object)
                return 0
            }
            
            for result in results! {
                if let cache = result as? NSManagedObject {
                    if let stepId = cache.valueForKey("stepId") as? String, dueDate = cache.valueForKey("dueDate") as? NSDate {
                        self.setTracking(stepId, tracking: false, dueDate: dueDate)
                    }
                }
            }
            
            objc_sync_exit(ProgramStepUnplugCache.object)
            return results!.count
        }
        
        objc_sync_exit(ProgramStepUnplugCache.object)
        return 0
    }
    
    private func setShowUnplugAlert() {
        
        /*
        NSUserDefaults.standardUserDefaults().setObject(true, forKey: "shouldShowUnplugAlert")
        NSUserDefaults.standardUserDefaults().synchronize()
        */
    }
    
    private func getShowUnplugAlert(unsetIfSet: Bool) -> Bool {
        return false
        
        /*
        if let _ = NSUserDefaults.standardUserDefaults().objectForKey("shouldShowUnplugAlert") {
            if unsetIfSet {
                NSUserDefaults.standardUserDefaults().removeObjectForKey("shouldShowUnplugAlert")
                NSUserDefaults.standardUserDefaults().synchronize()
            }
            
            return true
        }
        
        return false
        */
    }
    
    private func cleanup() {
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
            let context: NSManagedObjectContext = appDelegate.managedObjectContext
            
            if let calendar = Serv.calendar {
                if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Day, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                    let request = NSFetchRequest(entityName: entityName)
                    request.returnsObjectsAsFaults = false
                    request.predicate = NSPredicate(format: "cachedAt < %@", earlyDate)
                    
                    var results: NSArray?
                    
                    do {
                        results = try context.executeFetchRequest(request)
                    } catch {
                        return
                    }
                    
                    if results == nil {
                        return
                    }
                    
                    for result in results! {
                        if let programStepTracking = result as? NSManagedObject {
                            context.deleteObject(programStepTracking)
                        }
                    }
                    
                    do {
                        try context.save()
                    } catch {
                        // Do nothing.
                    }
                }
            }
        }
    }
}
