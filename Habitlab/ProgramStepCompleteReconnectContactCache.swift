//
//  ProgramStepCompleteReconnectContactCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/8/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class ProgramStepCompleteReconnectContactCache: NSObject {
    private static var caches = NSMutableDictionary()
    private static var object = NSObject()
    
    static func erase() {
        objc_sync_enter(ProgramStepCompleteReconnectContactCache.object)
        ProgramStepCompleteReconnectContactCache.caches.removeAllObjects()
        objc_sync_exit(ProgramStepCompleteReconnectContactCache.object)
    }
    
    func setStepComplete(stepId: String, contact: NSDictionary) {
        objc_sync_enter(ProgramStepCompleteReconnectContactCache.object)
        ProgramStepCompleteReconnectContactCache.caches.setObject(contact, forKey: stepId)
        objc_sync_exit(ProgramStepCompleteReconnectContactCache.object)
    }
    
    func unsetStepComplete(stepId: String) {
        objc_sync_enter(ProgramStepCompleteReconnectContactCache.object)
        ProgramStepCompleteReconnectContactCache.caches.removeObjectForKey(stepId)
        objc_sync_exit(ProgramStepCompleteReconnectContactCache.object)
    }
    
    func getStepCompleteReconnectContact(stepId: String) -> NSDictionary? {
        objc_sync_enter(ProgramStepCompleteReconnectContactCache.object)
        if let resultObject = ProgramStepCompleteReconnectContactCache.caches.objectForKey(stepId) {
            if let result = resultObject as? NSDictionary {
                objc_sync_exit(ProgramStepCompleteReconnectContactCache.object)
                return result
            }
        }
        
        objc_sync_exit(ProgramStepCompleteReconnectContactCache.object)
        return nil
    }
}