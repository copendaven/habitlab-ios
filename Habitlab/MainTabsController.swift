//
//  MainTabsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 25/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class MainTabsController: UITabBarController, UITabBarControllerDelegate {
    static var badges: NSMutableDictionary = NSMutableDictionary()
    static var mainTabControllerObject: MainTabsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        MainTabsController.mainTabControllerObject = self
        
        if let items = self.tabBar.items {
            for item in items {
                item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
            }
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        refresh()
    }
    
    static func incrementTabItem(tag: Int) {
        if mainTabControllerObject == nil {
            return
        }
        
        objc_sync_enter(mainTabControllerObject!)
        
        if let badge = badges.objectForKey(tag) as? Int {
            let newBadge = badge + 1
            badges.setObject(newBadge, forKey: tag)
        } else {
            badges.setObject(1, forKey: tag)
        }
        
        objc_sync_exit(mainTabControllerObject!)
        
        mainTabControllerObject!.refresh()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    
        refresh();
    }
    
    override func viewWillLayoutSubviews() {
        /*
        var tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
        tabFrame.size.height = 69;
        tabFrame.origin.y = self.view.frame.size.height - 69;
        self.tabBar.frame = tabFrame;
        */
    }
    
    func askPushNotificationPermission() {
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func refresh() {
        if MainTabsController.mainTabControllerObject == nil {
            return
        }
        
        if let items = self.tabBar.items {
            objc_sync_enter(MainTabsController.mainTabControllerObject!)
            
            for item in items {
                if let value = MainTabsController.badges.objectForKey(item.tag) {
                    item.badgeValue = String(value)
                } else {
                    item.badgeValue = nil
                }
            }
            
            objc_sync_exit(MainTabsController.mainTabControllerObject!)
        }
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        if let _ = viewController as? FeedbackController {
            if let checkURL = NSURL(string: "mailto://info@habitlab.me") {
                UIApplication.sharedApplication().openURL(checkURL)
            }
            
            return false
        }
        
        return true
    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if MainTabsController.mainTabControllerObject == nil {
            return
        }
        
        objc_sync_enter(MainTabsController.mainTabControllerObject!)
        
        if let _ = MainTabsController.badges.objectForKey(item.tag) {
            MainTabsController.badges.removeObjectForKey(item.tag)
        }
        
        objc_sync_exit(MainTabsController.mainTabControllerObject!)
        refresh()
    }
}
