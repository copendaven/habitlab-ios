//
//  UnplugCheckUIViewControllerExtension.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/11/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

extension UIViewController {
    func hasExitedUnplugCheck() {
        ProgramStepUnplugCache().hasExitedUnplugShowAlert(self)
    }
}