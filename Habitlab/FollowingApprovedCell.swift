//
//  FollowingApprovedCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class FollowingApprovedCell: FollowingCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var deleteUnfollowed = false
    
    @IBAction override func clickProfile(sender: AnyObject) {
        super.clickProfile(sender)
    }
    
    @IBAction func clickUnfollowButton(sender: AnyObject) {
        if deleteUnfollowed {
            return
        }
        
        if following != nil && controller != nil {
            if let user = following!["user"] as? NSDictionary {
                if let id = user["id"] as? NSString {
                    let cancelRequestAlert = UIAlertController(title: "Unfollow friend", message: "By unfollowing, you will not get updates about your friend anymore.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    cancelRequestAlert.addAction(UIAlertAction(title: "Unfollow", style: .Default, handler: { (action: UIAlertAction!) in
                        self.deleteUnfollowed = true
                        Serv.showSpinner()
                        
                        HabitlabRestAPI.deleteUnfollow(id as String, handler: { (error, followed) -> () in
                            Serv.hideSpinner()
                            
                            if error != nil {
                                self.deleteUnfollowed = false
                                Serv.showErrorPopupFromError(error, controller: self.controller, handler: nil)
                                return
                            }
                            
                            if let followingController = self.controller! as? FollowingController {
                                followingController.refresh()
                            }
                        })
                    }))
                
                    cancelRequestAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
                    self.controller!.presentViewController(cancelRequestAlert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func populate(following: NSDictionary, controller: UIViewController) {
        super.populate(following, controller: controller, userName: userName, userImage: userImage)
        deleteUnfollowed = false
    }
}
