//
//  CategoryCache.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/4/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class CategoryCache: NSObject {
    private static var categories: NSArray = NSArray()
    private static var object: NSObject = NSObject()
    
    static func erase() {
        objc_sync_enter(CategoryCache.object)
        CategoryCache.categories = NSArray()
        objc_sync_exit(CategoryCache.object)
    }
    
    static func getDriveCategories(handler: (NSError?, NSArray?) -> ()) {
        objc_sync_enter(CategoryCache.object)
        
        if CategoryCache.categories.count > 0 {
            let returnedCategories = NSArray(array: CategoryCache.categories)
            handler(nil, returnedCategories)
            objc_sync_exit(CategoryCache.object)
            return
        }
        
        Serv.showSpinner()
        
        HabitlabRestAPI.getDriveCategories({ (error, categoriesJson) -> () in
            Serv.hideSpinner()
            
            if (error != nil) {
                handler(error, nil)
                objc_sync_exit(CategoryCache.object)
                return
            }
            
            if let categories = categoriesJson as? NSArray {
                CategoryCache.categories = categories
                let returnedCategories = NSArray(array: CategoryCache.categories)
                handler(nil, returnedCategories)
                objc_sync_exit(CategoryCache.object)
            }
        })
    }
}
