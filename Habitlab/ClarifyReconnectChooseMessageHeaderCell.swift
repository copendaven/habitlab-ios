//
//  ClarifyReconnectChooseMessageHeaderCell.swift
//  Habitlab
//
//  Created by Vlad Manea on 3/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import Contacts

class ClarifyReconnectChooseMessageHeaderCell: UITableViewCell {
    private var template: NSDictionary?
    private var controller: UIViewController?
    
    @IBOutlet weak var contactPhoto: UIImageView!
    @IBOutlet weak var contactBorder: UIView!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var initialLabel: UILabel!
    
    func populate(contact: CNContact, contactPhotoBackgroundColor: UIColor) {
        
        self.contactBorder.layer.cornerRadius = 0.5 * self.contactBorder.bounds.size.width
        self.contactPhoto.layer.cornerRadius = 0.5 * self.contactPhoto.bounds.size.width
        self.initialLabel.layer.cornerRadius = 0.5 * self.initialLabel.bounds.size.width
        
        self.initialLabel.backgroundColor = contactPhotoBackgroundColor
        
        self.contactLabel.text = "Why would you like to reconnect with \(contact.givenName)?"
        
        if let imageData = contact.imageData {
            self.contactPhoto.hidden = false
            self.initialLabel.hidden = true
            self.contactPhoto.image = UIImage(data: imageData)
        } else {
            self.contactPhoto.image = nil
            self.initialLabel.hidden = false
            self.contactPhoto.hidden = true
            
            var initialLabelText = ""
            
            if contact.givenName.characters.count > 0 {
                initialLabelText += contact.givenName[contact.givenName.startIndex.advancedBy(0)..<contact.givenName.startIndex.advancedBy(1)]
            }
            
            if contact.familyName.characters.count > 0 {
                initialLabelText += contact.familyName[contact.familyName.startIndex.advancedBy(0)..<contact.familyName.startIndex.advancedBy(1)]
            }
            
            if initialLabelText == "" {
                initialLabelText = "?"
            }
            
            self.initialLabel.text = initialLabelText.uppercaseString
        }
    }
}
