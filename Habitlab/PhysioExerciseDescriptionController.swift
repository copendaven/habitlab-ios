//
//  PhysioExerciseDescriptionController.swift
//  Habitlab
//
//  Created by Vlad Manea on 6/22/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class PhysioExerciseDescriptionController: UIViewController {
    var exercise: NSDictionary?
    var step: NSDictionary?
    var delegate: UIViewController?
    
    @IBOutlet weak var dontShowAgainSelectedView: UIView!
    @IBOutlet weak var dontShowAgainButton: UIButton!
    @IBOutlet weak var allInOneTextView: UITextView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.allInOneTextView.textContainer.lineBreakMode = NSLineBreakMode.ByWordWrapping
        
        constructPhysioExerciseDescriptionText()
        
        self.dontShowAgainSelectedView.layer.cornerRadius = 0.5 * self.dontShowAgainSelectedView.bounds.size.width
        self.dontShowAgainButton.layer.cornerRadius = 0.5 * self.dontShowAgainButton.bounds.size.width
        self.dontShowAgainButton.layer.borderWidth = 1.0
        self.dontShowAgainButton.layer.borderColor = UIColor(red: 0.21, green: 0.84, blue: 0.59, alpha: 1.0).CGColor
        
        refresh()
    }
    
    private func refresh() {
        if let realStep = self.step, realExercise = self.exercise {
            if let stepId = realStep["id"] as? NSString, exerciseId = realExercise["id"] as? NSString {
                if PhysioExerciseCache().getDontShow(stepId as String, exerciseId: exerciseId as String) {
                    self.dontShowAgainSelectedView.hidden = false
                } else {
                    self.dontShowAgainSelectedView.hidden = true
                }
            }
        }
    }
    
    @IBAction func clickedClose(sender: AnyObject) {
        self.hideSilently()
    }
    
    @IBAction func clickedDontShowAgain(sender: AnyObject) {
        if let realStep = self.step, realExercise = self.exercise {
            if let stepId = realStep["id"] as? NSString, exerciseId = realExercise["id"] as? NSString, index = realExercise["index"] as? NSNumber {
                if PhysioExerciseCache().getDontShow(stepId as String, exerciseId: exerciseId as String) {
                    PhysioExerciseCache().unsetDontShow(stepId as String, exerciseId: exerciseId as String, index: Int(index))
                } else {
                    PhysioExerciseCache().setDontShow(stepId as String, exerciseId: exerciseId as String, index: Int(index))
                    
                    self.hideSilently()
                }
            }
        }
        
        self.refresh()
    }
    
    private func constructPhysioExerciseDescriptionText() {
        var htmlString = ""
        var defaultString = ""
        
        if let realExercise = self.exercise {
            if let title = realExercise["title"] as? NSString {
                htmlString += "<h1 style=\"font-weight:300; font-size: 20px;\">\(title)</h1>"
                defaultString += "\(title)\n"
            }
            
            if let generalAdvice = realExercise["generalAdvice"] as? NSString {
                htmlString += "<h2><span style=\"font-weight:400; font-size: 14px;\">GENERAL ADVICE</span></h2><p style=\"font-weight:300; font-size: 13px;\">\(generalAdvice)</p>"
                defaultString += "General advice:\n\(generalAdvice)\n"
            }
            
            if let personalAdvice = realExercise["personalAdvice"] as? NSString {
                htmlString += "<h2><span style=\"font-weight:400; font-size: 14px;\">PERSONAL ADVICE</span></h2><p style=\"font-weight:300; font-size: 13px;\">\(personalAdvice)</p>"
                defaultString += "Personal advice:\n\(personalAdvice)\n"
            }
        }
        
        htmlString = "<span style=\"font-family:'Helvetica Neue'; color: white;\">\(htmlString)</span>"
        
        let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: defaultString, lineBreakMode: NSLineBreakMode.ByWordWrapping, lineSpacing: CGFloat(5.0))
        allInOneTextView.attributedText = attributedString
    }
    
    func hideSilently() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func hideWithCallback() {
        if let _ = self.delegate as? PhysioModuleController {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
}
