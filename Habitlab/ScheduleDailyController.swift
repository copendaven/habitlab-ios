//
//  ScheduleDailyController.swift
//  Habitlab
//
//  Created by Vlad Manea on 4/16/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Contacts
import Analytics
import MGSwipeTableCell

class ScheduleDailyController: GAITrackedViewController {
    var drive: NSDictionary?
    var contacts: [CNContact] = []
    var timesOfDayDistribution: [Int] = [0, 0, 0]
    var contactMessages = NSMutableDictionary()
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var morningAbove: UIView!
    @IBOutlet weak var morningBelow: UIView!
    @IBOutlet weak var morningBelowButton: UIButton!
    @IBOutlet weak var morningAboveButton: UIButton!
    
    @IBOutlet weak var dayAbove: UIView!
    @IBOutlet weak var dayBelow: UIView!
    @IBOutlet weak var dayBelowButton: UIButton!
    @IBOutlet weak var dayAboveButton: UIButton!
    
    @IBOutlet weak var eveningAbove: UIView!
    @IBOutlet weak var eveningBelow: UIView!
    @IBOutlet weak var eveningBelowButton: UIButton!
    @IBOutlet weak var eveningAboveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.screenName = String(ScheduleDailyController)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hasExitedUnplugCheck()
        
        if let realDrive = self.drive {
            if let title = realDrive["title"] as? NSString, level = realDrive["level"] as? NSNumber {
                self.navigationItem.title = "\(title) - LEVEL \(level)".uppercaseString
            }
        }
        
        self.refresh()
        self.segment()
    }
    
    @IBAction func clickRevealMorning(sender: AnyObject) {
        self.dayAbove.hidden = false
        self.eveningAbove.hidden = false
        self.morningBelow.hidden = false
        
        UIView.animateWithDuration(0.35, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.morningAbove.alpha = 0.0
            self.dayAbove.alpha = 1.0
            self.eveningAbove.alpha = 1.0
            self.morningBelow.alpha = 1.0
            self.dayBelow.alpha = 0.0
            self.eveningBelow.alpha = 0.0
            
            }, completion: {(success) -> Void in
                self.morningAbove.hidden = true
                self.dayBelow.hidden = true
                self.eveningBelow.hidden = true
        })
    }
    
    @IBAction func clickRevealDay(sender: AnyObject) {
        self.morningAbove.hidden = false
        self.eveningAbove.hidden = false
        self.dayBelow.hidden = false
        
        UIView.animateWithDuration(0.35, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.morningAbove.alpha = 1.0
            self.dayAbove.alpha = 0.0
            self.eveningAbove.alpha = 1.0
            self.dayBelow.alpha = 1.0
            self.morningBelow.alpha = 0.0
            self.eveningBelow.alpha = 0.0
            
            }, completion: {(success) -> Void in
                self.dayAbove.hidden = true
                self.morningBelow.hidden = true
                self.eveningBelow.hidden = true
        })
    }
    
    @IBAction func clickRevealEvening(sender: AnyObject) {
        self.morningAbove.hidden = false
        self.dayAbove.hidden = false
        self.eveningBelow.hidden = false
        
        UIView.animateWithDuration(0.35, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.morningAbove.alpha = 1.0
            self.dayAbove.alpha = 1.0
            self.eveningAbove.alpha = 0.0
            self.eveningBelow.alpha = 1.0
            self.morningBelow.alpha = 0.0
            self.dayBelow.alpha = 0.0
            
            }, completion: {(success) -> Void in
                self.eveningAbove.hidden = true
                self.morningBelow.hidden = true
                self.dayBelow.hidden = true
        })
    }
    
    @IBAction func clickHideMorning(sender: AnyObject) {
        self.morningAbove.hidden = false
        
        UIView.animateWithDuration(0.35, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.morningAbove.alpha = 1.0
            self.morningBelow.alpha = 0.0
            
            }, completion: {(success) -> Void in
                self.morningBelow.hidden = true
        })
    }
    
    @IBAction func clickHideDay(sender: AnyObject) {
        self.dayAbove.hidden = false
        
        UIView.animateWithDuration(0.35, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.dayAbove.alpha = 1.0
            self.dayBelow.alpha = 0.0
            
            }, completion: {(success) -> Void in
                self.dayBelow.hidden = true
        })
    }
    
    @IBAction func clickHideEvening(sender: AnyObject) {
        self.eveningAbove.hidden = false
        
        UIView.animateWithDuration(0.35, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.eveningAbove.alpha = 1.0
            self.eveningBelow.alpha = 0.0
            
            }, completion: {(success) -> Void in
                self.eveningBelow.hidden = true
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func segment() {
        var dict: Dictionary<String, AnyObject> = Dictionary()
        
        if let realDrive = self.drive {
            if let categ = realDrive["driveCategory"] as? NSString {
                dict["category"] = categ as String
            }
            
            if let subcateg = realDrive["driveSubcategory"] as? NSString {
                dict["subcategory"] = subcateg as String
            }
            
            if let level = realDrive["level"] as? NSNumber {
                dict["level"] = String(level)
            }
        }
        
        if dict.count > 0 {
            SEGAnalytics.sharedAnalytics().screen(String(ScheduleDailyController), properties: dict)
        } else {
            SEGAnalytics.sharedAnalytics().screen(String(ScheduleDailyController))
        }
    }
    
    func refresh() {
        if drive == nil {
            return
        }
        
        if let countStepsPerDay = drive!["countStepsPerDay"] as? NSNumber {
            self.questionLabel.text = "What times of day would you like to do your \(countStepsPerDay) loops?"
        }
        
        var morningAboveText = "+"
        var dayAboveText = "+"
        var eveningAboveText = "+"
        
        var morningBelowText = "0"
        var dayBelowText = "0"
        var eveningBelowText = "0"
        
        print(self.timesOfDayDistribution)
        
        if self.timesOfDayDistribution.count >= 3 {
            if self.timesOfDayDistribution[0] > 0 {
                morningAboveText = String(timesOfDayDistribution[0])
                morningBelowText = String(timesOfDayDistribution[0])
            }
            
            if self.timesOfDayDistribution[1] > 0 {
                dayAboveText = String(timesOfDayDistribution[1])
                dayBelowText = String(timesOfDayDistribution[1])
            }
            
            if self.timesOfDayDistribution[2] > 0 {
                eveningAboveText = String(timesOfDayDistribution[2])
                eveningBelowText = String(timesOfDayDistribution[2])
            }
        }
        
        if morningAboveText.compare("+") == NSComparisonResult.OrderedSame {
            self.morningAboveButton.backgroundColor = UIColor.clearColor()
        } else {
            self.morningAboveButton.backgroundColor = UIColor(red: 181 / 255, green: 221 / 255, blue: 215 / 255, alpha: 1.0)
        }
        
        if dayAboveText.compare("+") == NSComparisonResult.OrderedSame {
            self.dayAboveButton.backgroundColor = UIColor.clearColor()
        } else {
            self.dayAboveButton.backgroundColor = UIColor(red: 44 / 255, green: 189 / 255, blue: 170 / 255, alpha: 1.0)
        }
        
        if eveningAboveText.compare("+") == NSComparisonResult.OrderedSame {
            self.eveningAboveButton.backgroundColor = UIColor.clearColor()
        } else {
            self.eveningAboveButton.backgroundColor = UIColor(red: 50 / 255, green: 52 / 255, blue: 62 / 255, alpha: 1.0)
        }
        
        self.morningAboveButton.setTitle(morningAboveText, forState: UIControlState.Normal)
        self.morningBelowButton.setTitle(morningBelowText, forState: UIControlState.Normal)
        
        self.dayAboveButton.setTitle(dayAboveText, forState: UIControlState.Normal)
        self.dayBelowButton.setTitle(dayBelowText, forState: UIControlState.Normal)
        
        self.eveningAboveButton.setTitle(eveningAboveText, forState: UIControlState.Normal)
        self.eveningBelowButton.setTitle(eveningBelowText, forState: UIControlState.Normal)
        
        var doneButtonEnabled = false
        var doneButtonTitle = "TAP + IN EACH TIME OF DAY TO CHOOSE"
        
        if let realDrive = self.drive {
            if let countStepsPerDay = realDrive["countStepsPerDay"] as? NSNumber {
                if self.timesOfDayDistribution[0] + self.timesOfDayDistribution[1] + self.timesOfDayDistribution[2] >= Int(countStepsPerDay) {
                    doneButtonEnabled = true
                    doneButtonTitle = "CONTINUE"
                }
            }
        }
        
        self.doneButton.enabled = doneButtonEnabled
        self.doneButton.setTitle(doneButtonTitle, forState: UIControlState.Normal)
    }
    
    // MARK: UIViewController Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showScheduleDateFromScheduleDaily") {
            if let viewController = segue.destinationViewController as? ScheduleDateController {
                viewController.drive = self.drive
                viewController.contacts = self.contacts
                viewController.contactMessages = self.contactMessages
                viewController.timesOfDayDistribution = self.timesOfDayDistribution
            }
        }
    }
    
    // MARK:  UITextFieldDelegate Methods
    
    @IBAction func clickDone(sender: AnyObject) {
        self.performSegueWithIdentifier("showScheduleDateFromScheduleDaily", sender: self)
    }
    
    @IBAction func clickMorningMinus(sender: AnyObject) {
        if self.timesOfDayDistribution[0] > 0 {
            self.timesOfDayDistribution[0] -= 1
        }
        
        self.refresh()
    }
    
    @IBAction func clickMorningPlus(sender: AnyObject) {
        if let realDrive = self.drive {
            if let countStepsPerDay = realDrive["countStepsPerDay"] as? NSNumber {
                if self.timesOfDayDistribution[0] + self.timesOfDayDistribution[1] + self.timesOfDayDistribution[2] < Int(countStepsPerDay) {
                    self.timesOfDayDistribution[0] += 1
                }
            }
        }
        
        self.refresh()
    }
    
    @IBAction func clickDayMinus(sender: AnyObject) {
        if self.timesOfDayDistribution[1] > 0 {
            self.timesOfDayDistribution[1] -= 1
        }
        
        self.refresh()
    }
    
    // Morning: #B5DDD7
    // Day: #2CBDAA
    // Evening: #32343E
    
    @IBAction func clickDayPlus(sender: AnyObject) {
        if let realDrive = self.drive {
            if let countStepsPerDay = realDrive["countStepsPerDay"] as? NSNumber {
                if self.timesOfDayDistribution[0] + self.timesOfDayDistribution[1] + self.timesOfDayDistribution[2] < Int(countStepsPerDay) {
                    self.timesOfDayDistribution[1] += 1
                }
            }
        }
        
        self.refresh()
    }
    
    @IBAction func clickEveningMinus(sender: AnyObject) {
        if self.timesOfDayDistribution[2] > 0 {
            self.timesOfDayDistribution[2] -= 1
        }
        
        self.refresh()
    }
    
    @IBAction func clickEveningPlus(sender: AnyObject) {
        if let realDrive = self.drive {
            if let countStepsPerDay = realDrive["countStepsPerDay"] as? NSNumber {
                if self.timesOfDayDistribution[0] + self.timesOfDayDistribution[1] + self.timesOfDayDistribution[2] < Int(countStepsPerDay) {
                    self.timesOfDayDistribution[2] += 1
                }
            }
        }
        
        self.refresh()
    }
}
