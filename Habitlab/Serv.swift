//
//  Serv.swift
//  Habitlab
//
//  Created by Vlad Manea on 13/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

enum Environment : String {
    case Release = "release", Staging = "staging", Development = "development"
}

class Serv: NSObject {
    static var serv = Serv()
    static var guids = NSMutableDictionary()
    static var indicators = NSMutableDictionary()
    static var calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
    static var weekDayNames = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    
    static func loadImage(location: String, handler: (NSError?, NSData?) -> ()) {
        let photoUrl = NSURL(string: location)
        
        if let photo = PhotoCache().getCache(location) {
            print("Serving image from cache: \(location)")
            handler(nil, photo)
            return
        }
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(photoUrl!) { (data, response, error) in
            if error != nil {
                handler(error, nil)
                return
            }
            
            if data != nil {
                PhotoCache().setCache(location, image: data!)
                handler(nil, data!)
                return
            }
            
            let error = NSError(domain: "ImageLoadError", code: 404, userInfo: nil)
            handler(error, nil)
        }
        
        task.resume()
    }
    
    static func resizeImageForFeedCapture(image: UIImage) -> UIImage {
        let newWidth = CGFloat(1080)
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    static func resizeImageForFeedAvatar(image: UIImage) -> UIImage {
        let newWidth = CGFloat(40)
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    static func resetCaches() {
        PhotoCache.erase()
        CategoryCache.erase()
        SubcategoryCache.erase()
    }
    
    static func getEndDate(step: NSDictionary) -> NSDate? {
        var endDate: NSDate? = nil
        
        if let end = step["end"] as? NSString {
            endDate = Serv.getDateFromServerStringNonUTC(end as String)
        }
        
        if let end = step["endTimeUTC"] as? NSNumber {
            endDate = Serv.getDateFromServerStringUTC(end)
        }
        
        return endDate
    }
    
    static func getHardEndDate(step: NSDictionary) -> NSDate? {
        var hardEndDate: NSDate? = nil
        
        if let end = step["end"] as? NSString {
            hardEndDate = Serv.getDateFromServerStringNonUTC(end as String)
        }
        
        if let end = step["endTimeUTC"] as? NSNumber {
            hardEndDate = Serv.getDateFromServerStringUTC(end)
        }
        
        if let end = step["hardEnd"] as? NSString {
            hardEndDate = Serv.getDateFromServerStringNonUTC(end as String)
        }
        
        if let end = step["hardEndTimeUTC"] as? NSNumber {
            hardEndDate = Serv.getDateFromServerStringUTC(end)
        }
        
        return hardEndDate
    }
    
    static func getStartDate(step: NSDictionary) -> NSDate? {
        var startDate: NSDate? = nil
        
        if let start = step["start"] as? NSString {
            startDate = Serv.getDateFromServerStringNonUTC(start as String)
        }
        
        if let start = step["startTimeUTC"] as? NSNumber {
            startDate = Serv.getDateFromServerStringUTC(start)
        }
        
        return startDate
    }
    
    static func getLikesText(likings: NSArray) -> String {
        var likeText = ""
        var youLikeIt = false
        var anotherUserName: String? = nil
        
        if let user = UserCache().getUser() {
            for likingObject in likings {
                if let liking = likingObject as? NSDictionary {
                    if let likingUser = liking["owner"] as? NSDictionary {
                        if let likingUserId = likingUser["id"] as? NSString {
                            if let userId = user["id"] as? NSString {
                                
                                if likingUserId.compare(userId as String) == NSComparisonResult.OrderedSame {
                                    youLikeIt = true
                                } else if anotherUserName == nil {
                                    if let likingUserFirstName = likingUser["firstName"] as? NSString, likingUserLastName = likingUser["lastName"] as? NSString {
                                        anotherUserName = "\(likingUserFirstName) \(likingUserLastName)"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if youLikeIt {
            if likings.count <= 1 {
                likeText = "You like this post."
            } else if likings.count <= 2 {
                if anotherUserName != nil {
                    likeText = "You and \(anotherUserName!) like this post."
                } else {
                    likeText = "You and one other person like this post."
                }
            } else if likings.count <= 3 {
                if anotherUserName != nil {
                    likeText = "You, \(anotherUserName!), and one other person like this post."
                } else {
                    likeText = "You and two others like this post."
                }
            } else {
                if anotherUserName != nil {
                    likeText = "You, \(anotherUserName!) and \(likings.count - 2) other people like this post."
                } else {
                    likeText = "You and \(likings.count - 1) other people like this post."
                }
            }
        } else {
            if likings.count <= 1 {
                if anotherUserName != nil {
                    likeText = "\(anotherUserName!) likes this post."
                } else {
                    likeText = "One person likes this post."
                }
            } else if likings.count <= 2 {
                if anotherUserName != nil {
                    likeText = "\(anotherUserName!) and one other person like this post."
                } else {
                    likeText = "Two people like this post."
                }
            } else {
                if anotherUserName != nil {
                    likeText = "\(anotherUserName!) and \(likings.count - 1) other people like this post."
                } else {
                    likeText = "\(likings.count) people likes this post."
                }
            }
        }
        
        return likeText
    }
    
    static func getTimeLabelString(interval: NSTimeInterval, enforceDays: Bool, enforceHours: Bool) -> String {
        let time = Int(interval)
        let seconds = max(Int(time % 60), 0)
        let minutes = max(Int(time / 60 % 60), 0)
        let hours = max(Int(time / 3600), 0) % 24
        let days = Int(max(Int(time / 3600), 0) / 24)
        
        var secondsString = "\(seconds)"
        if seconds < 10 {
            secondsString = "0\(seconds)"
        }
        
        var minutesString = "\(minutes)"
        if minutes < 10 {
            minutesString = "0\(minutes)"
        }
        
        var hoursString = "\(hours)"
        if hours < 10 {
            hoursString = "0\(hours)"
        }
        
        var daysString = "\(days)"
        if days < 10 {
            daysString = "0\(days)"
        }
        
        if enforceDays || days > 0 {
            // Include the days too...
            return "\(daysString):\(hoursString):\(minutesString):\(secondsString)"
        }
        
        if enforceHours || hours > 0 {
            // Include the hours too...
            return "\(hoursString):\(minutesString):\(secondsString)"
        }
        
        return "\(minutesString):\(secondsString)"
    }
    
    static func getTimeLabelStringByLargestCalendarUnit(interval: NSTimeInterval) -> String {
        let time = Int(interval)
        let seconds = max(Int(time % 60), 0)
        let minutes = max(Int(time / 60 % 60), 0)
        let hours = max(Int(time / 3600), 0) % 24
        let days = Int(max(Int(time / 3600), 0) / 24)
        
        var secondsString = "\(seconds)"
        if seconds < 10 {
            secondsString = "0\(seconds)"
        }
        
        var minutesString = "\(minutes)"
        if minutes < 10 {
            minutesString = "0\(minutes)"
        }
        
        var hoursString = "\(hours)"
        if hours < 10 {
            hoursString = "0\(hours)"
        }
        
        var daysString = "\(days)"
        if days < 10 {
            daysString = "0\(days)"
        }
        
        if days > 0 {
            return "\(daysString):\(hoursString)"
        }
        
        if hours > 0 {
            return "\(hoursString):\(minutesString)"
        }
        
        if minutes > 0 {
            return "\(minutesString):\(secondsString)"
        }
        
        return "00:\(secondsString)"
    }
    
    static func getLargestCalendarUnitDownToMinute(interval: NSTimeInterval) -> NSCalendarUnit {
        let time = Int(interval)
        let hours = max(Int(time / 3600), 0) % 24
        let days = Int(max(Int(time / 3600), 0) / 24)
        
        if days > 0 {
            return NSCalendarUnit.Day
        }
        
        if hours > 0 {
            return NSCalendarUnit.Hour
        }
        
        return NSCalendarUnit.Minute
    }
    
    static func getLargestCalendarUnitValueDownToMinute(interval: NSTimeInterval) -> Double {
        let time = Int(interval)
        let minutes = max(Int(time / 60 % 60), 0)
        let hours = max(Int(time / 3600), 0) % 24
        let days = Int(max(Int(time / 3600), 0) / 24)
        
        if days > 0 {
            return Double(days) + Double(hours / 24)
        }
        
        if hours > 0 {
            return Double(hours) + Double(minutes / 60)
        }
        
        return Double(minutes)
    }
    
    static func showSpinner() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    static func hideSpinner() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    static func getAttributedHtmlText(htmlString: String, defaultString: String, lineBreakMode: NSLineBreakMode, lineSpacing: CGFloat?) -> NSMutableAttributedString {
        let encodedData = htmlString.dataUsingEncoding(NSUTF8StringEncoding)!
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = lineBreakMode
        
        if let realLineSpacing = lineSpacing {
            paragraphStyle.lineSpacing = realLineSpacing
        }
        
        let attributedOptions = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding]
        
        do {
            let attributedString = try NSMutableAttributedString(data: encodedData, options: attributedOptions as! [String: AnyObject], documentAttributes: nil)
            attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
            
            return attributedString
        } catch {
            return NSMutableAttributedString(string: "")
        }
    }

    static func hasCapture(mobileNotification: NSDictionary) -> Bool {
        if let programStepObjectCaptured = mobileNotification["programStepObjectCaptured"] as? NSNumber {
            if programStepObjectCaptured == 1 {
                return true
            }
        }
        
        if let stepId = mobileNotification["programStepObjectId"] as? NSString {
            if let _ = PhotoCache().getCache("programstep\(stepId as String)") {
                return true
            }
        }
        
        return false
    }
    
    static func hasComments(mobileNotification: NSMutableDictionary) -> Bool {
        if let comments = mobileNotification["comments"] as? NSArray {
            if comments.count > 0 {
                return true
            }
        }
        
        return false
    }
    
    static func countComments(mobileNotification: NSDictionary) -> Int {
        if let comments = mobileNotification["comments"] as? NSArray {
            return comments.count
        }
        
        return 0
    }
    
    static func addCacheComments(notification: NSMutableDictionary) {
        let newComments: NSMutableArray = NSMutableArray()
        
        let initialComments = NSMutableArray()
        let normalComments = NSMutableArray()
        
        // Add the existing comments on the top of the new comments.
        if let comments = notification["comments"] as? NSArray {
            newComments.addObjectsFromArray(comments as [AnyObject])
        }
        
        // Find the comments with the cache
        var commentCacheIds = Set<String>()
        
        if let comments = notification["comments"] as? NSArray {
            for commentJson in comments {
                if let comment = commentJson as? NSDictionary {
                    if let commentCacheId = comment["cacheId"] as? NSString {
                        commentCacheIds.insert(commentCacheId as String)
                    }
                }
            }
        }
        
        var addedCacheCommentCacheIds = Set<String>()
        
        // Get all comments by notification ID and add them at the bottom
        if let id = notification["id"] as? NSString {
            let cacheComments = MobileNotificationCommentCache().getByMobileNotificationId(id as String)
            
            for cacheCommentJson in cacheComments {
                if let cacheComment = cacheCommentJson as? NSDictionary {
                    if let cacheCommentCacheId = cacheComment["cacheId"] as? String {
                        if !commentCacheIds.contains(cacheCommentCacheId) && !addedCacheCommentCacheIds.contains(cacheCommentCacheId) {
                            newComments.addObject(cacheComment)
                            addedCacheCommentCacheIds.insert(cacheCommentCacheId)
                        }
                    }
                }
            }
        }
        
        // Get all comments by notification cache ID and add them at the bottom
        if let cacheId = notification["cacheId"] as? NSString {
            let cacheComments = MobileNotificationCommentCache().getByMobileNotificationCacheId(cacheId as String)
            
            for cacheCommentJson in cacheComments {
                if let cacheComment = cacheCommentJson as? NSDictionary {
                    if let cacheCommentCacheId = cacheComment["cacheId"] as? String {
                        if !commentCacheIds.contains(cacheCommentCacheId) && !addedCacheCommentCacheIds.contains(cacheCommentCacheId) {
                            newComments.addObject(cacheComment)
                            addedCacheCommentCacheIds.insert(cacheCommentCacheId)
                        }
                    }
                }
            }
        }
        
        // Split the comments so that the initial ones (inspire) appear always on top.
        for commentJson in newComments {
            if let comment = commentJson as? NSDictionary {
                var isInitial = false
                
                if let initial = comment["initial"] as? NSNumber {
                    if initial == 1 {
                        isInitial = true
                    }
                }
                
                if isInitial {
                    if let message = comment["message"] as? NSString {
                        if message.compare("") != NSComparisonResult.OrderedSame {
                            initialComments.addObject(comment)
                        }
                    }
                } else {
                    normalComments.addObject(comment)
                }
            }
        }
        
        let finalComments = NSMutableArray()
        
        finalComments.addObjectsFromArray(initialComments as [AnyObject])
        finalComments.addObjectsFromArray(normalComments as [AnyObject])
        
        notification.setObject(finalComments, forKey: "comments")
    }
    
    static func addCacheLikes(notification: NSMutableDictionary) {
        var likeCacheIds = Set<String>()
        let newLikes: NSMutableArray = NSMutableArray()
        
        if let likes = notification["likes"] as? NSArray {
            for likeJson in likes {
                if let like = likeJson as? NSDictionary {
                    if let likeCacheId = like["cacheId"] as? NSString {
                        likeCacheIds.insert(likeCacheId as String)
                    }
                }
            }
        }
        
        var addedCache = false
        
        // Get all likes by notification ID
        if let id = notification["id"] as? NSString {
            if let cacheLike = MobileNotificationLikeCache().getCacheByMobileNotificationId(id as String) {
                if let cacheLikeCacheId = cacheLike["cacheId"] as? String {
                    if !likeCacheIds.contains(cacheLikeCacheId) && !addedCache {
                        newLikes.addObject(cacheLike)
                        addedCache = true
                    }
                }
            }
        }
        
        // Get all likes by notification cache ID
        if let cacheId = notification["cacheId"] as? NSString {
            if let cacheLike = MobileNotificationLikeCache().getCacheByMobileNotificationCacheId(cacheId as String) {
                if let cacheLikeCacheId = cacheLike["cacheId"] as? String {
                    if !likeCacheIds.contains(cacheLikeCacheId) && !addedCache {
                        newLikes.addObject(cacheLike)
                        addedCache = true
                    }
                }
            }
        }
        
        // Add the existing likes on the bottom of the cached likes.
        if let likes = notification["likes"] as? NSArray {
            newLikes.addObjectsFromArray(likes as [AnyObject])
        }
        
        notification.setObject(newLikes, forKey: "likes")
    }

    static func renderComments(comments: NSArray) -> String {
        var htmlString = ""
        
        for i in 0 ..< comments.count {
            if let comment = comments[i] as? NSDictionary {
                htmlString = htmlString + renderComment(comment, newLine: true)
            }
        }
        
        return htmlString
    }
    
    static func renderComment(comment: NSDictionary, newLine: Bool) -> String {
        if let message = comment["message"] as? NSString {
            var htmlString = ""
            
            var isInitial = false
            
            if let initial = comment["initial"] as? NSNumber {
                if initial == 1 {
                    isInitial = true
                }
            }
            
            if isInitial && message.compare("") == NSComparisonResult.OrderedSame {
                return htmlString
            }
            
            if newLine {
                htmlString = htmlString + "<p style=\"font-family:'Helvetica Neue';margin-bottom:2px;padding-bottom:0px;padding-top:25px;\">"
            } else {
                htmlString = htmlString + "<span style=\"font-family:'Helvetica Neue'\">"
            }
            
            if let owner = comment["owner"] as? NSDictionary {
                if let firstName = owner["firstName"] as? NSString {
                    var nameString = String(firstName)
                    
                    /*
                    if isInitial {
                        nameString = "<span style=\"font-weight:500;\">" + nameString + "</span>"
                    } else {
                        nameString = "<span style=\"font-weight:600;\">" + nameString + "</span>"
                    }
                    */
                    
                    nameString = "<span style=\"font-weight:600;\">" + nameString + "</span>"
                    
                    htmlString = htmlString + nameString + " " // Allow a space after the name...
                }
            }
            
            var spanString = String(message)
            
            /*
            if isInitial {
                spanString = "<span style=\"font-weight:300;\">" + spanString + "</span>"
            } else {
                spanString = "<span style=\"font-weight:400;\">" + spanString + "</span>"
            }
            */
            
            spanString = "<span style=\"font-weight:400;\">" + spanString + "</span>"
            
            htmlString = htmlString + spanString
            
            if newLine {
                htmlString = htmlString + "</p>"
            } else {
                htmlString = htmlString + "</span>"
            }
            
            return htmlString
        }
    
        return ""
    }
    
    static func getHumanDate(step: NSDictionary, state: ProgramState, timeOfDay: ScheduledTimeOfDay?) -> String? {
        switch state {
        case ProgramState.Idle:
            if let startDate = getStartDate(step) {
                return getHumanDateForIdle(startDate, timeOfDay: timeOfDay)
            }
            
            break
        case ProgramState.Progress, ProgramState.Warning0, ProgramState.Warning1, ProgramState.Warning2:
            if let endDate = getEndDate(step) {
                return getHumanDateForProgressAndWarnings(endDate, timeOfDay: timeOfDay)
            }
            
            break
        case ProgramState.BetweenSoftHard, ProgramState.Closing:
            return getHumanDateForClosingAndBetweenSoftHardDeadlines()
        default:
            break
        }
        
        return ""
    }
    
    private static func getHumanDateForIdle(startDate: NSDate, timeOfDay: ScheduledTimeOfDay?) -> String? {
        let now = NSDate()
        
        if let calendar = Serv.calendar {
            let todayComponents = calendar.components([.Day , .Month , .Year], fromDate: now)
            
            let todayYear =  todayComponents.year
            let todayMonth = todayComponents.month
            let todayDay = todayComponents.day
            
            let thenComponents = calendar.components([.Day , .Month , .Year, .Hour, .Minute, .Weekday], fromDate: startDate)
            
            let thenYear =  thenComponents.year
            let thenMonth = thenComponents.month
            let thenDay = thenComponents.day
            let thenHour = thenComponents.hour
            let thenMinute = thenComponents.minute
            
            if todayYear == thenYear && todayMonth == thenMonth && todayDay == thenDay {
                if timeOfDay == nil {
                    return "\(thenHour < 10 ? "0\(thenHour)" : "\(thenHour)"):\(thenMinute < 10 ? "0\(thenMinute)" : "\(thenMinute)")"
                }
                
                switch timeOfDay! {
                case ScheduledTimeOfDay.Morning:
                    return "this morning"
                case ScheduledTimeOfDay.Day:
                    return "this day"
                case ScheduledTimeOfDay.Evening:
                    return "this evening"
                }
            }
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en")
            dateFormatter.dateFormat = "EEEE"
            let dayOfWeekString = dateFormatter.stringFromDate(startDate)
            
            if timeOfDay == nil {
                return "\(dayOfWeekString.capitalizedString) \(thenHour < 10 ? "0\(thenHour)" : "\(thenHour)"):\(thenMinute < 10 ? "0\(thenMinute)" : "\(thenMinute)")"
            }
            
            switch timeOfDay! {
            case ScheduledTimeOfDay.Morning:
                return "\(dayOfWeekString.capitalizedString) morning"
            case ScheduledTimeOfDay.Day:
                return "\(dayOfWeekString.capitalizedString)"
            case ScheduledTimeOfDay.Evening:
                return "\(dayOfWeekString.capitalizedString) evening"
            }
        }
        
        return nil
    }
    
    private static func getHumanDateForProgressAndWarnings(endDate: NSDate, timeOfDay: ScheduledTimeOfDay?) -> String? {
        if let calendar = Serv.calendar {
            let thenComponents = calendar.components([.Hour, .Minute], fromDate: endDate)
            
            let thenHour = thenComponents.hour
            let thenMinute = thenComponents.minute
            
            if timeOfDay == nil {
                return "before \(thenHour < 10 ? "0\(thenHour)" : "\(thenHour)"):\(thenMinute < 10 ? "0\(thenMinute)" : "\(thenMinute)")"
            }
            
            switch timeOfDay! {
            case ScheduledTimeOfDay.Morning:
                return "before 11:00"
            case ScheduledTimeOfDay.Day:
                return "before 17:00"
            case ScheduledTimeOfDay.Evening:
                return "before midnight"
            }
        }
        
        return nil
    }
    
    private static func getHumanDateForClosingAndBetweenSoftHardDeadlines() -> String? {
        return "now" // That was simple...
    }
    
    private static func getHumanDateBefore(date: NSDate, timeOfDay: String?) -> String? {
        if let calendar = Serv.calendar {
            let thenComponents = calendar.components([.Hour, .Minute], fromDate: date)
            
            let thenHour = thenComponents.hour
            let thenMinute = thenComponents.minute
            
            return "before \(thenHour < 10 ? "0\(thenHour)" : "\(thenHour)"):\(thenMinute < 10 ? "0\(thenMinute)" : "\(thenMinute)")"
        }
        
        return nil
    }
    
    static func getCircleHtmlText(activity: String, time: String?) -> String {
        var htmlString = "<span style=\"font-family:'Helvetica Neue';text-align:center;color:#ffffff;\">" + activity
        
        if time != nil {
            htmlString += "<br />" + time!
        }
        
        htmlString += "</span>"
        return htmlString
    }
    
    static func showErrorPopup(title: String, message: String, okMessage: String, controller: UIViewController, handler: (()->())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction = UIAlertAction(title: okMessage, style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            if handler != nil {
                handler!()
            }
        }
        alert.addAction(alertAction)
        controller.presentViewController(alert, animated: true) { () -> Void in }
    }
    
    static func showAlertPopup(title: String, message: String, okMessage: String, controller: UIViewController, handler: (()->())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction = UIAlertAction(title: okMessage, style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            if handler != nil {
                handler!()
            }
        }
        alert.addAction(alertAction)
        controller.presentViewController(alert, animated: true) { () -> Void in }
    }
    
    static func showErrorPopupFromError(error: NSError?, controller: UIViewController?, handler: (()->())?) {
        if error != nil && controller != nil {
            var name = "Error"
            var message = "Something went wrong. We would appreciate your feedback. Tell us what happened, and we will get right back at it."
            
            switch(error!.domain) {
            case "NSURLErrorDomain":
                name = "Network error"
                message = "Something went wrong with the connection to our servers. Try again in a few moments. If the problem persists, send us a message and we will help you in no time."
            case "HabitlabHttpServerError":
                if let json = error!.userInfo["json"] as? NSDictionary {
                    if let jsonName = json["name"] as? NSString {
                        name = jsonName as String
                    }
                    
                    if let jsonMessage = json["message"] as? NSString {
                        message = jsonMessage as String
                    }
                }
            case "HabitlabHttpClientError":
                if let jsonName = error!.userInfo["name"] as? String {
                    name = jsonName
                }
                
                if let jsonMessage = error!.userInfo["message"] as? String {
                    message = jsonMessage
                }
            case "HabitlabAuthenticationError":
                if let jsonName = error!.userInfo["name"] as? String {
                    name = jsonName
                }
                
                if let jsonMessage = error!.userInfo["message"] as? String {
                    message = jsonMessage
                }
            default: break
                // Do nothing.
            }
            
            showErrorPopup(name, message: message, okMessage: "Okay, got it.", controller: controller!, handler: handler)
        }
    }
    
    static func isNotificationAboutOwner(notification: NSDictionary) -> Bool {
        if let isNotificationAbout = notification["isNotificationAboutOwner"] as? NSNumber {
            if isNotificationAbout == 1 {
                return true
            }
        }
        
        if let notificationOwnerId = notification["ownerId"] as? NSString, notificationUserSubjectId = notification["userSubjectId"] as? NSString {
            return (notificationOwnerId as String).compare(notificationUserSubjectId as String) == NSComparisonResult.OrderedSame
        }
        
        return false
    }
    
    static func isCommenyByOwner(notification: NSDictionary, comment: NSDictionary) -> Bool {
        if let notificationOwnerId = notification["ownerId"] as? NSString, commentOwner = comment["owner"] as? NSDictionary {
            if let commenterId = commentOwner["id"] as? NSString {
                return (notificationOwnerId as String).compare(commenterId as String) == NSComparisonResult.OrderedSame
            }
        }
        
        return false
    }
    
    static func isLikeByOwner(notification: NSDictionary, like: NSDictionary) -> Bool {
        if let notificationOwnerId = notification["ownerId"] as? NSString, likeOwner = like["owner"] as? NSDictionary {
            if let likerId = likeOwner["id"] as? NSString {
                return (notificationOwnerId as String).compare(likerId as String) == NSComparisonResult.OrderedSame
            }
        }
        
        return false
    }
    
    static func setChooseNewProgramText(category: NSDictionary, subcategory: NSDictionary, label: UILabel!) {
        var htmlString = ""
        var defaultString = ""
        
        if let title = subcategory["title"] as? NSString, levels = subcategory["levels"] as? NSNumber {
            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(title)</strong><br />Levels: <strong>\(levels)</strong></span>"
            defaultString = "\(title), \(levels) level\(levels == 1 ? "" : "s")"
            
            if let teaser = subcategory["teaser"] as? NSString {
                htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(title)</strong><br />\(teaser)</span>"
                defaultString = "\(title), \(levels) level\(levels == 1 ? "" : "s") - \(teaser)"
            }
        }
        
        let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: defaultString, lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
        label.attributedText = attributedString
        label.sizeToFit()
        label.setNeedsDisplay()
    }
    
    static func setMyHabitText(program: NSDictionary, label: UILabel!) {
        var htmlString = ""
        var defaultString = ""
        
        if let drive = program["drive"] as? NSDictionary {
            if let title = drive["title"] as? NSString, level = drive["level"] as? NSNumber {
                htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(title)</strong>, level \(level)</span>"
                defaultString = "\(title), level \(level)"
                
                if let teaser = drive["teaser"] as? NSString {
                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(title)</strong>, level \(level)<br />\(teaser)</span>"
                    defaultString = "\(title), level \(level) - \(teaser)"
                }
            }
        }
        
        let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: defaultString, lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
        label.attributedText = attributedString
        label.sizeToFit()
        label.setNeedsDisplay()
    }
    
    static func setDriveStatusText(drive: NSDictionary, label: UILabel!) {
        var htmlString = ""
        var defaultString = ""
        
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.Center
        
        if let countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber, driveCategory = drive["driveCategory"] as? NSString {
            switch (driveCategory as String) {
            case "running":
                if let countKilometersPerStep = drive["countKilometersPerStep"] as? NSNumber {
                    htmlString = "<font color=\"#ffffff\"><span style=\"font-family:'Helvetica Neue'\">Frequency: <font color=\"#3a3a45\">\(countStepsPerWeek) loop\(countStepsPerWeek == 1 ? "" : "s") per week</font><br />Duration: <font color=\"#3a3a45\">\(countWeeks) week\(countWeeks == 1 ? "": "s")</font> Distance: <font color=\"#3a3a45\">\(countKilometersPerStep) km</font></span></font>"
                    defaultString = "Frequency: \(countStepsPerWeek) loop\(countStepsPerWeek == 1 ? "" : "s") per week. Duration: \(countWeeks) week\(countWeeks == 1 ? "": "s"). Distance: \(countKilometersPerStep) km"
                }
                
                break
            case "reading":
                if let countChaptersPerStep = drive["countChaptersPerStep"] as? NSNumber {
                    htmlString = "<font color=\"#ffffff\"><span style=\"font-family:'Helvetica Neue'\">Frequency: <font color=\"#3a3a45\">\(countStepsPerWeek) loop\(countStepsPerWeek == 1 ? "" : "s") per week</font><br />Duration: <font color=\"#3a3a45\">\(countWeeks) week\(countWeeks == 1 ? "": "s")</font> Amount: <font color=\"#3a3a45\">\(countChaptersPerStep) chapter\(countChaptersPerStep == 1 ? "": "s")</font></span></font>"
                    defaultString = "Frequency: \(countStepsPerWeek) loop\(countStepsPerWeek == 1 ? "" : "s") per week. Duration: \(countWeeks) week\(countWeeks == 1 ? "": "s"). Amount: \(countChaptersPerStep) chapter\(countChaptersPerStep == 1 ? "": "s")"
                }
                
                break
            case "unplugging":
                if let countMinutesPerStep = drive["countMinutesPerStep"] as? NSNumber {
                    htmlString = "<font color=\"#ffffff\"><span style=\"font-family:'Helvetica Neue'\">Frequency: <font color=\"#3a3a45\">\(countStepsPerWeek) loop\(countStepsPerWeek == 1 ? "" : "s") per week</font><br />Duration: <font color=\"#3a3a45\">\(countWeeks) week\(countWeeks == 1 ? "": "s")</font> Time: <font color=\"#3a3a45\">\(countMinutesPerStep) min.</font></span></font>"
                    defaultString = "Frequency: \(countStepsPerWeek) loop\(countStepsPerWeek == 1 ? "" : "s") per week. Duration: \(countWeeks) week\(countWeeks == 1 ? "": "s"). Time: \(countMinutesPerStep) min."
                }
                
                break
            default:
                break
            }
        }
        
        let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: defaultString, lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
        attributedString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange(location: 0, length: attributedString.length))
        
        label.attributedText = attributedString
    }
    
    static func setLevelText(drive: NSDictionary, label: UILabel!) {
        var htmlString = ""
        var defaultString = ""
        
        if let titleStep = drive["step"] as? NSString, countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerWeek = drive["countStepsPerWeek"] as? NSNumber {
            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>To-Do</strong> \(titleStep)<br /><strong>Schedule</strong> \(countStepsPerWeek) loop\(countStepsPerWeek == 1 ? "" : "s") per week<br /><strong>Duration</strong> \(countWeeks) week\(countWeeks != 1 ? "s" : "")</span>"
            defaultString = "\(titleStep) - \(countStepsPerWeek) times for \(countWeeks) week\(countWeeks != 1 ? "s" : "")"
        } else if let titleStep = drive["step"] as? NSString, countWeeks = drive["countWeeks"] as? NSNumber, countStepsPerDay = drive["countStepsPerDay"] as? NSNumber {
            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>To-Do</strong> \(titleStep)<br /><strong>Schedule</strong> \(countStepsPerDay) loop\(countStepsPerDay == 1 ? "" : "s") each day<br /><strong>Duration</strong> \(countWeeks) week\(countWeeks != 1 ? "s" : "")</span>"
            defaultString = "\(titleStep) - \(countStepsPerDay) times each day for \(countWeeks) week\(countWeeks != 1 ? "s" : "")"
        }
        
        let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: defaultString, lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
        label.attributedText = attributedString
        label.sizeToFit()
        label.setNeedsDisplay()
    }
    
    // Run from DISPATCH_QUEUE_PRIORITY_HIGH
    static func setNotificationMessage(notification: NSDictionary, label: UILabel!) {
        if let mobileNotificationType = notification["mobileNotificationType"] as? NSString {
            var htmlString = ""
            var defaultString = ""
            
            if Serv.isNotificationAboutOwner(notification) {
                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You performed an unknown action.</span>"
                defaultString = "You performed an unknown action."
                
                switch mobileNotificationType {
                case "program-signup":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You signed up for <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You signed up for \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You signed up for <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You signed up for \(title), level \(level)."
                    }
                case "program-signup-level-1":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You signed up for <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You signed up for \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You signed up for <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You signed up for \(title), level \(level)."
                    }
                case "program-clarified":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You clarified <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You clarified \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You clarified <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You clarified \(title), level \(level)."
                    }
                case "program-scheduled":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You scheduled <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You scheduled \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You scheduled <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You scheduled \(title), level \(level)."
                    }
                case "program-complete":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You completed <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You completed \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You completed <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You completed \(title), level \(level)!"
                    }
                case "program-forfeited":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You failed <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You failed \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You failed <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You failed \(title), level \(level)."
                    }
                case "program-quitted":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You quit <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You quit \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You quit <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You quit \(title), level \(level)."
                    }
                case "program-first-step-complete":
                    if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You completed the first habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You completed the first habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                    }
                    
                    if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You completed the first habit loop in <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You completed the first habit loop in \(title), level \(level)!"
                    }
                case "program-step-complete":
                    if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You completed a habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You completed a habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                    }
                    
                    if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You completed a habit loop in <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You completed a habit loop in \(title), level \(level)!"
                    }
                case "program-step-overdue-first":
                    if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You are falling behind on <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You are falling behind on \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You are falling behind on <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You are falling behind on \(title), level \(level)."
                    }
                case "program-step-overdue-second":
                    if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You are about to miss <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You are about to miss \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You are about to miss <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You are about to miss \(title), level \(level)."
                    }
                case "program-step-fuckup":
                    if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You missed a habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You missed a habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                    }
                    
                    if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You missed a habit loop in <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You missed a habit loop in \(title), level \(level)."
                    }
                case "program-step-quitted":
                    if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You quit a habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You quit a habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                    }
                    
                    if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You quit a habit loop in <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You quit a habit loop in \(title), level \(level)!"
                    }
                case "user-follow-user":
                    if let userObjectFirstName = notification["userObjectFirstName"] as? NSString {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You follow <strong>\(userObjectFirstName)</strong>.</span>"
                        defaultString = "You follows \(userObjectFirstName)."
                    }
                case "user-unfollow-user":
                    if let userObjectFirstName = notification["userObjectFirstName"] as? NSString {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You unfollowed <strong>\(userObjectFirstName)</strong>.</span>"
                        defaultString = "You unfollowed \(userObjectFirstName)."
                    }
                case "user-like-mobile-notification":
                    if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> likes one of your posts.</span>"
                        defaultString = "\(userSubjectFirstName) likes one of your posts."
                    }
                case "user-comment-mobile-notification":
                    if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> commented on one of your posts.</span>"
                        defaultString = "\(userSubjectFirstName) commented on one of your posts."
                    }
                case "physio-progress-chart":
                    if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You progressed in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                        defaultString = "You progressed \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                    }
                    
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You progressed in <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You progressed in \(title), level \(level)!"
                    }
                case "reconnect-action":
                    if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        
                        htmlString = ""
                        defaultString = ""
                        
                        var otherFirstName = ""
                        var action: String = ""
                        
                        if let contactObjectFirstName = notification["contactObjectFirstName"] as? NSString {
                            otherFirstName = contactObjectFirstName as String
                        } else if let userObjectFirstName = notification["userObjectFirstName"] as? NSString {
                            otherFirstName = userObjectFirstName as String
                        } else {
                            otherFirstName = "your friend"
                        }
                        
                        if let reconnectAction = notification["reconnectAction"] as? String {
                            action = reconnectAction as String
                        }
                        
                        if let actionType = ReconnectActionType(rawValue: action) {
                            switch actionType {
                            case .GoOnATrip:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will go on a trip with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will go on a trip with \(otherFirstName) after having reconnected in \(title), level \(level)."
                            case .GoOut:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will go out with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will go out with \(otherFirstName) after having reconnected in \(title), level \(level)."
                            case .GoShopping:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will go shopping with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will go shopping with \(otherFirstName) after having reconnected in \(title), level \(level)."
                            case .GoToMovies:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will go to the movies with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will go to the movies with \(otherFirstName) after having reconnected in \(title), level \(level)."
                            case .HaveDinner:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will have dinner with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will have dinner with \(otherFirstName) after having reconnected in \(title), level \(level)."
                            case .InvitedNameOver:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will invite <strong>\(otherFirstName)</strong> over after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will invite \(otherFirstName) over after having reconnected in \(title), level \(level)."
                            case .MeetForCoffee:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will meet over a cup of coffee with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will meet over a cup of coffee with \(otherFirstName) after having reconnected in \(title), level \(level)."
                            case .TalkAgainSoon:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will talk again soon with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will talk again soon with \(otherFirstName) after having reconnected in \(title), level \(level)."
                            case .VisitName:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You will pay <strong>\(otherFirstName)</strong> a visit after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "You will pay \(otherFirstName) a visit after having reconnected in \(title), level \(level)."
                            }
                        } else {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\">You have reconnected with <strong>\(otherFirstName)</strong> within <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "You have reconnected with \(otherFirstName) within \(title), level \(level)."
                        }
                    }
                case "thinking-about-you":
                    if let userObjectsFirstNames = notification["userObjectsFirstNames"] as? NSArray, thinkingAboutYouState = notification["thinkingAboutYouState"] as? NSString {
                        if userObjectsFirstNames.count <= 0 {
                            switch (thinkingAboutYouState as String) {
                            case ThinkingAboutYouState.BeforeRun.rawValue:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You were about to run and were thinking of a few close friends.</span>"
                                defaultString = "You were about to run and were thinking of a few close friends."
                            case ThinkingAboutYouState.DuringRun.rawValue:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You were out running and were thinking of a few close friends.</span>"
                                defaultString = "You were out running and were thinking of a few close friends."
                            case ThinkingAboutYouState.AfterRun.rawValue:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You just completed a run and were thinking of a few close friends.</span>"
                                defaultString = "You just completed a run and were thinking of a few close friends."
                            default:
                                // Do nothing.
                                break
                            }
                        } else {
                            
                            // Construct the enumeration of friends.
                            let friendNames = computeFriendsEnumeration(userObjectsFirstNames, useYou: false)
                            
                            switch (thinkingAboutYouState as String) {
                            case ThinkingAboutYouState.BeforeRun.rawValue:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You were about to run and were thinking of \(friendNames[0]).</span>"
                                defaultString = "You were about to run and were thinking of \(friendNames[1])."
                            case ThinkingAboutYouState.DuringRun.rawValue:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You were out running and were thinking of \(friendNames[0]).</span>"
                                defaultString = "You were out running and were thinking of \(friendNames[1])."
                            case ThinkingAboutYouState.AfterRun.rawValue:
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\">You just completed a run and were thinking of \(friendNames[0]).</span>"
                                defaultString = "You just completed a run and were thinking of \(friendNames[1])."
                            default:
                                // Do nothing.
                                break
                            }
                        }
                    }
                case "unplugging-plan":
                    if let userObjectsFirstNames = notification["userObjectsFirstNames"] as? NSArray {
                        if userObjectsFirstNames.count <= 0 {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\">You are about to unplug.</span>"
                            defaultString = "You are about to unplug."
                        } else {
                            
                            // Construct the enumeration of friends.
                            let friendNames = computeFriendsEnumeration(userObjectsFirstNames, useYou: false)
                            
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\">You are about to unplug with \(friendNames[0]).</span>"
                            defaultString = "You are about to unplug with \(friendNames[1])."
                        }
                    }
                case "sharing-emotions":
                    if let emotions = notification["emotions"] as? NSArray, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                        
                        // Construct the enumeration of emotions.
                        let emotionNames = computeEmotionsEnumeration(emotions)
                        
                        htmlString = "<span style=\"font-family:'Helvetica Neue'\">You feel \(emotionNames[0]) after completing an unplug loop in <strong>\(title)</strong>, level \(level).</span>"
                        defaultString = "You feel \(emotionNames[1]) after completing an unplug loop in \(title), level \(level)."
                    }
                default: break
                    // Do nothing
                }
            } else {
                if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString {
                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> performed an unknown action.</span>"
                    defaultString = "\(userSubjectFirstName) performed an unknown action."
                    
                    switch mobileNotificationType {
                    case "program-signup":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> signed up for <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) signed up for \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> signed up for <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) signed up for \(title), level \(level)."
                        }
                    case "program-signup-level-1":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> signed up for <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) signed up for \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> signed up for <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) signed up for \(title), level \(level)."
                        }
                    case "program-clarified":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> clarified <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) clarified \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> clarified <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) clarified \(title), level \(level)."
                        }
                    case "program-scheduled":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> scheduled <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) scheduled \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> scheduled <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) scheduled \(title), level \(level)."
                        }
                    case "program-complete":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> completed <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) completed \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> completed <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) completed \(title), level \(level)!"
                        }
                    case "program-forfeited":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> failed <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) failed \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> failed <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) failed \(title), level \(level)."
                        }
                    case "program-quitted":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> quit <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) quit \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> quit <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) quit \(title), level \(level)."
                        }
                    case "program-first-step-complete":
                        if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> completed the first habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) completed the first habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                        }
                        
                        if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> completed the first habit loop in <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) completed the first habit loop in \(title), level \(level)!"
                        }
                    case "program-step-complete":
                        if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> completed a habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) completed a habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                        }
                        
                        if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> completed a habit loop in <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) completed a habit loop in \(title), level \(level)!"
                        }
                    case "program-step-overdue-first":
                        if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is falling behind on <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) is falling behind on \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is falling behind on <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) is falling behind on \(title), level \(level)."
                        }
                    case "program-step-overdue-second":
                        if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is about to miss <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) is about to miss \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is about to miss <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) is about to miss \(title), level \(level)."
                        }
                    case "program-step-fuckup":
                        if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> missed a habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) missed a habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)."
                        }
                        
                        if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> missed a habit loop in <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) missed a habit loop in \(title), level \(level)."
                        }
                    case "program-step-quitted":
                        if let _ = notification["programStepObjectId"] as? NSString, categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> quit a habit loop in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) quit a habit loop in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                        }
                        
                        if let _ = notification["programStepObjectId"] as? NSString, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> quit a habit loop in <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) quit a habit loop in \(title), level \(level)!"
                        }
                    case "user-follow-user":
                        if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, userObjectFirstName = notification["userObjectFirstName"] as? NSString {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> follows <strong>\(userObjectFirstName)</strong>.</span>"
                            defaultString = "\(userSubjectFirstName) follows \(userObjectFirstName)."
                        }
                    case "user-unfollow-user":
                        if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, userObjectFirstName = notification["userObjectFirstName"] as? NSString {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> unfollowed <strong>\(userObjectFirstName)</strong>.</span>"
                            defaultString = "\(userSubjectFirstName) unfollowed \(userObjectFirstName)."
                        }
                    case "user-like-mobile-notification":
                        if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, userObjectFirstName = notification["mobileNotificationObjectUserSubjectFirstName"] as? NSString {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> likes <strong>\(userObjectFirstName)</strong>'s post.</span>"
                            defaultString = "\(userSubjectFirstName) likes \(userObjectFirstName)'s post."
                        }
                    case "user-comment-mobile-notification":
                        if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, userObjectFirstName = notification["mobileNotificationObjectUserSubjectFirstName"] as? NSString {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> commented on <strong>\(userObjectFirstName)</strong>'s post.</span>"
                            defaultString = "\(userSubjectFirstName) commented on \(userObjectFirstName)'s post."
                        }
                        
                    case "physio-progress-chart":
                        if let categoryName = notification["programObjectDriveCategoryName"] as? NSString, subcategoryName = notification["programObjectDriveSubcategoryName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> progressed in <strong>\(categoryName.capitalizedString)</strong> - <strong>\(subcategoryName.capitalizedString)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) progressed in \(categoryName.capitalizedString) - \(subcategoryName.capitalizedString), level \(level)!"
                        }
                        
                        if let title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> progressed in <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) progressed in \(title), level \(level)!"
                        }
                    case "reconnect-action":
                        if let title = notification["programObjectDriveTitle"] as? NSString, userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            
                            htmlString = ""
                            defaultString = ""
                            
                            var otherFirstName = ""
                            var action: String = ""
                            
                            if let contactObjectFirstName = notification["contactObjectFirstName"] as? NSString {
                                otherFirstName = contactObjectFirstName as String
                            } else if let userObjectFirstName = notification["userObjectFirstName"] as? NSString {
                                otherFirstName = userObjectFirstName as String
                            } else {
                                otherFirstName = "your friend"
                            }
                            
                            if let reconnectAction = notification["reconnectAction"] as? String {
                                action = reconnectAction as String
                            }
                            
                            if let actionType = ReconnectActionType(rawValue: action) {
                                switch actionType {
                                case .GoOnATrip:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will go on a trip with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will go on a trip with \(otherFirstName) after having reconnected in \(title), level \(level)."
                                case .GoOut:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will go out with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will go out with \(otherFirstName) after having reconnected in \(title), level \(level)."
                                case .GoShopping:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will go shopping with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will go shopping with \(otherFirstName) after having reconnected in \(title), level \(level)."
                                case .GoToMovies:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will go to the movies with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will go to the movies with \(otherFirstName) after having reconnected in \(title), level \(level)."
                                case .HaveDinner:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will have dinner with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will have dinner with \(otherFirstName) after having reconnected in \(title), level \(level)."
                                case .InvitedNameOver:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will invite <strong>\(otherFirstName)</strong> over after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will invite \(otherFirstName) over after having reconnected in \(title), level \(level)."
                                case .MeetForCoffee:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will meet over a cup of coffee with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will meet over a cup of coffee with \(otherFirstName) after having reconnected in \(title), level \(level)."
                                case .TalkAgainSoon:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will talk again soon with <strong>\(otherFirstName)</strong> after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will talk again soon with \(otherFirstName) after having reconnected in \(title), level \(level)."
                                case .VisitName:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> will pay <strong>\(otherFirstName)</strong> a visit after having reconnected in <strong>\(title)</strong>, level \(level).</span>"
                                    defaultString = "\(userSubjectFirstName) will pay \(otherFirstName) a visit after having reconnected in \(title), level \(level)."
                                }
                            } else {
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> have reconnected with <strong>\(otherFirstName)</strong> within <strong>\(title)</strong>, level \(level).</span>"
                                defaultString = "\(userSubjectFirstName) have reconnected with \(otherFirstName) within \(title), level \(level)."
                            }
                        }
                    case "thinking-about-you":
                        if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, ownerId = notification["ownerId"] as? NSString, userObjectsFirstNames = notification["userObjectsFirstNames"] as? NSArray, userObjectsIds = notification["userObjectsIds"] as? NSArray, thinkingAboutYouState = notification["thinkingAboutYouState"] as? NSString {
                            if userObjectsFirstNames.count <= 0 {
                                switch (thinkingAboutYouState as String) {
                                case ThinkingAboutYouState.BeforeRun.rawValue:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is about to run and is thinking of you.</span>"
                                    defaultString = "\(userSubjectFirstName) is about to run and is thinking of you."
                                case ThinkingAboutYouState.DuringRun.rawValue:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is out running and is thinking of you.</span>"
                                    defaultString = "\(userSubjectFirstName) is out running and is thinking of you."
                                case ThinkingAboutYouState.AfterRun.rawValue:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> just completed a run and is thinking of you.</span>"
                                    defaultString = "\(userSubjectFirstName) just completed a run and is thinking of you."
                                default:
                                    // Do nothing.
                                    break
                                }
                            } else {
                                let filteredUserObjectsFirstNames = NSMutableArray()
                                var found = false
                                
                                // Do not add the current user (marked as found when not added) from the user subjects.
                                for i in 0 ..< userObjectsFirstNames.count {
                                    if let userObjectFirstName = userObjectsFirstNames[i] as? NSString, userObjectId = userObjectsIds[i] as? NSString {
                                        if (userObjectId as String).compare(ownerId as String) == NSComparisonResult.OrderedSame {
                                            found = true
                                        } else {
                                            filteredUserObjectsFirstNames.addObject(userObjectFirstName)
                                        }
                                    }
                                }
                                
                                let friendNames = computeFriendsEnumeration(filteredUserObjectsFirstNames, useYou: found)
                                
                                switch (thinkingAboutYouState as String) {
                                case ThinkingAboutYouState.BeforeRun.rawValue:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is about to run and is thinking of \(friendNames[0]).</span>"
                                    defaultString = "\(userSubjectFirstName) is about to run and is thinking of \(friendNames[1])."
                                case ThinkingAboutYouState.DuringRun.rawValue:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is out running and is thinking of \(friendNames[0]).</span>"
                                    defaultString = "\(userSubjectFirstName) is out running and is thinking of \(friendNames[1])."
                                case ThinkingAboutYouState.AfterRun.rawValue:
                                    htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> just completed a run and is thinking of \(friendNames[0]).</span>"
                                    defaultString = "\(userSubjectFirstName) just completed a run and is thinking of \(friendNames[1])."
                                default:
                                    // Do nothing.
                                    break
                                }
                            }
                        }
                    case "unplugging-plan":
                        if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, ownerId = notification["ownerId"] as? NSString, userObjectsFirstNames = notification["userObjectsFirstNames"] as? NSArray, userObjectsIds = notification["userObjectsIds"] as? NSArray {
                            if userObjectsFirstNames.count <= 0 {
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is about to unplug.</span>"
                                defaultString = "\(userSubjectFirstName) is about to unplug."
                            } else {
                                let filteredUserObjectsFirstNames = NSMutableArray()
                                var found = false
                                
                                // Do not add the current user (marked as found when not added) from the user subjects.
                                for i in 0 ..< userObjectsFirstNames.count {
                                    if let userObjectFirstName = userObjectsFirstNames[i] as? NSString, userObjectId = userObjectsIds[i] as? NSString {
                                        if (userObjectId as String).compare(ownerId as String) == NSComparisonResult.OrderedSame {
                                            found = true
                                        } else {
                                            filteredUserObjectsFirstNames.addObject(userObjectFirstName)
                                        }
                                    }
                                }
                                
                                let friendNames = computeFriendsEnumeration(filteredUserObjectsFirstNames, useYou: found)
                                
                                htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> is about to unplug with \(friendNames[0]).</span>"
                                defaultString = "\(userSubjectFirstName) is about to unplug with \(friendNames[1])."
                            }
                        }
                    case "sharing-emotions":
                        if let userSubjectFirstName = notification["userSubjectFirstName"] as? NSString, emotions = notification["emotions"] as? NSArray, title = notification["programObjectDriveTitle"] as? NSString, level = notification["programObjectDriveLevel"] as? NSNumber {
                            
                            // Construct the enumeration of emotions.
                            let emotionNames = computeEmotionsEnumeration(emotions)
                            
                            htmlString = "<span style=\"font-family:'Helvetica Neue'\"><strong>\(userSubjectFirstName)</strong> feels \(emotionNames[0]) after completing an unplug loop in <strong>\(title)</strong>, level \(level).</span>"
                            defaultString = "\(userSubjectFirstName) feels \(emotionNames[1]) after completing an unplug loop in \(title), level \(level)."
                        }
                    default: break
                        // Do nothing
                    }
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                let attributedString = Serv.getAttributedHtmlText(htmlString, defaultString: defaultString, lineBreakMode: NSLineBreakMode.ByTruncatingTail, lineSpacing: nil)
                label.attributedText = attributedString
                label.sizeToFit()
                label.setNeedsDisplay()
            })
        }
    }
    
    static func computeFriendsEnumeration(userObjectsFirstNames: NSArray, useYou: Bool) -> [String] {
        
        // Construct the enumeration of friends.
        var friendNamesHtml = ""
        var friendNamesDefa = ""
        
        let filteredNames = NSMutableArray()
        
        for i in 0 ..< userObjectsFirstNames.count {
            if let userObjectFirstName = userObjectsFirstNames[i] as? NSString {
                if (userObjectFirstName as String).compare("") != NSComparisonResult.OrderedSame {
                    filteredNames.addObject(userObjectFirstName as String)
                }
            }
        }
        
        if filteredNames.count <= 0 {
            if useYou {
                return ["you", "you"]
            }
            
            return ["", ""]
        }
        
        if filteredNames.count == 1 {
            if let userObjectFirstName = filteredNames[0] as? String {
                if useYou {
                    friendNamesHtml += "you and "
                    friendNamesDefa += "you and "
                }
                
                friendNamesHtml += "<strong>\(userObjectFirstName as String)</strong>"
                friendNamesDefa += "\(userObjectFirstName as String)"
            }
            
            return [friendNamesHtml, friendNamesDefa]
        }
        
        if useYou {
            friendNamesHtml += "you, "
            friendNamesDefa += "you, "
        }
        
        for i in 0 ..< filteredNames.count {
            if let userObjectFirstName = filteredNames[i] as? String {
                if i < filteredNames.count - 1 {
                    friendNamesHtml += "<strong>\(userObjectFirstName as String)</strong>"
                    friendNamesDefa += "\(userObjectFirstName as String)"
                    
                    if i < filteredNames.count - 2 {
                        friendNamesHtml += ", "
                        friendNamesDefa += ", "
                    }
                } else {
                    friendNamesHtml += " and <strong>\(userObjectFirstName as String)</strong>"
                    friendNamesDefa += " and \(userObjectFirstName as String)"
                }
            }
        }
    
        return [friendNamesHtml, friendNamesDefa]
    }
    
    static func computeEmotionsEnumeration(emotions: NSArray) -> [String] {
        
        if emotions.count <= 0 {
            return ["<strong>an emotion</strong>", "an emotion"]
        }
        
        if emotions.count == 1 {
            return ["<strong>\(emotions[0].lowercaseString)</strong>", "\(emotions[0].lowercaseString)"]
        }
        
        if emotions.count == 2 {
            return ["<strong>\(emotions[0].lowercaseString)</strong> and <strong>\(emotions[1].lowercaseString)</strong>", "\(emotions[0].lowercaseString) and \(emotions[1].lowercaseString)"]
        }
        
        // Construct the enumeration of emotions.
        var emotionNamesHtml = ""
        var emotionNamesDefa = ""
        
        for i in 0..<emotions.count - 2 {
            emotionNamesHtml += "<strong>\(emotions[i].lowercaseString)</strong>, "
            emotionNamesDefa += "\(emotions[i].lowercaseString)"
        }
        
        emotionNamesHtml += "<strong>\(emotions[emotions.count - 2].lowercaseString)</strong> and <strong>\(emotions[emotions.count - 1].lowercaseString)</strong>"
        emotionNamesDefa += "\(emotions[emotions.count - 2].lowercaseString) and \(emotions[emotions.count - 1].lowercaseString)"
        return [emotionNamesHtml, emotionNamesDefa]
    }
    
    static func getWeekDay(date: NSDate) -> Int? {
        // 1 sunday, 2 monday, 3 tuesday, 4 wednesday, 5 thursday, 6 friday, 7 saturday
        // Translated into:
        // 6 sunday, 0 monday, 1 tuesday, 2 wednesday, 3 thursday, 4 friday, 5 saturday
        
        if let calendar = Serv.calendar {
            let components = calendar.components(NSCalendarUnit.Weekday, fromDate: date)
            return (components.weekday + 5) % 7
        }
        
        return nil
    }
    
    static func getDatePlusHours(date: NSDate, hours: Int) -> NSDate? {
        if let calendar = Serv.calendar {
            return calendar.dateByAddingUnit(NSCalendarUnit.Hour, value: hours, toDate: date, options: NSCalendarOptions.MatchFirst)
        }
        
        return nil
    }
    
    static func getDatePlusSeconds(date: NSDate, seconds: Int) -> NSDate? {
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        return calendar.dateByAddingUnit(NSCalendarUnit.Second, value: seconds, toDate: date, options: NSCalendarOptions.MatchFirst)
    }
    
    static func getDatePlusMinutes(date: NSDate, minutes: Int) -> NSDate? {
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        return calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: minutes, toDate: date, options: NSCalendarOptions.MatchFirst)
    }
    
    static func getDatePlusDays(date: NSDate, days: Int) -> NSDate? {
        if let calendar = Serv.calendar {
            return calendar.dateByAddingUnit(NSCalendarUnit.Day, value: days, toDate: date, options: NSCalendarOptions.MatchFirst)
        }
        
        return nil
    }
    
    static func getWarning0Date(startDate: NSDate, endDate: NSDate) -> NSDate {
        let intervalStart = startDate.timeIntervalSince1970
        let intervalEnd = endDate.timeIntervalSince1970
        
        let intervalWarning0 = floor(intervalStart + 2 * (intervalEnd - intervalStart) / 10)
        return NSDate(timeIntervalSince1970: intervalWarning0)
    }
    
    static func getWarning1Date(startDate: NSDate, endDate: NSDate) -> NSDate {
        let intervalStart = startDate.timeIntervalSince1970
        let intervalEnd = endDate.timeIntervalSince1970
        
        let intervalWarning1 = floor(intervalStart + 8 * (intervalEnd - intervalStart) / 10)
        return NSDate(timeIntervalSince1970: intervalWarning1)
    }
    
    static func getWarning2Date(startDate: NSDate, endDate: NSDate) -> NSDate {
        let intervalStart = startDate.timeIntervalSince1970
        let intervalEnd = endDate.timeIntervalSince1970
        
        let intervalWarning2 = floor(intervalStart + 9 * (intervalEnd - intervalStart) / 10)
        return NSDate(timeIntervalSince1970: intervalWarning2)
    }
    
    static func getClosingDate(endDate: NSDate) -> NSDate {
        if let closingDate = getDatePlusSeconds(endDate, seconds: -30) {
            return closingDate
        }
        
        return endDate
    }
    
    static func getDateFromServerStringUTC(dateNumberInMilliseconds: NSNumber) -> NSDate? {
        let milliseconds = Double(dateNumberInMilliseconds)
        return NSDate(timeIntervalSince1970: milliseconds / 1000.0)
    }
    
    static func getDateFromServerStringNonUTC(dateString: String) -> NSDate? {
        let dateFormat = NSDateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormat.timeZone = NSTimeZone.localTimeZone()
        dateFormat.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        
        let date = dateFormat.dateFromString(dateString)
        
        if date == nil {
            return nil
        }
        
        let newDate = Serv.getDatePlusSeconds(date!, seconds: NSTimeZone.localTimeZone().secondsFromGMT)
        
        if newDate == nil {
            return nil
        }
        
        return newDate
    }
}
