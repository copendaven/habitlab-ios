//
//  InAppPurchaseService.swift
//  Physio
//
//  Created by Vlad Manea on 6/5/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import StoreKit

class InAppPurchaseService: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    static let IAPHelperPurchaseNotification = "IAPHelperPurchaseNotification"
    
    private let productIdentifiers: Set<String>
    private var purchasedProductIdentifiers = Set<String>()
    private var productsRequest: SKProductsRequest?
    private var productsRequestCompletionHandler: ((success: Bool, products: [SKProduct]?) -> ())?
    
    init(productIdentifiers: Set<String>) {
        self.productIdentifiers = productIdentifiers
        
        for productIdentifier in productIdentifiers {
            let purchased = NSUserDefaults.standardUserDefaults().boolForKey(productIdentifier)
            
            if purchased {
                print("Previously purchased: \(productIdentifier)")
            } else {
                print("Not purchased: \(productIdentifier)")
            }
        }
        
        super.init()
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
    }
    
    func requestProducts(completionHandler: (success: Bool, products: [SKProduct]?) -> ()) {
        if let realProductsRequest = productsRequest {
            realProductsRequest.cancel()
        }
        
        productsRequestCompletionHandler = completionHandler
        print("Product identifiers: \(productIdentifiers)")
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        
        if let realProductsRequestAgain = productsRequest {
            realProductsRequestAgain.delegate = self
            realProductsRequestAgain.start()
        }
    }
    
    func buyProduct(product: SKProduct) {
        print("Buying product: \(product.productIdentifier)")
        let payment = SKPayment(product: product)
        SKPaymentQueue.defaultQueue().addPayment(payment)
    }
    
    func isProductPurchased(productIdentifier: String) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
    
    class func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    static func validateReceipt(handler: ((Bool) -> ())?) {
        var foundReceipt = false
        
        if let appStoreReceiptURL = NSBundle.mainBundle().appStoreReceiptURL {
            if let receipt = NSData(contentsOfURL: appStoreReceiptURL) {
                let receiptData = receipt.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                foundReceipt = true
                
                HabitlabRestAPI.postValidateReceipt(receiptData, handler: {(error, success) -> () in
                    if error != nil {
                        if let realHandler = handler {
                            realHandler(false)
                        }
                        
                        return
                    }
                    
                    if let realHandler = handler {
                        realHandler(true)
                    }
                })
            }
        }
        
        if (!foundReceipt) {
            if let realHandler = handler {
                realHandler(true)
            }
        }
    }
    
    func restorePurchases() {
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }
    
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        print("Loaded list of products")
        let products = response.products
        
        print("invalid produ IDs: \(response.invalidProductIdentifiers)")
        print("debug description: \(response.debugDescription)")
        print("response descript: \(response.description)")
        
        if let realProductsRequestCompletionHandler = self.productsRequestCompletionHandler {
            realProductsRequestCompletionHandler(success: true, products: products)
        }
        
        productsRequest = nil
        productsRequestCompletionHandler = nil
        
        for product in products {
            print("Found product: \(product.productIdentifier) \(product.localizedTitle) \(product.price.floatValue) \(product.localizedPrice())")
        }
    }
    
    func request(request: SKRequest, didFailWithError error: NSError) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        
        if let realProductsRequestCompletionHandler = productsRequestCompletionHandler {
            realProductsRequestCompletionHandler(success: false, products: nil)
        }
        
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .Purchased:
                completeTransaction(transaction)
            case .Failed:
                failedTransaction(transaction)
            case .Restored:
                restoreTransaction(transaction)
            case .Deferred:
                break
            case .Purchasing:
                break
            }
        }
    }
    
    private func completeTransaction(transaction: SKPaymentTransaction) {
        print("Complete transaction")
        
        InAppPurchaseService.validateReceipt({(success) -> () in
            if success {
                SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                self.deliverPurchaseNotificationForIdentifier(transaction.payment.productIdentifier)
            }
        })
    }
    
    private func restoreTransaction(transaction: SKPaymentTransaction) {
        print("Restore transaction")
        
        if let realOriginalTransaction = transaction.originalTransaction {
            let productIdentifier = realOriginalTransaction.payment.productIdentifier
            print("Restore transaction: \(productIdentifier)")
            
            InAppPurchaseService.validateReceipt({(success) -> () in
                if success {
                    SKPaymentQueue.defaultQueue().finishTransaction(transaction)
                    self.deliverPurchaseNotificationForIdentifier(productIdentifier)
                }
            })
        }
    }
    
    private func failedTransaction(transaction: SKPaymentTransaction) {
        print("Failed transaction")
        
        if let transactionError = transaction.error {
            print("Transaction error: \(transactionError.localizedDescription)")
            
            if transactionError.code != SKErrorCode.PaymentCancelled.rawValue {
                print("Transaction error: \(transactionError.localizedDescription)")
            }
        }
        
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
    }
    
    private func deliverPurchaseNotificationForIdentifier(identifier: String?) {
        if let realIdentifier = identifier {
            purchasedProductIdentifiers.insert(realIdentifier)
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: realIdentifier)
            NSUserDefaults.standardUserDefaults().synchronize()
            NSNotificationCenter.defaultCenter().postNotificationName(InAppPurchaseService.IAPHelperPurchaseNotification, object: realIdentifier)
        }
    }
}

extension SKProduct {
    func localizedPrice() -> String {
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = self.priceLocale
        return formatter.stringFromNumber(self.price)!
    }
}


