//
//  InviteColleagueController.swift
//  Physio
//
//  Created by Vlad Manea on 7/4/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import MessageUI
import FBSDKShareKit
import Analytics

class InviteColleagueController: UIViewController, FBSDKAppInviteDialogDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    // MARK: UIViewController Methods
    
    @IBAction func clickInviteFriendsThroughEmail() {
        if MFMailComposeViewController.canSendMail() {
            let composeViewController = MFMailComposeViewController()
            
            composeViewController.navigationBar.barStyle = .Default
            composeViewController.navigationBar.backgroundColor = UIColor(red: 34 / 255, green: 176 / 255, blue: 155 / 255, alpha: 1.0)
            composeViewController.navigationBar.tintColor = UIColor.whiteColor()
            
            composeViewController.mailComposeDelegate = self
            composeViewController.setSubject("Try the new Physio app for physiotherapists!")
            composeViewController.setMessageBody("Hi, try out the Physio app, grow your compliance and business by helping your patients build good habits. Follow this link: https://www.habitlab.me/physio", isHTML: false)
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending email", message: "This device cannot send emails.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    @IBAction func clickInviteFriendsThroughSMS() {
        if MFMessageComposeViewController.canSendText() {
            let composeViewController = MFMessageComposeViewController()
            composeViewController.body = "Hi, try out the Physio app, grow your compliance and business by helping your patients build good habits. Follow this link: https://www.habitlab.me/physio"
            composeViewController.messageComposeDelegate = self
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending SMS", message: "This device cannot send SMS.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if error != nil {
            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
        }
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        if result == MessageComposeResultFailed {
            Serv.showErrorPopup("Error while sending SMS", message: "The SMS could not be sent.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        
        if results != nil {
            print(results)
        }
        
        // Do nothing
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!) {
        // Do nothing
        
        if error != nil {
            print(error)
        }
    }
}