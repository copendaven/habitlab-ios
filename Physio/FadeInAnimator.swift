//
//  NavigationControllerDelegate.swift
//  Physio
//
//  Created by David on 30/06/2016.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class FadeInAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(
        transitionContext: UIViewControllerContextTransitioning?
        ) -> NSTimeInterval {
        return 0.35
    }
    
    func animateTransition(
        transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView()
        let fromVC = transitionContext.viewControllerForKey(
            UITransitionContextFromViewControllerKey)
        let toVC = transitionContext.viewControllerForKey(
            UITransitionContextToViewControllerKey)
        
        containerView!.addSubview(toVC!.view)
        toVC!.view.alpha = 0.0
        
        let duration = transitionDuration(transitionContext)
        UIView.animateWithDuration(duration, animations: {
            toVC!.view.alpha = 1.0
            }, completion: { finished in
                let cancelled = transitionContext.transitionWasCancelled()
                transitionContext.completeTransition(!cancelled)
        })
    }

}
