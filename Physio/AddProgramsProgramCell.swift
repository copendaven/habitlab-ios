//
//  AddProgramsProgramCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddProgramsProgramCell: UITableViewCell {
    private var controller: UIViewController?
    private var program: NSDictionary?
    
    @IBOutlet weak var loopsLabel: UILabel!
    @IBOutlet weak var weeksLabel: UILabel!
    @IBOutlet weak var programTitle: UILabel!
    @IBOutlet weak var programImage: UIImageView!
    @IBOutlet weak var programInitialsLabel: UILabel!
    
    func populate(program: NSDictionary, controller: UIViewController) {
        self.controller = controller
        self.program = program
        self.programImage.layer.cornerRadius = self.programImage.frame.height / 2
        self.programInitialsLabel.layer.cornerRadius = self.programInitialsLabel.frame.height / 2
        
        if let title = program["title"] as? NSString {
            dispatch_async(dispatch_get_main_queue(), {
                self.programTitle.text = title as String
            })
        }
        
        populatePhoto()
        
        var countWeeksTotal = 0
        var countLoops = 0
        
        if let drives = program["drives"] as? NSArray {
            if drives.count > 0 {
                if let drive = drives[0] as? NSDictionary {
                    if let countStepsPerWeek = drive["countStepsPerWeek"] as? Int {
                        countLoops = countStepsPerWeek
                    }
                    
                    if let countStepsPerDay = drive["countStepsPerDay"] as? Int {
                        countLoops = 7 * countStepsPerDay
                    }
                }
            }
            
            for driveJson in drives {
                if let drive = driveJson as? NSDictionary {
                    if let countWeeks = drive["countWeeks"] as? Int {
                        countWeeksTotal += countWeeks
                    }
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.loopsLabel.text = "\(countLoops) loop\(countLoops == 1 ? "" : "s")"
            self.weeksLabel.text = "\(countWeeksTotal) week\(countWeeksTotal == 1 ? "" : "s")"
        })
    }
    
    @IBAction func clickedCell(sender: AnyObject) {
        if let realController = self.controller, realProgram = self.program {
            if let viewController = realController as? AddProgramsController {
                viewController.clickedExistingProgram(realProgram)
            }
        }
    }
    
    func populatePhoto() {
        if let realProgram = self.program {
            if let physioPatient = realProgram["physioPatient"] as? NSDictionary {
                var fullName = ""
                
                if let firstName = physioPatient["firstName"] as? NSString {
                    let firstNameString = firstName as String
                    
                    if firstNameString.characters.count > 0 {
                        fullName = fullName + "\(firstNameString[firstNameString.startIndex.advancedBy(0)])"
                    }
                }
                
                if let lastName = physioPatient["lastName"] as? NSString {
                    let lastNameString = lastName as String
                    
                    if lastNameString.characters.count > 0 {
                        fullName = fullName + "\(lastNameString[lastNameString.startIndex.advancedBy(0)])"
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.programInitialsLabel.alpha = 0.0
                    self.programImage.alpha = 0.0
                
                    self.programInitialsLabel.text = fullName.uppercaseString
                    self.programInitialsLabel.alpha = 1.0
                })
                
                if let email = physioPatient["email"] as? NSString {
                    HabitlabRestAPI.postPatientPhoto(email as String, handler: { (error, objectJson) in
                        if let object = objectJson as? NSDictionary {
                            if let photo = object["photo"] as? NSString {
                                Serv.loadImage(photo as String, handler: { (error, data) -> () in
                                    if error != nil {
                                        print(error)
                                    }
                                    
                                    if data != nil {
                                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                            self.programImage.image = UIImage(data: data!)
                                            self.programImage.alpha = 1.0
                                            self.programInitialsLabel.alpha = 0.0
                                        })
                                    }
                                })
                            }
                        }
                    })
                }
            }
        }
    }
}
