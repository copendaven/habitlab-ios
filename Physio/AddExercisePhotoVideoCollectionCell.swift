//
//  AddExercisePhotoVideoCollectionCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/29/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import MobileCoreServices

class AddExercisePhotoVideoCollectionCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    private var controller: UIViewController?
    private var media: NSArray = NSArray()
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    func populate(media: NSArray, controller: UIViewController) {
        self.controller = controller
        self.media = media
        
        imagesCollectionView.dataSource = self
        imagesCollectionView.delegate = self
        
        imagesCollectionView.reloadData()
    }
    
    @IBAction func clickedAddMediaButton(sender: AnyObject) {
        if let realController = self.controller {
            if let viewController = realController as? AddExerciseController {
                viewController.showAddMoreMedia()
            }
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return media.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(180, 110) // This is the size for one collection item with button and label
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let screenWidth = Int(imagesCollectionView.frame.size.width)
        var contentWidth = Int(media.count * 180) // This is the size for one collection item with button and label
        
        if screenWidth <= contentWidth {
            contentWidth = screenWidth / 180 * 180
        }
        
        let padding = CGFloat((screenWidth - contentWidth) / 2)
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if let medium = media[media.count - indexPath.item - 1] as? NSDictionary {
            if let mediaType = medium["physioMediaType"] as? String {
                if mediaType.compare(kUTTypeMovie as NSString as String) == NSComparisonResult.OrderedSame {
                    if let cell = imagesCollectionView.dequeueReusableCellWithReuseIdentifier("addExercisePhotoVideoCollectionMovieCell", forIndexPath: indexPath) as? AddExercisePhotoVideoCollectionMovieCell {
                        return cell
                    }
                    
                    return defaultCell
                }
                
                if mediaType.compare(kUTTypeImage as NSString as String) == NSComparisonResult.OrderedSame {
                    if let cell = imagesCollectionView.dequeueReusableCellWithReuseIdentifier("addExercisePhotoVideoCollectionImageCell", forIndexPath: indexPath) as? AddExercisePhotoVideoCollectionImageCell {
                        return cell
                    }
                    
                    return defaultCell
                }
            }
        }
        
        return defaultCell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let medium = media[media.count - indexPath.item - 1] as? NSDictionary, realController = self.controller {
            if let mediaType = medium["physioMediaType"] as? String {
                if mediaType.compare(kUTTypeMovie as NSString as String) == NSComparisonResult.OrderedSame {
                    if let realCell = cell as? AddExercisePhotoVideoCollectionMovieCell {
                        realCell.populate(medium, controller: realController)
                        return
                    }
                    
                    return
                }
                
                if mediaType.compare(kUTTypeImage as NSString as String) == NSComparisonResult.OrderedSame {
                    if let realCell = cell as? AddExercisePhotoVideoCollectionImageCell {
                        realCell.populate(medium, controller: realController)
                        return
                    }
                    
                    return
                }
            }
        }
    }
}
