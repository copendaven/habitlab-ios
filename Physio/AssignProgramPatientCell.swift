//
//  AssignProgramPatientCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AssignProgramPatientCell: UITableViewCell {
    private var controller: UIViewController?
    private var patient: NSMutableDictionary?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var patientImage: UIImageView!
    @IBOutlet weak var patientImageLabel: UILabel!
    
    func populate(patient: NSMutableDictionary, selected: Bool, controller: UIViewController) {
        self.controller = controller
        self.patient = patient
        self.setSelected(selected, animated: true)
        
        self.patientImage.layer.cornerRadius = self.patientImage.frame.height / 2
        self.patientImageLabel.layer.cornerRadius = self.patientImageLabel.frame.height / 2
        
        if let firstName = patient["firstName"] as? NSString, lastName = patient["lastName"] as? NSString {
            self.nameLabel.text = "\(firstName) \(lastName)"
        }
        
        self.populatePhoto()
    }
    
    @IBAction func clickedSelectPatient(sender: AnyObject) {
        if let realViewController = self.controller, let realPatient = self.patient {
            if let viewController = realViewController as? AssignProgramController {
                viewController.selectPatient(realPatient)
            }
        }
    }
    
    func populatePhoto() {
        if let physioPatient = self.patient {
            var fullName = ""
            
            if let firstName = physioPatient["firstName"] as? NSString {
                let firstNameString = firstName as String
                
                if firstNameString.characters.count > 0 {
                    fullName = fullName + "\(firstNameString[firstNameString.startIndex.advancedBy(0)])"
                }
            }
            
            if let lastName = physioPatient["lastName"] as? NSString {
                let lastNameString = lastName as String
                
                if lastNameString.characters.count > 0 {
                    fullName = fullName + "\(lastNameString[lastNameString.startIndex.advancedBy(0)])"
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.patientImageLabel.alpha = 0.0
                self.patientImage.alpha = 0.0
                
                self.patientImageLabel.text = fullName.uppercaseString
                self.patientImageLabel.alpha = 1.0
            })
            
            if let email = physioPatient["email"] as? NSString {
                HabitlabRestAPI.postPatientPhoto(email as String, handler: { (error, objectJson) in
                    if let object = objectJson as? NSDictionary {
                        if let photo = object["photo"] as? NSString {
                            Serv.loadImage(photo as String, handler: { (error, data) -> () in
                                if error != nil {
                                    print(error)
                                }
                                
                                if data != nil {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.patientImage.image = UIImage(data: data!)
                                        
                                        UIView.animateWithDuration(0.35, animations: {
                                            self.patientImage.alpha = 1.0
                                            self.patientImageLabel.alpha = 0.0
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }
    }
}
