//
//  AddProgramNumericCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/28/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddProgramNumericCell: UITableViewCell {
    private var controller: UIViewController?
    private var numericType: AddProgramController.NumericFieldType?
    private var minValue: Int?
    private var defaultValue: Int?
    private var maxValue: Int?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextField!
    
    func populate(labelText: String, labelPlaceholderText: String, minValue: Int, defaultValue: Int, maxValue: Int, numericType: AddProgramController.NumericFieldType, enabled: Bool, controller: UIViewController) {
        self.enable(enabled)
        
        self.titleLabel.text = labelText
        self.controller = controller
        self.numericType = numericType
        self.minValue = minValue
        self.defaultValue = defaultValue
        self.maxValue = maxValue
        
        contentTextField.addTarget(self, action: #selector(AddExerciseFieldCell.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        contentTextField.placeholder = labelPlaceholderText
        
        if let realController = controller as? AddProgramController {
            self.contentTextField.delegate = realController
        }
        
        let doneToolbar = self.constructInputAccessoryView()
        doneToolbar.sizeToFit()
        self.contentTextField.inputAccessoryView = doneToolbar
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddProgramNumericCell.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func enable(isEnabled: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            self.contentTextField.enabled = isEnabled
            
            if isEnabled {
                self.alpha = 1
            } else {
                self.alpha = 0.3
            }
        }
    }
    
    func doneButtonAction() {
        if let realController = self.controller as? AddProgramController {
            realController.view.endEditing(true)
        }
    }
    
    func validate(isValid: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            if isValid {
                self.contentTextField.textColor = UIColor(red: 34 / 255, green: 176 / 255, blue: 155 / 255, alpha: 1.0)
            } else {
                self.contentTextField.textColor = UIColor.redColor()
            }
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        dispatch_async(dispatch_get_main_queue()) {
            if let realText = textField.text {
                if let realController = self.controller, realNumericType = self.numericType {
                    if let viewController = realController as? AddProgramController {
                        if let value = Int(realText) {
                            viewController.handleProgramNumericUpdate(value, numericType: realNumericType)
                        } else {
                            viewController.handleProgramNumericUpdate(nil, numericType: realNumericType)
                        }
                    }
                }
            }
        }
    }
}
