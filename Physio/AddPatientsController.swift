//
//  AddPatientsController.swift
//  Physio
//
//  Created by David on 23/06/2016.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class AddPatientsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.programsController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    private var serverPrograms = []
    private var programs = []
    
    @IBOutlet weak var patientsLabel: UILabel!
    @IBOutlet weak var patientsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem?.enabled = false
        
        // Button Shadow
        patientsTableView.delegate = self
        patientsTableView.dataSource = self
        
        self.patientsTableView.estimatedRowHeight = 80
        self.patientsTableView.rowHeight = UITableViewAutomaticDimension
            
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            
        if self.serverPrograms.count <= 0 {
            self.refreshFromServer(true)
        } else {
            self.refreshFromClient(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.patientsLabel.hidden = true
        self.patientsTableView.hidden = true
        
        if self.serverPrograms.count <= 0 {
            self.refreshFromServer(true)
        } else {
            self.refreshFromClient(true)
        }
    }
    
    func didClickPatient(program: NSDictionary) {
        self.performSegueWithIdentifier("showViewProgramFromAddPatients", sender: program)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showViewProgramFromAddPatients" {
            if let viewController = segue.destinationViewController as? ViewProgramController, realProgram = sender as? NSDictionary {
                viewController.program = realProgram
            }
        }
    }
    
    func refreshFromServer(withReload: Bool) {
        dispatch_async(self.serialQueue, {
            Serv.showSpinner()
            
            HabitlabRestAPI.getPrograms({ (error, programsJson) -> () in
                Serv.hideSpinner()
                
                if (error != nil) {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                if let programsArray = programsJson as? NSArray {
                    dispatch_async(dispatch_get_main_queue(), {
                        self.serverPrograms = programsArray
                        self.refreshFromClient(withReload)
                    })
                }
            })
        })
    }
    
    func refreshFromClient(reloadData: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            self.programs = self.serverPrograms
            
            if reloadData {
                self.patientsTableView.reloadData()
                
                if self.serverPrograms.count > 0 {
                    self.patientsLabel.hidden = true
                    self.patientsTableView.hidden = false
                } else {
                    self.patientsLabel.hidden = false
                    self.patientsTableView.hidden = true
                }
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.programs.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if self.programs.count > indexPath.section {
            if let _ = self.programs[indexPath.section] as? NSDictionary {
                if let cell = tableView.dequeueReusableCellWithIdentifier("addPatientsPatientCell", forIndexPath: indexPath) as? AddPatientsPatientCell {
                    return cell
                }
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == self.programs.count {
            return
        }
        
        if let program = self.programs[indexPath.section] as? NSDictionary {
            if let realCell = cell as? AddPatientsPatientCell {
                realCell.populate(program, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
        }
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    
    /*
 
    @IBAction func didPressEdit(sender: AnyObject) {
        let optionMenu = UIAlertController(title: nil, message: "Patient Options", preferredStyle: .ActionSheet)
        
        let statsAction = UIAlertAction(title: "View Stats", style: .Default, handler: {
            (alert: UIAlertAction) -> Void in
        })
        
        let editAction = UIAlertAction(title: "Edit Details", style: .Default, handler: {
            (alert: UIAlertAction) -> Void in
        })

        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive , handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
 
        optionMenu.addAction(statsAction)
        optionMenu.addAction(editAction)
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
 
    */
}
