//
//  UserCache.swift
//  Physio
//
//  Created by Vlad Manea on 4/13/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Foundation

enum AuthenticationMethod: String {
    case AccountKit = "AccountKit"
    case Facebook = "Facebook"
}

class UserCache: NSObject {
    func setUser(user: NSDictionary?) {
        if user == nil {
            NSUserDefaults.standardUserDefaults().removeObjectForKey("user")
            NSUserDefaults.standardUserDefaults().synchronize()
            return
        }
        
        let userData = NSKeyedArchiver.archivedDataWithRootObject(user!)
        NSUserDefaults.standardUserDefaults().setObject(userData, forKey: "user")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func setAuthenticationMethod(method: AuthenticationMethod) {
        NSUserDefaults.standardUserDefaults().setObject(method.rawValue, forKey: "authenticationMethod")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func getAuthenticationMethod() -> AuthenticationMethod {
        if let method = NSUserDefaults.standardUserDefaults().objectForKey("authenticationMethod") as? String {
            if let authenticationMethod = AuthenticationMethod(rawValue: method) {
                return authenticationMethod
            }
        }
        
        return AuthenticationMethod.Facebook
    }
    
    func getUser() -> NSDictionary? {
        if let userData = NSUserDefaults.standardUserDefaults().objectForKey("user") as? NSData {
            return NSKeyedUnarchiver.unarchiveObjectWithData(userData) as? NSDictionary
        }
        
        return nil
    }
}
