//
//  AddExerciseController.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import MobileCoreServices
import AVKit
import AVFoundation

class AddExerciseController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.addExerciseController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    enum NumericType {
        case Repetitions
        case Duration
    }
    
    enum FieldType {
        case ExerciseTitle
        case GeneralAdvice
        case PersonalAdvice
    }
    
    enum RepetitionsDurationState {
        case Repetitions
        case Duration
    }
    
    enum PhotoVideoState {
        case Idle
        case Showcase
        case IdleAdd
    }
    
    @IBOutlet weak var progressLoaderView: UIProgressView!
    var repetitionsDurationState = RepetitionsDurationState.Repetitions
    var photoVideoState = PhotoVideoState.Idle
    
    var countRepetitionsMinValue = 1
    var countRepetitionsMaxValue = 100
    
    var exercises = []
    var media = NSMutableArray()
    
    var personalAdviceText: String?
    var generalAdviceText: String?
    var exerciseTitleText: String?
    var repetitions: Int?
    var minutes: Int?
    var seconds: Int?
    var imagePickerController: UIImagePickerController = UIImagePickerController()
    
    var exerciseTitleEnabled = false
    var generalAdviceEnabled = false
    var personalAdviceEnabled = false
    
    @IBOutlet weak var fieldsTableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var photoButtonImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePickerController.navigationBar.tintColor = UIColor.whiteColor()
        self.imagePickerController.navigationBar.barTintColor = UIColor.blackColor()
        
        self.photoButtonImage.layer.shadowColor = UIColor.blackColor().CGColor
        self.photoButtonImage.layer.shadowOffset = CGSizeMake(1, 1)
        self.photoButtonImage.layer.shadowRadius = 2
        self.photoButtonImage.layer.shadowOpacity = 0.4
        
        self.fieldsTableView.delegate = self
        self.fieldsTableView.dataSource = self
        
        self.fieldsTableView.estimatedRowHeight = 60
        self.fieldsTableView.rowHeight = UITableViewAutomaticDimension
        
        self.imagePickerController.delegate = self
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddExerciseController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddExerciseController.keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddExerciseController.keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refresh(false)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh(reloadData: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            var exerciseTitleHasText = false
            var personalAdviceHasText = false
            var isValidMinutes = false
            var isValidSeconds = false
            var repetitionsHasValue = false
            
            switch self.repetitionsDurationState {
            case .Duration:
                print(self.minutes)
                print(self.seconds)
                
                if let realMinutes = self.minutes {
                    if 0 <= realMinutes && realMinutes < 60 {
                        isValidMinutes = true
                    }
                }
                
                if let realSeconds = self.seconds {
                    if 0 <= realSeconds && realSeconds < 60 {
                        isValidSeconds = true
                    }
                }
                
                if let realSeconds = self.seconds, realMinutes = self.minutes {
                    if realSeconds <= 0 && realMinutes <= 0 {
                        isValidSeconds = false
                    }
                }
                
                let indexPath2d = NSIndexPath(forRow: 0, inSection: 2)
                
                if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath2d) as? AddExerciseDurationCell {
                    realCell.validate(isValidMinutes: isValidMinutes, isValidSeconds: isValidSeconds)
                }
            case .Repetitions:
                if let realRepetitions = self.repetitions {
                    if self.countRepetitionsMinValue <= realRepetitions && realRepetitions <= self.countRepetitionsMaxValue {
                        repetitionsHasValue = true
                    }
                }
                
                let indexPath2r = NSIndexPath(forRow: 0, inSection: 2)
                
                if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath2r) as? AddExerciseNumericCell {
                    realCell.validate(repetitionsHasValue)
                }
            }
            
            if let realExerciseTitle = self.exerciseTitleText {
                if realExerciseTitle.characters.count > 0 {
                    exerciseTitleHasText = true
                }
            }
            
            if let realPersonalAdvice = self.personalAdviceText {
                if realPersonalAdvice.characters.count > 0 {
                    personalAdviceHasText = true
                }
            }
            
            self.exerciseTitleEnabled = isValidMinutes && isValidSeconds || repetitionsHasValue
            self.generalAdviceEnabled = (isValidMinutes && isValidSeconds || repetitionsHasValue) && exerciseTitleHasText
            self.personalAdviceEnabled = (isValidMinutes && isValidSeconds || repetitionsHasValue) && exerciseTitleHasText
            
            let indexPath3 = NSIndexPath(forRow: 0, inSection: 3)
            
            if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath3) as? AddExerciseFieldCell {
                realCell.enable(self.exerciseTitleEnabled)
            }
            
            let indexPath4 = NSIndexPath(forRow: 0, inSection: 4)
            
            if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath4) as? AddExerciseAreaCell {
                realCell.enable(self.generalAdviceEnabled)
            }
            
            let indexPath5 = NSIndexPath(forRow: 0, inSection: 5)
            
            if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath5) as? AddExerciseAreaCell {
                realCell.enable(self.personalAdviceEnabled)
            }
            
            if exerciseTitleHasText && personalAdviceHasText && (isValidMinutes && isValidSeconds || repetitionsHasValue) {
                self.doneButton.enabled = true
                self.doneButton.alpha = 1.0
            } else {
                self.doneButton.enabled = false
                self.doneButton.alpha = 0.5
            }
            
            if (reloadData) {
                self.fieldsTableView.reloadData()
            }
        }
    }
    
    @IBAction func clickedDone(sender: AnyObject) {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showAddExercisesFromAddExercise", sender: self)
        }
    }
    
    func deleteMedia(media: NSDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            if let deleteCacheId = media["cacheId"] as? String {
                for i in 0..<self.media.count {
                    let currentMedia = self.media[i]
                    
                    if let deleteCurrentCacheId = currentMedia["cacheId"] as? String {
                        if deleteCacheId.compare(deleteCurrentCacheId) == NSComparisonResult.OrderedSame {
                            self.media.removeObjectAtIndex(i)
                            break
                        }
                    }
                }
                
                if self.media.count > 0 {
                    self.photoVideoState = PhotoVideoState.Showcase
                } else {
                    self.photoVideoState = PhotoVideoState.Idle
                }
                
                let indexPath = NSIndexPath(forRow: 0, inSection: 0)
                self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            }
        }
    }
    
    func handleExerciseNumericUpdate(value: Int?, numericType: AddExerciseController.NumericType) {
        switch numericType {
        case .Repetitions:
            self.handleExerciseRepetitions(value)
        default:
            break
        }
    }
    
    func handleExerciseDurationUpdate(minutes minutes: Int?, seconds: Int?, numericType: AddExerciseController.NumericType) {
        switch numericType {
        case .Duration:
            self.handleExerciseDuration(minutes: minutes, seconds: seconds)
        default:
            break
        }
    }
    
    func handleExerciseFieldUpdate(text: String, fieldType: AddExerciseController.FieldType) {
        switch fieldType {
        case .ExerciseTitle:
            self.handleExerciseTitle(text)
        case .GeneralAdvice:
            self.handleGeneralAdvice(text)
        case .PersonalAdvice:
            self.handlePersonalAdvice(text)
        }
    }
    
    private func handleExerciseRepetitions(value: Int?) {
        dispatch_async(dispatch_get_main_queue()) {
            self.repetitions = value
            self.refresh(false)
        }
    }
    
    private func handleExerciseDuration(minutes minutes: Int?, seconds: Int?) {
        dispatch_async(dispatch_get_main_queue()) {
            self.minutes = minutes
            self.seconds = seconds
            self.refresh(false)
        }
    }
    
    private func handleExerciseTitle(text: String) {
        dispatch_async(dispatch_get_main_queue(), {
            self.exerciseTitleText = text
            self.refresh(false)
        })
    }
    
    private func handleGeneralAdvice(text: String) {
        dispatch_async(dispatch_get_main_queue(), {
            self.generalAdviceText = text
            self.refresh(false)
        })
    }
    
    private func handlePersonalAdvice(text: String) {
        dispatch_async(dispatch_get_main_queue(), {
            self.personalAdviceText = text
            self.refresh(false)
        })
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let info  = notification.userInfo, realTabBarController = self.tabBarController {
            let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]!
            let rawFrame = value.CGRectValue
            let keyboardFrame = view.convertRect(rawFrame, fromView: nil)
            let keyboardHeight = keyboardFrame.height + 70 // For the extra view on top of the keyboard
            let tabBarHeight = realTabBarController.tabBar.frame.size.height
            self.view.frame.origin.y = (tabBarHeight - keyboardHeight) * 0.75
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    func setRepetitionsState() {
        dispatch_async(dispatch_get_main_queue()) {
            self.repetitionsDurationState = RepetitionsDurationState.Repetitions
            let indexPath = NSIndexPath(forRow: 0, inSection: 2)
            self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            self.refresh(false)
        }
    }
    
    func setDurationState() {
        dispatch_async(dispatch_get_main_queue()) {
            self.repetitionsDurationState = RepetitionsDurationState.Duration
            let indexPath = NSIndexPath(forRow: 0, inSection: 2)
            self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            self.refresh(false)
        }
    }
    
    private func buildExercise() -> NSMutableDictionary? {
        let exercise = NSMutableDictionary()
        
        let cacheId = NSUUID().UUIDString
        exercise.setObject(cacheId, forKey: "cacheId")
        
        if let realExerciseTitle = self.exerciseTitleText {
            exercise.setObject(realExerciseTitle, forKey: "title")
        }
        
        for i in 0..<media.count {
            let medium = media[i]
            medium.setObject(i + 1, forKey: "index")
        }
        
        exercise.setObject(media, forKey: "media")
        
        if let realExerciseGeneralAdvice = self.generalAdviceText {
            exercise.setObject(realExerciseGeneralAdvice, forKey: "generalAdvice")
        }
        
        if let realExercisePersonalAdvice = self.personalAdviceText {
            exercise.setObject(realExercisePersonalAdvice, forKey: "personalAdvice")
        }
        
        switch self.repetitionsDurationState {
        case .Duration:
            if let realMinutes = self.minutes, realSeconds = self.seconds {
                let duration = realSeconds + 60 * realMinutes
                exercise.setObject(duration, forKey: "duration")
            } else {
                return nil
            }
        case .Repetitions:
            if let realRepetitions = self.repetitions {
                exercise.setObject(realRepetitions, forKey: "countRepetitions")
            } else {
                return nil
            }
        }
        
        return exercise
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "showAddExercisesFromAddExercise" {
            if let viewController = segue.destinationViewController as? AddExercisesController {
                if let realExercise = self.buildExercise() {
                    viewController.addExercise(realExercise)
                }
            }
        }
        
        if segue.identifier == "showPhotoOptionsFromAddExercise" {
            if let viewController = segue.destinationViewController as? PhotoOptionsController, photo = sender as? NSDictionary {
                viewController.photo = photo
            }
        }
        
        if segue.identifier == "showVideoOptionsFromAddExercise" {
            if let viewController = segue.destinationViewController as? VideoOptionsController, video = sender as? NSDictionary {
                viewController.video = video
            }
        }
    }
    
    @IBAction func didClickCamera(sender: AnyObject) {
        startCamera([kUTTypeImage as NSString as String])
    }
    
    func startCamera(mediaTypes: [String]) {
        var sourceType: UIImagePickerControllerSourceType?
        
        if UIImagePickerController.isSourceTypeAvailable(.SavedPhotosAlbum) {
            sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary) {
            sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            sourceType = UIImagePickerControllerSourceType.Camera
        }
        
        if let realSourceType = sourceType {
            imagePickerController.sourceType = realSourceType
            imagePickerController.mediaTypes = mediaTypes
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        } else {
            Serv.showErrorPopup("Photo error", message: "This device cannot take photos.", okMessage: "OK", controller: self, handler: nil)
        }
    }
    
    func showAddMoreMedia() {
        dispatch_async(dispatch_get_main_queue(), {
            if (self.media.count <= 0) {
                return
            }
            
            self.photoVideoState = PhotoVideoState.IdleAdd
            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
            self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        })
    }
    
    func showCollectionMedia() {
        dispatch_async(dispatch_get_main_queue(), {
            if (self.media.count <= 0) {
                return
            }
            
            self.photoVideoState = PhotoVideoState.Showcase
            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
            self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        })
    }
    
    func showImage(medium: NSDictionary) {
        self.performSegueWithIdentifier("showPhotoOptionsFromAddExercise", sender: medium)
    }
    
    func showMovie(medium: NSDictionary) {
        self.performSegueWithIdentifier("showVideoOptionsFromAddExercise", sender: medium)
    }
    
    private func handlePickedMedia() {
        dispatch_async(dispatch_get_main_queue()) {
            self.refresh(false)
            
            let indexPath = NSIndexPath(forRow: 0, inSection: 0)
            self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        }
    }
    
    private func handlePickedImage(info: [String : AnyObject]) {
        dispatch_async(self.serialQueue, {
            var image: UIImage?
            
            if let realImageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
                image = realImageOriginal
            }
            
            if let realImageCrop = info[UIImagePickerControllerCropRect] as? UIImage {
                image = realImageCrop
            }
            
            if let realImageEdited = info[UIImagePickerControllerEditedImage] as? UIImage {
                image = realImageEdited
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            
            if let realImage = image {
                let cacheId = NSUUID().UUIDString
                
                do {
                    let documentsDirectoryURL = try NSFileManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
                    
                    let toURL = documentsDirectoryURL.URLByAppendingPathComponent("HabitlabPhysioImage\(cacheId)").URLByAppendingPathExtension("jpg")
                    
                    if let realImageJpeg = UIImageJPEGRepresentation(realImage, 0.0) {
                        try realImageJpeg.writeToURL(toURL, options: NSDataWritingOptions.AtomicWrite)
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            self.progressLoaderView.setProgress(0.0, animated: false)
                            self.progressLoaderView.alpha = 1.0
                        })
                        
                        PhysioS3API.uploadImage(cacheId, url: toURL, handler: { (error) in
                            dispatch_async(dispatch_get_main_queue(), {
                                self.progressLoaderView.alpha = 0.0
                            })
                            
                            if error != nil {
                                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                self.handlePickedMedia()
                            }
                            
                            let dictionary = NSMutableDictionary()
                            dictionary.setObject(cacheId, forKey: "cacheId")
                            dictionary.setObject(String(toURL), forKey: "urlImage")
                            dictionary.setObject(kUTTypeImage as NSString as String, forKey: "physioMediaType")
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.media.addObject(dictionary)
                                self.photoVideoState = PhotoVideoState.Showcase
                                self.handlePickedMedia()
                            })
                            }, progress: { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                                var progress = Float(0.0)
                                
                                if totalBytesExpectedToSend >= 1 {
                                    progress = Float(totalBytesSent / totalBytesExpectedToSend)
                                }
                                
                                progress = 0.1 + 0.9 * progress
                                
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.progressLoaderView.setProgress(progress, animated: true)
                                })
                        })
                    } else {
                        self.handlePickedMedia()
                    }
                } catch let error as NSError {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    self.handlePickedMedia()
                } catch {
                    self.handlePickedMedia()
                }
            }
        })
    }
    
    private func handlePickedMovie(info: [String : AnyObject]) {
        if let url = info[UIImagePickerControllerMediaURL] as? NSURL {
            dispatch_async(self.serialQueue, {
                let group = dispatch_group_create()
                let cacheId = NSUUID().UUIDString
                var savedImage = false
                var savedMovie = false
                
                let dictionary = NSMutableDictionary()
                dictionary.setObject(cacheId, forKey: "cacheId")
                dictionary.setObject(kUTTypeMovie as NSString as String, forKey: "physioMediaType")
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.dismissViewControllerAnimated(true, completion: nil)
                })
                
                do {
                    dispatch_group_enter(group)
                    
                    let documentsDirectoryURLMovie = try NSFileManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
                    
                    let toURLMovie = documentsDirectoryURLMovie.URLByAppendingPathComponent("HabitlabPhysioMovie\(cacheId)").URLByAppendingPathExtension("mov")
                    try  NSFileManager().moveItemAtURL(url, toURL: toURLMovie)
                    dictionary.setObject(String(toURLMovie), forKey: "urlMovie")
                    
                    PhysioS3API.uploadVideo(cacheId, url: toURLMovie, handler: { (error) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            self.handlePickedMedia()
                            dispatch_group_leave(group)
                            return
                        }
                        
                        savedMovie = true
                        dispatch_group_leave(group)
                        }, progress: { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                            // Progress thing.
                    })
                    
                    do {
                        dispatch_group_enter(group)
                        
                        let asset = AVURLAsset(URL: toURLMovie, options: nil)
                        let imgGenerator = AVAssetImageGenerator(asset: asset)
                        imgGenerator.appliesPreferredTrackTransform = true
                        
                        let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
                        let image = UIImage(CGImage: cgImage)
                        
                        let documentsDirectoryURLImage = try NSFileManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
                        
                        let toURLImage = documentsDirectoryURLImage.URLByAppendingPathComponent("HabitlabPhysioVideoImage\(cacheId)").URLByAppendingPathExtension("jpg")
                        dictionary.setObject(String(toURLImage), forKey: "urlImage")
                        
                        if let realImageJpeg = UIImageJPEGRepresentation(image, 0.0) {
                            try realImageJpeg.writeToURL(toURLImage, options: NSDataWritingOptions.AtomicWrite)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.progressLoaderView.setProgress(0.0, animated: false)
                                self.progressLoaderView.alpha = 1.0
                                self.progressLoaderView.setProgress(0.0, animated: true)
                            })
                            
                            PhysioS3API.uploadImage(cacheId, url: toURLImage, handler: { (error) in
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.progressLoaderView.alpha = 0.0
                                })
                                
                                if error != nil {
                                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                    self.handlePickedMedia()
                                    dispatch_group_leave(group)
                                    return
                                }
                                
                                savedImage = true
                                dispatch_group_leave(group)
                                }, progress: { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                                    var progress = Float(0.0)
                                    
                                    if totalBytesExpectedToSend >= 1 {
                                        progress = Float(totalBytesSent / totalBytesExpectedToSend)
                                    }
                                    
                                    progress = 0.1 + 0.9 * progress
                                    
                                    dispatch_async(dispatch_get_main_queue(), {
                                        self.progressLoaderView.setProgress(progress, animated: true)
                                    })
                            })
                        } else {
                            self.handlePickedMedia()
                        }
                    } catch let error as NSError {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        self.handlePickedMedia()
                    } catch {
                        dispatch_group_leave(group)
                        self.handlePickedMedia()
                    }
                } catch let error as NSError {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    self.handlePickedMedia()
                } catch {
                    dispatch_group_leave(group)
                    self.handlePickedMedia()
                }
                
                dispatch_group_notify(group, self.serialQueue) {
                    if savedMovie && savedImage {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.media.addObject(dictionary)
                            self.photoVideoState = PhotoVideoState.Showcase
                            self.handlePickedMedia()
                        })
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.handlePickedMedia()
                        })
                    }
                }
            })
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let type = info[UIImagePickerControllerMediaType] as? NSString {
            if type == kUTTypeImage {
                self.handlePickedImage(info)
            }
            
            if type == kUTTypeMovie {
                self.handlePickedMovie(info)
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if self.photoVideoState == .Showcase {
                return 110
            }
            
            return 0
        }
        
        if indexPath.section == 1 {
            return 60
        }
        
        if indexPath.section == 2 {
            return 80
        }
        
        if indexPath.section == 3 {
            return 80
        }
        
        if indexPath.section == 4 {
            return 120
        }
        
        if indexPath.section == 5 {
            return 120
        }
        
        return 80
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        
        print(section)
        
        if section <= 2 {
            invisibleView.backgroundColor = UIColor.clearColor()
        } else {
            invisibleView.backgroundColor = UIColor.lightGrayColor()
        }
        
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            if self.photoVideoState == .Showcase {
                if let cell = tableView.dequeueReusableCellWithIdentifier("addExercisePhotoVideoCollectionCell", forIndexPath: indexPath) as? AddExercisePhotoVideoCollectionCell {
                    return cell
                }
            } else {
                if let cell = tableView.dequeueReusableCellWithIdentifier("addExerciseEmptyCell") {
                    return cell
                }
            }
        }
        
        if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addExerciseChooseRepetitionsOrDurationCell", forIndexPath: indexPath) as? AddExerciseChooseRepetitionsOrDurationCell {
                return cell
            }
        }
        
        if indexPath.section == 2 {
            switch self.repetitionsDurationState {
            case .Repetitions:
                if let cell = tableView.dequeueReusableCellWithIdentifier("addExerciseNumericCell", forIndexPath: indexPath) as? AddExerciseNumericCell {
                    return cell
                }
            case .Duration:
                if let cell = tableView.dequeueReusableCellWithIdentifier("addExerciseDurationCell", forIndexPath: indexPath) as? AddExerciseDurationCell {
                    return cell
                }
            }
            
            return defaultCell
        }
        
        if indexPath.section == 3 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addExerciseFieldCell", forIndexPath: indexPath) as? AddExerciseFieldCell {
                return cell
            }
            
            return defaultCell
        }
        
        if indexPath.section == 4 || indexPath.section == 5 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addExerciseAreaCell", forIndexPath: indexPath) as? AddExerciseAreaCell {
                return cell
            }
            
            return defaultCell
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            if self.photoVideoState == .Showcase {
                if let realCell = cell as? AddExercisePhotoVideoCollectionCell {
                    realCell.populate(self.media, controller: self)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                    return
                }
            } else {
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
        }
        
        if indexPath.section == 1 {
            if let realCell = cell as? AddExerciseChooseRepetitionsOrDurationCell {
                realCell.populate(self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
        }
        
        if indexPath.section == 2 {
            switch self.repetitionsDurationState {
            case .Repetitions:
                if let realCell = cell as? AddExerciseNumericCell {
                    realCell.populate("Number of repetitions", labelPlaceholderText: "Min. \(countRepetitionsMinValue), max. \(countRepetitionsMaxValue)", minValue: countRepetitionsMinValue, maxValue: countRepetitionsMaxValue, numericType: AddExerciseController.NumericType.Repetitions, enabled: true, controller: self)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                }
            case .Duration:
                if let realCell = cell as? AddExerciseDurationCell {
                    realCell.populate("Duration", labelPlaceholderTextMinutes: "0", labelPlaceholderTextSeconds: "0", numericType: AddExerciseController.NumericType.Duration, enabled: true, controller: self)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                }
            }
            
            return
        }
        
        if indexPath.section == 3 {
            if let realCell = cell as? AddExerciseFieldCell {
                realCell.populate("Exercise title", labelPlaceholderText: "e.g. Arm lifting", fieldType: AddExerciseController.FieldType.ExerciseTitle, enabled: self.exerciseTitleEnabled, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if indexPath.section == 4 {
            if let realCell = cell as? AddExerciseAreaCell {
                realCell.populate("Personal advice", labelPlaceholderText: "e.g. Lift your arm", fieldType: AddExerciseController.FieldType.PersonalAdvice, enabled: self.personalAdviceEnabled, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if indexPath.section == 5 {
            if let realCell = cell as? AddExerciseAreaCell {
                realCell.populate("General advice (optional)", labelPlaceholderText: "e.g., Make sure that...", fieldType: AddExerciseController.FieldType.GeneralAdvice, enabled: self.generalAdviceEnabled, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    @IBAction func addExerciseControllerDeletedMedia(segue: UIStoryboardSegue) {
        // Do nothing.
    }
}
