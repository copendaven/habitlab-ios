//
//  PhotoOptionsController.swift
//  Physio
//
//  Created by Vlad Manea on 5/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class PhotoOptionsController: UIViewController {
    var controller: UIViewController?
    var photo: NSDictionary?
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.photoOptionsController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var deleteButtonImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    
    
        self.deleteButtonImage.layer.shadowColor = UIColor.blackColor().CGColor
        self.deleteButtonImage.layer.shadowOffset = CGSizeMake(1, 1)
        self.deleteButtonImage.layer.shadowRadius = 2
        self.deleteButtonImage.layer.shadowOpacity = 0.4
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh() {
        dispatch_async(self.serialQueue) {
            if let realPhoto = self.photo {
                if let urlString = realPhoto["urlImage"] as? String {
                    if let url = NSURL(string: urlString) {
                        if let data = NSData(contentsOfURL: url) {
                            if let image = UIImage(data: data) {
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.photoImage.image = image
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func deletePhoto(sender: AnyObject) {
        let confirmDeleteAlert = UIAlertController(title: "Remove", message: "Remove this photo?", preferredStyle: UIAlertControllerStyle.Alert)
        
        confirmDeleteAlert.addAction(UIAlertAction(title: "Yes", style: .Destructive, handler: { (action: UIAlertAction!) in
            if let realPhoto = self.photo {
                if let urlString = realPhoto["urlImage"] as? String {
                    if let url = NSURL(string: urlString) {
                        let fileManager = NSFileManager.defaultManager()
                        
                        do {
                            try fileManager.removeItemAtURL(url)
                            self.performSegueWithIdentifier("showAddExerciseFromPhotoOptions", sender: realPhoto)
                        } catch {
                            // Do nothing.
                        }
                    }
                }
            }
        }))
        
        confirmDeleteAlert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: { (action: UIAlertAction!) in
            // Do nothing.
        }))
        
        self.presentViewController(confirmDeleteAlert, animated: true, completion: nil)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "showAddExerciseFromPhotoOptions" {
            if let viewController = segue.destinationViewController as? AddExerciseController, realPhoto = sender as? NSDictionary {
                viewController.deleteMedia(realPhoto)
            }
        }
    }
}