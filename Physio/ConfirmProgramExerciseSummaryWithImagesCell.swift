//
//  ConfirmProgramExerciseSummaryWithImagesCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import MobileCoreServices
import AVKit
import AVFoundation

class ConfirmProgramExerciseSummaryWithImagesCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var countRepetitionsDurationLabel: UILabel!
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var exerciseTitleLabel: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    private var media: NSArray?
    private var program: NSDictionary?
    private var level: Int?
    private var exercise: Int?
    private var controller: UIViewController?
    
    func populate(program: NSDictionary, level: Int, exercise: Int, controller: UIViewController) {
        self.program = program
        self.controller = controller
        
        if let drives = program["drives"] as? NSArray {
            if drives.count > level {
                if let drive = drives[level] as? NSDictionary {
                    if let physioExercises = drive["physioExercises"] as? NSArray {
                        if physioExercises.count > exercise {
                            if let theExercise = physioExercises[exercise] as? NSDictionary {
                                if let theMedia = theExercise["media"] as? NSArray {
                                    self.media = theMedia
                                }
                            }
                        }
                    }
                }
            }
        }
        
        imagesCollectionView.dataSource = self
        imagesCollectionView.delegate = self
        imagesCollectionView.reloadData()
        
        exerciseTitleLabel.text = ""
        indexLabel.text = ""
        countRepetitionsDurationLabel.text = ""
        
        if let drives = program["drives"] as? NSArray {
            if drives.count > level {
                if let drive = drives[level] as? NSDictionary {
                    if let physioExercises = drive["physioExercises"] as? NSArray {
                        if physioExercises.count > exercise {
                            if let physioExercise = physioExercises[exercise] as? NSDictionary {
                                if let exerciseTitle = physioExercise["title"] as? String {
                                    exerciseTitleLabel.text = "Exercise \(1 + exercise): \(exerciseTitle)"
                                }
                                
                                indexLabel.text = "Exercise \(1 + exercise) out of \(physioExercises.count)"
                                
                                if let countRepetitions = physioExercise["countRepetitions"] as? Int {
                                    countRepetitionsDurationLabel.text = "Repetitions: \(countRepetitions)"
                                }
                                
                                if let duration = physioExercise["duration"] as? Int {
                                    let minutes = Int(duration / 60)
                                    let seconds = Int(duration) % 60
                                    
                                    var durationText = "Duration:"
                                    
                                    if minutes > 0 {
                                        durationText = " \(minutes) minute\(minutes == 1 ? "" : "s")"
                                    }
                                    
                                    if seconds > 0 {
                                        durationText = " \(seconds) seconds\(seconds == 1 ? "" : "s")"
                                    }
                                    
                                    countRepetitionsDurationLabel.text = durationText
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let realMedia = self.media {
            return realMedia.count
        }
        
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(80, 50) // This is the size for one collection item with button and label
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let defaultCell = UICollectionViewCell()
        
        if let realMedia = self.media, realControler = self.controller {
            if realMedia.count > indexPath.item {
                if let medium = realMedia[indexPath.item] as? NSDictionary {
                    if let mediaType = medium["physioMediaType"] as? String {
                        if mediaType.compare(kUTTypeMovie as NSString as String) == NSComparisonResult.OrderedSame {
                            if let cell = imagesCollectionView.dequeueReusableCellWithReuseIdentifier("confirmProgramExerciseSummaryMovieCell", forIndexPath: indexPath) as? ConfirmProgramExerciseSummaryMovieCell {
                                return cell
                            }
                            
                            return defaultCell
                        }
                        
                        if mediaType.compare(kUTTypeImage as NSString as String) == NSComparisonResult.OrderedSame {
                            if let cell = imagesCollectionView.dequeueReusableCellWithReuseIdentifier("confirmProgramExerciseSummaryImageCell", forIndexPath: indexPath) as? ConfirmProgramExerciseSummaryImageCell {
                                return cell
                            }
                            
                            return defaultCell
                        }
                    }
                }
            }
        }
        
        return defaultCell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let realMedia = self.media, realController = self.controller {
            if realMedia.count > indexPath.item {
                if let medium = realMedia[indexPath.item] as? NSDictionary {
                    if let mediaType = medium["physioMediaType"] as? String {
                        if mediaType.compare(kUTTypeMovie as NSString as String) == NSComparisonResult.OrderedSame {
                            if let realCell = cell as? ConfirmProgramExerciseSummaryMovieCell {
                                realCell.populate(medium, controller: realController)
                                return
                            }
                            
                            return
                        }
                        
                        if mediaType.compare(kUTTypeImage as NSString as String) == NSComparisonResult.OrderedSame {
                            if let realCell = cell as? ConfirmProgramExerciseSummaryImageCell {
                                realCell.populate(medium, controller: realController)
                                return
                            }
                            
                            return
                        }
                    }
                }
            }
        }
    }
}
