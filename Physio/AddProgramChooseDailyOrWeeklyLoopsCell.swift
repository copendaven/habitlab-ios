//
//  AddProgramChooseDailyOrWeeklyLoopsCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/28/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddProgramChooseDailyOrWeeklyLoopsCell: UITableViewCell {
    private var controller: UIViewController?
    
    @IBOutlet weak var dailyWeeklyLoopsToggle: UISegmentedControl!
    
    func populate(controller: UIViewController) {
        self.controller = controller
        
        self.dailyWeeklyLoopsToggle.tintColor = UIColor(red: 3/255.0, green: 46/255.0, blue: 73/255.0, alpha: 1.0)
    }
    
    @IBAction func didToggleDailyWeeklyLoops(sender: AnyObject) {
        switch dailyWeeklyLoopsToggle.selectedSegmentIndex {
        case 0:
            if let realController = self.controller {
                if let viewController = realController as? AddProgramController {
                    viewController.setDailyLoopsState()
                }
            }
        case 1:
            if let realController = self.controller {
                if let viewController = realController as? AddProgramController {
                    viewController.setWeeklyLoopsState()
                }
            }
        default:
            break
        }
    }
}
