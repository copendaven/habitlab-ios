//
//  AddExercisesAddNewExerciseCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddExercisesAddNewExerciseCell: UITableViewCell {
    private var controller: UIViewController?
    
    @IBOutlet weak var addImage: UIImageView!
    func populate(controller: UIViewController) {
        self.controller = controller
        
        self.addImage.layer.shadowColor = UIColor.blackColor().CGColor
        self.addImage.layer.shadowOffset = CGSizeMake(1, 1)
        self.addImage.layer.shadowRadius = 3
        self.addImage.layer.shadowOpacity = 0.4
    }
    
    @IBAction func clickedAddNewExercise(sender: AnyObject) {
        if let realController = self.controller {
            if let viewController = realController as? AddExercisesController {
                viewController.clickedAddNewExercise()
            }
        }
    }
}
