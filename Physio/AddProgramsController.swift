//
//  AddProgramsController.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class AddProgramsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.programsController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    private var serverPrograms = []
    private var programs = []
    
    @IBOutlet weak var addImage: UIImageView!
    
    @IBOutlet weak var programLabel: UILabel!
    @IBOutlet weak var programsTableView: UITableView!
    
    @IBOutlet weak var addNewProgram: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem?.enabled = false
        
        // Button Shadow
        self.addImage.layer.shadowColor = UIColor.blackColor().CGColor
        self.addImage.layer.shadowOffset = CGSizeMake(1, 1)
        self.addImage.layer.shadowRadius = 2
        self.addImage.layer.shadowOpacity = 0.4
        
        programsTableView.delegate = self
        programsTableView.dataSource = self
        
        dispatch_async(dispatch_get_main_queue()) {
            self.programsTableView.estimatedRowHeight = 80
            self.programsTableView.rowHeight = UITableViewAutomaticDimension
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            
            if self.serverPrograms.count <= 0 {
                self.refreshFromServer(true)
            } else {
                self.refreshFromClient(true)
            }
        }
    }
    
    @IBAction func clickedAddNewProgram() {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showAssignProgramFromAddPrograms", sender: self)
        }
    }
    
    func clickedExistingProgram(program: NSDictionary) {
        self.performSegueWithIdentifier("showViewProgramFromAddPrograms", sender: program)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showViewProgramFromAddPrograms" {
            if let viewController = segue.destinationViewController as? ViewProgramController, realProgram = sender as? NSDictionary {
                viewController.program = realProgram
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.programLabel.hidden = true
        self.programsTableView.hidden = true
        
        if self.serverPrograms.count <= 0 {
            self.refreshFromServer(true)
        } else {
            self.refreshFromClient(true)
        }
    }
    
    func refreshFromServer(withReload: Bool) {
        dispatch_async(self.serialQueue, {
            Serv.showSpinner()
            
            HabitlabRestAPI.getPrograms({ (error, programsJson) -> () in
                Serv.hideSpinner()
                
                if (error != nil) {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                if let programsArray = programsJson as? NSArray {
                    dispatch_async(dispatch_get_main_queue(), {
                        self.serverPrograms = programsArray
                        self.refreshFromClient(withReload)
                    })
                }
            })
        })
    }
    
    func refreshFromClient(reloadData: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            self.programs = self.serverPrograms
            
            if reloadData {
                self.programsTableView.reloadData()
                
                if self.serverPrograms.count > 0 {
                    self.programLabel.hidden = true
                    self.programsTableView.hidden = false
                } else {
                    self.programLabel.hidden = false
                    self.programsTableView.hidden = true
                }
            }
        }
    }
    
  
    @IBAction func addProgramsControllerCreatedProgram(segue: UIStoryboardSegue) {
        self.refreshFromServer(true)
    }
    
    // MARK: Unwind segues
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.programs.count + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == self.programs.count {
            return defaultCell
        }
        
        if let _ = self.programs[indexPath.section] as? NSDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addProgramsProgramCell", forIndexPath: indexPath) as? AddProgramsProgramCell {
                return cell
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == self.programs.count {
            return
        }
        
        if let program = self.programs[indexPath.section] as? NSDictionary {
            if let realCell = cell as? AddProgramsProgramCell {
                realCell.populate(program, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
