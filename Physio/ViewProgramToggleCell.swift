//
//  ViewProgramToggleCell.swift
//  Physio
//
//  Created by Vlad Manea on 7/3/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ViewProgramToggleCell: UITableViewCell {
    var controller: UIViewController?
    var program: NSDictionary?
    
    @IBOutlet weak var painScaleToggle: UISegmentedControl!
    
    func populate(controller: UIViewController, program: NSDictionary) {
        self.painScaleToggle.tintColor = UIColor(red: 3/255.0, green: 46/255.0, blue: 73/255.0, alpha: 1.0)
        
        self.controller = controller
        self.program = program
    }
    
    @IBAction func didTogglePainScale(sender: AnyObject) {
        print(painScaleToggle.selectedSegmentIndex)
        
        switch painScaleToggle.selectedSegmentIndex
        {
        case 0:
            if let realController = self.controller {
                if let viewController = realController as? ViewProgramController {
                    viewController.setChartType(ViewProgramController.ChartType.PainScale)
                }
            }
            
        case 1:
            if let realController = self.controller {
                if let viewController = realController as? ViewProgramController {
                    viewController.setChartType(ViewProgramController.ChartType.Loops)
                }
            }
            
        default:
            break;
        }
    }
}
