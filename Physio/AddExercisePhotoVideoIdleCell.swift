//
//  AddExercisePhotoVideoIdleCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import MobileCoreServices

class AddExercisePhotoVideoIdleCell: UITableViewCell {
    private var controller: UIViewController?
    
    @IBOutlet weak var backButton: UIButton!
    
    func populate(showBackButton: Bool, controller: UIViewController) {
        self.controller = controller
        backButton.hidden = !showBackButton
        backButton.enabled = showBackButton
    }
    
    @IBAction func clickedImage(sender: AnyObject) {
        if let realController = self.controller {
            if let viewController = realController as? AddExerciseController {
                viewController.startCamera([kUTTypeImage as NSString as String])
            }
        }
    }
    
    @IBAction func clickedMovie(sender: AnyObject) {
        if let realController = self.controller {
            if let viewController = realController as? AddExerciseController {
                viewController.startCamera([kUTTypeMovie as NSString as String])
            }
        }
    }
    
    @IBAction func clickedBackButton(sender: AnyObject) {
        if let realController = self.controller {
            if let viewController = realController as? AddExerciseController {
                viewController.showCollectionMedia()
            }
        }
    }
}
