//
//  ConfirmProgramController.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ConfirmProgramController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.programsController.serialQueue", DISPATCH_QUEUE_SERIAL)
    var sentProgram = false
    var driveTemplate: NSDictionary?
    var programTitle: String?
    var patient: NSDictionary?
    var program: NSDictionary?
    var countExercises: Int?
    var sendCopyToOwnEmail: Bool?
    
    @IBOutlet weak var patientImageLabel: UILabel!
    @IBOutlet weak var patientImage: UIImageView!
    @IBOutlet weak var summaryTableView: UITableView!
    
    private func getLevelDistribution(countWeeks: Int) -> [Int] {
        if countWeeks <= 0 {
            let numbers: [Int] = []
            return numbers
        }
        
        if countWeeks == 1 {
            return [1] // 1
        }
        
        if countWeeks == 2 {
            return [1, 1] // 2
        }
        
        if countWeeks == 3 {
            return [1, 1, 1] // 3
        }
        
        if countWeeks == 4 {
            return [1, 2, 1] // 4
        }
        
        if countWeeks == 5 {
            return [1, 2, 2] // 5
        }
        
        if countWeeks == 6 {
            return [1, 2, 2, 1] // 6
        }
        
        if countWeeks == 7 {
            return [1, 2, 2, 2] // 7
        }
        
        if countWeeks == 8 {
            return [1, 2, 2, 2, 1] // 8
        }
        
        if countWeeks == 9 {
            return [1, 2, 3, 2, 1] // 9
        }
        
        if countWeeks == 10 {
            return [1, 2, 3, 2, 2] // 10
        }
        
        if countWeeks == 11 {
            return [1, 2, 3, 3, 2] // 11
        }
        
        if countWeeks == 12 {
            return [1, 2, 3, 3, 2, 1] // 12
        }
        
        if countWeeks == 13 {
            return [1, 2, 3, 3, 2, 2] // 13
        }
        
        if countWeeks == 14 {
            return [1, 2, 3, 3, 3, 2] // 14
        }
        
        if countWeeks == 15 {
            return [1, 2, 3, 3, 3, 2, 1] // 15
        }
        
        if countWeeks == 16 {
            return [1, 2, 3, 4, 3, 2, 1] // 16
        }
        
        var array: [Int] = []
        array.append(1)
        array.append(2)
        array.append(3)
        
        for _ in 1...((countWeeks - 12 - countWeeks % 4) / 4) {
            array.append(4)
        }
        
        if (countWeeks % 4 == 0) {
            array.append(3)
            array.append(2)
            array.append(1)
        }
        
        if (countWeeks % 4 == 1) {
            array.append(3)
            array.append(2)
            array.append(2)
        }
        
        if (countWeeks % 4 == 2) {
            array.append(3)
            array.append(3)
            array.append(2)
        }
        
        if (countWeeks % 4 == 3) {
            array.append(4)
            array.append(3)
            array.append(2)
        }
        
        return array
    }
    
    @IBOutlet weak var sendButton: UIButton!
    
    func populatePhoto() {
        if let physioPatient = self.patient {
            var fullName = ""
            
            if let firstName = physioPatient["firstName"] as? NSString {
                let firstNameString = firstName as String
                
                if firstNameString.characters.count > 0 {
                    fullName = fullName + "\(firstNameString[firstNameString.startIndex.advancedBy(0)])"
                }
            }
            
            if let lastName = physioPatient["lastName"] as? NSString {
                let lastNameString = lastName as String
                
                if lastNameString.characters.count > 0 {
                    fullName = fullName + "\(lastNameString[lastNameString.startIndex.advancedBy(0)])"
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.patientImageLabel.alpha = 0.0
                self.patientImage.alpha = 0.0
                
                self.patientImageLabel.text = fullName.uppercaseString
                self.patientImageLabel.alpha = 1.0
            })
            
            if let email = physioPatient["email"] as? NSString {
                HabitlabRestAPI.postPatientPhoto(email as String, handler: { (error, objectJson) in
                    if let object = objectJson as? NSDictionary {
                        if let photo = object["photo"] as? NSString {
                            Serv.loadImage(photo as String, handler: { (error, data) -> () in
                                if error != nil {
                                    print(error)
                                }
                                
                                if data != nil {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.patientImage.image = UIImage(data: data!)
                                        
                                        UIView.animateWithDuration(0.35, animations: {
                                            self.patientImage.alpha = 1.0
                                            self.patientImageLabel.alpha = 0.0
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
    private func buildProgram() -> NSMutableDictionary {
        let program = NSMutableDictionary()
        
        if let realPatient = self.patient {
            program.setObject(realPatient, forKey: "patient")
        }
        
        if let realSendCopyToOwnEmail = self.sendCopyToOwnEmail {
            program.setObject(realSendCopyToOwnEmail, forKey: "sendCopyToOwnEmail")
        }
        
        if let realTitle = self.programTitle {
            program.setObject(realTitle, forKey: "title")
        }
        
        let drives = buildDrives()
        program.setObject(drives, forKey: "drives")
        
        if let user = UserCache().getUser() {
            if let id = user["id"] as? String {
                program.setObject(id, forKey: "physioTherapist")
            }
        }
        
        return program
    }
    
    private func buildDrives() -> NSArray {
        let programDrives = NSMutableArray()
        
        if let realDriveTemplate = self.driveTemplate {
            if let countWeeks = realDriveTemplate["countWeeks"] as? Int {
                let weekDistributions = self.getLevelDistribution(countWeeks)
                print(weekDistributions)
                
                for i in 0..<weekDistributions.count {
                    let programDrive = NSMutableDictionary(dictionary: realDriveTemplate)
                    programDrive.setObject(weekDistributions[i], forKey: "countWeeks")
                    programDrive.setObject(1 + i, forKey: "level")
                    programDrive.setObject("Follow your exercise", forKey: "step")
                    programDrive.setObject("/images/programs/exercising-physio-\(1 + i)/square.jpg", forKey: "photo")
                    programDrive.setObject("/images/programs/exercising-physio-\(1 + i)/background.jpg", forKey: "cover")
                    programDrives.addObject(programDrive)
                    
                    if let exercises = programDrive["physioExercises"] as? NSArray {
                        self.countExercises = exercises.count
                    }
                }
            }
        }
        
        return programDrives
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.sentProgram = false
        self.populatePhoto()
        self.program = self.buildProgram()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.summaryTableView.delegate = self
        self.summaryTableView.dataSource = self
        
        self.summaryTableView.estimatedRowHeight = 80
        self.summaryTableView.rowHeight = UITableViewAutomaticDimension
        
        patientImage.layer.cornerRadius = patientImage.frame.height / 2
        patientImage.layer.borderWidth = 5
        patientImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        patientImageLabel.layer.cornerRadius = patientImageLabel.frame.height / 2
        patientImageLabel.layer.borderWidth = 5
        patientImageLabel.layer.borderColor = UIColor.whiteColor().CGColor
    }
    
    @IBAction func clickedSend() {
        if (self.sentProgram) {
            return
        }
        
        self.sentProgram = true
        
        dispatch_async(self.serialQueue, {
            if let realProgram = self.program {
                Serv.showSpinner()
                
                let handler = {(success: Bool) -> () in
                    let realProgramMutable = NSMutableDictionary(dictionary: realProgram)
                    
                    print(realProgramMutable)
                    
                    HabitlabRestAPI.postProgram(realProgramMutable, handler: { (error, programsJson) -> () in
                        Serv.hideSpinner()
                        
                        if (error != nil) {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.sentProgram = false
                            })
                            
                            return
                        }
                        
                        self.performSegueWithIdentifier("showAddProgramsFromConfirmProgram", sender: self)
                    })
                }
                
                if environment == Environment.Release {
                    InAppPurchaseService.validateReceipt(handler)
                } else {
                    handler(true)
                }
            }
        })
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let realCountExercises = self.countExercises {
            return 1 + 1 + realCountExercises
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    //    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        let invisibleView = UIView.init()
    //        invisibleView.backgroundColor = UIColor.clearColor()
    //        return invisibleView
    //    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if let realProgram = self.program, realPatient = self.patient {
            if indexPath.section == 0 {
                
                // Recipient summary
                if let cell = summaryTableView.dequeueReusableCellWithIdentifier("confirmProgramRecipientSummaryCell", forIndexPath: indexPath) as? ConfirmProgramRecipientSummaryCell {
                    return cell
                }
                
                return defaultCell
            }
            
            if indexPath.section == 1 {
                
                // Program description
                if let cell = summaryTableView.dequeueReusableCellWithIdentifier("confirmProgramSummaryCell", forIndexPath: indexPath) as? ConfirmProgramSummaryCell {
                    return cell
                }
                
                return defaultCell
            }
            
            if indexPath.section >= 2 {
                var hasImages = false
                
                if let drives = realProgram["drives"] as? NSArray {
                    if drives.count > 0 {
                        if let drive = drives[0] as? NSDictionary {
                            if let physioExercises = drive["physioExercises"] as? NSArray {
                                if physioExercises.count > indexPath.section - 2 {
                                    if let theExercise = physioExercises[indexPath.section - 2] as? NSDictionary {
                                        if let theMedia = theExercise["media"] as? NSArray {
                                            if theMedia.count > 0 {
                                                hasImages = true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
        
                if hasImages {
                    
                    // Exercise content
                    if let cell = summaryTableView.dequeueReusableCellWithIdentifier("confirmProgramExerciseSummaryWithImagesCell", forIndexPath: indexPath) as? ConfirmProgramExerciseSummaryWithImagesCell {
                        return cell
                    }
                } else {
                    
                    // Exercise content
                    if let cell = summaryTableView.dequeueReusableCellWithIdentifier("confirmProgramExerciseSummaryCell", forIndexPath: indexPath) as? ConfirmProgramExerciseSummaryCell {
                        return cell
                    }
                }
                
                return defaultCell
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let realProgram = self.program, realPatient = self.patient {
            if indexPath.section == 0 {
                
                // Recipient summary
                if let realCell = cell as? ConfirmProgramRecipientSummaryCell {
                    realCell.populate(realPatient)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                    return
                }
            }
            
            if indexPath.section == 1 {
                
                // Program description
                if let realCell = cell as? ConfirmProgramSummaryCell {
                    realCell.populate(realProgram)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                    return
                }
            }
            
            if indexPath.section >= 2 {
                let exercise = indexPath.section - 2
                let level = 0
                
                // Exercise content
                if let realCell = cell as? ConfirmProgramExerciseSummaryCell {
                    realCell.populate(realProgram, level: level, exercise: exercise, controller: self)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                    return
                }
                
                // Exercise content with images
                if let realCell = cell as? ConfirmProgramExerciseSummaryWithImagesCell {
                    realCell.populate(realProgram, level: level, exercise: exercise, controller: self)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                    return
                }
            }
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
