//
//  ConfirmProgramRecipientSummaryCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ConfirmProgramRecipientSummaryCell: UITableViewCell {

    @IBOutlet weak var patientEmailLabel: UILabel!
    @IBOutlet weak var patientNameLabel: UILabel!
    
    func populate(patient: NSDictionary) {
        patientNameLabel.text = ""
        patientEmailLabel.text = ""
        
        if let patientFirstName = patient["firstName"] as? String, patientLastName = patient["lastName"] as? String {
            patientNameLabel.text = "\(patientFirstName) \(patientLastName)"
        }
        
        if let patientEmail = patient["email"] as? String {
            patientEmailLabel.text = patientEmail
        }
    }
}
