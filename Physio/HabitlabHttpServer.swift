//
//  HabitlabHttpServer.swift
//  Physio
//
//  Created by Vlad Manea on 19/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class HabitlabHttpServer: NSObject {
    static func startTask(request: NSMutableURLRequest, shouldBlock: Bool, handler: (NSError?, AnyObject?) -> ()) {
        print(request.URL)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            data, response, error in
            
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                if error != nil {
                    print(error)
                    handler(error, nil)
                    return
                }
                
                var json: AnyObject?
                
                do {
                    json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                    
                    if json == nil {
                        throw NSError(domain: "HabitlabHttpClientError", code: 500, userInfo: ["name": "Response error", "message": "The response from the server could not be interpreted"])
                    }
                } catch let error as NSError {
                    print(error)
                    handler(error, nil)
                    return
                }
                
                if let httpResponse = response as? NSHTTPURLResponse {
                    if httpResponse.statusCode > 299 || httpResponse.statusCode < 200 {
                        do {
                            throw NSError(domain: "HabitlabHttpServerError", code: httpResponse.statusCode, userInfo: ["code": httpResponse.statusCode, "json": json!])
                        } catch let error as NSError {
                            print(error)
                            handler(error, nil)
                            return
                        }
                    }
                }
                
                handler(nil, json)
            }
        }
        
        task.resume()
    }
}