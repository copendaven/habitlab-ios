//
//  Serv.swift
//  Physio
//
//  Created by Vlad Manea on 13/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

enum Environment : String {
    case Release = "release", Staging = "staging", Development = "development"
}

class Serv: NSObject {
    static var serv = Serv()
    static var guids = NSMutableDictionary()
    static var indicators = NSMutableDictionary()
    static var calendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)
    static var weekDayNames = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]

    static func showSpinner() {
        dispatch_async(dispatch_get_main_queue()) {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        }
    }
    
    static func hideSpinner() {
        dispatch_async(dispatch_get_main_queue()) {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
    }
    
    static func showErrorPopup(title: String, message: String, okMessage: String, controller: UIViewController, handler: (()->())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let alertAction = UIAlertAction(title: okMessage, style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            if handler != nil {
                handler!()
            }
        }
        
        alert.addAction(alertAction)
        
        dispatch_async(dispatch_get_main_queue()) {
            controller.presentViewController(alert, animated: true) { () -> Void in }
        }
    }
    
    static func loadImage(location: String, handler: (NSError?, NSData?) -> ()) {
        let photoUrl = NSURL(string: location)
        
        if let photo = PhotoCache().getCache(location) {
            print("Serving image from cache: \(location)")
            handler(nil, photo)
            return
        }
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(photoUrl!) { (data, response, error) in
            if error != nil {
                handler(error, nil)
                return
            }
            
            if data != nil {
                PhotoCache().setCache(location, image: data!)
                handler(nil, data!)
                return
            }
            
            let error = NSError(domain: "ImageLoadError", code: 404, userInfo: nil)
            handler(error, nil)
        }
        
        task.resume()
    }
    
    static func showErrorPopupFromError(error: NSError?, controller: UIViewController?, handler: (()->())?) {
        if error != nil && controller != nil {
            var name = "Error"
            var message = "Something went wrong. We would appreciate your feedback. Tell us what happened, and we will get right back at it."
            
            switch(error!.domain) {
            case "NSURLErrorDomain":
                name = "Network error"
                message = "Something went wrong with the connection to our servers. Try again in a few moments. If the problem persists, send us a message and we will help you in no time."
            case "HabitlabHttpServerError":
                if let json = error!.userInfo["json"] as? NSDictionary {
                    if let jsonName = json["name"] as? NSString {
                        name = jsonName as String
                    }
                    
                    if let jsonMessage = json["message"] as? NSString {
                        message = jsonMessage as String
                    }
                }
            case "HabitlabHttpClientError":
                if let jsonName = error!.userInfo["name"] as? String {
                    name = jsonName
                }
                
                if let jsonMessage = error!.userInfo["message"] as? String {
                    message = jsonMessage
                }
            case "HabitlabAuthenticationError":
                if let jsonName = error!.userInfo["name"] as? String {
                    name = jsonName
                }
                
                if let jsonMessage = error!.userInfo["message"] as? String {
                    message = jsonMessage
                }
            default: break
                // Do nothing.
            }
            
            showErrorPopup(name, message: message, okMessage: "Okay, got it.", controller: controller!, handler: handler)
        }
    }
    
    static func showAlertPopup(title: String, message: String, okMessage: String, controller: UIViewController, handler: (()->())?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction = UIAlertAction(title: okMessage, style: UIAlertActionStyle.Default) { (UIAlertAction) -> Void in
            if handler != nil {
                handler!()
            }
        }
        alert.addAction(alertAction)
        controller.presentViewController(alert, animated: true) { () -> Void in }
    }
    
    static func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(testStr)
        return result
    }
    
    static func resetCaches() {
        PhotoCache.erase()
    }
}
