//
//  AddExerciseAreaCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddExerciseAreaCell: UITableViewCell, UITextViewDelegate {
    private var controller: UIViewController?
    private var fieldType: AddExerciseController.FieldType?
    private var labelPlaceholderText: String?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    
    func populate(labelText: String, labelPlaceholderText: String, fieldType: AddExerciseController.FieldType, enabled: Bool, controller: UIViewController) {
        self.enable(enabled)
        
        self.titleLabel.text = labelText
        self.controller = controller
        self.fieldType = fieldType
        self.labelPlaceholderText = labelPlaceholderText
        
        contentTextView.textContainerInset = UIEdgeInsetsMake(8, 5, 8, 5)
        
        if let realTextView = self.contentTextView {
            realTextView.delegate = self
            realTextView.text = labelPlaceholderText
            realTextView.textColor = UIColor.lightGrayColor()
        }
        
        let doneToolbar = self.constructInputAccessoryView()
        doneToolbar.sizeToFit()
        self.contentTextView.inputAccessoryView = doneToolbar
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddExerciseAreaCell.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)

        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func doneButtonAction() {
        if let realController = self.controller as? AddExerciseController {
            realController.view.endEditing(true)
        }
    }
    
    func enable(isEnabled: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            self.contentTextView.userInteractionEnabled = isEnabled
            
            if isEnabled {
                self.alpha = 1.0
            } else {
                self.alpha = 0.25
            }
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if let realLabelPlaceholderText = self.labelPlaceholderText {
            if textView.text.compare(realLabelPlaceholderText) == NSComparisonResult.OrderedSame {
                textView.text = ""
                textView.textColor = UIColor(red: 34 / 255, green: 176 / 255, blue: 155 / 255, alpha: 1.0)
            }
        }
        
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if let realLabelPlaceholderText = self.labelPlaceholderText {
            if textView.text.compare("") == NSComparisonResult.OrderedSame {
                textView.text = realLabelPlaceholderText
                textView.textColor = UIColor.lightGrayColor()
            }
        }
        
        textView.resignFirstResponder()
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func textViewDidChange(textView: UITextView) {
        dispatch_async(dispatch_get_main_queue()) {
            if let realText = textView.text {
                var isPlaceholderText = false
                
                if let realLabelPlaceholderText = self.labelPlaceholderText {
                    if realText.compare(realLabelPlaceholderText) == NSComparisonResult.OrderedSame {
                        isPlaceholderText = true
                    }
                }
                
                if let realController = self.controller, realFieldType = self.fieldType {
                    if let viewController = realController as? AddExerciseController {
                        if isPlaceholderText {
                            viewController.handleExerciseFieldUpdate("", fieldType: realFieldType)
                        } else {
                            viewController.handleExerciseFieldUpdate(realText, fieldType: realFieldType)
                        }
                    }
                }
            }
        }
    }
}
