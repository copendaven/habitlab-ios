//
//  AddPatientEmailController.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//
import Mixpanel


class AddPatientEmailController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate {
    var driveTemplate: NSDictionary?
    var patient: NSMutableDictionary?
    var programTitle: String?
    var sendCopyToOwnEmail: Bool?
    
    enum FieldType {
        case PatientEmail
        case SendCopyToOwnEmail
    }
    
    @IBOutlet weak var patientImageLabel: UILabel!
    @IBOutlet weak var patientImage: UIImageView!
    @IBOutlet weak var fieldsTableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fieldsTableView.delegate = self
        self.fieldsTableView.dataSource = self
        
        self.fieldsTableView.estimatedRowHeight = 110
        self.fieldsTableView.rowHeight = UITableViewAutomaticDimension
        
        patientImage.layer.cornerRadius = patientImage.frame.height / 2
        patientImage.layer.borderWidth = 5
        patientImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        patientImageLabel.layer.cornerRadius = patientImageLabel.frame.height / 2
        patientImageLabel.layer.borderWidth = 5
        patientImageLabel.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddPatientEmailController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddPatientEmailController.keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddPatientEmailController.keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.populatePhoto()
        self.refresh()
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh() {
        dispatch_async(dispatch_get_main_queue()) {
            var patientEmailHasText = false
            
            if let realPatient = self.patient {
                if let email = realPatient["email"] as? String {
                    if Serv.isValidEmail(email) {
                        patientEmailHasText = true
                    }
                }
            }
            
            if patientEmailHasText {
                self.doneButton.enabled = true
                self.doneButton.alpha = 1.0
            } else {
                self.doneButton.enabled = false
                self.doneButton.alpha = 0.5
            }
        }
    }
    
    func populatePhoto() {
        if let physioPatient = self.patient {
            var fullName = ""
            
            if let firstName = physioPatient["firstName"] as? NSString {
                let firstNameString = firstName as String
                
                if firstNameString.characters.count > 0 {
                    fullName = fullName + "\(firstNameString[firstNameString.startIndex.advancedBy(0)])"
                }
            }
            
            if let lastName = physioPatient["lastName"] as? NSString {
                let lastNameString = lastName as String
                
                if lastNameString.characters.count > 0 {
                    fullName = fullName + "\(lastNameString[lastNameString.startIndex.advancedBy(0)])"
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.patientImageLabel.alpha = 0.0
                self.patientImage.alpha = 0.0
                
                self.patientImageLabel.text = fullName.uppercaseString
                self.patientImageLabel.alpha = 1.0
            })
            
            if let email = physioPatient["email"] as? NSString {
                HabitlabRestAPI.postPatientPhoto(email as String, handler: { (error, objectJson) in
                    if let object = objectJson as? NSDictionary {
                        if let photo = object["photo"] as? NSString {
                            Serv.loadImage(photo as String, handler: { (error, data) -> () in
                                if error != nil {
                                    print(error)
                                }
                                
                                if data != nil {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.patientImage.image = UIImage(data: data!)
                                        
                                        UIView.animateWithDuration(0.35, animations: {
                                            self.patientImage.alpha = 1.0
                                            self.patientImageLabel.alpha = 0.0
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func clickedDone(sender: AnyObject) {
        if let realPatient = self.patient {
            AssignProgramController.addPatient(realPatient)
        
            let mixpanel = Mixpanel.sharedInstance()
            
            let properties = ["Email": String(realPatient["email"]!), "Sent to self": String(sendCopyToOwnEmail)]
            mixpanel.track("Program Sent", properties: properties)
            
        }
        
        
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showConfirmProgramFromAddPatientEmail", sender: self)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let info  = notification.userInfo, realTabBarController = self.tabBarController {
            let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]!
            let rawFrame = value.CGRectValue
            let keyboardFrame = view.convertRect(rawFrame, fromView: nil)
            let keyboardHeight = keyboardFrame.height + 70 // For the extra view on top of the keyboard
            let tabBarHeight = realTabBarController.tabBar.frame.size.height
            self.view.frame.origin.y = (tabBarHeight - keyboardHeight) * 0.75
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    func handleProgramFieldUpdate(text: String, fieldType: AddPatientEmailController.FieldType) {
        switch fieldType {
        case .PatientEmail:
            self.handlePatientEmail(text)
        case .SendCopyToOwnEmail:
            // Do nothing.
            break
        }
    }
    
    func handleProgramToggleUpdate(value: Bool, fieldType: AddPatientEmailController.FieldType) {
        switch fieldType {
        case .PatientEmail:
            // Do nothing.
            break
        case .SendCopyToOwnEmail:
            self.handleSendCopyToOwnEmail(value)
            break
        }
    }
    
    private func handleSendCopyToOwnEmail(value: Bool) {
        self.sendCopyToOwnEmail = value
        
        dispatch_async(dispatch_get_main_queue()) {
            self.refresh()
        }
    }
    
    private func handlePatientEmail(text: String) {
        dispatch_async(dispatch_get_main_queue(), {
            if let realPatient = self.patient {
                realPatient.setObject(text, forKey: "email")
            }
    
            self.refresh()
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "showConfirmProgramFromAddPatientEmail" {
            if let viewController = segue.destinationViewController as? ConfirmProgramController {
                viewController.driveTemplate = self.driveTemplate
                viewController.patient = self.patient
                viewController.programTitle = self.programTitle
                viewController.sendCopyToOwnEmail = self.sendCopyToOwnEmail
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addPatientEmailFieldCell", forIndexPath: indexPath) as? AddPatientEmailFieldCell {
                return cell
            }
            
            return defaultCell
        }
        
        if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addPatientSendCopyOfEmailToggleCell", forIndexPath: indexPath) as? AddPatientSendCopyOfEmailToggleCell {
                return cell
            }
            
            return defaultCell
        }

        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            if let realCell = cell as? AddPatientEmailFieldCell {
                var defaultEmail: String? = nil
                
                
                if let realPatient = self.patient {
                    if let email = realPatient["email"] as? String {
                        defaultEmail = email
                    }
                }
                
                realCell.populate("Patient email", labelPlaceholderText: "e.g., mads.hansen@gmail.com", defaultText: defaultEmail, fieldType: AddPatientEmailController.FieldType.PatientEmail, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if indexPath.section == 1 {
            if let realCell = cell as? AddPatientSendCopyOfEmailToggleCell {
                realCell.populate("Send copy of program to own email", fieldType: AddPatientEmailController.FieldType.SendCopyToOwnEmail, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }

    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
