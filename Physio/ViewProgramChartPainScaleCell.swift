//
//  ViewProgramChartPainScaleCell.swift
//  Physio
//
//  Created by Vlad Manea on 7/3/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Charts

class ViewProgramChartPainScaleCell: UITableViewCell, ChartViewDelegate, ChartXAxisValueFormatter {
    @IBOutlet weak var lineChartView: LineChartView!
    
    var controller: UIViewController?
    var program: NSDictionary?
    
    func populate(controller: UIViewController, program: NSDictionary) {
        self.controller = controller
        self.program = program
        
        self.setChartStyle()
        
        if let realProgram = self.program {
            var programId: String?
            var lastCreatedAt: String = "0"
            
            if let drives = realProgram["drives"] as? NSArray {
                for driveJson in drives {
                    if let drive = driveJson as? NSDictionary {
                        if let prog = drive["program"] as? NSDictionary {
                            if let createdAt = prog["createdAt"] as? NSString, progrId = prog["id"] as? NSString {
                                if createdAt.compare(lastCreatedAt) == NSComparisonResult.OrderedDescending {
                                    programId = progrId as String
                                    lastCreatedAt = createdAt as String
                                }
                            }
                        }
                    }
                }
            }
            
            if let realProgramId = programId {
                HabitlabRestAPI.getPhysioInspireChartData(realProgramId, handler: { (error, stepsJson) in
                    if let steps = stepsJson as? NSArray {
                        self.setChartData(steps)
                    }
                })
            }
        }
    }
    
    func setChartStyle() {
        dispatch_async(dispatch_get_main_queue()) {
            self.lineChartView.drawGridBackgroundEnabled = false
            self.lineChartView.drawBordersEnabled = false
            self.lineChartView.multipleTouchEnabled = false
            self.lineChartView.dragEnabled = false
            self.lineChartView.setScaleEnabled(false)
            self.lineChartView.pinchZoomEnabled = false
            self.lineChartView.doubleTapToZoomEnabled = false
            self.lineChartView.highlightPerTapEnabled = false
            self.lineChartView.highlightPerDragEnabled = false
            self.lineChartView.descriptionText = ""
            self.lineChartView.animate(xAxisDuration: 1.0)
            self.lineChartView.legend.textColor = UIColor.whiteColor()
            self.lineChartView.infoTextColor = UIColor.whiteColor()
            
            let xAxis = self.lineChartView.xAxis
            xAxis.labelPosition = ChartXAxis.LabelPosition.Bottom
            xAxis.drawGridLinesEnabled = false
            xAxis.drawAxisLineEnabled = false
            xAxis.labelTextColor = UIColor.whiteColor()
            xAxis.avoidFirstLastClippingEnabled = true
            xAxis.valueFormatter = self
            xAxis.setLabelsToSkip(1)
            
            let leftAxis = self.lineChartView.leftAxis
            leftAxis.gridColor = UIColor.whiteColor()
            leftAxis.drawAxisLineEnabled = false
            leftAxis.drawZeroLineEnabled = false
            leftAxis.labelTextColor = UIColor.whiteColor()
            leftAxis.axisMaxValue = 10
            leftAxis.axisMinValue = 0
            leftAxis.spaceTop = 25.0
            leftAxis.spaceBottom = 25.0
            
            let rightAxis = self.lineChartView.rightAxis
            rightAxis.enabled = false
        }
    }
    
    func stringForXValue(index: Int, original: String, viewPortHandler: ChartViewPortHandler) -> String {
        return original
    }
    
    func setChartData(programSteps: NSArray) {
        
        // Create arrays of data entries
        var loops = [String]()
        var painLevelsBefore = [ChartDataEntry]()
        var painLevelsAfters = [ChartDataEntry]()
        
        var maxIndex = -1
        
        for i in 0..<programSteps.count {
            if let programStep = programSteps[i] as? NSDictionary {
                if let _ = programStep["beforePainLevel"] as? NSNumber, _ = programStep["afterPainLevel"] as? NSNumber {
                    maxIndex = i
                }
            }
        }
        
        var lastWeek: Int? = nil
        var lastProgram: String? = nil
        var weekIndex = 1
        
        if maxIndex >= 0 {
            for i in 0...maxIndex {
                var textForLoops = ""
                
                if let programStep = programSteps[i] as? NSDictionary {
                    if let week = programStep["week"] as? NSNumber, program = programStep["program"] as? NSString {
                        if Int(week) != lastWeek || lastProgram == nil || program.compare(lastProgram!) != NSComparisonResult.OrderedSame {
                            textForLoops = "w\(weekIndex)"
                        }
                        
                        lastWeek = Int(week)
                        weekIndex += 1
                        lastProgram = program as String
                    }
                }
                
                loops.append(textForLoops)
                
                if let programStep = programSteps[i] as? NSDictionary {
                    if let beforePainLevel = programStep["beforePainLevel"] as? NSNumber, aftersPainLevel = programStep["afterPainLevel"] as? NSNumber {
                        painLevelsBefore.append(ChartDataEntry(value: Double(beforePainLevel), xIndex: i))
                        painLevelsAfters.append(ChartDataEntry(value: Double(aftersPainLevel), xIndex: i))
                    }
                }
            }
        }
        
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = NSNumberFormatterStyle.NoStyle
        
        // Create data set before with our array
        let setBefore = LineChartDataSet(yVals: painLevelsBefore, label: "Pain level before")
        setBefore.axisDependency = .Left
        setBefore.setColor(UIColor.redColor().colorWithAlphaComponent(0.5)) // our line's opacity is 50%
        setBefore.setCircleColor(UIColor.redColor()) // our circle will be dark red
        setBefore.lineWidth = 2.0
        setBefore.circleRadius = 3.0 // the radius of the node circle
        setBefore.fillAlpha = 65 / 255.0
        setBefore.fillColor = UIColor.redColor()
        setBefore.highlightColor = UIColor.whiteColor()
        setBefore.valueFormatter = numberFormatter
        setBefore.drawValuesEnabled = false
        setBefore.mode = LineChartDataSet.Mode.CubicBezier
        setBefore.drawCircleHoleEnabled = false
        
        // Create data set after with our array
        let setAfter = LineChartDataSet(yVals: painLevelsAfters, label: "Pain level after")
        setAfter.axisDependency = .Left
        setAfter.setColor(UIColor.greenColor().colorWithAlphaComponent(0.5)) // our line's opacity is 50%
        setAfter.setCircleColor(UIColor.greenColor()) // our circle will be dark red
        setAfter.lineWidth = 2.0
        setAfter.circleRadius = 3.0 // the radius of the node circle
        setAfter.fillAlpha = 65 / 255.0
        setAfter.fillColor = UIColor.greenColor()
        setAfter.highlightColor = UIColor.whiteColor()
        setAfter.valueFormatter = numberFormatter
        setAfter.drawValuesEnabled = false
        setAfter.mode = LineChartDataSet.Mode.CubicBezier
        setAfter.drawCircleHoleEnabled = false
        
        // Create an array to store our LineChartDataSets
        var dataSets = [LineChartDataSet]()
        dataSets.append(setBefore)
        dataSets.append(setAfter)
        
        // Pass our months in for our x-axis label value along with our dataSets
        let data: LineChartData = LineChartData(xVals: loops, dataSets: dataSets)
        data.setValueTextColor(UIColor.whiteColor())
        
        dispatch_async(dispatch_get_main_queue()) {
            self.lineChartView.data = data
        }
    }
}
