//
//  NavigationControllerDelegate.swift
//  Physio
//
//  Created by David on 30/06/2016.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class NavigationControllerDelegate: NSObject,
UINavigationControllerDelegate {
    
    func navigationController(
        navigationController: UINavigationController,
        animationControllerForOperation operation:
        UINavigationControllerOperation,
                                        fromViewController fromVC: UIViewController,
                                                           toViewController toVC: UIViewController
        ) -> UIViewControllerAnimatedTransitioning? {
        
        return FadeInAnimator()
    }
}