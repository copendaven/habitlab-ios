//
//  PhotoCache.swift
//  Physio
//
//  Created by Vlad Manea on 2/25/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import CoreData

class PhotoCache: NSObject {
    private static var object: NSObject = NSObject()
    private static var caches: NSMutableDictionary = NSMutableDictionary()
    
    private func cleanup() {
        if let calendar = Serv.calendar {
            if let earlyDate = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: -10, toDate: NSDate(), options: NSCalendarOptions.WrapComponents) {
                let ids = NSMutableArray()
                
                for (id, cacheObject) in PhotoCache.caches {
                    if let cache = cacheObject as? NSDictionary {
                        if let createdAt = cache["cachedAt"] as? NSDate {
                            if createdAt.compare(earlyDate) == NSComparisonResult.OrderedAscending {
                                ids.addObject(id)
                            }
                        }
                    }
                }
                
                for id in ids {
                    PhotoCache.caches.removeObjectForKey(id)
                }
            }
        }
    }
    
    static func erase() {
        objc_sync_enter(PhotoCache.object)
        PhotoCache.caches.removeAllObjects()
        objc_sync_exit(PhotoCache.object)
    }
    
    func setCache(id: String, image: NSData) {
        objc_sync_enter(PhotoCache.object)
        
        cleanup()
        
        let newPhoto = NSMutableDictionary()
        newPhoto.setObject(id, forKey: "id")
        newPhoto.setObject(image, forKey: "image")
        newPhoto.setObject(NSDate(), forKey: "cachedAt")
        
        PhotoCache.caches.setObject(newPhoto, forKey: id)
        
        objc_sync_exit(PhotoCache.object)
    }
    
    func getCache(id: String) -> NSData? {
        objc_sync_enter(PhotoCache.object)
        
        if let cacheJson = PhotoCache.caches.objectForKey(id) {
            if let cache = cacheJson as? NSDictionary {
                if let data = cache["image"] as? NSData {
                    objc_sync_exit(PhotoCache.object)
                    return data
                }
            }
        }
        
        objc_sync_exit(PhotoCache.object)
        return nil
    }
}
