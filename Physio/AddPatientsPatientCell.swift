//
//  AddPatientsPatientCell.swift
//  Physio
//
//  Created by Vlad Manea on 7/3/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddPatientsPatientCell: UITableViewCell {
    private var controller: UIViewController?
    private var program: NSDictionary?
    
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var lastAction: UILabel!
    @IBOutlet weak var programTitle: UILabel!

    @IBAction func didClickPatient(sender: AnyObject) {
        if let viewController = self.controller as? AddPatientsController, realProgram = self.program {
            viewController.didClickPatient(realProgram)
        }
    }
    
    func populate(program: NSDictionary, controller: UIViewController) {
        self.program = program
        self.controller = controller
        
        if let title = program["title"] as? NSString {
            self.programTitle.text = title as String
        }
        
        if let physioPatient = program["physioPatient"] as? NSDictionary {
            if let firstName = physioPatient["firstName"] as? NSString, lastName = physioPatient["lastName"] as? NSString {
                self.patientName.text = "\(firstName) \(lastName)"
            }
        }
        
        var sharedWithPhysio = false
        
        if let shareProgressWithPhysiotherapist = program["shareProgressWithPhysiotherapist"] as? NSNumber {
            if shareProgressWithPhysiotherapist == 1 {
                sharedWithPhysio = true
            }
        }
        
        var lastActionText = ""
        
        if sharedWithPhysio {
            lastActionText = "Not started yet"
        } else {
            lastActionText = "No info available"
        }
        
        var theLevel: NSNumber?
        var theWeek: NSNumber?
        var theIndex: NSNumber?
        var action: String?
        
        if sharedWithPhysio {
            if let drives = program["drives"] as? NSArray {
                var lastStepDate = "0"
                
                for driveJson in drives {
                    if let drive = driveJson as? NSDictionary {
                        if let program = drive["program"] as? NSDictionary, level = drive["level"] as? NSNumber {
                            if let steps = program["steps"] as? NSArray {
                                lastActionText = "Started level \(level)"
                                
                                for stepJson in steps {
                                    if let step = stepJson as? NSDictionary {
                                        if let week = step["week"] as? NSNumber, index = step["index"] as? NSNumber {
                                            if let startedAt = step["startedAt"] as? NSString {
                                                if startedAt.compare(lastStepDate) == NSComparisonResult.OrderedDescending {
                                                    lastStepDate = startedAt as String
                                                    theLevel = level
                                                    theWeek = week
                                                    theIndex = index
                                                    action = "Started"
                                                }
                                            }
                                            
                                            if let completedAt = step["completedAt"] as? NSString {
                                                if completedAt.compare(lastStepDate) == NSComparisonResult.OrderedDescending {
                                                    lastStepDate = completedAt as String
                                                    theLevel = level
                                                    theWeek = week
                                                    theIndex = index
                                                    action = "Did"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if let realLevel = theLevel, realWeek = theWeek, realIndex = theIndex, realAction = action {
            lastActionText = "\(realAction) loop \(realIndex), week \(Int(realWeek) + 1), level \(realLevel)"
        }
        
        self.lastAction.text = lastActionText
    }
}
