//
//  VideoOptionsController.swift
//  Physio
//
//  Created by Vlad Manea on 5/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import AVFoundation
import AVKit

class VideoOptionsController: UIViewController {
    var player: AVPlayer?
    var video: NSDictionary?
    var controller: UIViewController?
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.videoOptionsController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    @IBOutlet weak var photoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh() {
        if let realVideo = self.video {
            dispatch_async(self.serialQueue) {
                if let urlString = realVideo["urlMovie"] as? String {
                    if let url = NSURL(string: urlString) {
                        let asset = AVURLAsset(URL: url, options: nil)
                        let imgGenerator = AVAssetImageGenerator(asset: asset)
                        imgGenerator.appliesPreferredTrackTransform = true
                        
                        do {
                            let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
                            let uiImage = UIImage(CGImage: cgImage)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.photoImage.image = uiImage
                            })
                        } catch {
                            // Do nothing...
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func clickedVideo(sender: AnyObject) {
        if let realVideo = self.video {
            if let urlString = realVideo["urlMovie"] as? String {
                if let url = NSURL(string: urlString) {
                    self.performSegueWithIdentifier("showVideoFromVideoOptions", sender: url)
                }
            }
        }
    }
    
    @IBAction func deleteVideo(sender: AnyObject) {
        let confirmDeleteAlert = UIAlertController(title: "Remove", message: "Remove this photo?", preferredStyle: UIAlertControllerStyle.Alert)
        
        confirmDeleteAlert.addAction(UIAlertAction(title: "Yes", style: .Destructive, handler: { (action: UIAlertAction!) in
            if let realVideo = self.video {
                if let urlString = realVideo["urlMovie"] as? String {
                    if let url = NSURL(string: urlString) {
                        let fileManager = NSFileManager.defaultManager()
                        
                        do {
                            try fileManager.removeItemAtURL(url)
                            self.performSegueWithIdentifier("showAddExerciseFromVideoOptions", sender: realVideo)
                        } catch {
                            // Do nothing.
                        }
                    }
                }
            }
        }))
        
        confirmDeleteAlert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: { (action: UIAlertAction!) in
            // Do nothing.
        }))
        
        self.presentViewController(confirmDeleteAlert, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "showAddExerciseFromVideoOptions" {
            if let viewController = segue.destinationViewController as? AddExerciseController, realVideo = sender as? NSDictionary {
                viewController.deleteMedia(realVideo)
            }
        }
        
        if segue.identifier == "showVideoFromVideoOptions" {
            if let viewController = segue.destinationViewController as? VideoController, realUrl = sender as? NSURL {
                viewController.url = realUrl
            }
        }
    }
}
