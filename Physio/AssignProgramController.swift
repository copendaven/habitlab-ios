//
//  AssignProgramController.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AssignProgramController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private static let serialQueue = dispatch_queue_create("me.habitlab.physio.assignProgramController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    private static var patients = NSMutableArray()
    private var selectedPatient: NSMutableDictionary?
    
    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var patientsTableView: UITableView!
    @IBOutlet weak var patientsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addImage.layer.shadowColor = UIColor.blackColor().CGColor
        self.addImage.layer.shadowOffset = CGSizeMake(1, 1)
        self.addImage.layer.shadowRadius = 2
        self.addImage.layer.shadowOpacity = 0.4
        
        patientsTableView.delegate = self
        patientsTableView.dataSource = self
        
        dispatch_async(dispatch_get_main_queue()) {
            self.patientsTableView.estimatedRowHeight = 80
            self.patientsTableView.rowHeight = UITableViewAutomaticDimension
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            
            self.refreshFromServer()
        }
    }
    
    func refreshFromServer() {
        dispatch_async(AssignProgramController.serialQueue) {
            Serv.showSpinner()
            
            HabitlabRestAPI.getPatients { (error, patientsJson) in
                Serv.hideSpinner()
                
                if error != nil {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                if let realPatients = patientsJson as? NSArray {
                    dispatch_async(dispatch_get_main_queue(), {
                        AssignProgramController.patients = NSMutableArray(array: realPatients)
                        self.refresh(true)
                    })
                }
            }
        }
    }
    
    @IBAction func clickedAddNewPatient() {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showAddPatientFromAssignProgram", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    
        self.refresh(false)
    }
    
    func refresh(reloadData: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            if (reloadData) {
                self.patientsTableView.reloadData()
            }
            
            if AssignProgramController.patients.count > 0 {
                self.patientsTableView.hidden = false
                self.patientsLabel.hidden = true
            } else {
                self.patientsTableView.hidden = true
                self.patientsLabel.hidden = false
            }
        }
    }

    @IBAction func assignProgramControllerAddedPatient(segue: UIStoryboardSegue) {
        self.refresh(true)
    }
    
    func selectPatient(patient: NSMutableDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            self.selectedPatient = patient
            self.refresh(true)
            self.performSegueWithIdentifier("showAddExercisesFromAssignProgram", sender: self)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    static func addPatient(patient: NSDictionary) {
        dispatch_async(AssignProgramController.serialQueue) {
            if let firstName = patient["firstName"] as? String, lastName = patient["lastName"] as? String, email = patient["email"] as? String {
                Serv.showSpinner()
                
                HabitlabRestAPI.postPatient(email, firstName: firstName, lastName: lastName, handler: { (error, patientJson) in
                    Serv.hideSpinner()
                })
            }
        }
    }
    
    // MARK: Unwind segues
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return AssignProgramController.patients.count + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == AssignProgramController.patients.count {
            return defaultCell
        }
        
        if let _ = AssignProgramController.patients[indexPath.section] as? NSDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("assignProgramPatientCell", forIndexPath: indexPath) as? AssignProgramPatientCell {
                return cell
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == AssignProgramController.patients.count {
            if let realCell = cell as? AssignProgramAddNewPatientCell {
                realCell.populate(self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if let patient = AssignProgramController.patients[indexPath.section] as? NSMutableDictionary {
            if let realCell = cell as? AssignProgramPatientCell {
                var isSelected = false
                
                if let realSelectedPatient = self.selectedPatient {
                    if let selectedEmail = realSelectedPatient["email"] as? NSString, patientEmail = patient["email"] as? NSString {
                        if (selectedEmail as String).compare(patientEmail as String) == NSComparisonResult.OrderedSame {
                            isSelected = true
                        }
                    }
                }
                
                realCell.populate(patient, selected: isSelected, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if (segue.identifier == "showAddExercisesFromAssignProgram") {
            if let viewController = segue.destinationViewController as? AddExercisesController, realPatient = self.selectedPatient {
                viewController.patient = realPatient
            }
        }
    }
}
