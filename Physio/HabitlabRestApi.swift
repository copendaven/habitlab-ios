//
//  HabitlabRestAPI.swift
//  Physio
//
//  Created by Vlad Manea on 12/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit

class HabitlabRestAPI: NSObject {
    static let Endpoint: String = Configuration.getEndpoint() + "/rest/ios"
    static let EndpointAPI: String = Configuration.getEndpoint() + "/rest/physio"
    
    static func getPrograms(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/program")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    
    static func getPhysioInspireChartData(programId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(Endpoint + "/physioinspirechartdata/\(programId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func isValidUnlimitedProgramsSubscription(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/isvalidunlimitedprogramssubscription")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func getPatients(handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/patient")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "GET"
        HabitlabHttpServer.startTask(request!, shouldBlock: false, handler: handler)
    }
    
    static func postPatient(email: String, firstName: String, lastName: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/patient")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["email": email, "firstName": firstName, "lastName": lastName] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postPatientPhoto(email: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/patient-photo")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["email": email] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postProgram(program: NSMutableDictionary, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/program")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        
        let body = ["program": program] as Dictionary<String, AnyObject>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func putPhysiotherapistWorkplace(workplace: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/physiotherapistworkplace")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "PUT"
        
        let body = ["physioTherapistWorkplace": workplace] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func deletePatient(patientId: String, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/patient/\(patientId)")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "DELETE"
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    static func postValidateReceipt(receiptData: NSString, handler: (NSError?, AnyObject?) -> ()) {
        var request: NSMutableURLRequest?
        
        do {
            request = try HabitlabAuthentication.requestDecoratedWithHabitlabToken(EndpointAPI + "/receipt")
        } catch let error as NSError {
            handler(error, nil)
            return
        }
        
        request!.HTTPMethod = "POST"
        let body = ["receipt": receiptData as String] as Dictionary<String, String>
        
        do {
            try request!.HTTPBody = NSJSONSerialization.dataWithJSONObject(body, options: .PrettyPrinted)
        } catch let error as NSError {
            return handler(error, nil)
        }
        
        HabitlabHttpServer.startTask(request!, shouldBlock: true, handler: handler)
    }
    
    private static func getVersion() -> Int {
        var version = 1
        
        if let dictionary = NSBundle.mainBundle().infoDictionary {
            if let realVersion = dictionary["CFBundleVersion"] as? String {
                version = Int(realVersion)!
            }
        }
        
        return version
    }
}
