//
//  AddExerciseTimerDurationCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/28/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddExerciseTimerDurationCell: UITableViewCell {
    private var controller: UIViewController?
    private var timerType: AddExerciseController.TimerFieldType?
    private var minValue: Int?
    private var maxValue: Int?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timer: UIDatePicker!
    
    func populate(labelText: String, labelPlaceholderText: String, minValue: Int, maxValue: Int, timerType: AddExerciseController.TimerFieldType, controller: UIViewController) {

        self.controller = controller
        self.timerType = timerType
        self.minValue = minValue
        self.maxValue = maxValue
        
      //  self.titleLabel.text = labelText
        
        //self.timer.countDownDuration = defaultCountMinutes
        self.updateController()
    }
    
    @IBAction func timerDidChange(sender: AnyObject) {
        dispatch_async(dispatch_get_main_queue()) {
            self.updateController()
        }
    }
    
    private func updateController() {
        if let realController = self.controller, realTimerType = self.timerType {
            if let viewController = realController as? AddExerciseController {
                viewController.handleExerciseTimerUpdate(self.timer.countDownDuration, timerType: realTimerType)
            }
        }
    }
}
