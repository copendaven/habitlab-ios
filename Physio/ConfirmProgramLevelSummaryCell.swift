//
//  ConfirmProgramLevelSummaryCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ConfirmProgramLevelSummaryCell: UITableViewCell {

    @IBOutlet weak var countWeeksLabel: UILabel!
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var levelTitleLabel: UILabel!
    
    func populate(program: NSDictionary, level: Int) {
        countWeeksLabel.text = ""
        indexLabel.text = ""
        levelTitleLabel.text = ""
        
        if let drives = program["drives"] as? NSArray {
            if drives.count > level {
                if let drive = drives[level] as? NSDictionary {
                    levelTitleLabel.text = "Level \(1 + level)"
                    indexLabel.text = "Level \(1 + level) out of \(drives.count)"
                    
                    if let countWeeks = drive["countWeeks"] as? Int {
                        countWeeksLabel.text = "\(countWeeks) week\(countWeeks == 1 ? "" : "s")"
                    }
                }
            }
        }
    }
}
