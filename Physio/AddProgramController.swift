//
//  AddProgramController.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddProgramController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate {
    var exercises: NSArray?
    var patient: NSMutableDictionary?
    
    enum SliderFieldType {
        // No cases.
    }
    
    enum NumericFieldType {
        case CountLoopsPerDay
        case CountLoopsPerWeek
        case CountWeeks
    }
    
    enum DailyWeeklyLoopsState {
        case DailyLoops
        case WeeklyLoops
    }
    
    var dailyWeeklyLoopsState = DailyWeeklyLoopsState.DailyLoops
    var countLoopsPerDay: Int?
    var countLoopsPerWeek: Int?
    var countWeeks: Int?
    
    var minCountLoopsPerDay = 1
    var defaultCountLoopsPerDay = 1
    var maxCountLoopsPerDay = 10
    
    var minCountLoopsPerWeek = 1
    var defaultCountLoopsPerWeek = 1
    var maxCountLoopsPerWeek = 7
    
    var minCountWeeks = 1
    var defaultCountWeeks = 1
    var maxCountWeeks = 52
    
    @IBOutlet weak var patientImageLabel: UILabel!
    @IBOutlet weak var patientImage: UIImageView!
    @IBOutlet weak var fieldsTableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fieldsTableView.delegate = self
        self.fieldsTableView.dataSource = self
        
        self.fieldsTableView.estimatedRowHeight = 110
        self.fieldsTableView.rowHeight = UITableViewAutomaticDimension
        
        patientImage.layer.cornerRadius = patientImage.frame.height / 2
        patientImage.layer.borderWidth = 5
        patientImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        patientImageLabel.layer.cornerRadius = patientImageLabel.frame.height / 2
        patientImageLabel.layer.borderWidth = 5
        patientImageLabel.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddProgramController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddProgramController.keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddProgramController.keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        dailyWeeklyLoopsState = DailyWeeklyLoopsState.DailyLoops
        
        self.populatePhoto()
        self.refresh()
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func populatePhoto() {
        if let physioPatient = self.patient {
            var fullName = ""
            
            if let firstName = physioPatient["firstName"] as? NSString {
                let firstNameString = firstName as String
                
                if firstNameString.characters.count > 0 {
                    fullName = fullName + "\(firstNameString[firstNameString.startIndex.advancedBy(0)])"
                }
            }
            
            if let lastName = physioPatient["lastName"] as? NSString {
                let lastNameString = lastName as String
                
                if lastNameString.characters.count > 0 {
                    fullName = fullName + "\(lastNameString[lastNameString.startIndex.advancedBy(0)])"
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.patientImageLabel.alpha = 0.0
                self.patientImage.alpha = 0.0
                
                self.patientImageLabel.text = fullName.uppercaseString
                self.patientImageLabel.alpha = 1.0
            })
            
            if let email = physioPatient["email"] as? NSString {
                HabitlabRestAPI.postPatientPhoto(email as String, handler: { (error, objectJson) in
                    if let object = objectJson as? NSDictionary {
                        if let photo = object["photo"] as? NSString {
                            Serv.loadImage(photo as String, handler: { (error, data) -> () in
                                if error != nil {
                                    print(error)
                                }
                                
                                if data != nil {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.patientImage.image = UIImage(data: data!)
                                        
                                        UIView.animateWithDuration(0.35, animations: {
                                            self.patientImage.alpha = 1.0
                                            self.patientImageLabel.alpha = 0.0
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh() {
        dispatch_async(dispatch_get_main_queue()) {
            var countLoopsPerDayHasValue = false
            var countLoopsPerWeekHasValue = false
            var countWeeksHasValue = false
            
            switch self.dailyWeeklyLoopsState {
            case .DailyLoops:
                if let realCountLoopsPerDay = self.countLoopsPerDay {
                    if self.minCountLoopsPerDay <= realCountLoopsPerDay && realCountLoopsPerDay <= self.maxCountLoopsPerDay {
                        countLoopsPerDayHasValue = true
                    }
                }
                
                let indexPath = NSIndexPath(forRow: 0, inSection: 1)
                
                if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath) as? AddProgramNumericCell {
                    realCell.validate(countLoopsPerDayHasValue)
                }
            case .WeeklyLoops:
                if let realCountLoopsPerWeek = self.countLoopsPerWeek {
                    if self.minCountLoopsPerWeek <= realCountLoopsPerWeek && realCountLoopsPerWeek <= self.maxCountLoopsPerWeek {
                        countLoopsPerWeekHasValue = true
                    }
                }
                
                let indexPath = NSIndexPath(forRow: 0, inSection: 1)
                
                if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath) as? AddProgramNumericCell {
                    realCell.validate(countLoopsPerWeekHasValue)
                }
            }
            
            if self.minCountWeeks <= self.countWeeks && self.countWeeks <= self.maxCountWeeks {
                countWeeksHasValue = true
            }
            
            let indexPath = NSIndexPath(forRow: 0, inSection: 2)
            
            if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath) as? AddProgramNumericCell {
                realCell.validate(countWeeksHasValue)
            }
            
            if countLoopsPerDayHasValue || countLoopsPerWeekHasValue {
                let indexPath = NSIndexPath(forRow: 0, inSection: 2)
                
                if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath) as? AddProgramNumericCell {
                    realCell.enable(true)
                }
            } else {
                let indexPath = NSIndexPath(forRow: 0, inSection: 2)
                
                if let realCell = self.fieldsTableView.cellForRowAtIndexPath(indexPath) as? AddProgramNumericCell {
                    realCell.enable(false)
                }
            }
            
            if countWeeksHasValue && (countLoopsPerWeekHasValue || countLoopsPerDayHasValue) {
                self.doneButton.enabled = true
                self.doneButton.alpha = 1.0
            } else {
                self.doneButton.enabled = false
                self.doneButton.alpha = 0.5
            }
        }
    }
    
    @IBAction func didClickDone() {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showAddProgramTitleFromAddProgram", sender: self)
        }
    }
    
    func handleProgramNumericUpdate(value: Int?, numericType: AddProgramController.NumericFieldType) {
        switch numericType {
        case .CountLoopsPerDay:
            self.handleProgramCountLoopsPerDay(value)
        case .CountLoopsPerWeek:
            self.handleProgramCountLoopsPerWeek(value)
        case .CountWeeks:
            self.handleProgramCountWeeks(value)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let info  = notification.userInfo, realTabBarController = self.tabBarController {
            let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]!
            let rawFrame = value.CGRectValue
            let keyboardFrame = view.convertRect(rawFrame, fromView: nil)
            let keyboardHeight = keyboardFrame.height + 70 // For the extra view on top of the keyboard
            let tabBarHeight = realTabBarController.tabBar.frame.size.height
            self.view.frame.origin.y = (tabBarHeight - keyboardHeight) * 0.75
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    func setDailyLoopsState() {
        dispatch_async(dispatch_get_main_queue()) {
            self.dailyWeeklyLoopsState = DailyWeeklyLoopsState.DailyLoops
            let indexPath = NSIndexPath(forRow: 0, inSection: 1)
            self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            self.refresh()
        }
    }
    
    func setWeeklyLoopsState() {
        dispatch_async(dispatch_get_main_queue()) {
            self.dailyWeeklyLoopsState = DailyWeeklyLoopsState.WeeklyLoops
            let indexPath = NSIndexPath(forRow: 0, inSection: 1)
            self.fieldsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            self.refresh()
        }
    }
    
    private func handleProgramCountLoopsPerDay(value: Int?) {
        if let realValue = value {
            dispatch_async(dispatch_get_main_queue(), {
                self.countLoopsPerDay = realValue
                self.refresh()
            })
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                self.countLoopsPerDay = nil
                self.refresh()
            })
        }
    }
    
    private func handleProgramCountLoopsPerWeek(value: Int?) {
        if let realValue = value {
            dispatch_async(dispatch_get_main_queue(), {
                self.countLoopsPerWeek = realValue
                self.refresh()
            })
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                self.countLoopsPerWeek = nil
                self.refresh()
            })
        }
    }
    
    private func handleProgramCountWeeks(value: Int?) {
        if let realValue = value {
            dispatch_async(dispatch_get_main_queue(), {
                self.countWeeks = realValue
                self.refresh()
            })
        } else {
            dispatch_async(dispatch_get_main_queue(), {
                self.countWeeks = nil
                self.refresh()
            })
        }
    }
    
    private func buildDrive() -> NSMutableDictionary {
        let drive = NSMutableDictionary()
        
        let cacheId = NSUUID().UUIDString
        drive.setObject(cacheId, forKey: "cacheId")
        
        if let realExercises = self.exercises {
            drive.setObject(realExercises, forKey: "physioExercises")
        }
        
        if let realCountLoopsPerDay = self.countLoopsPerDay {
            drive.setObject(realCountLoopsPerDay, forKey: "countStepsPerDay")
        }
        
        if let realCountLoopsPerWeek = self.countLoopsPerWeek {
            drive.setObject(realCountLoopsPerWeek, forKey: "countStepsPerWeek")
        }
        
        if let realCountWeeks = self.countWeeks {
            drive.setObject(realCountWeeks, forKey: "countWeeks")
        }
        
        if let user = UserCache().getUser() {
            if let id = user["id"] as? NSString {
                drive.setObject(id as String, forKey: "owner")
            }
        }
        
        return drive
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "showAddProgramTitleFromAddProgram" {
            if let viewController = segue.destinationViewController as? AddProgramTitleController {
                viewController.driveTemplate = self.buildDrive()
                viewController.patient = self.patient
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
//    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let invisibleView = UIView.init()
//        invisibleView.backgroundColor = UIColor.clearColor()
//        return invisibleView
//    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            switch self.dailyWeeklyLoopsState {
            case .DailyLoops, .WeeklyLoops:
                if let cell = tableView.dequeueReusableCellWithIdentifier("addProgramChooseDailyOrWeeklyLoopsCell", forIndexPath: indexPath) as? AddProgramChooseDailyOrWeeklyLoopsCell {
                    return cell
                }
            }
            
            return defaultCell
        }
        
        if indexPath.section == 1 || indexPath.section == 2 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addProgramNumericCell", forIndexPath: indexPath) as? AddProgramNumericCell {
                return cell
            }
            
            return defaultCell
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            if let realCell = cell as? AddProgramChooseDailyOrWeeklyLoopsCell {
                realCell.populate(self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if indexPath.section == 1 {
            if let realCell = cell as? AddProgramNumericCell {
                switch self.dailyWeeklyLoopsState {
                case .DailyLoops:
                    realCell.populate("Number of loops per day", labelPlaceholderText: "Min. \(minCountLoopsPerDay), max. \(maxCountLoopsPerDay)", minValue: minCountLoopsPerDay, defaultValue: defaultCountLoopsPerDay, maxValue: maxCountLoopsPerDay, numericType: AddProgramController.NumericFieldType.CountLoopsPerDay, enabled: true, controller: self)
                case .WeeklyLoops:
                    realCell.populate("Number of loops per week", labelPlaceholderText: "Min. \(minCountLoopsPerWeek), max. \(maxCountLoopsPerWeek)", minValue: minCountLoopsPerWeek, defaultValue: defaultCountLoopsPerWeek, maxValue: maxCountLoopsPerWeek, numericType: AddProgramController.NumericFieldType.CountLoopsPerWeek, enabled: true, controller: self)
                }
                
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if indexPath.section == 2 {
            if let realCell = cell as? AddProgramNumericCell {
                realCell.populate("Program length in weeks", labelPlaceholderText: "Min. \(minCountWeeks), max. \(maxCountWeeks)", minValue: minCountWeeks, defaultValue: defaultCountWeeks, maxValue: maxCountWeeks, numericType: AddProgramController.NumericFieldType.CountWeeks, enabled: false, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
