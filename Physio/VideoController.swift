//
//  VideoController.swift
//  Physio
//
//  Created by Vlad Manea on 5/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import AVKit
import AVFoundation

class VideoController: AVPlayerViewController {
    var url: NSURL?
    
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.videoController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh() {
        if let url = self.url {
            self.player = AVPlayer(URL: url)
        }
    }
}
