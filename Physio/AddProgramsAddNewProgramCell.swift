//
//  AddProgramsAddNewProgramCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddProgramsAddNewProgramCell: UITableViewCell {
    private var controller: UIViewController?
    
    func populate(controller: UIViewController) {
        self.controller = controller
    }
    
    @IBAction func clickedAddNewExercise(sender: AnyObject) {
        if let realController = self.controller {
            if let viewController = realController as? AddProgramsController {
                viewController.clickedAddNewProgram()
            }
        }
    }
}
