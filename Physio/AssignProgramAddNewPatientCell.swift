//
//  AssignProgramAddNewPatientCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AssignProgramAddNewPatientCell: UITableViewCell {
    private var controller: UIViewController?
    
    func populate(controller: UIViewController) {
        self.controller = controller
    }
    
    @IBAction func clickedAddNewPatient(sender: AnyObject) {
        if let realController = self.controller {
            if let viewController = realController as? AssignProgramController {
                viewController.clickedAddNewPatient()
            }
        }
    }
}
