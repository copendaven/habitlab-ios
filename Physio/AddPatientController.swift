//
//  AddPatientController.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddPatientController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate {
    enum TextFieldType {
        case FirstName
        case LastName
    }
    
    var patientFirstName: String?
    var patientLastName: String?
    
    
    @IBOutlet weak var profileImageLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fieldsTableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fieldsTableView.delegate = self
        self.fieldsTableView.dataSource = self
        
        self.fieldsTableView.estimatedRowHeight = 110
        self.fieldsTableView.rowHeight = UITableViewAutomaticDimension
        
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.layer.borderWidth = 5
        profileImage.layer.borderColor = UIColor.whiteColor().CGColor

        profileImageLabel.layer.cornerRadius = profileImageLabel.frame.height / 2
        profileImageLabel.layer.borderWidth = 5
        profileImageLabel.layer.borderColor = UIColor.whiteColor().CGColor

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddPatientController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddPatientController.keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddPatientController.keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refresh()
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh() {
        dispatch_async(dispatch_get_main_queue()) {
            var patientFirstNameHasText = false
            var patientLastNameHasText = false
            
            var initialsString = ""
            
            if let realPatientFirstName = self.patientFirstName {
                if realPatientFirstName.characters.count > 0 {
                    patientFirstNameHasText = true
                    initialsString += "\(realPatientFirstName[realPatientFirstName.startIndex.advancedBy(0)])"
                }
            }
            
            if let realPatientLastName = self.patientLastName {
                if realPatientLastName.characters.count > 0 {
                    patientLastNameHasText = true
                    initialsString += "\(realPatientLastName[realPatientLastName.startIndex.advancedBy(0)])"
                }
            }
            
            self.profileImageLabel.text = initialsString.uppercaseString
            
            if initialsString.characters.count > 0 {
                UIView.animateWithDuration(0.35, animations: {
                    self.profileImageLabel.alpha = 1.0
                })
            } else {
                UIView.animateWithDuration(0.35, animations: {
                    self.profileImageLabel.alpha = 0.0
                })
            }
            
            if patientFirstNameHasText && patientLastNameHasText {
                self.doneButton.enabled = true
                self.doneButton.alpha = 1.0
                
                
            } else {
                self.doneButton.enabled = false
                self.doneButton.alpha = 0.5
            }
        }
    }
    
    @IBAction func clickedDone(sender: AnyObject) {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showAddExercisesFromAddPatient", sender: self)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let info  = notification.userInfo, realTabBarController = self.tabBarController {
            let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]!
            let rawFrame = value.CGRectValue
            let keyboardFrame = view.convertRect(rawFrame, fromView: nil)
            let keyboardHeight = keyboardFrame.height + 70 // For the extra view on top of the keyboard
            let tabBarHeight = realTabBarController.tabBar.frame.size.height
            self.view.frame.origin.y = (tabBarHeight - keyboardHeight) * 0.75
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    func handlePatientFieldUpdate(text: String, fieldType: AddPatientController.TextFieldType) {
        switch fieldType {
        case .FirstName:
            self.handlePatientFirstName(text)
        case .LastName:
            self.handlePatientLastName(text)
        }
    }
    
    private func handlePatientFirstName(text: String) {
        dispatch_async(dispatch_get_main_queue(), {
            self.patientFirstName = text
            self.refresh()
        })
    }
    
    private func handlePatientLastName(text: String) {
        dispatch_async(dispatch_get_main_queue(), {
            self.patientLastName = text
            self.refresh()
        })
    }
    
    private func buildPatient() -> NSMutableDictionary {
        let patient = NSMutableDictionary()
        
        let cacheId = NSUUID().UUIDString
        patient.setObject(cacheId, forKey: "cacheId")
        
        if let realFirstName = self.patientFirstName {
            patient.setObject(realFirstName, forKey: "firstName")
        }
        
        if let realLastName = self.patientLastName {
            patient.setObject(realLastName, forKey: "lastName")
        }
        
        return patient
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if segue.identifier == "showAddExercisesFromAddPatient" {
            if let viewController = segue.destinationViewController as? AddExercisesController {
                let patient = self.buildPatient()
                viewController.patient = patient
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 || indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addPatientFieldCell", forIndexPath: indexPath) as? AddPatientFieldCell {
                return cell
            }
            
            return defaultCell
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            if let realCell = cell as? AddPatientFieldCell {
                realCell.populate("First Name", labelPlaceholderText: "", fieldType: AddPatientController.TextFieldType.FirstName, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if indexPath.section == 1 {
            if let realCell = cell as? AddPatientFieldCell {
                realCell.populate("Last Name", labelPlaceholderText: "", fieldType: AddPatientController.TextFieldType.LastName, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
