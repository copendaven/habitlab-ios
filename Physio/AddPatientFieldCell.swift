//
//  AddPatientFieldCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddPatientFieldCell: UITableViewCell {
    private var controller: UIViewController?
    private var fieldType: AddPatientController.TextFieldType?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextField!
    
    func populate(labelText: String, labelPlaceholderText: String, fieldType: AddPatientController.TextFieldType, controller: UIViewController) {
        self.titleLabel.text = labelText
        self.controller = controller
        self.fieldType = fieldType
        
        contentTextField.addTarget(self, action: #selector(AddPatientFieldCell.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)

        contentTextField.placeholder = labelPlaceholderText
        
        contentTextField.textColor = UIColor.init(red: 44/255, green: 180/255, blue: 170/255, alpha: 1)
        
        if let realController = controller as? AddPatientController {
            contentTextField.delegate = realController
        }
        
        let doneToolbar = self.constructInputAccessoryView()
        doneToolbar.sizeToFit()
        self.contentTextField.inputAccessoryView = doneToolbar
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddPatientFieldCell.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func doneButtonAction() {
        if let realController = self.controller as? AddPatientController {
            realController.view.endEditing(true)
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        dispatch_async(dispatch_get_main_queue()) {
            if let realText = textField.text {
                if let realController = self.controller, realFieldType = self.fieldType {
                    if let viewController = realController as? AddPatientController {
                        viewController.handlePatientFieldUpdate(realText, fieldType: realFieldType)
                    }
                }
            }
        }
    }
}
