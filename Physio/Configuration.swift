//
//  Configuration.swift
//  Physio
//
//  Created by Vlad Manea on 2/14/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import AWSCognito
import Firebase

let environment = Environment.Release
 
class Configuration: NSObject {
    static func getEndpoint() -> String {
        switch environment {
        case Environment.Development:
            return "http://localhost:1337"
        case Environment.Staging:
            return "http://habitlab-staging.herokuapp.com"
        case Environment.Release:
            return "https://www.habitlab.me"
        }
    }
    
    static func getS3ImageBucket() -> String {
        switch environment {
        case Environment.Development:
            return "habitlabphysiodevelopment"
        case Environment.Staging:
            return "habitlabphysiostaging"
        case Environment.Release:
            return "habitlabphysiorelease"
        }
    }
    
    static func activateFacebookApp() {
        FBSDKAppEvents.activateApp()
    }
    
        
    static func initializeFirebase() {
        FIRApp.configure()
    }
    
    static func initializeAmazonCognito() {
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.EUWest1, identityPoolId:"eu-west-1:0caf0f9b-ba60-4f60-a067-4b7325da3a0d")
        
        let defaultServiceConfiguration = AWSServiceConfiguration(region:.EUWest1, credentialsProvider:credentialsProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = defaultServiceConfiguration
    }
    
    static func initializeFacebook(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    static func initializeFacebook(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) {
        FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
}