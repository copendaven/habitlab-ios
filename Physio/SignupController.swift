//
//  ViewController.swift
//  Physio
//
//  Created by Vlad is Manea on 06/12/15.
//  Copyright © 2015 Habitlab IVS. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import AccountKit

class SignupController: UIViewController, AKFViewControllerDelegate {
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var emailSignupButton: UIButton!
    
    let accountKit = AKFAccountKit(responseType: AKFResponseType.AccessToken)
    var pendingLoginViewController: AKFViewController?
    
    var facebookPermissions = ["public_profile", "email", "user_friends"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.signupButton.layer.cornerRadius = self.signupButton.frame.height / 2
        self.emailSignupButton.layer.cornerRadius = self.emailSignupButton.frame.height / 2
        
        if let realPendingLoginViewController = accountKit.viewControllerForLoginResume() as? AKFViewController {
            pendingLoginViewController = realPendingLoginViewController
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideSigninSignupButtons()
        
        if let _ = UserCache().getUser() {
            let authenticationMethod = UserCache().getAuthenticationMethod()
            authenticate(authenticationMethod)
            return
        }
        
        self.showSigninSignupButtons()
    }
    
    // Facebook Delegate Methods
    
    @IBAction func signupFacebook(sender: AnyObject) {
        authenticate(AuthenticationMethod.Facebook)
    }
    
    @IBAction func signinFacebook(sender: AnyObject) {
        authenticate(AuthenticationMethod.Facebook)
    }
    
    @IBAction func signupWithEmailButtonPressed() {
        self.authenticateEmail()
    }
    
    func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AKFAccessToken!, state: String!) {
        self.authenticateHabitlab(AuthenticationMethod.AccountKit, token: accessToken.tokenString, handler: self.authenticationHandler)
    }
    
    func viewController(viewController: UIViewController!, didFailWithError error: NSError!) {
        print(error)
    }
    
    func hideSigninSignupButtons() {
        signinButton.hidden = true
        signupButton.hidden = true
        emailSignupButton.hidden = true
    }
    
    func showSigninSignupButtons() {
        signinButton.hidden = false
        signupButton.hidden = false
        emailSignupButton.hidden = false
    }
    
    func authenticateEmail() {
        let inputState = NSUUID().UUIDString
        
        if let viewController = accountKit.viewControllerForEmailLoginWithEmail(nil, state: inputState) as? AKFViewController {
            viewController.delegate = self
            
            if let uiViewController = viewController as? UIViewController {
                self.presentViewController(uiViewController, animated: true, completion: nil)
            }
        }
    }
    
    func authenticate(method: AuthenticationMethod) {
        switch method {
        case .AccountKit:
            if let token = accountKit.currentAccessToken {
                self.authenticateHabitlab(method, token: token.tokenString, handler: self.authenticationHandler)
                return
            }
            
            if let realPendingLoginViewController = pendingLoginViewController {
                realPendingLoginViewController.delegate = self
                
                if let uiRealPendingLoginViewController = realPendingLoginViewController as? UIViewController {
                    self.presentViewController(uiRealPendingLoginViewController, animated: true, completion: nil)
                }
            }
            
        case .Facebook:
            if let token = FBSDKAccessToken.currentAccessToken() {
                self.authenticateHabitlab(method, token: token.tokenString, handler: self.authenticationHandler)
                return
            }
            
            Serv.showSpinner()
            
            let loginManager = FBSDKLoginManager()
            
            loginManager.logInWithReadPermissions(facebookPermissions, fromViewController: self, handler: { (result, error) -> Void in
                Serv.hideSpinner()
                
                if error != nil {
                    self.showSigninSignupButtons()
                    Serv.showErrorPopup("Facebook login error", message: "Facebook could not log you in. Try again.", okMessage: "Okay, got it.", controller: self, handler: nil)
                    return
                }
                
                if let token = FBSDKAccessToken.currentAccessToken() {
                    self.authenticateHabitlab(method, token: token.tokenString, handler: self.authenticationHandler)
                }
            })
        }
    }
    
    func authenticateHabitlab(method: AuthenticationMethod, token: String?, handler: () -> ()) {
        if let realToken = token {
            Serv.showSpinner()
            
            HabitlabAuthentication.authenticate(realToken, method: method, handler: { (error, user) -> () in
                Serv.hideSpinner()
                
                if error != nil {
                    print(error)
                    self.showSigninSignupButtons()
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    
                    HabitlabAuthentication.deauthenticate({ (success) -> () in
                        self.accountKit.logOut()
                        FBSDKLoginManager().logOut()
                        HabitlabAuthentication.habitlabAccessToken = nil
                        UserCache().setUser(nil)
                    })
                    
                    return
                }
                
                if let realUser = user {
                    UserCache().setAuthenticationMethod(method)
                    
                    print(realUser)
                    
                    UserCache().setUser(realUser)
                    self.registerAuthenticatedUserForAnalytics(handler)
                }
            })
        } else {
            self.showSigninSignupButtons()
            Serv.showErrorPopup("Facebook login error", message: "Facebook did not give us access. Try again.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    func registerAuthenticatedUserForAnalytics(handler: () -> ()) {
        handler()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    func authenticationHandler() {
        self.performSegueWithIdentifier("showMainTabsFromSignup", sender: self)
    }
    
    @IBAction func signupControllerPrepareForLogout(segue: UIStoryboardSegue){
        
    }
}