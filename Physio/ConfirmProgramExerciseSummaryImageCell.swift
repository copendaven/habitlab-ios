//
//  ConfirmProgramExerciseSummaryImageCell.swift
//  Physio
//
//  Created by Vlad Manea on 7/1/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ConfirmProgramExerciseSummaryImageCell: UICollectionViewCell {
    private static let serialQueue = dispatch_queue_create("me.habitlab.physio.confirmProgramExerciseSummaryImageCell.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    private var controller: UIViewController?
    private var medium: NSDictionary?
    
    @IBOutlet weak var photoImage: UIImageView!
    
    func populate(medium: NSDictionary, controller: UIViewController) {
        self.controller = controller
        self.medium = medium
        
        dispatch_async(ConfirmProgramExerciseSummaryImageCell.serialQueue) {
            if let realMedium = self.medium {
                if let urlString = realMedium["urlImage"] as? String {
                    if let url = NSURL(string: urlString) {
                        if let data = NSData(contentsOfURL: url) {
                            if let image = UIImage(data: data) {
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.photoImage.image = image
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func clickedImage(sender: AnyObject) {
        if let realController = self.controller, realMedium = self.medium {
            if let viewController = realController as? AddExerciseController {
                viewController.showImage(realMedium)
            }
        }
    }
}
