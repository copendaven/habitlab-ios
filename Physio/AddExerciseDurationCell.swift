//
//  AddExerciseDurationCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/29/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddExerciseDurationCell: UITableViewCell, UITextFieldDelegate {
    private var controller: UIViewController?
    private var numericType: AddExerciseController.NumericType?
    private var numericValue: Int?
    
    @IBOutlet weak var secondsTextField: UITextField!
    @IBOutlet weak var minutesTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    func populate(labelText: String, labelPlaceholderTextMinutes: String, labelPlaceholderTextSeconds: String, numericType: AddExerciseController.NumericType, enabled: Bool, controller: UIViewController) {
        self.enable(enabled)
        
        self.titleLabel.text = labelText
        self.controller = controller
        self.numericType = numericType
        
        self.minutesTextField.placeholder = labelPlaceholderTextMinutes
        self.minutesTextField.delegate = self
        
        self.secondsTextField.placeholder = labelPlaceholderTextSeconds
        self.secondsTextField.delegate = self
        
        minutesTextField.addTarget(self, action: #selector(AddExerciseDurationCell.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        secondsTextField.addTarget(self, action: #selector(AddExerciseDurationCell.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        if let realController = controller as? AddExerciseController {
            self.secondsTextField.delegate = realController
            self.minutesTextField.delegate = realController
        }
        
        let doneToolbar1 = self.constructInputAccessoryView()
        doneToolbar1.sizeToFit()
        self.secondsTextField.inputAccessoryView = doneToolbar1
        
        let doneToolbar2 = self.constructInputAccessoryView()
        doneToolbar2.sizeToFit()
        self.minutesTextField.inputAccessoryView = doneToolbar2
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddExerciseDurationCell.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func doneButtonAction() {
        if let realController = self.controller as? AddExerciseController {
            realController.view.endEditing(true)
        }
    }
    
    func enable(isEnabled: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            self.minutesTextField.enabled = isEnabled
            self.secondsTextField.enabled = isEnabled
            
            if isEnabled {
                self.alpha = 1
            } else {
                self.alpha = 0.3
            }
        }
    }
    
    func validate(isValidMinutes isValidMinutes: Bool, isValidSeconds: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            if isValidMinutes {
                self.minutesTextField.textColor = UIColor(red: 34 / 255, green: 176 / 255, blue: 155 / 255, alpha: 1.0)
            } else {
                self.minutesTextField.textColor = UIColor.redColor()
            }
            
            if isValidSeconds {
                self.secondsTextField.textColor = UIColor(red: 34 / 255, green: 176 / 255, blue: 155 / 255, alpha: 1.0)
            } else {
                self.secondsTextField.textColor = UIColor.redColor()
            }
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        var minutes: Int? = nil
        var seconds: Int? = nil
        
        if let realSecondsString = secondsTextField.text {
            if let realSeconds = Int(realSecondsString) {
                seconds = realSeconds
            }
        }
        
        if let realMinutesString = minutesTextField.text {
            if let realMinutes = Int(realMinutesString) {
                minutes = realMinutes
            }
        }
        
        if let realController = self.controller, realNumericType = self.numericType {
            if let viewController = realController as? AddExerciseController {
                viewController.handleExerciseDurationUpdate(minutes: minutes, seconds: seconds, numericType: realNumericType)
            }
        }
    }
}



