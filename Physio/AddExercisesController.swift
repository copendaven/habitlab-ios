//
//  AddExercisesController.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class AddExercisesController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var exercises = NSMutableArray()
    var patient: NSMutableDictionary?
    
    @IBOutlet weak var exercisesTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var profileImageLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exercisesTableView.delegate = self
        exercisesTableView.dataSource = self
        
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.layer.borderWidth = 5
        profileImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        profileImageLabel.layer.cornerRadius = profileImageLabel.frame.height / 2
        profileImageLabel.layer.borderWidth = 5
        profileImageLabel.layer.borderColor = UIColor.whiteColor().CGColor
        
        dispatch_async(dispatch_get_main_queue()) {
            self.exercisesTableView.estimatedRowHeight = 80
            self.exercisesTableView.rowHeight = UITableViewAutomaticDimension
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
            
            self.refreshFromClient(true)
        }
    }
    
    @IBAction func clickedAddNewExercise() {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showAddExerciseFromAddExercises", sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let realPatient = self.patient {
            if let firstName = realPatient["firstName"] as? String, lastName = realPatient["lastName"] as? String {
                self.titleLabel.text = "\(firstName) \(lastName)"
            }
        }
        
        self.populatePhoto()
        self.refreshFromClient(false)
    }
    
    func populatePhoto() {
        if let physioPatient = self.patient {
            var fullName = ""
            
            if let firstName = physioPatient["firstName"] as? NSString {
                let firstNameString = firstName as String
                
                if firstNameString.characters.count > 0 {
                    fullName = fullName + "\(firstNameString[firstNameString.startIndex.advancedBy(0)])"
                }
            }
            
            if let lastName = physioPatient["lastName"] as? NSString {
                let lastNameString = lastName as String
                
                if lastNameString.characters.count > 0 {
                    fullName = fullName + "\(lastNameString[lastNameString.startIndex.advancedBy(0)])"
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
                self.profileImageLabel.alpha = 0.0
                self.profileImage.alpha = 0.0
                
                self.profileImageLabel.text = fullName.uppercaseString
                self.profileImageLabel.alpha = 1.0
            })
            
            if let email = physioPatient["email"] as? NSString {
                HabitlabRestAPI.postPatientPhoto(email as String, handler: { (error, objectJson) in
                    if let object = objectJson as? NSDictionary {
                        if let photo = object["photo"] as? NSString {
                            Serv.loadImage(photo as String, handler: { (error, data) -> () in
                                if error != nil {
                                    print(error)
                                }
                                
                                if data != nil {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        self.profileImage.image = UIImage(data: data!)
                                        
                                        UIView.animateWithDuration(0.35, animations: {
                                            self.profileImage.alpha = 1.0
                                            self.profileImageLabel.alpha = 0.0
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
    func refreshFromClient(reloadData: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            if self.exercises.count > 0 {
                self.doneButton.enabled = true
                self.doneButton.alpha = 1.0
            } else {
                self.doneButton.enabled = false
                self.doneButton.alpha = 0.5
            }
            
            if (reloadData) {
                self.exercisesTableView.reloadData()
            }
        }
    }

    @IBAction func clickedDone(sender: AnyObject) {
        dispatch_async(dispatch_get_main_queue()) {
            self.performSegueWithIdentifier("showAddProgramFromAddExercises", sender: self)
        }
    }
    
    @IBAction func addExercisesControllerAddedExercise(segue: UIStoryboardSegue) {
        refreshFromClient(true)
    }
    
    func addExercise(exercise: NSMutableDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            self.exercises.addObject(exercise)
            
            for i in 0..<self.exercises.count {
                let exercise = self.exercises[i]
                exercise.setObject(i + 1, forKey: "index")
            }
            
            self.refreshFromClient(true)
        }
    }
    
    func deleteExercise(exercise: NSDictionary) {
        dispatch_async(dispatch_get_main_queue()) {
            var deleteIndex: Int? = nil
            
            if let id = exercise["cacheId"] as? NSString {
                for i in 0..<self.exercises.count {
                    let existingExercise = self.exercises[i]
                    
                    if let existingId = existingExercise["cacheId"] as? NSString {
                        if id.compare(existingId as String) == NSComparisonResult.OrderedSame {
                            deleteIndex = i
                            break
                        }
                    }
                }
            }
            
            if let realDeleteIndex = deleteIndex {
                self.exercises.removeObjectAtIndex(realDeleteIndex)
            }
            
            for i in 0..<self.exercises.count {
                let exercise = self.exercises[i]
                exercise.setObject(i + 1, forKey: "index")
            }
            
            self.refreshFromClient(true)
        }
    }
    
    // MARK: Unwind segues
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.exercises.count + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addExercisesAddNewExerciseCell", forIndexPath: indexPath) as? AddExercisesAddNewExerciseCell {
                return cell
            }
            
            return defaultCell
        }
        
        if let _ = self.exercises[indexPath.section - 1] as? NSDictionary {
            if let cell = tableView.dequeueReusableCellWithIdentifier("addExercisesExerciseCell", forIndexPath: indexPath) as? AddExercisesExerciseCell {
                return cell
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            if let realCell = cell as? AddExercisesAddNewExerciseCell {
                realCell.populate(self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if let exercise = self.exercises[indexPath.section - 1] as? NSDictionary {
            if let realCell = cell as? AddExercisesExerciseCell {
                realCell.populate(exercise, controller: self)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
        }
    }
    
    // MARK:  UITableViewDelegate Methods
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if (segue.identifier == "showAddProgramFromAddExercises") {
            if let viewController = segue.destinationViewController as? AddProgramController {
                viewController.exercises = self.exercises
                viewController.patient = self.patient
            }
        }
    }
}
