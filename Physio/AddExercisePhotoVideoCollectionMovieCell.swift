//
//  AddExercisePhotoVideoCollectionMovieCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import AVFoundation

class AddExercisePhotoVideoCollectionMovieCell: UICollectionViewCell {
    private static let serialQueue = dispatch_queue_create("me.habitlab.physio.addExercisePhotoVideoCollectionMovieCell.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    private var controller: UIViewController?
    private var medium: NSDictionary?
    
    @IBOutlet weak var photoImage: UIImageView!
    
    func populate(medium: NSDictionary, controller: UIViewController) {
        self.controller = controller
        self.medium = medium
        
        dispatch_async(AddExercisePhotoVideoCollectionMovieCell.serialQueue) {
            if let realMedium = self.medium {
                if let urlString = realMedium["urlMovie"] as? String {
                    if let url = NSURL(string: urlString) {
                        let asset = AVURLAsset(URL: url, options: nil)
                        let imgGenerator = AVAssetImageGenerator(asset: asset)
                        imgGenerator.appliesPreferredTrackTransform = true
                        
                        do {
                            let cgImage = try imgGenerator.copyCGImageAtTime(CMTimeMake(0, 1), actualTime: nil)
                            let uiImage = UIImage(CGImage: cgImage)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.photoImage.image = uiImage
                            })
                        } catch {
                            // Do nothing...
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func clickedMovie(sender: AnyObject) {
        if let realController = self.controller, realMedium = self.medium {
            if let viewController = realController as? AddExerciseController {
                viewController.showMovie(realMedium)
            }
        }
    }
}
