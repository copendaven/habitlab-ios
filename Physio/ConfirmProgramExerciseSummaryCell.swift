//
//  ConfirmProgramExerciseSummaryCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import MobileCoreServices
import AVKit
import AVFoundation

class ConfirmProgramExerciseSummaryCell: UITableViewCell {

    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var countRepetitionsDurationLabel: UILabel!
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var exerciseTitleLabel: UILabel!
    
    private var program: NSDictionary?
    private var level: Int?
    private var exercise: Int?
    private var controller: UIViewController?
    
    func populate(program: NSDictionary, level: Int, exercise: Int, controller: UIViewController) {
        self.program = program
        self.controller = controller
        
        exerciseTitleLabel.text = ""
        indexLabel.text = ""
        countRepetitionsDurationLabel.text = ""
        
        if let drives = program["drives"] as? NSArray {
            if drives.count > level {
                if let drive = drives[level] as? NSDictionary {
                    if let physioExercises = drive["physioExercises"] as? NSArray {
                        if physioExercises.count > exercise {
                            if let physioExercise = physioExercises[exercise] as? NSDictionary {
                                if let exerciseTitle = physioExercise["title"] as? String {
                                    exerciseTitleLabel.text = "Exercise \(1 + exercise): \(exerciseTitle)"
                                }
                                
                                indexLabel.text = "Exercise \(1 + exercise) out of \(physioExercises.count)"
                                
                                if let countRepetitions = physioExercise["countRepetitions"] as? Int {
                                    countRepetitionsDurationLabel.text = "Repetitions: \(countRepetitions)"
                                }
                                
                                if let duration = physioExercise["duration"] as? Int {
                                    let minutes = Int(duration / 60)
                                    let seconds = Int(duration) % 60
                                    
                                    var durationText = "Duration:"
                                    
                                    if minutes > 0 {
                                        durationText = " \(minutes) minute\(minutes == 1 ? "" : "s")"
                                    }
                                    
                                    if seconds > 0 {
                                        durationText = " \(seconds) seconds\(seconds == 1 ? "" : "s")"
                                    }
                                    
                                    countRepetitionsDurationLabel.text = durationText
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
