//
//  AddPatientEmailFieldCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/27/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddPatientEmailFieldCell: UITableViewCell {
    private var controller: UIViewController?
    private var fieldType: AddPatientEmailController.FieldType?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextField!
    
    func populate(labelText: String, labelPlaceholderText: String, defaultText: String?, fieldType: AddPatientEmailController.FieldType, controller: UIViewController) {
        self.titleLabel.text = labelText
        self.controller = controller
        self.fieldType = fieldType
        
        if let realDefaultText = defaultText {
            self.contentTextField.text = realDefaultText
            self.contentTextField.enabled = false
        } else {
            self.contentTextField.enabled = true
        }
        
        contentTextField.addTarget(self, action: #selector(AddPatientEmailFieldCell.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        contentTextField.placeholder = labelPlaceholderText
        
        if let realController = controller as? AddPatientEmailController {
            contentTextField.delegate = realController
        }
        
        let doneToolbar = self.constructInputAccessoryView()
        doneToolbar.sizeToFit()
        self.contentTextField.inputAccessoryView = doneToolbar
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddPatientEmailFieldCell.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func doneButtonAction() {
        if let realController = self.controller as? AddPatientEmailController {
            realController.view.endEditing(true)
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        dispatch_async(dispatch_get_main_queue()) {
            if let realText = textField.text {
                if let realController = self.controller, realFieldType = self.fieldType {
                    if let viewController = realController as? AddPatientEmailController {
                        viewController.handleProgramFieldUpdate(realText, fieldType: realFieldType)
                    }
                }
            }
        }
    }
}
