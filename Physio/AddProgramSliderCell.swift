//
//  AddProgramSliderCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class AddProgramSliderCell: UITableViewCell, UITextViewDelegate {
    private var controller: UIViewController?
    private var sliderType: AddProgramController.SliderFieldType?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    func populate(labelText: String, minValue: Int, defaultValue: Int, maxValue: Int, sliderType: AddProgramController.SliderFieldType, controller: UIViewController) {
        self.titleLabel.text = labelText
        self.controller = controller
        self.sliderType = sliderType
        
        self.slider.minimumValue = Float(minValue)
        self.slider.maximumValue = Float(maxValue)
        self.slider.value = Float(defaultValue)
        
        self.valueLabel.text = String(Int(self.slider.value))
        self.updateController()
    }
    
    @IBAction func sliderDidChange(sender: UISlider) {
        self.valueLabel.text = String(Int(self.slider.value))
        self.updateController()
    }
    
    private func updateController() {
        if let realController = self.controller, realSliderType = self.sliderType {
            if let viewController = realController as? AddProgramController {
                // viewController.handleProgramSliderUpdate(Int(self.slider.value), sliderType: realSliderType)
            }
        }
    }
}
