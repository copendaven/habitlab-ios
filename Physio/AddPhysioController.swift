//
//  AddPhysioController.swift
//  Physio
//
//  Created by David on 01/06/2016.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class AddPhysioController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.addPhysioController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    @IBOutlet weak var encouragementImage: UIImageView!
    @IBOutlet weak var successImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var workplaceName: UITextField!
    var profileImagePicker = UIImagePickerController()
    var succesImagePicker = UIImagePickerController()
    var encouragementImagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        
        userImage.layer.cornerRadius = userImage.frame.height / 2
        userImage.layer.borderWidth = 5
        userImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddPhysioController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        populateProfileImage()
        populateSuccessImage()
        populateEncouragementImage()
        populateUser()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddPhysioController.keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AddPhysioController.keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        self.workplaceName.delegate = self
        
        let doneToolbar = self.constructInputAccessoryView()
        doneToolbar.sizeToFit()
        self.workplaceName.inputAccessoryView = doneToolbar
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddPhysioController.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func didPressSaveWorkplace(sender: AnyObject) {
        if let realText = self.workplaceName.text {
            dispatch_async(self.serialQueue, {
                HabitlabRestAPI.putPhysiotherapistWorkplace(realText, handler: { (error, user) in
                    if error != nil {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        return
                    }
                    
                    if let realUser = user as? NSDictionary {
                        UserCache().setUser(realUser)
                        self.populateUser()
                    }
                })
            })
        }
    }
    
    @IBAction func didPressTakePhoto(sender: UIButton) {
        profileImagePicker.delegate = self
        profileImagePicker.sourceType = .Camera
        
        presentViewController(profileImagePicker, animated: true, completion: nil)
    }
    
    @IBAction func didPressChooseFromLibrary(sender: AnyObject) {
        profileImagePicker.delegate = self
        profileImagePicker.sourceType = .PhotoLibrary
        
        presentViewController(profileImagePicker, animated: true, completion: nil)
    }
    
    @IBAction func didPressTakeSuccessPhoto(sender: AnyObject) {
        succesImagePicker.delegate = self
        succesImagePicker.sourceType = .Camera
        
        presentViewController(succesImagePicker, animated: true, completion: nil)
    }
    
    @IBAction func didPressEncouragementPhoto(sender: AnyObject) {
        encouragementImagePicker.delegate = self
        encouragementImagePicker.sourceType = .Camera
        
        presentViewController(encouragementImagePicker, animated: true, completion: nil)
    }
    
    private func uploadImage(picker: UIImagePickerController, info: [String : AnyObject], id: String, url: NSURL, upload: (() -> ())) {
        
        print({(x: Int, y: Int) -> Int in
            return x + y
        }(2, 3))
        
        var image: UIImage?
        
        if let realImageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = realImageOriginal
        }
        
        if let realImageCrop = info[UIImagePickerControllerCropRect] as? UIImage {
            image = realImageCrop
        }
        
        if let realImageEdited = info[UIImagePickerControllerEditedImage] as? UIImage {
            image = realImageEdited
        }
        
        if let realImage = image {
            do {
                if let realImageJpeg = UIImageJPEGRepresentation(realImage, 0.0) {
                    try realImageJpeg.writeToURL(url, options: NSDataWritingOptions.AtomicWrite)
                    upload()
                }
            } catch let error as NSError {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
            } catch {
                // Do nothing.
            }
            
            return
        }
    }
    
    private func populateUser() {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let firstName = user["firstName"] as? NSString, lastName = user["lastName"] as? NSString {
                    if let physioTherapistWorkplace = user["physioTherapistWorkplace"] as? NSString {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.userName.text = "\(firstName) \(lastName)\n\(physioTherapistWorkplace)"
                        })
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.userName.text = "\(firstName) \(lastName)"
                        })
                    }
                    if let _ = user["facebookPhoto"] as? NSString {
                        // TODO: Show the user photo with the image cache.
                    }
                }
            }
        }
    }
    
    private func populateProfileImage() {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let id = user["id"] as? NSString {
                    PhysioS3API.downloadProfilePicture(id as String, temporaryAddress: "HabitlabPhysiotherapistProfileImage\(id).jpg", handler: { (error, data) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            return
                        }
                        
                        if let realData = data {
                            let image = UIImage(data: realData)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.userImage.image = image
                            })
                        }
                        
                        }, progress: { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                            // Do nothing.
                    })
                }
            }
        }
    }
    
    private func populateSuccessImage() {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let id = user["id"] as? NSString {
                    PhysioS3API.downloadSuccessPicture(id as String, temporaryAddress: "HabitlabPhysiotherapistSuccessImage\(id).jpg", handler: { (error, data) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            return
                        }
                        
                        if let realData = data {
                            let image = UIImage(data: realData)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.successImage.image = image
                            })
                        }
                        
                        }, progress: { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                            // Do nothing.
                    })
                }
            }
        }
    }
    
    private func populateEncouragementImage() {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let id = user["id"] as? NSString {
                    PhysioS3API.downloadEncouragementPicture(id as String, temporaryAddress: "HabitlabPhysiotherapistEncouragementImage\(id)", handler: { (error, data) in
                        if error != nil {
                            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                            return
                        }
                        
                        if let realData = data {
                            let image = UIImage(data: realData)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.encouragementImage.image = image
                            })
                        }
                        
                        }, progress: { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                            // Do nothing.
                    })
                }
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let info  = notification.userInfo, realTabBarController = self.tabBarController {
            let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]!
            let rawFrame = value.CGRectValue
            let keyboardFrame = view.convertRect(rawFrame, fromView: nil)
            let keyboardHeight = keyboardFrame.height + 70 // For the extra view on top of the keyboard
            let tabBarHeight = realTabBarController.tabBar.frame.size.height
            self.view.frame.origin.y = (tabBarHeight - keyboardHeight) * 0.75
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let id = user["id"] as? NSString {
                    do {
                        let documentsDirectoryURL = try NSFileManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
                        
                        if picker == self.profileImagePicker {
                            let toURL = documentsDirectoryURL.URLByAppendingPathComponent("HabitlabPhysiotherapistProfileImage\(id)").URLByAppendingPathExtension("jpg")
                            
                            self.uploadImage(picker, info: info, id: id as String, url: toURL, upload: {() -> () in
                                PhysioS3API.uploadProfilePicture(id as String, url: toURL, handler: { (error) in
                                    if error != nil {
                                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                        return
                                    }
                                    
                                    self.populateProfileImage()
                                    }, progress: { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                                        // Do nothing.
                                })
                            })
                        }
                        
                        if picker == self.succesImagePicker {
                            let toURL = documentsDirectoryURL.URLByAppendingPathComponent("HabitlabPhysiotherapistSuccessImage\(id)").URLByAppendingPathExtension("jpg")
                            
                            self.uploadImage(picker, info: info, id: id as String, url: toURL, upload: {() -> () in
                                PhysioS3API.uploadSuccessPicture(id as String, url: toURL, handler: { (error) in
                                    if error != nil {
                                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                        return
                                    }
                                    
                                    self.populateSuccessImage()
                                    }, progress: { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                                        // Do nothing.
                                })
                            })
                        }
                        
                        if picker == self.encouragementImagePicker {
                            let toURL = documentsDirectoryURL.URLByAppendingPathComponent("HabitlabPhysiotherapistEncouragementImage\(id)").URLByAppendingPathExtension("jpg")
                            
                            self.uploadImage(picker, info: info, id: id as String, url: toURL, upload: {() -> () in
                                PhysioS3API.uploadEncouragementPicture(id as String, url: toURL, handler: { (error) in
                                    if error != nil {
                                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                        return
                                    }
                                    
                                    self.populateEncouragementImage()
                                    }, progress: { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                                        // Do nothing.
                                })
                            })
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            picker.dismissViewControllerAnimated(true, completion: nil)
                        })
                    } catch let error as NSError {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    } catch {
                        // Do nothing.
                    }
                } else {
                    picker.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
}
