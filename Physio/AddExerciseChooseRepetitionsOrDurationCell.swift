//
//  AddExerciseChooseRepetitionsOrDurationCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/28/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddExerciseChooseRepetitionsOrDurationCell: UITableViewCell {
    private var controller: UIViewController?
    
    @IBOutlet weak var repsDurationToggle: UISegmentedControl!
    
    func populate(controller: UIViewController) {
        self.repsDurationToggle.tintColor = UIColor(red: 3/255.0, green: 46/255.0, blue: 73/255.0, alpha: 1.0)
        self.controller = controller
    }
    
    @IBAction func didToggleRepsDuration(sender: AnyObject) {
        print(repsDurationToggle.selectedSegmentIndex)
        
        switch repsDurationToggle.selectedSegmentIndex
        {
        case 0:
            if let realController = self.controller {
                if let viewController = realController as? AddExerciseController {
                    viewController.setRepetitionsState()
                }
            }

        case 1:
            if let realController = self.controller {
                if let viewController = realController as? AddExerciseController {
                    viewController.setDurationState()
                }
            }

        default:
            break; 
        }
    }
}
