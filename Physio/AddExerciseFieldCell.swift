//
//  AddExerciseFieldCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddExerciseFieldCell: UITableViewCell {
    private var controller: UIViewController?
    private var fieldType: AddExerciseController.FieldType?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextField!
    
    func populate(labelText: String, labelPlaceholderText: String, fieldType: AddExerciseController.FieldType, enabled: Bool, controller: UIViewController) {
        self.enable(enabled)
        
        self.titleLabel.text = labelText
        self.controller = controller
        self.fieldType = fieldType
        
        contentTextField.addTarget(self, action: #selector(AddExerciseFieldCell.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        contentTextField.placeholder = labelPlaceholderText
        
        if let realController = controller as? AddExerciseController {
            self.contentTextField.delegate = realController
        }
        
        let doneToolbar = self.constructInputAccessoryView()
        doneToolbar.sizeToFit()
        self.contentTextField.inputAccessoryView = doneToolbar
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AddExerciseFieldCell.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func doneButtonAction() {
        if let realController = self.controller as? AddExerciseController {
            realController.view.endEditing(true)
        }
    }
    
    func enable(isEnabled: Bool) {
        dispatch_async(dispatch_get_main_queue()) {
            self.contentTextField.enabled = isEnabled
            
            if isEnabled {
                self.alpha = 1
            } else {
                self.alpha = 0.25
            }
        }
    }
    
    func textFieldDidChange(textField: UITextField) {
        dispatch_async(dispatch_get_main_queue()) {
            if let realText = textField.text {
                if let realController = self.controller, realFieldType = self.fieldType {
                    if let viewController = realController as? AddExerciseController {
                        viewController.handleExerciseFieldUpdate(realText, fieldType: realFieldType)
                    }
                }
            }
        }
    }
}
