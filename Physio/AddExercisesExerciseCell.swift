//
//  AddExercisesExerciseCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class AddExercisesExerciseCell: UITableViewCell {
    private var controller: UIViewController?
    private var exercise: NSDictionary?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func populate(exercise: NSDictionary, controller: UIViewController) {
        self.controller = controller
        self.exercise = exercise
        
        if let title = exercise["title"] as? NSString {
            self.titleLabel.text = title as String
        }

        if let personalAdvice = exercise["personalAdvice"] as? NSString {
            self.descriptionLabel.text = personalAdvice as String
        }
    }
    
    
    @IBAction func showEditActionSheet(sender: AnyObject) {
        
        let optionMenu = UIAlertController(title: nil, message: "Permenantly delete this exercise?", preferredStyle: .ActionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            
            dispatch_async(dispatch_get_main_queue(), {
                let confirmDeleteAlert = UIAlertController(title: "Delete can't be undone.", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.Alert)
                
                confirmDeleteAlert.addAction(UIAlertAction(title: "Yes", style: .Destructive, handler: { (action: UIAlertAction!) in
                    if let realController = self.controller {
                        if let viewController = realController as? AddExercisesController, realExercise = self.exercise {
                            viewController.deleteExercise(realExercise)
                        }
                    }
                }))
                
                confirmDeleteAlert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: { (action: UIAlertAction!) in
                    // Do nothing.
                }))
                
                if let realController = self.controller {
                    if let viewController = realController as? AddExercisesController {
                        viewController.presentViewController(confirmDeleteAlert, animated: true, completion: nil)
                    }
                }
            })
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(cancelAction)
        
        if let realController = self.controller {
            realController.presentViewController(optionMenu, animated: true, completion: nil)
        }
    }
}
