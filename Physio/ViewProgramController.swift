//
//  ViewProgramController.swift
//  Physio
//
//  Created by Vlad Manea on 7/3/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import Charts

class ViewProgramController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    enum ChartType {
        case PainScale
        case Loops
    }
    
    var chartType = ChartType.PainScale
    var program: NSDictionary?
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    @IBOutlet weak var patientNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        itemsTableView.delegate = self
        itemsTableView.dataSource = self
        
        self.itemsTableView.estimatedRowHeight = 200
        self.itemsTableView.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func refresh() {
        self.itemsTableView.reloadData()
        
        if let realProgram = self.program {
            if let physioPatient = realProgram["physioPatient"] as? NSDictionary {
                if let firstName = physioPatient["firstName"] as? NSString, lastName = physioPatient["lastName"] as? NSString {
                    self.patientNameLabel.text = "\(firstName) \(lastName)"
                }
            }
        }
    }
    
    func setChartType(chartType: ChartType) {
        self.chartType = chartType
        self.refresh()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let invisibleView = UIView.init()
        invisibleView.backgroundColor = UIColor.clearColor()
        return invisibleView
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "")
        
        if indexPath.section == 0 {
            if let cell = itemsTableView.dequeueReusableCellWithIdentifier("viewProgramToggleCell", forIndexPath: indexPath) as? ViewProgramToggleCell {
                return cell
            }
        }
        
        if indexPath.section == 1 {
            switch self.chartType {
            case .PainScale:
                if let cell = itemsTableView.dequeueReusableCellWithIdentifier("viewProgramChartPainScaleCell", forIndexPath: indexPath) as? ViewProgramChartPainScaleCell {
                    return cell
                }
            case .Loops:
                if let cell = itemsTableView.dequeueReusableCellWithIdentifier("viewProgramChartLoopsCell", forIndexPath: indexPath) as? ViewProgramChartLoopsCell {
                    return cell
                }
            }
        }
        
        if indexPath.section == 2 {
            if let cell = itemsTableView.dequeueReusableCellWithIdentifier("viewProgramDetailsCell", forIndexPath: indexPath) as? ViewProgramDetailsCell {
                return cell
            }
        }
        
        return defaultCell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            if let realCell = cell as? ViewProgramToggleCell, realProgram = self.program {
                realCell.populate(self, program: realProgram)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
            
            return
        }
        
        if indexPath.section == 1 {
            switch self.chartType {
            case .PainScale:
                if let realCell = cell as? ViewProgramChartPainScaleCell, realProgram = self.program {
                    realCell.populate(self, program: realProgram)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                    return
                }
            case .Loops:
                if let realCell = cell as? ViewProgramChartLoopsCell, realProgram = self.program {
                    realCell.populate(self, program: realProgram)
                    realCell.selectionStyle = UITableViewCellSelectionStyle.None
                    return
                }
            }
        }
        
        if indexPath.section == 2 {
            if let realCell = cell as? ViewProgramDetailsCell, realProgram = self.program {
                realCell.populate(self, program: realProgram)
                realCell.selectionStyle = UITableViewCellSelectionStyle.None
                return
            }
        }
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Nothing.
    }
}
