//
//  ConfirmProgramSummaryCell.swift
//  Physio
//
//  Created by Vlad Manea on 6/30/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ConfirmProgramSummaryCell: UITableViewCell {

    @IBOutlet weak var countWeeksLabel: UILabel!
    @IBOutlet weak var countLoopsLabel: UILabel!
    @IBOutlet weak var programTitleLabel: UILabel!
    
    func populate(program: NSDictionary) {
        countWeeksLabel.text = ""
        countLoopsLabel.text = ""
        programTitleLabel.text = ""
        
        if let programTitle = program["title"] as? String {
            programTitleLabel.text = "Program: \(programTitle)"
        }
        
        if let drives = program["drives"] as? NSArray {
            var countWeeksTotal = 0
            
            if drives.count > 0 {
                if let drive = drives[0] as? NSDictionary {
                    if let countStepsPerWeek = drive["countStepsPerWeek"] as? Int {
                        countLoopsLabel.text = "\(countStepsPerWeek) day\(countStepsPerWeek == 1 ? "" : "s") per week, one loop in each day"
                    }
                    
                    if let countStepsPerDay = drive["countStepsPerDay"] as? Int {
                        countLoopsLabel.text = "7 days per week, \(countStepsPerDay) loop\(countStepsPerDay == 1 ? "" : "s") per day"
                    }
                }
            }
            
            for driveJson in drives {
                if let drive = driveJson as? NSDictionary {
                    if let countWeeks = drive["countWeeks"] as? Int {
                        countWeeksTotal += countWeeks
                    }
                }
            }
            
            countWeeksLabel.text = "\(countWeeksTotal) week\(countWeeksTotal == 1 ? "" : "s")"
        }
    }
}
