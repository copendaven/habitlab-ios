//
//  ViewProgramDetailsCell.swift
//  Physio
//
//  Created by Vlad Manea on 7/3/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class ViewProgramDetailsCell: UITableViewCell {
    @IBOutlet weak var smallSquare1: UIView!
    @IBOutlet weak var smallSquare2: UIView!
    @IBOutlet weak var smallSquare3: UIView!
    
    @IBOutlet weak var programTitleLabel: UILabel!
    @IBOutlet weak var currentWeekLabel: UILabel!
    @IBOutlet weak var averagePainRatingThisWeekLabel: UILabel!
    @IBOutlet weak var lastActionLabel: UILabel!
    
    var controller: UIViewController?
    var program: NSDictionary?
    
    func populate(controller: UIViewController, program: NSDictionary) {
        self.controller = controller
        self.program = program
        
        self.averagePainRatingThisWeekLabel.layer.cornerRadius = 0.5 * self.averagePainRatingThisWeekLabel.bounds.size.width
        self.smallSquare1.layer.cornerRadius = 0.5 * self.smallSquare1.bounds.size.width
        self.smallSquare2.layer.cornerRadius = 0.5 * self.smallSquare2.bounds.size.width
        self.smallSquare3.layer.cornerRadius = 0.5 * self.smallSquare3.bounds.size.width
        
        if let realProgram = self.program {
            var sharedWithPhysio = false
            
            if let shareProgressWithPhysiotherapist = realProgram["shareProgressWithPhysiotherapist"] as? NSNumber {
                if shareProgressWithPhysiotherapist == 1 {
                    sharedWithPhysio = true
                }
            }
            
            var programId: String?
            var lastCreatedAt: String = "0"
            
            if sharedWithPhysio {
                if let drives = realProgram["drives"] as? NSArray {
                    for driveJson in drives {
                        if let drive = driveJson as? NSDictionary {
                            if let prog = drive["program"] as? NSDictionary {
                                if let createdAt = prog["createdAt"] as? NSString, progrId = prog["id"] as? NSString {
                                    if createdAt.compare(lastCreatedAt) == NSComparisonResult.OrderedDescending {
                                        programId = progrId as String
                                        lastCreatedAt = createdAt as String
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if let realProgramId = programId {
                HabitlabRestAPI.getPhysioInspireChartData(realProgramId, handler: { (error, stepsJson) in
                    if let steps = stepsJson as? NSArray {
                        self.setDetailsData(steps: steps)
                    }
                })
            } else {
                self.setDetailsData(sharedWithPhysio)
            }
        }
    }
    
    func setDetailsData(sharedWithPhysio: Bool) {
        if let realProgram = self.program {
            if let title = realProgram["title"] as? NSString {
                programTitleLabel.text = title as String
            }
        }
        
        if sharedWithPhysio {
            currentWeekLabel.text = "Not started yet"
            lastActionLabel.text = "Not started yet"
        } else {
            currentWeekLabel.text = "No info available"
            lastActionLabel.text = "No info available"
            averagePainRatingThisWeekLabel.text = "N/A"
        }
    }
    
    func setDetailsData(steps programSteps: NSArray) {
        if let realProgram = self.program {
            if let title = realProgram["title"] as? NSString {
                programTitleLabel.text = title as String
            }
        }
        
        var maxLevel: Int = -1
        var maxWeek: Int = -1
        var maxTotalWeek: Int = -1
        var maxTotalWeeks: Int = 0
        var maxIndex: Int = -1
        var countWeeks: Int = 0
        
        if let realProgram = self.program {
            if let drives = realProgram["drives"] as? NSArray {
                for driveJson in drives {
                    if let drive = driveJson as? NSDictionary {
                        if let prog = drive["program"] as? NSDictionary, level = drive["level"] as? NSNumber, countWeeksNumber = drive["countWeeks"] as? NSNumber {
                            let intLevel = Int(level)
                            countWeeks = Int(countWeeksNumber)
                            maxTotalWeeks += countWeeks
                            
                            if let steps = prog["steps"] as? NSArray {
                                for stepJson in steps {
                                    if let step = stepJson as? NSDictionary {
                                        if let week = step["week"] as? NSNumber, index = step["index"] as? NSNumber {
                                            let intWeek = Int(week)
                                            let intIndex = Int(index)
                                            
                                            print(intWeek)
                                            print(intIndex)
                                            
                                            var stepReached = false
                                            
                                            if let fuckup = step["fuckup"] as? NSNumber {
                                                if fuckup == 1 {
                                                    stepReached = true
                                                }
                                            }
                                            
                                            if let complete = step["complete"] as? NSNumber {
                                                if complete == 1 {
                                                    stepReached = true
                                                }
                                            }
                                            
                                            if let quitted = step["quitted"] as? NSNumber {
                                                if quitted == 1 {
                                                    stepReached = true
                                                }
                                            }
                                            
                                            if let started = step["started"] as? NSNumber {
                                                if started == 1 {
                                                    stepReached = true
                                                }
                                            }
                                            
                                            if stepReached {
                                                let totalWeek = (intLevel - 1) * countWeeks + intWeek
                                                
                                                if intLevel > maxLevel {
                                                    maxLevel = intLevel
                                                    maxTotalWeek = totalWeek
                                                    maxWeek = intWeek
                                                    maxIndex = intIndex
                                                } else if intLevel == maxLevel {
                                                    if totalWeek > maxTotalWeek {
                                                        maxTotalWeek = totalWeek
                                                        maxWeek = intWeek
                                                        maxIndex = intIndex
                                                    } else if totalWeek == maxTotalWeek {
                                                        if intIndex > maxIndex {
                                                            maxIndex = intIndex
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if maxLevel >= 0 && maxWeek >= 0 && maxIndex >= 0 && maxTotalWeek >= 0 {
            // Compute the average pains.
            
            var averageTotalPain: Double = 0.0
            var countTotalSteps: Int = 0
            
            for stepJson in programSteps {
                if let step = stepJson as? NSDictionary {
                    if let level = step["level"] as? NSNumber, week = step["week"] as? NSNumber {
                        let intLevel = Int(level)
                        let intWeek = Int(week)
                        
                        if intLevel != maxLevel || intWeek != maxWeek {
                            continue
                        }
                        
                        var countVars: Int = 0
                        var averagePain: Double = 0.0
                        
                        if let beforePainLevel = step["beforePainLevel"] as? NSNumber {
                            averagePain += Double(beforePainLevel)
                            countVars += 1
                        }
                        
                        if let afterPainLevel = step["afterPainLevel"] as? NSNumber {
                            averagePain += Double(afterPainLevel)
                            countVars += 1
                        }
                        
                        if countVars > 0 {
                            averagePain /= Double(countVars)
                        }
                        
                        averageTotalPain += averagePain
                        countTotalSteps += 1
                        
                        print(averageTotalPain)
                        print(countTotalSteps)
                    }
                }
            }
            
            if countTotalSteps > 0 {
                averageTotalPain /= Double(countTotalSteps)
            }
            
            let averageTotalPainString = String(format: "%.1f", averageTotalPain)
            print(averageTotalPainString)
            averagePainRatingThisWeekLabel.text = averageTotalPainString
            
            let currentWeekText = "Week \(1 + maxTotalWeek) of \(maxTotalWeeks) in the program"
            currentWeekLabel.text = currentWeekText
        } else {
            averagePainRatingThisWeekLabel.text = "N/A"
            currentWeekLabel.text = "Not started yet"
        }
        
        var lastActionText = "Not started yet"
        var theLevel: NSNumber?
        var theWeek: NSNumber?
        var theIndex: NSNumber?
        var action: String?
        
        if let realProgram = self.program {
            if let drives = realProgram["drives"] as? NSArray {
                var lastStepDate = "0"
                
                for driveJson in drives {
                    if let drive = driveJson as? NSDictionary {
                        if let program = drive["program"] as? NSDictionary, level = drive["level"] as? NSNumber {
                            if let steps = program["steps"] as? NSArray {
                                lastActionText = "Started level \(level)"
                                
                                for stepJson in steps {
                                    if let step = stepJson as? NSDictionary {
                                        if let week = step["week"] as? NSNumber, index = step["index"] as? NSNumber {
                                            if let startedAt = step["startedAt"] as? NSString {
                                                if startedAt.compare(lastStepDate) == NSComparisonResult.OrderedDescending {
                                                    lastStepDate = startedAt as String
                                                    theLevel = level
                                                    theWeek = week
                                                    theIndex = index
                                                    action = "Started"
                                                }
                                            }
                                            
                                            if let completedAt = step["completedAt"] as? NSString {
                                                if completedAt.compare(lastStepDate) == NSComparisonResult.OrderedDescending {
                                                    lastStepDate = completedAt as String
                                                    theLevel = level
                                                    theWeek = week
                                                    theIndex = index
                                                    action = "Did"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if let realLevel = theLevel, realWeek = theWeek, realIndex = theIndex, realAction = action {
                lastActionText = "\(realAction) loop \(realIndex), week \(Int(realWeek) + 1), level \(realLevel)"
            }
        }
        
        lastActionLabel.text = lastActionText
    }
}
