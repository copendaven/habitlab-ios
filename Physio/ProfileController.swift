//
//  AccountSettingsController.swift
//  Habitlab
//
//  Created by Vlad Manea on 01/01/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import AccountKit
import StoreKit
import MessageUI
import Mixpanel

struct PhysioProducts {
    private static let Prefix = "me.habitlab.physio."
    
    static let Unlimited1Month = "Unlimited_1"
    static let Unlimited3Months = "Unlimited_3"
    static let Unlimited12Months = "Unlimited_12"
    
    private static let productIdentifiers: Set<String> = [PhysioProducts.Unlimited1Month, PhysioProducts.Unlimited3Months, PhysioProducts.Unlimited12Months]
    static let store = InAppPurchaseService(productIdentifiers: PhysioProducts.productIdentifiers)
}

// TODO: Thread this class

class ProfileController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, MFMailComposeViewControllerDelegate {
    private let serialQueue = dispatch_queue_create("me.habitlab.physio.profileController.serialQueue", DISPATCH_QUEUE_SERIAL)
    
    let accountKit = AKFAccountKit(responseType: AKFResponseType.AccessToken)
    var products = [SKProduct]()
    
    @IBOutlet weak var workplaceSaveButton: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var button1month: UIButton!
    @IBOutlet weak var button3months: UIButton!
    @IBOutlet weak var button12months: UIButton!
    
    @IBOutlet weak var priceLabel1Month: UILabel!
    @IBOutlet weak var priceLabel3Months: UILabel!
    @IBOutlet weak var priceLabel12Months: UILabel!
    
    @IBOutlet weak var workplaceName: UITextField!
    
    var profileImagePicker = UIImagePickerController()
    
    @IBAction func signOut(sender: AnyObject) {
        HabitlabAuthentication.deauthenticate({ (success) -> () in
            
            // FBSDKAccessToken
            FBSDKLoginManager().logOut()
            self.accountKit.logOut()
            HabitlabAuthentication.habitlabAccessToken = nil
            UserCache().setUser(nil)
            
            if !success {
                return
            }
            
            self.performSegueWithIdentifier("showSignupFromProfile", sender: self)
        })
    }
    
    @IBAction func click1MonthSubscription(sender: AnyObject) {
        self.handleClick(PhysioProducts.Unlimited1Month)
    
        let mixpanel = Mixpanel.sharedInstance()
        
        let properties = ["Plan": "1 Month"]
        mixpanel.track("Plan Purchased", properties: properties)
    
    
    }
    
    @IBAction func click3MonthsSubscription(sender: AnyObject) {
        self.handleClick(PhysioProducts.Unlimited3Months)
        
        let mixpanel = Mixpanel.sharedInstance()
        
        let properties = ["Plan": "3 Month"]
        mixpanel.track("Plan Purchased", properties: properties)
    }
    
    @IBAction func click12MonthsSubscription(sender: AnyObject) {
        self.handleClick(PhysioProducts.Unlimited12Months)
        
        let mixpanel = Mixpanel.sharedInstance()
        
        let properties = ["Plan": "12 Months"]
        mixpanel.track("Plan Purchased", properties: properties)
        
    }
    
    private func handleClick(productIdentifier: String) {
        for product in products {
            if product.productIdentifier.compare(productIdentifier) == NSComparisonResult.OrderedSame {
                PhysioProducts.store.buyProduct(product)
            }
        }
    }
    
    @IBAction func clickRestore(sender: AnyObject) {
        PhysioProducts.store.restorePurchases()
    }
    
    @IBAction func deleteAccount(sender: AnyObject) {
        let destroyAlert = UIAlertController(title: "Delete Account?", message: "By deleting this account, all data will be lost. Do you want to proceed?", preferredStyle: UIAlertControllerStyle.Alert)
        
        destroyAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction!) in
            HabitlabAuthentication.destroy({ (success) -> () in
                if !success {
                    Serv.showErrorPopup("Delete error", message: "We could not delete your account. Please send us an email so we can look into why this happened.", okMessage: "OK", controller: self, handler: nil)
                }
                
                FBSDKLoginManager().logOut()
                HabitlabAuthentication.habitlabAccessToken = nil
                UserCache().setUser(nil)
                
                self.performSegueWithIdentifier("showSignupFromProfile", sender: self)
            })
        }))
        
        destroyAlert.addAction(UIAlertAction(title: "Not now", style: .Cancel, handler: nil))
        
        presentViewController(destroyAlert, animated: true, completion: nil)
    }
    
    @IBAction func privacyPolicy(sender: AnyObject) {
        if let checkURL = NSURL(string: "https://www.habitlab.me/terms/HabitlabTerms.pdf") {
            UIApplication.sharedApplication().openURL(checkURL)
        }
    }
    
    @IBAction func supportPage(sender: AnyObject) {
        if MFMailComposeViewController.canSendMail() {
            let composeViewController = MFMailComposeViewController()
            
            composeViewController.navigationBar.barStyle = .Default
            composeViewController.navigationBar.backgroundColor = UIColor(red: 34 / 255, green: 176 / 255, blue: 155 / 255, alpha: 1.0)
            composeViewController.navigationBar.tintColor = UIColor.whiteColor()
            
            composeViewController.mailComposeDelegate = self
            composeViewController.setToRecipients(["info@habitlab.me"])
            composeViewController.setSubject("Support or Feedback request for Physio")
            
            self.presentViewController(composeViewController, animated: true, completion: nil)
        } else {
            Serv.showAlertPopup("Error while sending email", message: "This device cannot send emails.", okMessage: "Okay, got it.", controller: self, handler: nil)
        }
    }
    
    @IBAction func didPressSaveWorkplace(sender: AnyObject) {
        if let realText = self.workplaceName.text {
            dispatch_async(dispatch_get_main_queue(), {
                self.workplaceSaveButton.enabled = false
                self.workplaceSaveButton.setTitle("Saving", forState: UIControlState.Normal)
                self.workplaceSaveButton.setTitle("Saving", forState: UIControlState.Selected)
                self.workplaceSaveButton.setTitle("Saving", forState: UIControlState.Disabled)
            })
            
            dispatch_async(self.serialQueue, {
                HabitlabRestAPI.putPhysiotherapistWorkplace(realText, handler: { (error, user) in
                    dispatch_async(dispatch_get_main_queue(), {
                        self.workplaceSaveButton.alpha = 1.0
                        self.workplaceSaveButton.enabled = true
                        self.workplaceSaveButton.setTitle("Save", forState: UIControlState.Normal)
                        self.workplaceSaveButton.setTitle("Save", forState: UIControlState.Selected)
                        self.workplaceSaveButton.setTitle("Save", forState: UIControlState.Disabled)
                    })
                    
                    if error != nil {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                        return
                    }
                    
                    if let realUser = user as? NSDictionary {
                        UserCache().setUser(realUser)
                        self.populateUser()
                    }
                })
            })
        }
    }
    
    @IBAction func didPressChoosePhoto(sender: AnyObject) {
        let cameraAction = UIAlertAction(title: "Camera", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.didPressTakePhoto()
        })
        
        let libraryAction = UIAlertAction(title: "Library", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.didPressChooseFromLibrary()
        })
        
        let albumsAction = UIAlertAction(title: "Albums", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.didPressChooseFromAlbum()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            // Do nothing.
        })
        
        let optionMenu = UIAlertController(title: nil, message: "Choose photo source", preferredStyle: .ActionSheet)
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(libraryAction)
        optionMenu.addAction(albumsAction)
        optionMenu.addAction(cancelAction)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    private func didPressTakePhoto() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            profileImagePicker.delegate = self
            profileImagePicker.sourceType = .Camera
            
            presentViewController(profileImagePicker, animated: true, completion: nil)
        } else {
            Serv.showErrorPopup("Error while taking a photo", message: "This photo source is unavailable on this device. Try another source.", okMessage: "Okay", controller: self, handler: nil)
        }
    }
    
    private func didPressChooseFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            profileImagePicker.delegate = self
            profileImagePicker.sourceType = .PhotoLibrary
            
            presentViewController(profileImagePicker, animated: true, completion: nil)
        } else {
            Serv.showErrorPopup("Error while taking a photo", message: "This photo source is unavailable on this device. Try another source.", okMessage: "Okay", controller: self, handler: nil)
        }
    }
    
    private func didPressChooseFromAlbum() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum) {
            profileImagePicker.delegate = self
            profileImagePicker.sourceType = .SavedPhotosAlbum
            
            presentViewController(profileImagePicker, animated: true, completion: nil)
        } else {
            Serv.showErrorPopup("Error while taking a photo", message: "This photo source is unavailable on this device. Try another source.", okMessage: "Okay", controller: self, handler: nil)
        }
    }
    
    func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let info  = notification.userInfo, realTabBarController = self.tabBarController {
            let value: AnyObject = info[UIKeyboardFrameEndUserInfoKey]!
            let rawFrame = value.CGRectValue
            let keyboardFrame = view.convertRect(rawFrame, fromView: nil)
            let keyboardHeight = keyboardFrame.height + 70 // For the extra view on top of the keyboard
            let tabBarHeight = realTabBarController.tabBar.frame.size.height
            self.view.frame.origin.y = (tabBarHeight - keyboardHeight) * 0.75
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileController.handlePurchaseNotification(_:)), name: InAppPurchaseService.IAPHelperPurchaseNotification, object: nil)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddPhysioController.dismissKeyboard(_:)))
        tapGestureRecognizer.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileController.keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ProfileController.keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        self.userImage.layer.cornerRadius = self.userImage.frame.height / 2
        
        self.userImage.layer.borderWidth = 5
        self.userImage.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.workplaceName.delegate = self
    }
    
    private func populateUser() {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let firstName = user["firstName"] as? NSString, lastName = user["lastName"] as? NSString {
                    if let physioTherapistWorkplace = user["physioTherapistWorkplace"] as? NSString {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.userName.text = "\(firstName) \(lastName)"
                            self.workplaceName.text = physioTherapistWorkplace as String
                        })
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.userName.text = "\(firstName) \(lastName)"
                        })
                    }
                    if let _ = user["facebookPhoto"] as? NSString {
                        // TODO: Show the user photo with the image cache.
                    }
                }
            }
        }
    }
    
    private func populateProfileImage() {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let id = user["id"] as? NSString {
                    PhysioS3API.downloadProfilePicture(id as String, temporaryAddress: "HabitlabPhysiotherapistProfileImage\(id).jpg", handler: { (error, data) in
                        if let realData = data {
                            let image = UIImage(data: realData)
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.userImage.image = image
                            })
                        }
                        
                        }, progress: { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                            // Do nothing.
                    })
                }
            }
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        dispatch_async(self.serialQueue) {
            if let user = UserCache().getUser() {
                if let id = user["id"] as? NSString {
                    do {
                        let documentsDirectoryURL = try NSFileManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
                        
                        if picker == self.profileImagePicker {
                            let toURL = documentsDirectoryURL.URLByAppendingPathComponent("HabitlabPhysiotherapistProfileImage\(id)").URLByAppendingPathExtension("jpg")
                            
                            self.uploadImage(picker, info: info, id: id as String, url: toURL, upload: {() -> () in
                                PhysioS3API.uploadProfilePicture(id as String, url: toURL, handler: { (error) in
                                    if error != nil {
                                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                                        return
                                    }
                                    
                                    self.populateProfileImage()
                                    }, progress: { (bytesSent, totalBytesSent, totalBytesExpectedToSend) in
                                        // Do nothing.
                                })
                            })
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            picker.dismissViewControllerAnimated(true, completion: nil)
                        })
                    } catch let error as NSError {
                        Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    } catch {
                        // Do nothing.
                    }
                } else {
                    picker.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
    
    private func uploadImage(picker: UIImagePickerController, info: [String : AnyObject], id: String, url: NSURL, upload: (() -> ())) {
        
        print({(x: Int, y: Int) -> Int in
            return x + y
            }(2, 3))
        
        var image: UIImage?
        
        if let realImageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = realImageOriginal
        }
        
        if let realImageCrop = info[UIImagePickerControllerCropRect] as? UIImage {
            image = realImageCrop
        }
        
        if let realImageEdited = info[UIImagePickerControllerEditedImage] as? UIImage {
            image = realImageEdited
        }
        
        if let realImage = image {
            do {
                if let realImageJpeg = UIImageJPEGRepresentation(realImage, 0.0) {
                    try realImageJpeg.writeToURL(url, options: NSDataWritingOptions.AtomicWrite)
                    upload()
                }
            } catch let error as NSError {
                Serv.showErrorPopupFromError(error, controller: self, handler: nil)
            } catch {
                // Do nothing.
            }
            
            return
        }
    }
    
    func reload() {
        PhysioProducts.store.requestProducts { (success, products) in
            if success {
                if let realProducts = products {
                    self.products = realProducts
                    
                    for product in realProducts {
                        switch product.productIdentifier {
                        case PhysioProducts.Unlimited1Month:
                            self.priceLabel1Month.text = product.localizedPrice()
                        case PhysioProducts.Unlimited3Months:
                            self.priceLabel3Months.text = product.localizedPrice()
                        case PhysioProducts.Unlimited12Months:
                            self.priceLabel12Months.text = product.localizedPrice()
                        default:
                            break
                        }
                    }
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), {
            self.button1month.enabled = false
            self.button3months.enabled = false
            self.button12months.enabled = false
            
            self.button1month.alpha = 0.5
            self.button3months.alpha = 0.5
            self.button12months.alpha = 0.5
        })
        
        dispatch_async(serialQueue) {
            HabitlabRestAPI.isValidUnlimitedProgramsSubscription { (error, validation) in
                if error != nil {
                    Serv.showErrorPopupFromError(error, controller: self, handler: nil)
                    return
                }
                
                if let realValidationJson = validation {
                    if let realValidation = realValidationJson as? NSDictionary {
                        if let validUnlimitedProgramsSubscription = realValidation["validUnlimitedProgramsSubscription"] as? NSNumber {
                            if validUnlimitedProgramsSubscription != 1 {
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.button1month.enabled = true
                                    self.button3months.enabled = true
                                    self.button12months.enabled = true
                                    
                                    self.button1month.alpha = 1
                                    self.button3months.alpha = 1
                                    self.button12months.alpha = 1
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
    func handlePurchaseNotification(notification: NSNotification) {
        print(notification.object)
        self.reload()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.profileImagePicker.navigationBar.barStyle = .Default
        self.profileImagePicker.navigationBar.backgroundColor = UIColor(red: 34 / 255, green: 176 / 255, blue: 155 / 255, alpha: 1.0)
        self.profileImagePicker.navigationBar.tintColor = UIColor.whiteColor()
        
        self.populateUser()
        self.populateProfileImage()
        
        let doneToolbar = self.constructInputAccessoryView()
        doneToolbar.sizeToFit()
        self.workplaceName.inputAccessoryView = doneToolbar
        
        self.reload()
    }
    
    private func constructInputAccessoryView() -> UIToolbar {
        let flexSpaceLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let flexSpaceRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Hide keyboard", style: UIBarButtonItemStyle.Done, target: self, action: #selector(ProfileController.doneButtonAction))
        
        if let font = UIFont(name: "HelveticaNeue-Light", size: 15) {
            done.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blackColor()], forState: UIControlState.Normal)
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 80))
        doneToolbar.barStyle = UIBarStyle.Default
        doneToolbar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        doneToolbar.items = [flexSpaceLeft, done, flexSpaceRight]
        return doneToolbar
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        if error != nil {
            Serv.showErrorPopupFromError(error, controller: self, handler: nil)
        }
        
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func doneButtonAction() {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        Serv.resetCaches()
    }
    
    private func resourceNameForProductIdentifier(productIdentifier: String) -> String? {
        return productIdentifier.componentsSeparatedByString(".").last
    }
}