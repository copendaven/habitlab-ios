//
//  PhysioS3Api.swift
//  Physio
//
//  Created by Vlad Manea on 5/31/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit
import AWSS3

class PhysioS3API {
    static func uploadVideoImage(cacheId: String, url: NSURL, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage(url, bucketKey: "physiovideoimage\(cacheId).jpg", contentType: "image/jpeg", handler: handler, progress: progress)
    }
    
    static func downloadVideoImage(cacheId: String, temporaryAddress: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage(temporaryAddress, bucketKey: "physiovideoimage\(cacheId).jpg", handler: handler, progress: progress)
    }
    
    static func uploadImage(cacheId: String, url: NSURL, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage(url, bucketKey: "physioimage\(cacheId).jpg", contentType: "image/jpeg", handler: handler, progress: progress)
    }
    
    static func downloadImage(cacheId: String, temporaryAddress: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage(temporaryAddress, bucketKey: "physioimage\(cacheId).jpg", handler: handler, progress: progress)
    }
    
    static func uploadProfilePicture(userId: String, url: NSURL, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage(url, bucketKey: "physiotherapistprofilepicture\(userId).jpg", contentType: "image/jpeg", handler: handler, progress: progress)
    }
    
    static func downloadProfilePicture(userId: String, temporaryAddress: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage(temporaryAddress, bucketKey: "physiotherapistprofilepicture\(userId).jpg", handler: handler, progress: progress)
    }
    
    static func uploadSuccessPicture(userId: String, url: NSURL, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage(url, bucketKey: "physiotherapistsuccesspicture\(userId).jpg", contentType: "image/jpeg", handler: handler, progress: progress)
    }
    
    static func downloadSuccessPicture(userId: String, temporaryAddress: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage(temporaryAddress, bucketKey: "physiotherapistsuccesspicture\(userId).jpg", handler: handler, progress: progress)
    }
    
    static func uploadEncouragementPicture(userId: String, url: NSURL, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage(url, bucketKey: "physiotherapistencouragementpicture\(userId).jpg", contentType: "image/jpeg", handler: handler, progress: progress)
    }
    
    static func downloadEncouragementPicture(userId: String, temporaryAddress: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage(temporaryAddress, bucketKey: "physiotherapistencouragementpicture\(userId).jpg", handler: handler, progress: progress)
    }
    
    static func uploadVideo(cacheId: String, url: NSURL, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        uploadImage(url, bucketKey: "physiovideo\(cacheId).mov", contentType: "video/quicktime", handler: handler, progress: progress)
    }
    
    static func downloadVideo(cacheId: String, temporaryAddress: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        downloadImage(temporaryAddress, bucketKey: "physiovideo\(cacheId).mov", handler: handler, progress: progress)
    }
    
    private static func uploadImage(url: NSURL, bucketKey: String, contentType: String, handler: (NSError?) -> (), progress: ((bytesSent:Int64, totalBytesSent:Int64, totalBytesExpectedToSend:Int64) -> ())?) {
        if let uploadRequest = AWSS3TransferManagerUploadRequest() {
            let bucket = Configuration.getS3ImageBucket()
            
            uploadRequest.bucket = bucket
            uploadRequest.ACL = AWSS3ObjectCannedACL.BucketOwnerRead
            uploadRequest.key = "\(bucket)/\(bucketKey)"
            uploadRequest.contentType = contentType
            uploadRequest.body = url
            
            if let realProgress = progress {
                uploadRequest.uploadProgress = realProgress
            }
            
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
            transferManager.upload(uploadRequest).continueWithBlock({ (task) -> AnyObject? in
                if task.error != nil {
                    handler(task.error)
                    return false
                }
                
                handler(nil)
                return nil
            })
        }
    }
    
    private static func downloadImage(temporaryAddress: String, bucketKey: String, handler: (NSError?, NSData?) -> (), progress: ((bytesWritten:Int64, totalBytesWritten:Int64, totalBytesExpectedToWrite:Int64) -> ())?) {
        let bucket = Configuration.getS3ImageBucket()
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        
        let path = NSTemporaryDirectory().stringByAppendingString(temporaryAddress)
        let url = NSURL(fileURLWithPath: path as String)
        
        downloadRequest.bucket = bucket
        downloadRequest.key = "\(bucket)/\(bucketKey)"
        downloadRequest.downloadingFileURL = url
        
        if let realProgress = progress {
            downloadRequest.downloadProgress = realProgress
        }
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.download(downloadRequest).continueWithBlock({ (task) -> AnyObject? in
            if task.error != nil {
                handler(task.error, nil)
                return false
            }
            
            let error = NSError(domain: "S3ClientError", code: 404, userInfo: nil)
            
            if let image = UIImage(contentsOfFile: path) {
                let data = UIImageJPEGRepresentation(image, 0.0)
                
                if data == nil {
                    handler(error, nil)
                    return false
                }
                
                handler(nil, data!)
                return true
            }
            
            do {
                throw error
            } catch let error as NSError {
                handler(error, nil)
                return false
            } catch {
                let error = NSError(domain: "S3ClientError", code: 500, userInfo: nil)
                handler(error, nil)
                return false
            }
        })
    }
}