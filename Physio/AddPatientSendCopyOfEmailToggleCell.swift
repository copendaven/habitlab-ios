//
//  AddPatientSendCopyOfEmailToggleCell.swift
//  Physio
//
//  Created by David on 21/06/2016.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

class AddPatientSendCopyOfEmailToggleCell: UITableViewCell {
    private var controller: UIViewController?
   
    private var fieldType: AddPatientEmailController.FieldType?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchingControl: UISwitch!
    
    func populate(labelText: String, fieldType: AddPatientEmailController.FieldType, controller: UIViewController) {
        self.fieldType = fieldType
        self.titleLabel.text = labelText
        self.controller = controller
        
        self.switchingControl.addTarget(self, action: #selector(AddPatientSendCopyOfEmailToggleCell.stateChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func stateChanged(switchState: UISwitch) {
        if let viewController = self.controller as? AddPatientEmailController, realFieldType = self.fieldType {
            viewController.handleProgramToggleUpdate(switchState.on, fieldType: realFieldType)
        }
    }
}
