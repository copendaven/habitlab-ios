//
//  AddProgramSliderCell.swift
//  Physio
//
//  Created by Vlad Manea on 5/26/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

import UIKit

class AddProgramDoneCell: UITableViewCell, UITextViewDelegate {
    private var controller: UIViewController?
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var doneButtonImage: UIImageView!
    
    func populate(controller: UIViewController) {
        self.controller = controller
    }
    
    @IBAction func didClickDone(sender: AnyObject) {
        if let viewController = self.controller {
            if let addProgramController = viewController as? AddProgramController {
                addProgramController.didClickDone()
            }
        }
    }
}
