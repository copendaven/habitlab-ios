//
//  HabitlabAuthentication.h
//  HabitlabAuthentication
//
//  Created by Vlad Manea on 5/21/16.
//  Copyright © 2016 Habitlab IVS. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HabitlabAuthentication.
FOUNDATION_EXPORT double HabitlabAuthenticationVersionNumber;

//! Project version string for HabitlabAuthentication.
FOUNDATION_EXPORT const unsigned char HabitlabAuthenticationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HabitlabAuthentication/PublicHeader.h>


